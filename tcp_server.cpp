#include <cstdlib>
#include <iostream>
#include <sstream>

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/algorithm/string.hpp>


#include "tcp_server.hpp"



//
// -- TcpServerSession --
//
mbp::buoy::TcpServerSession::TcpServerSession(
        boost::asio::io_service& a_ioService,
        boost::posix_time::time_duration a_cmdTimeout)
    :
        m_ioService(a_ioService),
        m_socket(a_ioService),
        m_deadlineTimer(a_ioService),
        m_cmdTimeout(a_cmdTimeout)
{
    // No deadline is required until the first socket operation is started. We
    // set the deadline to positive infinity so that the actor takes no action
    // until a specific deadline is set.
    m_deadlineTimer.expires_at(boost::posix_time::pos_infin);
    // Start the persistent actor that checks for deadline expiry.
    check_deadline();
    return;
} // mbp::buoy::TcpServerSession::TcpServerSession()


void mbp::buoy::TcpServerSession::start()
{
    // Set a deadline for the asynchronous operation.
    m_deadlineTimer.expires_from_now(m_cmdTimeout);

    boost::asio::async_read_until(
            m_socket,
            m_streamBuffer,
            "\n",
            boost::bind(&mbp::buoy::TcpServerSession::handle_read,
                this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred));
    return;
} // mbp::buoy::TcpServerSession::start()


void mbp::buoy::TcpServerSession::check_deadline()
{
    // Check whether the deadline has passed. We compare the deadline against
    // the current time since a new asynchronous operation may have moved the
    // deadline before this actor had a chance to run.
    if (m_deadlineTimer.expires_at() <= boost::asio::deadline_timer::traits_type::now())
    {
        // The deadline has passed. The outstanding asynchronous operation needs
        // to be cancelled so that the blocked receive() function will return.
        //
        // Please note that cancel() has portability issues on some versions of
        // Microsoft Windows, and it may be necessary to use close() instead.
        // Consult the documentation for cancel() for further information.
        std::cout << "Cancel ..." << std::endl;
        m_socket.cancel();

        // There is no longer an active deadline. The expiry is set to positive
        // infinity so that the actor takes no action until a new deadline is set.
        m_deadlineTimer.expires_at(boost::posix_time::pos_infin);
    }

    // Put the actor back to sleep.
    m_deadlineTimer.async_wait(boost::bind(&mbp::buoy::TcpServerSession::check_deadline, this));
} // mbp::buoy::TcpServerSession::check_deadline()



void mbp::buoy::TcpServerSession::handle_read(
        const boost::system::error_code& a_error,
        size_t bytes_transferred)
{
    if (! a_error ) {
        std::cout << "bytes_transferred = " << bytes_transferred << std::endl;
        std::string command, argsLine;

        std::istream is(&m_streamBuffer);
        is.unsetf(std::ios_base::skipws);
        is >> command;

        std::istreambuf_iterator<char> eos;
        argsLine = std::string(std::istreambuf_iterator<char>(is), eos);

        boost::algorithm::trim(command);
        boost::algorithm::to_upper(command);
        boost::algorithm::trim(argsLine);
        boost::algorithm::to_upper(argsLine);

        std::ostringstream sstr("");
        sstr << "cmd = '" << command << "' / args = '" << argsLine << "'" << std::endl;
        std::cout << sstr.str();

        if (command.compare("QUIT") == 0) {
            std::cout << "Got QUIT request ..." << std::endl;
            m_ioService.stop();
        }

        // Set a deadline for the asynchronous operation.
        m_deadlineTimer.expires_from_now(m_cmdTimeout);
        boost::asio::async_write(
                m_socket,
                boost::asio::buffer(sstr.str().c_str(), sstr.str().length()),
                boost::bind(
                    &mbp::buoy::TcpServerSession::handle_write,
                    this,
                    boost::asio::placeholders::error));
    } else {
        delete this;
    }
    return;
} // mbp::buoy::TcpServerSession::handle_read()


void mbp::buoy::TcpServerSession::handle_write(
        const boost::system::error_code& a_error)
{
    if ( ! a_error ) {
        boost::asio::async_read_until(
                m_socket,
                m_streamBuffer,
                "\n",
                boost::bind(&mbp::buoy::TcpServerSession::handle_read,
                    this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
    } else {
        delete this;
    }
    return;
} // mbp::buoy::TcpServerSession::handle_write()




//
// -- TcpServer --
//
mbp::buoy::TcpServer::TcpServer(
        uint16_t a_port,
        boost::posix_time::time_duration a_cmdTimeout)
    :
        m_tcpAcceptor(m_ioService, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), a_port)),
        m_cmdTimeout(a_cmdTimeout)
{
    start_accept();
    return;
} // mbp::buoy::TcpServer::TcpServer()


void mbp::buoy::TcpServer::start_accept()
{
    mbp::buoy::TcpServerSession *newSession_p =
        new mbp::buoy::TcpServerSession(m_ioService, m_cmdTimeout);
    m_tcpAcceptor.async_accept(
            newSession_p->socket(),
            boost::bind(
                &mbp::buoy::TcpServer::handle_accept,
                this,
                newSession_p,
                boost::asio::placeholders::error));
    return;
} // mbp::buoy::start_accept()


void mbp::buoy::TcpServer::handle_accept(
        mbp::buoy::TcpServerSession* a_newSession_p,
        const boost::system::error_code& error)
{
    if (!error) {
        a_newSession_p->start();
    } else {
        delete a_newSession_p;
    }

    start_accept();
} // mbp::buoy::TcpServer::handle_accept()
