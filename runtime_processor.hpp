#ifndef _RUNTIME_PROCESSOR_HPP
#define _RUNTIME_PROCESSOR_HPP

#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/deadline_timer.hpp>

#include "buoy_udp_connector.hpp"

#define MBP_BUOY_TCP_SERVER_CMD_TIMEOUT_s       10
#define NUM_OF_RUNTIME_DP_VECTORS               3       // 0 -> instruments data;  1 -> instruments statuses;  2 -> buoy statuses

namespace mbp {
namespace buoy {

class RuntimeProcessor {
private:
    std::vector<mbp::buoy::runtime_hdr_pair_t> m_dataPoints_v[NUM_OF_RUNTIME_DP_VECTORS];

public:
    RuntimeProcessor();
    RuntimeProcessor(const mbp::buoy::RuntimeProcessor& other ) { std::cout << "RuntimeProcessor(const RuntimeProcessor& other)" << std::endl; };
    RuntimeProcessor(mbp::buoy::RuntimeProcessor *other ) { std::cout << "RuntimeProcessor(RuntimeProcessor *other)" << std::endl; };
    ~RuntimeProcessor();

    void run(const bool  *a_doLoop_p);
}; // class RuntimeProcessor


}; // namespace buoy
}; // namespace mbp

#endif // _RUNTIME_PROCESSOR_HPP
