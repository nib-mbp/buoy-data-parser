#ifndef _RUNTIME_DATA_HPP
#define _RUNTIME_DATA_HPP

#include <iostream>
#include <string>
#include <sstream>

#include <boost/cstdint.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "runtime_data.hpp"
#include "runtime_alarms.hpp"

#define RUNTIME_NUM_CURRENT_LOOPS   3

namespace mbp {

typedef enum {
    PARSING_TYPE_NA_e                       = 0,
    PARSING_TYPE_MYSQL_e,
    PARSING_TYPE_RUNTIME_e
} parsing_type_e;

namespace buoy {
}; // namespace buoy

#define NUM_of_BATTERIES        (4)
#define NUM_of_BATTERY_PACKS    (2)

class RuntimeData {
private:
    // -- buoy interior --
    boost::posix_time::ptime    m_bInteriorPTime;
    double                      m_bInteriorTemperature;
    uint16_t                    m_bInteriorHumidity;

    // -- buoy board --
    boost::posix_time::ptime    m_bBoardPTime;
    double                      m_bBoardCurrent;
    double                      m_bBoardTemperature;
    double                      m_bBoardHumidity;

    // -- buoyCO2 --
    boost::posix_time::ptime    m_bCo2PTime;
    double                      m_bCo2Concentration;

    // -- all instruments (errors, statuses, min/max/avg currents and currents measurement time --
    int32_t                     m_instrumentsError[BUOY_num_of_ALL_INSTRUMENTS];
    int32_t                     m_instrumentsStatus[BUOY_num_of_ALL_INSTRUMENTS];
    boost::posix_time::ptime    m_instrumentCurrentPtime[BUOY_num_of_ALL_INSTRUMENTS];
    double                      m_instrumentCurrentMin[BUOY_num_of_ALL_INSTRUMENTS];
    double                      m_instrumentCurrentMax[BUOY_num_of_ALL_INSTRUMENTS];
    double                      m_instrumentCurrentAvg[BUOY_num_of_ALL_INSTRUMENTS];
    // -- wind --
    boost::posix_time::ptime    m_windPTime;
    double                      m_windU, m_windV, m_windW, m_windVSpeed, m_windVDirection;
    // -- air (humidity) --
    boost::posix_time::ptime    m_airPTime;
    double                      m_airTemperature, m_airHumidity;
    // -- compass --
    boost::posix_time::ptime    m_compassPTime;
    double                      m_compassRoll, m_compassPitch, m_compassYaw;
    // -- sea_water --
    boost::posix_time::ptime    m_seawaterPTime;
    double                      m_seawaterTemperature, m_seawaterConductivity, m_seawaterSalinity, m_seawaterChlorophyll;
    // -- oxygen --
    boost::posix_time::ptime    m_oxygenPTime[3];
    double                      m_oxygenConcentration[3];
    // -- par --
    boost::posix_time::ptime    m_parPTime;
    int                         m_parValueRaw;
    // -- awac --
    boost::posix_time::ptime    m_awacPTime;
    double                      m_awacTemperature, m_awacPressure;
    double                      m_awacWavesHeight, m_awacWavesPeakPeriod, m_awacWavesDirection;
    int                         m_awacIsCurrentPresent;

    // -- batteries currents and battery packs voltage --
    boost::posix_time::ptime    m_batPTime;
    double_t                    m_batCurrMin[NUM_of_BATTERIES];
    double_t                    m_batCurrMax[NUM_of_BATTERIES];
    double_t                    m_batCurrAvg[NUM_of_BATTERIES];
    double_t                    m_batPackVoltageMin[NUM_of_BATTERY_PACKS];
    double_t                    m_batPackVoltageMax[NUM_of_BATTERY_PACKS];
    double_t                    m_batPackVoltageAvg[NUM_of_BATTERY_PACKS];
    // -- common system voltage --
    // -- batteries - as per 2016-01-01 all batteries are wired together --
    boost::posix_time::ptime    m_cmnVoltagePTime;
    double                      m_cmnVoltage;
    int                         m_cmnVoltageStatus;
    // -- ethanol power cells --
    boost::posix_time::ptime    m_fuelCellPTime;
    int                         m_fuelCellStatus;
    double                      m_fuelCellCurrent;
    // -- current loops --
    boost::posix_time::ptime    m_currentLoopPTime[RUNTIME_NUM_CURRENT_LOOPS];
    double                      m_currentLoopCurrent[RUNTIME_NUM_CURRENT_LOOPS];
    // -- solar cells (MPPT) --
    boost::posix_time::ptime    m_mpptPTime;
    double                      m_mpptInI;
    double                      m_mpptOutI;
    double                      m_mpptInU;
    double                      m_mpptOutU;

    mbp::buoy::RuntimeAlarms    m_alarms;

    void _getInstrumentsJson(std::ostringstream& sstr) const;
    void _getPowerJson(std::ostringstream& sstr) const;
    void _getStatusJson(std::ostringstream& sstr) const;

public:
    RuntimeData();
    ~RuntimeData() {};
    void Init() throw(std::invalid_argument);

    void setBInterior(boost::posix_time::ptime recPTime, double temperature, uint16_t humidity);
    void setBBoard(boost::posix_time::ptime recPTime, double current, double temperature, double humidity);
    void setWind(boost::posix_time::ptime recPTime, double u, double v, double w, int error);
    void setAir(boost::posix_time::ptime recPTime, double temperature, double humidity, int error);
    void setCompass(boost::posix_time::ptime recPTime, double roll, double pitch, double yaw, int error);
    void setSeaWater(boost::posix_time::ptime recPTime, double temperature, double conductivity, double salinity, double chlorophyll, int error);
    void setOxygen(int devID, boost::posix_time::ptime recPTime, double concentration, int error);
    void setBuoyCO2(boost::posix_time::ptime recPTime, double concentration, int error);
    void setPar(boost::posix_time::ptime recPTime, int valueRaw, int error);
    void setBatteries(boost::posix_time::ptime recPTime,
                      double currBatMin1, double currBatMin2, double currBatMin3, double currBatMin4,
                      double currBatMax1, double currBatMax2, double currBatMax3, double currBatMax4,
                      double currBatAvg1, double currBatAvg2, double currBatAvg3, double currBatAvg4,
                      double voltagePackMin1, double voltagePackMin2,
                      double voltagePackMax1, double voltagePackMax2,
                      double voltagePackAvg1, double voltagePackAvg2);
    void setCmnVoltage(boost::posix_time::ptime recPTime, double voltage);
    void setFuelCell(boost::posix_time::ptime recPTime, double current);
    void setAwac(boost::posix_time::ptime recPTime, double temperature, double pressure);
    void setCurrentLoop(int currentLoopId, boost::posix_time::ptime recPTime, double current) throw(std::invalid_argument);
    void setMppt(boost::posix_time::ptime recPTime, double inI, double outI, double inU, double outU);

    void setInstrumentCurrent(uint16_t a_board, uint16_t a_device, boost::posix_time::ptime a_recPTime, double a_min, double a_max, double a_avg);

    void setIoAlarm(boost::posix_time::ptime recPTime, uint16_t board, uint16_t device, uint16_t mode, uint16_t state, uint16_t code);
    void setAlarm(boost::posix_time::ptime recPTime, uint16_t code);
    // std::string _test() const;
    std::string getJson() const;

}; // class RuntimeData

std::ostream& operator<<(std::ostream& a_os, const mbp::RuntimeData& a_runtimeData);

}; // namespace mbp
#endif // _RUNTIME_DATA_HPP
