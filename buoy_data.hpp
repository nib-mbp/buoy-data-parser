#ifndef _BUOY_DATA_HPP
#define _BUOY_DATA_HPP

#include <string>

#include <boost/cstdint.hpp>

#include "buoy_shared_array.hpp"
#include "buoy_data_holder_utils.hpp"
#include "buoy_awac_data.hpp"
#include "runtime_data.hpp"


namespace mbp {

class BuoyData {
private:
// TODO / ATTENTION / CHECK ME
//  2016-03-06 -> m_buoyArray_sp:
//    -> changed from:  mbp::BuoySharedArray<uint8_t>
//    -> changed to:    mbp::BuoySharedArray<uint8_t>&
//       ^^^
//       Check if this causes any fail
    mbp::BuoySharedArray<uint8_t>&  m_buoyArray_sp;     //!< The BuoySharedArray buffer that is going to be parse by this class
    mbp::buoy::tbls_msp             m_tblFields;        //!< Data metadata holder - should be filled among first things done as e.g. in the constructor
    mbp::buoy::rows_spt             m_rows;
    mbp::BuoyAwacData               m_awacData;
    mbp::parsing_type_e             m_parsingType;
    mbp::RuntimeData                *m_runtimeData_p;

    void _FillColumns(void);
    void _MapNextDataset(void) throw(std::runtime_error);

public:
    BuoyData(mbp::BuoySharedArray<uint8_t>& a_buoyArray_sp);
    ~BuoyData() {};

    void ParseBuffer(mbp::parsing_type_e a_parsingType, mbp::RuntimeData *runtimeData_p=NULL) throw(std::runtime_error);
    const std::string Dump(void) const;
    mbp::buoy::tbls_msp getTables(void) const { return m_tblFields; };
    mbp::buoy::rows_spt getRows(void) const { return m_rows; };
}; // class BuoyData

}; // namespace mbp

#endif // _BUOY_DATA_HPP
