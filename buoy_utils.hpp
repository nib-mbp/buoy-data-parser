#ifndef _BUOY_UTILS_HPP
#define _BUOY_UTILS_HPP

#include <iostream>
#include <string>
#include <vector>

#include <boost/date_time/posix_time/posix_time.hpp>

#include "buoy_log.hpp"

namespace mbp {
namespace buoy {

/*
// TODO:
//  To be removed
//
uint16_t crc16(uint8_t *s, uint32_t n,int8_t flag);
*/

typedef enum {
    CONFIG_READER_e,
    CONFIG_RUNTIME_e
} config_type_e;

class OptionParser;
extern OptionParser g_options;

class OptionParser {
private:
    std::string                 m_dbUser;
    std::string                 m_dbPasswd;
    std::string                 m_dbHost;
    std::string                 m_dbDbPrefix;
    std::string                 m_dbMarkerTable;
    std::string                 m_configFile;
    mbp::loglevel_e             m_logLevel;
    std::string                 m_logFile;
    bool                        m_doTimeScalling;
    uint32_t                    m_ignoreDuplicates;
    bool                        m_dbgDoDbInsert;
    bool                        m_dbgDoSqlDump;
    std::string                 m_serverIp;
    uint16_t                    m_serverPort;
    bool                        m_doDaemonize;

public:
    OptionParser();
    ~OptionParser();

    const std::string& getDbUser(void) const { return m_dbUser; };
    const std::string& getDbPasswd(void) const { return m_dbPasswd; };
    const std::string& getDbHost(void) const { return m_dbHost; };
    const std::string& getDbDbPrefix(void) const { return m_dbDbPrefix; };
    const std::string& getDbMarkerTable(void) const { return m_dbMarkerTable; };
    const std::string& getServerIp(void) const { return m_serverIp; };
    const uint16_t getServerPort(void) const { return m_serverPort; };
    const bool getDoTimestampScalling(void) const { return m_doTimeScalling; };
    const uint32_t getNumIgnoreDuplicates(void) const { return m_ignoreDuplicates; };
    const bool getDbgDoDbInsert(void) const { return m_dbgDoDbInsert; };
    const bool getDbgDoSqlDump(void) const { return m_dbgDoSqlDump; };
    const bool getDoDaemonize(void) const { return m_doDaemonize; };

    bool Parse(int a_argc, char **a_argv_pp, mbp::buoy::config_type_e a_configType);
}; // class OptionParser

const std::string PtimeToStr(boost::posix_time::ptime);
const std::string PtimeToDateStr(const boost::posix_time::ptime& posixTime);

std::string BufferToHexString(
        void *a_buffer_p, size_t bufLen,
        size_t a_perLine,
        bool a_delim,
        bool a_addr,
        bool a_ascii,
        bool a_skipLineBreaks=false);



const std::string Uchar2Hex(uint8_t inchar, bool outputBase=true);
const std::string Ushort2Hex(uint16_t in, bool outputBase=true);



void DaemonizeOnRequest(bool doDaemonize);

} // namespace buoy
} // namespace mbp




#endif // _BUOY_UTILS_HPP
