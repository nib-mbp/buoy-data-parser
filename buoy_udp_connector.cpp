#include <iostream>
#include <string>
#include <cstring>

#include <boost/cstdint.hpp>
#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

#include <boost/crc.hpp>

#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/chrono/duration.hpp>
#include <boost/thread.hpp>

#include <sys/time.h>

#include "buoy_udp_connector.hpp"
#include "buoy_shared_array.hpp"
#include "buoy_utils.hpp"

#define ANEMOMETER_DATA                         32
#define COMPASS_DATA                            33
#define HYGROMETER_DATA                         35


namespace asio = boost::asio;

struct mbp::buoy::_instruments_matrix_s g_instruments[] = {
    {1, 0},         // BUOY_INSTRUMENT_WIND_e  = 0,
    {1, 1},         // BUOY_INSTRUMENT_HUMIDITY_e = 1,
    {2, 0},         // BUOY_INSTRUMENT_COMPASS_e = 2,
    {2, 1},         // BUOY_INSTRUMENT_SALINITY_e = 3,
    {3, 0},         // BUOY_INSTRUMENT_AWAC_e = 4,
    {3, 1},         // BUOY_INSTRUMENT_OXYGEN1_e = 5,
    {4, 1},         // BUOY_INSTRUMENT_OXYGEN2_e = 6,
    {5, 1},         // BUOY_INSTRUMENT_OXYGEN3_e = 7,
    {4, 0},         // BUOY_INSTRUMENT_PAR_e = 8,
    {5, 0}          // BUOY_INSTRUMENT_CO2_e = 9,
};

mbp::buoy::internal::UdpClient::UdpClient(
        boost::asio::ip::udp::socket &a_socket,
        boost::asio::io_service &a_ioService)
        :
        socket_(a_socket),
        io_service_(a_ioService),
        deadline_(a_ioService)
  {
    // No deadline is required until the first socket operation is started. We
    // set the deadline to positive infinity so that the actor takes no action
    // until a specific deadline is set.
    deadline_.expires_at(boost::posix_time::pos_infin);

    // Start the persistent actor that checks for deadline expiry.
    check_deadline();
  }

mbp::buoy::internal::UdpClient::~UdpClient()  {
    deadline_.cancel();
}

std::size_t mbp::buoy::internal::UdpClient::receive(const boost::asio::mutable_buffer &buffer,
                                                    boost::posix_time::time_duration timeout,
                                                    boost::system::error_code &ec) {
    // Set a deadline for the asynchronous operation.
    deadline_.expires_from_now(timeout);

    // Set up the variables that receive the result of the asynchronous
    // operation. The error code is set to would_block to signal that the
    // operation is incomplete. Asio guarantees that its asynchronous
    // operations will never fail with would_block, so any other value in
    // ec indicates completion.
    ec = boost::asio::error::would_block;
    std::size_t length = 0;

    // Start the asynchronous operation itself. The handle_receive function
    // used as a callback will update the ec and length variables.
    socket_.async_receive(boost::asio::buffer(buffer),
                          boost::bind(
                                  &mbp::buoy::internal::UdpClient::handle_receive,
                                  _1, _2,
                                  &ec, &length));

// Block until the asynchronous operation has completed.
    do io_service_.run_one(); while (ec == boost::asio::error::would_block);

    return length;
}


void mbp::buoy::internal::UdpClient::check_deadline() {
    // Check whether the deadline has passed. We compare the deadline against
    // the current time since a new asynchronous operation may have moved the
    // deadline before this actor had a chance to run.
    if (deadline_.expires_at() <= boost::asio::deadline_timer::traits_type::now()) {
        // The deadline has passed. The outstanding asynchronous operation needs
        // to be cancelled so that the blocked receive() function will return.
        //
        // Please note that cancel() has portability issues on some versions of
        // Microsoft Windows, and it may be necessary to use close() instead.
        // Consult the documentation for cancel() for further information.
        socket_.cancel();

        // There is no longer an active deadline. The expiry is set to positive
        // infinity so that the actor takes no action until a new deadline is set.
        deadline_.expires_at(boost::posix_time::pos_infin);
    }

    // Put the actor back to sleep.
    deadline_.async_wait(boost::bind(&mbp::buoy::internal::UdpClient::check_deadline, this));
}

















mbp::BuoyUdpConnector::BuoyUdpConnector(std::string a_host, uint16_t a_port)
:
    m_isInitialized(false),
    m_ioService(),
    m_remoteHost(a_host),
    m_remoteUdpPort(a_port),
    m_isMysqlPacketOutOfRange(true),
    m_udpDataPoints_p(NULL),
    m_udpPacketBufSize(UDP_PACKET_LEN),
    m_udpPacketBuf_p(NULL),
    m_socket(m_ioService),
    m_client(m_socket, m_ioService)

{
    m_udpDataPoints_p = new buoy_udp_pkt_runtime_dpoint_t[MAX_UDP_DATA_POINTS];
    m_udpPacketBuf_p = new uint8_t[m_udpPacketBufSize];
    m_socket.open(asio::ip::udp::v4());

} // mbp::BuoyUdpConnector::BuoyUdpConnector()


mbp::BuoyUdpConnector::~BuoyUdpConnector()
{
    if (m_udpDataPoints_p != NULL) {
        delete[] m_udpDataPoints_p;
        m_udpDataPoints_p = NULL;
    }

    if (m_udpPacketBuf_p != NULL) {
        delete[] m_udpPacketBuf_p;
        m_udpPacketBuf_p = NULL;
    }
}

void mbp::BuoyUdpConnector::Init(void)
{

    m_serverEndpoint = asio::ip::udp::endpoint(
            asio::ip::address_v4::from_string(m_remoteHost),
            m_remoteUdpPort);
    m_isInitialized = true;
} // mbp::BuoyUdpConnector::Init()


void mbp::BuoyUdpConnector::QueryRuntime(
        mbp::BuoySharedArray<uint8_t>& a_buoyArray_sp,
        const std::vector<mbp::buoy::runtime_hdr_pair_t>& a_dataPoints_v)
    throw(std::invalid_argument, std::string)
{
    BOOST_LOG_FUNCTION();

    if (a_dataPoints_v.size() > MAX_UDP_DATA_POINTS) {
        std::ostringstream sstr;
        sstr.str("");
        sstr
            << "Number of requested data points exceeds the maximum allowed ("
            << a_dataPoints_v.size()
            << " > "
            << MAX_UDP_DATA_POINTS
            << ")!";
        throw std::invalid_argument(sstr.str().c_str());
    }

    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv,&tz);

    int dataPointsBufSize = a_dataPoints_v.size() * sizeof(buoy_udp_pkt_runtime_dpoint_t);

    buoy_udp_pkt_t *buoyRequest_p = reinterpret_cast<buoy_udp_pkt_t *>(m_udpPacketBuf_p);
    buoyRequest_p->hdr.flag = '@';
    buoyRequest_p->hdr.command = 'R';
    buoyRequest_p->hdr.data_len = dataPointsBufSize;
    buoyRequest_p->hdr.utc_secs = tv.tv_sec;
    buoyRequest_p->hdr.utc_usecs = tv.tv_usec;
    buoyRequest_p->hdr.packet = 0;
    buoyRequest_p->hdr.crc = 0;

    int idx = 0;
    BOOST_FOREACH(mbp::buoy::runtime_hdr_pair_t dataPoint, a_dataPoints_v) {
        m_udpDataPoints_p[idx].data_type = dataPoint.first;
        m_udpDataPoints_p[idx++].data_ident = dataPoint.second;
    }
    memcpy(buoyRequest_p->data, m_udpDataPoints_p, dataPointsBufSize);

    boost::crc_16_type crcResult;
    crcResult.process_bytes(reinterpret_cast<unsigned char *>(&(buoyRequest_p->hdr)), sizeof(buoy_udp_pkt_hdr_t) - sizeof(uint16_t));
    crcResult.process_bytes(reinterpret_cast<char *>(buoyRequest_p->data), buoyRequest_p->hdr.data_len);
    buoyRequest_p->hdr.crc = crcResult.checksum();

    m_socket.send_to(
            asio::buffer(reinterpret_cast<uint8_t *>(buoyRequest_p), sizeof(buoy_udp_pkt_hdr_t) + buoyRequest_p->hdr.data_len),
            m_serverEndpoint);

    boost::system::error_code ec;
    size_t len = m_client.receive(
            asio::buffer(a_buoyArray_sp.get(), a_buoyArray_sp.getFreeLength()),
            boost::posix_time::seconds(1), ec);

    a_buoyArray_sp.setTotalDataLength(len);
    a_buoyArray_sp.setUsableDataLength(len);        // TODO / FIXME: change this after the buoy_shared_array function getUsableDataLen is reviewed and/or fixed.
    if (ec) {
        // -- timeout --
        // std::cout << "Receive error: " << ec.message() << std::endl;
        return;
    }

    if (len < BUOY_UDP_PKT_HEADER_SIZE) {
        L_(error) << "Got invalid UDP packet (len = " << len << ").";
        return;
    }
    a_buoyArray_sp.setPosition(BUOY_UDP_PKT_HEADER_SIZE);

    buoy_udp_pkt_t *buoyReply_p = reinterpret_cast<buoy_udp_pkt_t *>(a_buoyArray_sp.get());

    crcResult.reset();
    crcResult.process_bytes(reinterpret_cast<unsigned char *>(&buoyReply_p->hdr), sizeof(buoy_udp_pkt_hdr_t) - sizeof(uint16_t));
    crcResult.process_bytes(reinterpret_cast<char *>(buoyReply_p->data), buoyReply_p->hdr.data_len);
    if (buoyReply_p->hdr.crc != crcResult.checksum()) {
        L_(error) << "Got invalid UDP packet (CRC mismatch).";
        return;
    }

    if (len == BUOY_UDP_PKT_HEADER_SIZE)
    {
        // -- got an empty UDP packet meaning the query was out of range for this day --
        L_(warning) << "0 -> " << a_buoyArray_sp.getDebugDetails();
        return;
    }

    return;
} // mbp::BuoyUdpConnector::QueryRuntime()


/**
 * Query the data provider for data to be inserted into MySQL.
 *
 * Return value:
 *  0 if succed in getting the package;
 *  1 in case of invalid package retrived (e.g. out of range for the current day,
 *    but the day isn't over yet).
 *  2 in case of timeout
 *  3 in case of UDP packet error (e.g. CRC or other error)
 *  Please note the function still throws an exception when an unexpected package
 *  is received.
 */
int mbp::BuoyUdpConnector::QueryMysql(
        mbp::BuoySharedArray<uint8_t>& a_buoyArray_sp,
        uint16_t a_year,
        uint8_t a_month,
        uint8_t a_day,
        int32_t a_packetNumber)
    throw(std::string)
{
    m_isMysqlPacketOutOfRange = true;

    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv,&tz);

    buoy_udp_pkt_t buoyRequest;
    buoyRequest.hdr.flag = '@';
    buoyRequest.hdr.command = 'M';
    buoyRequest.hdr.data_len = sizeof(buoy_udp_pkt_mysql_hdr_t);
    buoyRequest.hdr.utc_secs = 0;
    buoyRequest.hdr.utc_usecs = 0;
    buoyRequest.hdr.packet = 0;
    buoyRequest.hdr.crc = 0;

    buoy_udp_pkt_mysql_hdr_t *mysqlRequest_p = reinterpret_cast<buoy_udp_pkt_mysql_hdr_t *>(&buoyRequest.data[0]);
    mysqlRequest_p->year = a_year;
    mysqlRequest_p->month = a_month;
    mysqlRequest_p->day = a_day;
    mysqlRequest_p->packet_no = a_packetNumber;

    boost::crc_16_type crcResult;
    crcResult.process_bytes(reinterpret_cast<unsigned char *>(&buoyRequest.hdr), sizeof(buoy_udp_pkt_hdr_t) - sizeof(uint16_t));
    crcResult.process_bytes(reinterpret_cast<char *>(&buoyRequest.data), buoyRequest.hdr.data_len);
    buoyRequest.hdr.crc = crcResult.checksum();

    m_socket.send_to(
            asio::buffer(reinterpret_cast<uint8_t *>(&buoyRequest), sizeof(buoy_udp_pkt_hdr_t) + buoyRequest.hdr.data_len),
            m_serverEndpoint);

    boost::system::error_code ec;
    size_t len = m_client.receive(
            asio::buffer(a_buoyArray_sp.get(), a_buoyArray_sp.getFreeLength()),
            boost::posix_time::seconds(1), ec);

    if (ec) {
        // -- timeout --
        // std::cout << "Receive error: " << ec.message() << std::endl;
        return 2;
    }
    a_buoyArray_sp.setTotalDataLength(len);

    if (len < sizeof(buoy_udp_pkt_hdr_t)) {
        L_(error) << "Got invalid UDP packet (len = " << len << ").";
        return 3;
    }
    buoy_udp_pkt_t *udpPkt_p = reinterpret_cast<buoy_udp_pkt_t *>(a_buoyArray_sp.get());

    crcResult.reset();
    crcResult.process_bytes(reinterpret_cast<unsigned char *>(&udpPkt_p->hdr), sizeof(buoy_udp_pkt_hdr_t) - sizeof(uint16_t));
    crcResult.process_bytes(reinterpret_cast<char *>(udpPkt_p->data), udpPkt_p->hdr.data_len);
    if (udpPkt_p->hdr.crc != crcResult.checksum()) {
        L_(error) << "Got invalid UDP packet (CRC mismatch).";
        return 3;
    }

    if (len == BUOY_UDP_PKT_HEADER_SIZE)
    {
        // -- got an empty UDP packet meaning the query was out of range for this day --
        L_(warning) << "Got invalid UDP data buffer [buf len=" << len << " / data size=" << udpPkt_p->hdr.data_len << "]";
        return 1;
    }
    else if (len < (BUOY_UDP_PKT_HEADER_SIZE + BUOY_MYSQL_DATA_PKT_HEADER_SIZE))
    {
        // -- an invalid package for the mysql was received --
        // TODO: Throw a valid exception
        throw std::string("ERROR: UDP packet length is smaller than the minimum expected!");
    }

    a_buoyArray_sp.setUsableDataLength(0);
    buoy_udp_pkt_t *udpPacket_p = reinterpret_cast<buoy_udp_pkt_t *>(a_buoyArray_sp.get());
    mysql_packet_data_t *mysqlReply_p = reinterpret_cast<mysql_packet_data_t *>(udpPacket_p->data);
    if (mysqlReply_p->hdr.packet_no >= 1) {
        m_isMysqlPacketOutOfRange = false;
    }

    a_buoyArray_sp.setUsableDataLength(udpPacket_p->hdr.data_len);
    a_buoyArray_sp.setPosition(
            (BUOY_UDP_PKT_HEADER_SIZE + BUOY_MYSQL_DATA_PKT_HEADER_SIZE));

    return 0;
} // mbp::BuoyUdpConnector::QueryMysql()


bool mbp::BuoyUdpConnector::isMysqlPacketOutOfRange() const
{
    return m_isMysqlPacketOutOfRange;
} // mbp::BuoyUdpConnector::isMysqlPacketOutOfRange()






void mbp::BuoyUdpConnector::RuntimeInstrumentControl(
        mbp::BuoySharedArray<uint8_t>& a_buoyArray_sp,
        const std::string &a_instrumentStr,
        mbp::buoy::instrument_command_e a_cmd)
    throw(std::invalid_argument, std::string)
{
    mbp::buoy::instruments_index_e instrument_e = getInstrument(a_instrumentStr);
    if (instrument_e == mbp::buoy::BUOY_INSTRUMENT_NA_e) {
        std::ostringstream sstr;
        sstr.str("");
        sstr << "Specified instrument does not exists";
        throw std::invalid_argument(sstr.str().c_str());
    }

    // boost::posix_time::ptime cmdDt = boost::posix_time::time_from_string(a_cmdDtStr);
    // boost::posix_time::ptime statusDt = boost::posix_time::time_from_string(a_statusDt);
    L_(warning)
        << "SCADA CMD "
        << "[instrument='" << a_instrumentStr << " (" << instrument_e << ")"
        << " / cmd=" << a_cmd
        << "]";

//    boost::asio::io_service ios;
//    asio::ip::udp::socket socket(ios); //m_ioService);
//    socket.open(asio::ip::udp::v4());

    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv,&tz);

    int commandBufSize = sizeof(mbp::buoy::instrument_command_data_t);

    buoy_udp_pkt_t *buoyRequest_p = reinterpret_cast<buoy_udp_pkt_t *>(m_udpPacketBuf_p);
    buoyRequest_p->hdr.flag = '@';
    buoyRequest_p->hdr.command = 'W';
    buoyRequest_p->hdr.data_len = commandBufSize;
    buoyRequest_p->hdr.utc_secs = tv.tv_sec;
    buoyRequest_p->hdr.utc_usecs = tv.tv_usec;
    buoyRequest_p->hdr.packet = 0;
    buoyRequest_p->hdr.crc = 0;

    mbp::buoy::instrument_command_data_t instrumentCommand;
    if (a_cmd == mbp::buoy::BUOY_INSTRUMENT_CMD_EXEC_STATUS_e) {
L_(warning) << "SCADA CMD sent STATUS to <0:0>";
        instrumentCommand.instrument.board = 0;
        instrumentCommand.instrument.port = 0;
        instrumentCommand.command = 0;
    } else {
        instrumentCommand.instrument = g_instruments[instrument_e];
        instrumentCommand.command = a_cmd;
    }
    instrumentCommand.res = 0;
    memcpy(buoyRequest_p->data, &instrumentCommand, commandBufSize);

    boost::crc_16_type crcResult;
    crcResult.process_bytes(reinterpret_cast<unsigned char *>(&(buoyRequest_p->hdr)), sizeof(buoy_udp_pkt_hdr_t) - sizeof(uint16_t));
    crcResult.process_bytes(reinterpret_cast<char *>(buoyRequest_p->data), buoyRequest_p->hdr.data_len);
    buoyRequest_p->hdr.crc = crcResult.checksum();

    m_socket.send_to(
            asio::buffer(reinterpret_cast<uint8_t *>(buoyRequest_p), sizeof(buoy_udp_pkt_hdr_t) + buoyRequest_p->hdr.data_len),
            m_serverEndpoint);

    /* ---- wait for the response ---- */
//    std::cout << "a_buoyArray_sp.getFreeLength() = " << a_buoyArray_sp.getFreeLength() << std::endl;

    // boost::asio::io_service ios;
//    client client(socket, ios); //m_ioService);
    boost::system::error_code ec;
    size_t len = m_client.receive(
            asio::buffer(a_buoyArray_sp.get(), a_buoyArray_sp.getFreeLength()), // 1000), //a_buoyArray_sp.getFreeLength()),
            boost::posix_time::seconds(1), ec);
//    socket.close();
    a_buoyArray_sp.setTotalDataLength(len);
    a_buoyArray_sp.setUsableDataLength(len);
    if (ec) {
        // -- timeout --
        // std::cout << "Receive error: " << ec.message() << std::endl;
        return;
    }

    if (len < BUOY_UDP_PKT_HEADER_SIZE) {
        L_(error) << "Got invalid UDP packet (len = " << len << ").";
        return;
    }
    a_buoyArray_sp.setPosition(BUOY_UDP_PKT_HEADER_SIZE);

    buoy_udp_pkt_t *buoyReply_p = reinterpret_cast<buoy_udp_pkt_t *>(a_buoyArray_sp.get());

    crcResult.reset();
    crcResult.process_bytes(reinterpret_cast<unsigned char *>(&buoyReply_p->hdr), sizeof(buoy_udp_pkt_hdr_t) - sizeof(uint16_t));
    crcResult.process_bytes(reinterpret_cast<char *>(buoyReply_p->data), buoyReply_p->hdr.data_len);
    if (buoyReply_p->hdr.crc != crcResult.checksum()) {
        L_(error) << "Got invalid UDP packet (CRC mismatch).";
        // TODO: Throw an exception
        return;
    }

    size_t expectedPktLen = BUOY_UDP_PKT_HEADER_SIZE + sizeof(mbp::buoy::instrument_command_reply_t);
    if (len == BUOY_UDP_PKT_HEADER_SIZE)
    {
        // -- got an empty UDP packet meaning the query was out of range for this day --
        // TODO: Throw an exception
        return;
    }
    else if (len != expectedPktLen)
    {
        // -- an invalid package for the mysql was received --
        // TODO: Throw a valid exception
        std::ostringstream sstr;
        sstr.str("");
        sstr
            << "ERROR: UDP packet length is smaller than the minimum expected"
            << " (got " << len
            << " / expecting " << expectedPktLen
            << ")!";
        throw std::string(sstr.str());
    }

/*
    mbp::buoy::instrument_command_reply_t commandReply;
    memcpy(&commandReply, reinterpret_cast<char *>(buoyReply_p->data), sizeof(mbp::buoy::instrument_command_reply_t));
    std::cout
        << "Buoy CMD reply"
        << " [udp.cmd = " << buoyReply_p->hdr.command
        << " / udp.len = " << a_buoyArray_sp.getTotalLength()
        << " / pkt.full_len = " << (buoyReply_p->hdr.data_len + BUOY_UDP_PKT_HEADER_SIZE)
        << " / pkt.data_only_len = " << buoyReply_p->hdr.data_len
        << " / pkt.packet = " << (uint32_t) buoyReply_p->hdr.packet
        << " / instrument = " << getInstrument(getInstrument(commandReply.cmd.instrument.board, commandReply.cmd.instrument.port)) << " (" << (uint32_t)commandReply.cmd.instrument.board << ":" << (uint32_t)commandReply.cmd.instrument.port << ")"
        << " / instrument cmd = " << (uint32_t)commandReply.cmd.command
        << " / execution status = " << commandReply.status
        << "]"
        << std::endl;
*/

    return;
} // mbp::BuoyUdpConnector::TODO TODO TODO()


mbp::buoy::instruments_index_e mbp::BuoyUdpConnector::getInstrument(
        uint8_t a_board,
        uint8_t a_port) const
{
    for (size_t idx=0; idx<(sizeof(g_instruments)/sizeof(mbp::buoy::_instruments_matrix_s )); ++idx) {
        if (
               (g_instruments[idx].board == a_board)
            && (g_instruments[idx].port == a_port)
           )
        {
            return (mbp::buoy::instruments_index_e)idx;
        }
    }

    return mbp::buoy::BUOY_INSTRUMENT_NA_e;
} // mbp::BuoyUdpConnector::getInstrument()


mbp::buoy::instruments_index_e mbp::BuoyUdpConnector::getInstrument(
        const std::string& a_instrument) const
{
    mbp::buoy::instruments_index_e instrument_e = mbp::buoy::BUOY_INSTRUMENT_NA_e;

    if (boost::iequals(a_instrument, "humidity")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_HUMIDITY_e;
    } else if (boost::iequals(a_instrument, "salinity")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_SALINITY_e;
    } else if (boost::iequals(a_instrument, "wind")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_WIND_e;
    } else if (boost::iequals(a_instrument, "compass")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_COMPASS_e;
    } else if (boost::iequals(a_instrument, "awac")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_AWAC_e;
    } else if (boost::iequals(a_instrument, "oxygen1")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_OXYGEN1_e;
    } else if (boost::iequals(a_instrument, "oxygen2")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_OXYGEN2_e;
    } else if (boost::iequals(a_instrument, "oxygen3")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_OXYGEN3_e;
    } else if (boost::iequals(a_instrument, "par")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_PAR_e;
    } else if (boost::iequals(a_instrument, "co2")) {
        instrument_e = mbp::buoy::BUOY_INSTRUMENT_CO2_e;
    }

    return instrument_e;
} // mbp::BuoyUdpConnector::getInstrument()


std::string mbp::BuoyUdpConnector::getInstrument(
        mbp::buoy::instruments_index_e a_instrument_e) const
{
    std::string instrument("");

    switch (a_instrument_e) {
        case mbp::buoy::BUOY_INSTRUMENT_HUMIDITY_e:
            instrument = "humidity";
            break;
        case mbp::buoy::BUOY_INSTRUMENT_SALINITY_e:
            instrument = "salinity";
            break;
        case mbp::buoy::BUOY_INSTRUMENT_WIND_e:
            instrument = "wind";
            break;
        case mbp::buoy::BUOY_INSTRUMENT_COMPASS_e:
            instrument = "compass";
            break;
        case mbp::buoy::BUOY_INSTRUMENT_AWAC_e:
            instrument = "awac";
            break;
        case mbp::buoy::BUOY_INSTRUMENT_OXYGEN1_e:
            instrument = "oxygen1";
            break;
        case mbp::buoy::BUOY_INSTRUMENT_OXYGEN2_e:
            instrument = "oxygen2";
            break;
        case mbp::buoy::BUOY_INSTRUMENT_OXYGEN3_e:
            instrument = "oxygen3";
            break;
        case mbp::buoy::BUOY_INSTRUMENT_PAR_e:
            instrument = "par";
            break;
        case mbp::buoy::BUOY_INSTRUMENT_CO2_e:
            instrument = "co2";
            break;
        default:
            instrument = "_n/a_";
            break;
    } // switch (a_instrument)

    return instrument;
}; // mbp::BuoyUdpConnector::getInstrumen()
