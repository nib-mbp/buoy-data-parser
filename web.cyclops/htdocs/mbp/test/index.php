<?php
//header("Content-Type: text/plain");
echo "<pre>";
echo <<<__EOF__

----
_SERVER:
__EOF__;
var_dump($_SERVER);

echo <<<__EOF__

----
_REQUEST:
__EOF__;
var_dump($_REQUEST);

echo <<<__EOF__

----
_COOKIE:
__EOF__;
var_dump($_COOKIE);
echo "</pre>";
