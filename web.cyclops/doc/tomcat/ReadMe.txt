#-- context.xml --#
 File:          context.xml
 Description:
  The file defines the JNDI connection to the DB data source. The DB connection
  has to be defined in the Tomcat configuration.
  Check the 'Resource' entry from the mentioned file.
 

#-- web.xml --#
 File:          web.xml
 Description:
  This file defines the resource reference to be used for web applications.
  Check the 'resource-ref' entry.


#-- RESTful specs --
http://www.restapitutorial.com/lessons/httpmethods.html
