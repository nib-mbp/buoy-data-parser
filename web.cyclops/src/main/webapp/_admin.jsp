<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<div class="page-header">
  <h1 class="pull-left">Energetika</h1>
  <div class="pull-right">
    <tags:calendar/>
  </div>
</div>


<!-- WIND -->
<div class="row" id="wind">
  <div class="col-md-2">
    <strong>Vetromer (wind)</strong>
  </div>
  <div class="col-md-2">
    stanje: <span id="wind-status">nalagam</span>
  </div>
  <div class="span8">
    <button class="btn btn-success buoy-cmd" id="wind-on" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOn">
      <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
      Vklop
    </button>
    <span class="rightMargin">&nbsp;</span>
    <button class="btn btn-danger buoy-cmd" id="wind-off" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOff">
      <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
      Izklop
    </button>
  </div>
</div>
<p>&nbsp;</p>

<!-- AIR (HUMIDITY) -->
<div class="row" id="air">
  <div class="col-md-2">
    <strong>Vlagomer (air)</strong>
  </div>
  <div class="col-md-2">
    stanje: <span id="air-status">nalagam</span>
  </div>
  <div class="span8">
    <button class="btn btn-success buoy-cmd" id="air-on" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOn">
      <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
      Vklop
    </button>
    <span class="rightMargin">&nbsp;</span>
    <button class="btn btn-danger buoy-cmd" id="air-off" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOff">
      <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
      Izklop
    </button>
  </div>
</div>
<p>&nbsp;</p>

<!-- COMPASS -->
<div class="row" id="compass">
  <div class="col-md-2">
    <strong>Kompas (compass)</strong>
  </div>
  <div class="col-md-2">
    stanje: <span id="compass-status">nalagam</span>
  </div>
  <div class="span8">
    <button class="btn btn-success buoy-cmd" id="compass-on" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOn">
      <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
      Vklop
    </button>
    <span class="rightMargin">&nbsp;</span>
    <button class="btn btn-danger buoy-cmd" id="compass-off" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOff">
      <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
      Izklop
    </button>
  </div>
</div>
<p>&nbsp;</p>

<!-- SALINITY (SeaBird) -->
<div class="row" id="seaWater">
  <div class="col-md-2">
    <strong>SeaBird (sea_water)</strong>
  </div>
  <div class="col-md-2">
    stanje: <span id="seaWater-status">nalagam</span>
  </div>
  <div class="span8">
    <button class="btn btn-success buoy-cmd" id="seaWater-on" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOn">
      <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
      Vklop
    </button>
    <span class="rightMargin">&nbsp;</span>
    <button class="btn btn-danger buoy-cmd" id="seaWater-off" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOff">
      <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
      Izklop
    </button>
  </div>
</div>
<p>&nbsp;</p>

<!-- OXYGEN -->
<div class="row" id="o2">
  <div class="col-md-2">
    <strong>Kisikomer (O<sub>2</sub>)</strong>
  </div>
  <div class="col-md-2">
    stanje: <span id="o2-status">nalagam</span>
  </div>
  <div class="span8">
    <button class="btn btn-success buoy-cmd" id="o2-on" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOn">
      <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
      Vklop
    </button>
    <span class="rightMargin">&nbsp;</span>
    <button class="btn btn-danger buoy-cmd" id="o2-off" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOff">
      <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
      Izklop
    </button>
  </div>
</div>
<p>&nbsp;</p>

<!-- AWAC -->
<div class="row" id="awac">
  <div class="col-md-2">
    <strong>Tokomer (AWAC)</strong>
  </div>
  <div class="col-md-2">
    stanje: <span id="awac-status">nalagam</span>
  </div>
  <div class="span8">
    <button class="btn btn-success buoy-cmd" id="awac-on" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOn">
      <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
      Vklop
    </button>
    <span class="rightMargin">&nbsp;</span>
    <button class="btn btn-danger buoy-cmd" id="awac-off" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOff">
      <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
      Izklop
    </button>
  </div>
</div>
<p>&nbsp;</p>

<!-- PAR -->
<div class="row" id="par">
  <div class="col-md-2">
    <strong>PAR tipalo</strong>
  </div>
  <div class="col-md-2">
    stanje: <span id="par-status">nalagam</span>
  </div>
  <div class="span8">
    <button class="btn btn-success buoy-cmd" id="par-on" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOn">
      <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
      Vklop
    </button>
    <span class="rightMargin">&nbsp;</span>
    <button class="btn btn-danger buoy-cmd" id="par-off" data-toggle="buoyCmd" data-cmd-func="instrumentCmdOff">
      <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
      Izklop
    </button>
  </div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
