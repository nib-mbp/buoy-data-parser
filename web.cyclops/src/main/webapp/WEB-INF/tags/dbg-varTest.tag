<%@ tag language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="inVar" fragment="false" required="true" type="java.lang.String"%>
<%@ variable name-given="outVar" scope="AT_END"%>

<pre>
Echo (inVar):  '${inVar}'
Echo (outVar): '${outVar}'
</pre>

<c:set var="outVar" value="Test value set inside the custom tag"/>
