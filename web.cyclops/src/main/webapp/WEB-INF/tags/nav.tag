<%@ tag language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="customMenu" fragment="false" type="java.lang.String" required="false" %>
<%@ attribute name="isLoggedIn" fragment="false" type="java.lang.Boolean" required="true" %>
<%@ attribute name="showLoginMenu" fragment="false" type="java.lang.Boolean" required="false" %>
<%@ attribute name="hasAdminRights" fragment="false" type="java.lang.Boolean" required="true" %>
<%@ attribute name="menuChoice" fragment="false" type="org.dezo.mbp.buoy.cyclops.servlet.NavMenu" required="true" %>

<c:if test="${showLoginMenu == null}">
  <c:set var="showLoginMenu" value="true" />
</c:if>

<nav class="navbar navbar-default navbar-fixed-top navbar-inverse">
  <div class="container">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed"
          data-toggle="collapse"
          data-target="#bs-example-navbar-collapse-1"
          aria-expanded="false">
          <span class="sr-only">Toggle navigation</span> <span
            class="icon-bar"></span> <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Boja Vida</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse"
        id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <c:if test="${not empty customMenu}">
            <li ${(menuChoice == 'CUSTOM_MENU') ? 'class="active"' : ''}><a href="#">${customMenu}</a></li>
          </c:if>
          <c:if test="${showLoginMenu}">
            <li ${(menuChoice == 'LOGIN') ? 'class="active"' : ''}><a href="${rootUrlPrefix}?op=login">Prijava</a></li>
          </c:if>
          <c:choose>
            <c:when test="${isLoggedIn}">
              <li ${(menuChoice == 'INSTRUMENTS') ? 'class="active"' : ''}><a href="${rootUrlPrefix}?op=instruments">Merilniki<span class="sr-only">(current)</span></a></li>
              <li ${(menuChoice == 'ENERGY') ? 'class="active"' : ''}><a href="${rootUrlPrefix}?op=energy">Energetika</a></li>
              <li ${(menuChoice == 'STATUS') ? 'class="active"' : ''}><a href="${rootUrlPrefix}?op=status">Stanje boje</a></li>
              <c:if test="${hasAdminRights}">
                <li ${(menuChoice == 'ADMIN') ? 'class="active"' : ''}><a href="${rootUrlPrefix}?op=admin">Kontrola instrumentov</a></li>
              </c:if>
            </c:when>
          </c:choose>
        </ul>

        <c:if test="${isLoggedIn}">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" 
                 class="dropdown-toggle" data-toggle="dropdown"
                 role="button" aria-haspopup="true" aria-expanded="false">${sessionDetails.displayName} &nbsp;<span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a id="logout-btn" href="#">Odjava</a></li>
              </ul></li>
          </ul>
        </c:if>

        <input id="root-url-prefix" type="hidden" name="rootUrlPrefix" value="${rootUrlPrefix}" />

      </div>
    </div>
  </div>
</nav>
