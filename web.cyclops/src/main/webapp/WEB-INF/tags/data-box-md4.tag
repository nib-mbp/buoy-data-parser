<%@ tag language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="boxVar" fragment="false" type="java.lang.String" required="true" %>
<%@ attribute name="title" fragment="false" type="java.lang.String" required="true" %>
<%@ attribute name="leftCustomHtml" fragment="false" type="java.lang.String" required="false" %>

<div class="box-outer col-md-4">
  <div class="box" id="${boxVar}">
    <div class="box-header">
      <i class="glyphicon glyphicon-repeat" id="${boxVar}-icon"></i> ${title} (<span id="${boxVar}-dt" class="updateTime">nalagam</span>)
    </div>
    <div class="box-content">
      <c:if test="${not empty leftCustomHtml}">${leftCustomHtml}</c:if>
    </div>
  </div>
</div>
