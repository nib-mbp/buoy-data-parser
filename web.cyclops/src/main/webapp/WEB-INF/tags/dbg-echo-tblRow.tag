<%@ tag language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="name" fragment="false" type="java.lang.String" required="true" %>
<%@ attribute name="val" fragment="false" type="java.lang.String" required="true" %>

<tr>
  <td>${name}</td>
  <td><code>${val}</code></td>
</tr>
