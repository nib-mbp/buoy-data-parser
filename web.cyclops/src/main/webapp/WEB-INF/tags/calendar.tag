<%@ tag language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="calendar">
  <jsp:useBean id="now" class="java.util.Date" />
  <fmt:setLocale value="sl_SI" scope="page" />
  <fmt:setTimeZone value="GMT" scope="page" />
  <fmt:formatDate value="${now}" pattern="dd. MMMM yyyy" /><br/>
  <small><fmt:formatDate value="${now}" pattern="EEEE" />, <span id="time"></span><fmt:formatDate value="${now}" pattern="HH:mm" /></small>
</div>
