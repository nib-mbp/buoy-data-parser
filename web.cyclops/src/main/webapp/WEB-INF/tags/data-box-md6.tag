<%@ tag language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="boxVar" fragment="false" type="java.lang.String" required="true" %>
<%@ attribute name="title" fragment="false" type="java.lang.String" required="true" %>
<%@ attribute name="displayErrCode" fragment="false" type="java.lang.Boolean" required="false" %>
<%@ attribute name="leftCustomHtml" fragment="false" type="java.lang.String" required="false" %>
<%@ attribute name="rightDisplayStatus" fragment="false" type="java.lang.Boolean" required="false" %>
<%@ attribute name="rightCustomHtml" fragment="false" type="java.lang.String" required="false" %>

<div class="box-outer col-md-6">
  <div class="box" id="${boxVar}">
    <div class="box-header">
      <i class="glyphicon glyphicon-repeat" id="${boxVar}-icon"></i> ${title} (<span id="${boxVar}-dt" class="updateTime">nalagam</span>)
      <c:if test="${displayErrCode}">
        <span class="pull-right">
          Err.koda = <span id="${boxVar}-err">nalagam</span>
        </span>
      </c:if>
    </div>
    <div class="box-content">
      <div class="row">
        <c:if test="${not empty leftCustomHtml}">
          <div class="col-md-8">${leftCustomHtml}</div>
        </c:if>
        <c:if test="${rightDisplayStatus or not empty rightCustomHtml}">
          <div class="col-md-4">
            <c:if test="${rightDisplayStatus}">
              <strong>Stanje</strong> = <span id="${boxVar}-status">nalagam</span><br/>
              <br/>
            </c:if>
            <c:if test="${not empty rightCustomHtml}">${rightCustomHtml}</c:if>
          </div>
        </c:if>
      </div>
    </div>
  </div>
</div>
