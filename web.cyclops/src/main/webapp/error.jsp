<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%@ include file="_header.jsp" %>

<div id="nav">
  <tags:nav menuChoice="CUSTOM_MENU" customMenu="ERROR" showLoginMenu="false" isLoggedIn="false" hasAdminRights="false" />
</div>

<div id="container" class="container-fluid" style="min-height: 100%; position: relative;">
  <div class="page-header">
<%--
    <div class="pull-right">
      <tags:calendar />
    </div>
--%>
    <div class="alert alert-danger">
      <h1>ERROR</h1>
      <ul>
        <c:choose>
          <c:when test="${rootUrlPrefix eq null}">
            <li><strong>Invalid server configuration</strong> - missing servlet root!</li>
          </c:when>
          <c:when test="${not isProxyAuthenticated}">
            <li><strong>Invalid server configuration</strong> - application not properly secured!</li>
          </c:when>
          <c:otherwise>
            <li><strong>Unknown error occurred!</strong></li>
          </c:otherwise>
        </c:choose>
      </ul>
      <p>&nbsp;</p>
    </div>
  </div>
</div>

<%@ include file="_footer.jsp" %>
