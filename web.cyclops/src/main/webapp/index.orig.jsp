<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%-- These libraries are required for the <c> and <sql> tags --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %> 
<!--  %@taglib uri="http://example.com/scada" prefix="f" % -->
<!-- %@ taglib prefix="f" uri="WEB-INF/scada.tld"% -->
<%@ taglib uri="http://dezo.org/mbp/buoy/cyclops" prefix="scada" %> 
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "java.util.Enumeration" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>

<sql:query var="result" dataSource="jdbc/SCADA">
  SELECT `buoy_data_4_web`.`f_getScadaData`()
</sql:query>

<table border="1">
  <%-- Output column names on a header row --%>
  <tr>
    <c:forEach var="columnName" items="${result.columnNames}">
      <th><c:out value="${columnName}"/></th>
    </c:forEach>
  </tr>

  <%-- Output each row of data --%>
  <c:forEach var="row" items="${result.rowsByIndex}">
    <tr>
      <%-- Output each column of data --%>
      <c:forEach var="col" items="${row}">
        <td><c:out value="${col}"/></td>
      </c:forEach>
    </tr>
  </c:forEach>
</table>
<pre>
URI:  ${pageContext.request.requestURI}
Port: ${pageContext.request.remotePort}
Host: ${pageContext.request.remoteHost}
Addr: ${pageContext.request.remoteAddr}
header:    ${header["host"]}
req.scope: ${requestScope['javax.servlet.forward.servlet_path']}
ctx.path:  ${pageContext.request.contextPath}
test1:     {scada:hello("Aaron")}
test2:     {scada:isHello("test")}
isSession: {scada:isSessionValidStatic("bla")}
</pre>
<%
/*
String username = request.getParameter("username");
String password = request.getParameter("password");
User user = userService.find(username, password);

if (user != null) { 
  request.getSession().setAttribute("user", user); // Make available by ${user} in session scope.
  response.sendRedirect("userhome");
} else {
  request.setAttribute("message", "Unknown login, try again"); // Make available by ${message} in request scope.
  request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
}
*/
  ArrayList<String> parameterNames = new ArrayList<String>();
  Enumeration<String> attrs = request.getAttributeNames();
  while (attrs.hasMoreElements()) {
      out.println(attrs.toString() + "<br/>");
      attrs.nextElement();
  }

  out.println(request.getRemoteAddr());
  out.println( request.getRemoteHost() );
  int a = 5;
  out.print(a);
%>

Test <%= a %>
<br/>

<tags:displayProducts>
  <jsp:attribute name="normalPrice">
    Item: ${name}<br/>
    Price: ${price}
  </jsp:attribute>
  <jsp:attribute name="onSale">
    Item: ${name}<br/>
    <font color="red"><strike>Was: ${origPrice}</strike></font><br/>
    <b>Now: ${salePrice}</b>
  </jsp:attribute>
</tags:displayProducts>

<tags:echo-long echo="4" index="bala"/>

<tags:echo-long echo="55" index="kaka" />

</body>
</html>