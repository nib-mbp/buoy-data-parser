<!DOCTYPE html>
<%@page import="com.sun.xml.internal.txw2.Document"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%-- These libraries are required for the <c> and <sql> tags --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %> 
<!--  %@taglib uri="http://example.com/scada" prefix="f" % -->
<!-- %@ taglib prefix="f" uri="WEB-INF/scada.tld"% -->
<%--
  <%@ taglib uri="http://dezo.org/mbp/buoy/cyclops" prefix="scada" %> 
 --%>
 <%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "java.util.Enumeration" %>


<%@page import="java.sql.*, javax.sql.*, javax.naming.*"%>
<%
boolean isSessionValid = false;

DataSource ds = null;
Connection conn = null;
ResultSet result = null;
Statement stmt = null;
ResultSetMetaData rsmd = null;
try {
    Context context = new InitialContext();
    Context envCtx = (Context) context.lookup("java:comp/env");
    ds =  (DataSource)envCtx.lookup("jdbc/SCADA");
    if (ds != null) {
        conn = ds.getConnection();
        stmt = conn.createStatement();
        result = stmt.executeQuery("SELECT 1");
        if (result.next()) {
            isSessionValid = result.getBoolean(1);
        }
    }
} catch (SQLException e) {
    System.out.println("DB connection error occurred " + e);
}
%>

<!--  DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd" -->
<html lang="sl">
<head>
<head>
  <meta charset="uutf-8">
  <title>Project Buoy</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="DEZO.org">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <!-- Optional theme -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

  <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,300,700|Droid+Sans:400,700|Lato:300,400,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link href="css/style.css" rel="stylesheet">

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="ico/favicon.png">

  <script src="js/jquery.js"></script>
  <script src="js/jquery.confirm.min.js"></script>
  <script src="js/custom.js"></script>
  <script>
  if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      var msViewportStyle = document.createElement("style");
      msViewportStyle.appendChild(
          document.createTextNode(
              "@-ms-viewport{width:auto!important}"
          )
      );
      document.getElementsByTagName("head")[0].
          appendChild(msViewportStyle);
  }
  </script>
</head>

<body>
<hr width="60%"/>
<h2>Nazaj</h2>
<pre>
inVar1: ${inVar1}
outVar2: ${outVar2}
</pre>
<hr width="60%"/>
<div>
<sql:query dataSource="jdbc/SCADA" var="result" maxRows="1">
  SELECT TRUE AS id;
</sql:query>
<c:choose>
  <c:when test="${result.rowCount == 1}">
    <c:set var="isSessionValid" value="${result.rows[0].id}"></c:set>
  </c:when>
  <c:otherwise>
    <c:set var="isSessionValid" value="false"></c:set>
  </c:otherwise>
</c:choose>

isSessionValid = ${isSessionValid}

<h2>Parameters</h2>
<c:forEach items='${initParam}' var='parameter'>  
 <ul>
            <%-- Display the key of the current item, which
                 corresponds to the name of the init param --%>
            <li>Name: <c:out value='${parameter.key}'/></li>

            <%-- Display the value of the current item, which
                 corresponds to the value of the init param --%>
            <li>Value: <c:out value='${parameter.value}'/></li>
         </ul>
      </c:forEach>



Bla = ${pageContext.request.remoteAddr} <br/>
<h2>Parameters:</h2>
<div>
<%
Enumeration<String> paramNames = request.getParameterNames();
while(paramNames.hasMoreElements()) {
    String current = (String) paramNames.nextElement();

    out.println(current + "=" + request.getAttribute(current) + "<br/>");
}
%>
</div>

<h2>Attirbutes:</h2>
<div>
<%
Enumeration<String> attributeNames = request.getAttributeNames();
while(attributeNames.hasMoreElements()) {
    String current = (String) attributeNames.nextElement();

    out.println(current + "=" + request.getAttribute(current) + "<br/>");
}
%>
</div>

<table>
  <thead>
    <tr>
      <th>Param name</th>
      <th>Value</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach items="${param}" var="entry">
      <tr>
        <td>${entry.key}</td>
        <td>
          <c:forEach items="${entry.value}" var="paramValue">
            <code>${paramValue}</code>
          </c:forEach>
        </td>
      </tr>
    </c:forEach>
  </tbody>
</table>

<h2>HTTP Headers</h2>
<table>
  <thead>
    <tr>
      <th>HTTP Header</th>
      <th>Value</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach items="${headerValues}" var="entry">
      <tr>
        <td>${entry.key}</td>
        <td>
          <c:forEach items="${entry.value}" var="headerValue">
            <code>${headerValue}</code>
          </c:forEach>
        </td>
      </tr>
    </c:forEach>
  </tbody>
</table>

</div>
  <div id="nav">
    <!-- ?php include('nav.php'); ? -->
  </div>

  <div id="container" class="container-fluid" style="min-height: 100%; position: relative;">
    <div class="row-fluid">
      <div id="sidebar" class="hidden">
        <!-- ?php include('sidebar.php'); ? -->
      </div>

      <div id="content">
        <div id="main" class="container-fluid">
          <!-- ?php
            include('content.php');
          ? -->
        </div> 
      </div>
    </div>
  </div> <!-- /container -->


  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </body>
</html>
<%
    Cookie cookie = new Cookie("test-name", "My Value");
    response.addCookie(cookie);
%>