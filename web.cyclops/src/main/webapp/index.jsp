<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%@ include file="_header.jsp" %>

<p>&nbsp;</p>

<c:set var="contentJsp" scope="request" value=""/>
<c:set var="scriptJs" scope="request" value=""/>
<c:choose>
  <c:when test="${param.op.equalsIgnoreCase('LOGIN')}">
    <c:set var="op" scope="request" value="LOGIN"/>
  </c:when>
  <c:when test="${param.op.equalsIgnoreCase('INSTRUMENTS')}">
    <c:set var="op" scope="request" value="INSTRUMENTS"/>
    <c:set var="contentJsp" scope="request" value="_instruments.jsp"/>
    <c:set var="scriptJs" scope="request" value="js/instruments.js"/>
  </c:when>
  <c:when test="${param.op.equalsIgnoreCase('ENERGY')}">
    <c:set var="op" scope="request" value="ENERGY"/>
    <c:set var="contentJsp" scope="request" value="_energy.jsp"/>
    <c:set var="scriptJs" scope="request" value="js/energy.js"/>
  </c:when>
  <c:when test="${param.op.equalsIgnoreCase('STATUS')}">
    <c:set var="op" scope="request" value="STATUS"/>
    <c:set var="contentJsp" scope="request" value="_status.jsp"/>
    <c:set var="scriptJs" scope="request" value="js/status.js"/>
  </c:when>
  <c:when test="${param.op.equalsIgnoreCase('ADMIN')}">
    <c:set var="op" scope="request" value="ADMIN"/>
    <c:set var="contentJsp" scope="request" value="_admin.jsp"/>
    <c:set var="scriptJs" scope="request" value="js/admin.js"/>
  </c:when>
  <c:otherwise>
    <c:set var="op" scope="request" value="INSTRUMENTS"/>
  </c:otherwise>
</c:choose>

<div id="nav">
  <tags:nav menuChoice="${op}" showLoginMenu="${not isUserSessionValid}" isLoggedIn="${isUserSessionValid}" hasAdminRights="${isUserCertAuthenticated}" />
</div>

<c:if test="${op.equalsIgnoreCase('LOGIN')}">
  <c:redirect url="login.jsp" />
</c:if>

<div id="container" class="container-fluid" style="min-height: 100%; position: relative;">

  <c:if test="${not empty contentJsp}">
    <jsp:include page="${contentJsp}" flush="false" />
  </c:if>

<%--   <div class="form-signin" style="margin-top: 30px; border-radius: 0;">
    <h2 class="form-signin-heading">Prijavi se</h2>
    <input id="login-username" type="text" class="form-control" placeholder="Uporabniško ime" name="uname" value="${pageContext.request.remoteUser}" disabled="disabled" />
    <input id="login-password" type="password" class="form-control" placeholder="Geslo" name="passwd"/>
    <button id="login-btn" class="btn btn-primary" type="submit" name="login">Prijava</button>
 --%>

</div>




<script src="js/nav.js"></script>
<c:if test="${not empty scriptJs}">
  <script src="${scriptJs}"></script>
</c:if>

<%@ include file="_footer.jsp" %>
