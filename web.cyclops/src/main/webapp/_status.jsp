<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<div class="page-header">
  <h1 class="pull-left">Energetika</h1>
  <div class="pull-right">
    <tags:calendar/>
  </div>
</div>


<!-- ROW.1 -->
<div class="row">
  <!-- buoy interior -->
  <c:set var="varName" value="bInterior"/>
  <tags:data-box-md6 title="Trup boje" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Temperatura</strong> = <span class="row12" id="bInterior-d0">nalagam</span><br/>
      <strong>Vlaga zraka</strong> = <span class="row12" id="bInterior-d1">nalagam</span> %
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>

  <!-- buoy co2 -->
  <c:set var="varName" value="co2"/>
  <tags:data-box-md6 title="CO2" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="co2-status">nalagam</span><br/>
      <strong>Koncentracija</strong> = <span class="row12" id="bCo2-d1">nalagam</span>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>
</div>


<!-- ROW.2 -->
<div class="row">
  <!-- loputa -->
  <c:set var="varName" value="door"/>
  <tags:data-box-md6 title="Loputa" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="door-status">nalagam</span>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>

  <!-- energetika / energetski modul -->
  <c:set var="varName" value="energy"/>
  <tags:data-box-md6 title="Energetika" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="energy-status">nalagam</span>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>
</div>


<!-- ROW.3 -->
<div class="row">
  <!-- radijski povezavi (summary) -->
  <c:set var="varName" value="wifi"/>
  <tags:data-box-md4 title="Radijski povezavi" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="wifi-status">nalagam</span>
    </jsp:attribute>
  </tags:data-box-md4>

  <!-- vdor vode (summary) -->
  <c:set var="varName" value="waterCmn"/>
  <tags:data-box-md4 title="Vdor vode" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="waterCmn-status">nalagam</span>
    </jsp:attribute>
  </tags:data-box-md4>

  <!-- kamere -->
  <c:set var="varName" value="camCmn"/>
  <tags:data-box-md4 title="Kamere" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="camCmn-status">nalagam</span>
    </jsp:attribute>
  </tags:data-box-md4>
</div>


<!-- ROW.4 -->
<div class="row">
  <c:set var="varName" value="gps"/>
  <tags:data-box-md12 title="GPS" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="gps-status">nalagam</span>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md12>
</div>


<!-- ROW.5 -->
<div class="row">
  <!-- WiFI 2.4 GHz -->
  <c:set var="varName" value="wifi24g"/>
  <tags:data-box-md6 title="WiFI 2.4 GHz" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="wifi24g-status">nalagam</span>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>

  <!-- WiFI 5 GHz -->
  <c:set var="varName" value="wifi5g"/>
  <tags:data-box-md6 title="WiFI 5 GHz" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="wifi5g-status">nalagam</span>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>
</div>


<!-- ROW.6 -->
<div class="row">
  <!-- Vdor vode - telo boje -->
  <c:set var="varName" value="waterBody"/>
  <tags:data-box-md6 title="Vdor vode - telo boje" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="waterBody-status">nalagam</span>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>

  <!-- Vdor vode - komora kamere -->
  <c:set var="varName" value="waterCam"/>
  <tags:data-box-md6 title="Vdor vode - komora kamere" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="waterCam-status">nalagam</span>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>
</div>

<!-- ROW.7 -->
<div class="row">
  <!-- kontrolna kamera 1 -->
  <c:set var="varName" value="cam1"/>
  <tags:data-box-md3 title="Cam.1<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="cam1-status">nalagam</span>
    </jsp:attribute>
  </tags:data-box-md3>

  <!-- kontrolna kamera 2 -->
  <c:set var="varName" value="cam2"/>
  <tags:data-box-md3 title="Cam.2<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="cam2-status">nalagam</span>
    </jsp:attribute>
  </tags:data-box-md3>

  <!-- kontrolna kamera 3 -->
  <c:set var="varName" value="cam3"/>
  <tags:data-box-md3 title="Cam.3<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="cam3-status">nalagam</span>
    </jsp:attribute>
  </tags:data-box-md3>

  <!-- kontrolna kamera 4 -->
  <c:set var="varName" value="cam4"/>
  <tags:data-box-md3 title="Cam.4<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="cam4-status">nalagam</span>
    </jsp:attribute>
  </tags:data-box-md3>
</div>


<!-- ROW.8 -->
<div class="row">
  <c:set var="varName" value="uwCam"/>
  <tags:data-box-md12 title="Podvodna kamera" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="uwCam-status">nalagam</span>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md12>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>

