<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
   <head>
      <title>Scoped Variables</title>
   </head>

   <body>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

      <%-- Set a page-scoped attribute named scope to 
           pageScope, requestScope, sessionScope, or
           applicationScope, depending on the value of a
           request parameter named scope --%>
      <c:choose>
         <c:when test='${param.scope == "page"}'>
            <c:set var='scope' value='${pageScope}'/>
         </c:when>
         <c:when test='${param.scope == "request"}'>
            <c:set var='scope' value='${requestScope}'/>
         </c:when>
         <c:when test='${param.scope == "session"}'>
            <c:set var='scope' value='${sessionScope}'/>
         </c:when>
         <c:when test='${param.scope == "application"}'>
            <c:set var='scope' value='${applicationScope}'/>
         </c:when>
      </c:choose>

      <font size='5'>
         <c:out value='${param.scope}'/>-scope attributes:
      </font><p>

      <%-- Loop over the JSTL implicit object, stored in the
           page-scoped attribute named scope that was set above.
           That implicit object is a map --%>
      <c:forEach items='${scope}' var='p'>
         <ul>
            <%-- Display the key of the current item, which
                 represents the parameter name --%>
            <li>Parameter Name: <c:out value='${p.key}'/></li>

            <%-- Display the value of the current item, which
                 represents the parameter value --%>
            <li>Parameter Value: <c:out value='${p.value}'/></li>
         </ul>
      </c:forEach>
   </body>
</html> 