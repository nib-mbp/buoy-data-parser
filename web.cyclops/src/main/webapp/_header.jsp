<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Buoy Vida</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Buoy Vida">
  <meta name="author" content="DEZO.org - Damir Deželjin">

  <!-- Bootstrap and its theme -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!--   <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,300,700|Droid+Sans:400,700|Lato:300,400,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
 -->
  <link href='//fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

  <link href="css/custom.css" rel="stylesheet">


  <!-- Fav and touch icons -->
<!--
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="ico/favicon.png">
 -->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<!--
  <script src="js/jquery.js"></script>
  <script src="js/jquery.confirm.min.js"></script>
  <script src="js/custom.js"></script>
 -->
 <script src="js/mbp-common.js"></script>
</head>

<body>
