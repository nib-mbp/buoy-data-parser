<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %> 

<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "java.util.Enumeration" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %> 


<!--  DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd" -->
<html lang="sl">
<head>
<head>
  <meta charset="uutf-8">
  <title>Project Buoy</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="DEZO.org">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <!-- Optional theme -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

  <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,300,700|Droid+Sans:400,700|Lato:300,400,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link href="css/custom.css" rel="stylesheet">

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="ico/favicon.png">

  <script src="js/jquery.js"></script>
  <script src="js/jquery.confirm.min.js"></script>
  <script src="js/custom.js"></script>
  <script>
  if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      var msViewportStyle = document.createElement("style");
      msViewportStyle.appendChild(
          document.createTextNode(
              "@-ms-viewport{width:auto!important}"
          )
      );
      document.getElementsByTagName("head")[0].
          appendChild(msViewportStyle);
  }
  </script>
</head>

<body>
<div>
<sql:query dataSource="jdbc/SCADA" var="result" maxRows="1">
  SELECT TRUE AS id;
</sql:query>
<c:choose>
  <c:when test="${result.rowCount == 1}">
    <c:set var="isSessionValid" value="${result.rows[0].id}"></c:set>
  </c:when>
  <c:otherwise>
    <c:set var="isSessionValid" value="false"></c:set>
  </c:otherwise>
</c:choose>

isSessionValid = ${isSessionValid}
</div>
  <div id="nav">
    <?php include('nav.php'); ?>
  </div>

  <div id="container" class="container-fluid" style="min-height: 100%; position: relative;">
    <div class="row-fluid">
      <div id="sidebar" class="hidden">
        <?php include('sidebar.php'); ?>
      </div>

      <div id="content">
        <div id="main" class="container-fluid">
          <?php
          include('content.php');
            ?>
        </div> 
      </div>
    </div>
  </div> <!-- /container -->

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </body>
</html>