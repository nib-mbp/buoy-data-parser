<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<div class="page-header">
  <h1 class="pull-left">Energetika</h1>
  <div class="pull-right">
    <tags:calendar/>
  </div>
</div>


<!-- ROW.1 -->
<div class="row">
  <!-- napetost sistema -->
  <c:set var="varName" value="voltage"/>
  <tags:data-box-md6 title="Napetost sistema" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Napetost</strong> = <span id="voltage-d0">nalagam</span> V<br/>
      &nbsp;<br/>
      &nbsp;
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml">
      <strong>Stanje</strong> = <span id="voltage-status">nalagam</span><br/>
    </jsp:attribute>
  </tags:data-box-md6>

  <!-- CPE -->
  <c:set var="varName" value="cpe"/>
  <tags:data-box-md6 title="CPE enota" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Temperatura</strong> = <span id="cpe-d0">nalagam</span> °C<br/>
      <strong>Vlaga</strong> = <span id="cpe-d1">nalagam</span> %<br/>
      <strong>Tok</strong> = <span id="cpe-d2">nalagam</span> A
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>
</div>


<!-- ROW.2 -->
<div class="row">
  <!-- battery pack 1 -->
  <c:set var="varName" value="bat1"/>
  <tags:data-box-md6 title="Akumulatorski sklop 1" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Napetost</strong> = <span id="bat1-d0">nalagam</span> V<br/>
      <strong>El.tok (min / max / avg)</strong><br/>
      <ul>
        <li><strong>Bat1:</strong> <span id="bat1-d1">nalagam</span> A</li>
        <li><strong>Bat2:</strong> <span id="bat1-d2">nalagam</span> A</li>
      </ul><br/>
      <strong>Stanje</strong> = <span id="bat1-d3">nalagam</span> (<span id="bat1-d4">nalagam</span>)<br/>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>

  <!-- battery pack 2 -->
  <c:set var="varName" value="bat2"/>
  <tags:data-box-md6 title="Akumulatorski sklop 2" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Napetost</strong> = <span id="bat2-d0">nalagam</span> V<br/>
      <strong>El.tok (min / max / avg)</strong><br/>
      <ul>
        <li><strong>Bat1:</strong> <span id="bat2-d1">nalagam</span> A</li>
        <li><strong>Bat2:</strong> <span id="bat2-d2">nalagam</span> A</li>
      </ul><br/>
      <strong>Stanje</strong> = <span id="bat2-d3">nalagam</span> (<span id="bat2-d4">nalagam</span>)<br/>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md6>
</div>


<!-- ROW.3 -->
<!-- Current consumption -->
<div class="row">
  <c:set var="varName" value="elCurr1"/>
  <tags:data-box-md4 title="Tok.veja #1" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Tok</strong> = <span id="elCurr1-d0">nalagam</span> A
    </jsp:attribute>
  </tags:data-box-md4>

  <c:set var="varName" value="elCurr2"/>
  <tags:data-box-md4 title="Tok.veja #2" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Tok</strong> = <span id="elCurr2-d0">nalagam</span> A
    </jsp:attribute>
  </tags:data-box-md4>

  <c:set var="varName" value="elCurr3"/>
  <tags:data-box-md4 title="Tok.veja #3" boxVar="${varName}">
    <jsp:attribute name="leftCustomHtml">
      <strong>Tok</strong> = <span id="elCurr3-d0">nalagam</span> A
    </jsp:attribute>
  </tags:data-box-md4>
</div>


<!-- ROW.4 -->
<div class="row">
  <c:set var="varName" value="mppt"/>
  <tags:data-box-md12 title="Solarni paneli" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Celice</strong><br>
      <strong>Napetost</strong> = <span id="mppt-d0">nalagam</span> V<br>
      <strong>Tok</strong> = <span id="mppt-d1">nalagam</span> A
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml">
      <strong>Baterije</strong><br>
      <strong>Napetost</strong> = <span id="mppt-d2">nalagam</span> V<br>
      <strong>Tok</strong> = <span id="mppt-d3">nalagam</span> A
    </jsp:attribute>
  </tags:data-box-md12>
</div>

<!-- ROW.5 -->
<div class="row">
  <c:set var="varName" value="fuel"/>
  <tags:data-box-md12 title="Gorivna celica" boxVar="${varName}" displayErrCode="false" rightDisplayStatus="false">
    <jsp:attribute name="leftCustomHtml">
      <strong>Stanje</strong> = <span class="row12" id="fuel-d0">nalagam</span> <br>
      <strong>Tok</strong> = <span id="fuel-d1">nalagam</span> A
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml"></jsp:attribute>
  </tags:data-box-md12>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
