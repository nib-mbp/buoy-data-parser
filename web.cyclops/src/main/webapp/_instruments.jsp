<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<div class="page-header">
  <h1 class="pull-left">Merilniki</h1>
  <div class="pull-right">
    <tags:calendar/>
  </div>
</div>


<!-- WIND -->
<div class="row">
  <c:set var="varName" value="wind"/>
  <tags:data-box-md12 title="Vetromer" boxVar="${varName}" displayErrCode="true" rightDisplayStatus="true">
    <jsp:attribute name="leftCustomHtml">
      <strong>U</strong> = <span id="${varName}-l0">nalagam</span><br/>
      <strong>V</strong> = <span id="${varName}-l1">nalagam</span><br/>
      <strong>W</strong> = <span id="${varName}-l2">nalagam</span><br/>
      <strong>Hitrost</strong> = <span id="${varName}-l3">nalagam</span> m/s<br/>
      <strong>Smer</strong> = <span id="${varName}-l4">nalagam</span> °
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml">
      <strong>El.tok (min / max / avg)</strong><br/>
      <span id="${varName}-r0">nalagam</span> /
      <span id="${varName}-r1">nalagam</span> /
      <span id="${varName}-r2">nalagam</span> A<br/>
      (<span id="${varName}-r3">nalagam</span>)
    </jsp:attribute>
  </tags:data-box-md12>
</div>

<!-- AIR (HUMIDITY) -->
<div class="row">
  <c:set var="varName" value="air"/>
  <tags:data-box-md12 title="Vlagomer" boxVar="${varName}" displayErrCode="true" rightDisplayStatus="true">
    <jsp:attribute name="leftCustomHtml">
      <strong>Temperatura</strong> = <span id="air-l0">nalagam</span> °C<br/>
      <strong>Vlaga zraka</strong> = <span id="air-l1">nalagam</span> %
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml">
      <strong>El.tok (min / max / avg)</strong><br/>
      <span id="air-r0">nalagam</span> /
      <span id="air-r1">nalagam</span> /
      <span id="air-r2">nalagam</span> A<br/>
      (<span id="air-r3">nalagam</span>)
    </jsp:attribute>
  </tags:data-box-md12>
</div>

<!-- COMPASS -->
<div class="row">
  <c:set var="varName" value="compass"/>
  <tags:data-box-md12 title="Kompas" boxVar="${varName}" displayErrCode="true" rightDisplayStatus="true">
    <jsp:attribute name="leftCustomHtml">
      <strong>Heading</strong> = <span id="compass-l0">nalagam</span> °<br/>
      <strong>Pitch</strong> = <span id="compass-l1">nalagam</span> °<br/>
      <strong>Roll</strong> = <span id="compass-l2">nalagam</span> °
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml">
      <strong>El.tok (min / max / avg)</strong><br/>
      <span id="compass-r0">nalagam</span> /
      <span id="compass-r1">nalagam</span> /
      <span id="compass-r2">nalagam</span> A<br/>
      (<span id="compass-r3">nalagam</span>)
    </jsp:attribute>
  </tags:data-box-md12>
</div>

<!-- SALINITY (SeaBird) -->
<div class="row">
  <c:set var="varName" value="seaWater"/>
  <tags:data-box-md12 title="SeaBird" boxVar="${varName}" displayErrCode="true" rightDisplayStatus="true">
    <jsp:attribute name="leftCustomHtml">
      <strong>Temperatura</strong> = <span id="seaWater-l0">nalagam</span> °C<br/>
      <strong>Prevodnost</strong> = <span id="seaWater-l1">nalagam</span> S<br/>
      <strong>Slanost</strong> = <span id="seaWater-l2">nalagam</span> %<br/>
      <strong>Chl-A</strong> = <span id="seaWater-l3">nalagam</span> mg/m<sup>3</sup>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml">
      <strong>El.tok (min / max / avg)</strong><br/>
      <span id="seaWater-r0">nalagam</span> /
      <span id="seaWater-r1">nalagam</span> /
      <span id="seaWater-r2">nalagam</span> A<br/>
      (<span id="seaWater-r3">nalagam</span>)
    </jsp:attribute>
  </tags:data-box-md12>
</div>

<!-- OXYGEN -->
<div class="row">
  <c:set var="varName" value="o2"/>
  <tags:data-box-md12 title="Kisikomer" boxVar="${varName}" displayErrCode="true" rightDisplayStatus="true">
    <jsp:attribute name="leftCustomHtml">
      <strong>Koncentracija O<sub>2</sub></strong> = <span id="o2-l0">nalagam</span> mol/m<sup>3</sup>
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml">
      <strong>El.tok (min / max / avg)</strong><br/>
      <span id="o2-r0">nalagam</span> /
      <span id="o2-r1">nalagam</span> /
      <span id="o2-r2">nalagam</span> A<br/>
      (<span id="o2-r3">nalagam</span>)
    </jsp:attribute>
  </tags:data-box-md12>
</div>

<!-- AWAC -->
<div class="row">
  <c:set var="varName" value="awac"/>
  <tags:data-box-md12 title="Tokomer AWAC" boxVar="${varName}" displayErrCode="true" rightDisplayStatus="true">
    <jsp:attribute name="leftCustomHtml">
      <strong>Temperatura</strong> = <span id="awac-l0">nalagam</span> °C<br/>
      <strong>Pritisk</strong> = <span id="awac-l1">nalagam</span> hPa
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml">
      <strong>El.tok (min / max / avg)</strong><br/>
      <span id="awac-r0">nalagam</span> /
      <span id="awac-r1">nalagam</span> /
      <span id="awac-r2">nalagam</span> A<br/>
      (<span id="awac-r3">nalagam</span>)
    </jsp:attribute>
  </tags:data-box-md12>
</div>

<!-- PAR -->
<div class="row">
  <c:set var="varName" value="par"/>
  <tags:data-box-md12 title="PAR tipalo" boxVar="${varName}" displayErrCode="true" rightDisplayStatus="true">
    <jsp:attribute name="leftCustomHtml">
      <strong>Meritev</strong> = <span id="par-l0">nalagam</span> &#181;mol/sm2
    </jsp:attribute>
    <jsp:attribute name="rightCustomHtml">
      <strong>El.tok (min / max / avg)</strong><br/>
      <span id="par-r0">nalagam</span> /
      <span id="par-r1">nalagam</span> /
      <span id="par-r2">nalagam</span> A<br/>
      (<span id="par-r3">nalagam</span>)
    </jsp:attribute>
  </tags:data-box-md12>
</div>


<p>&nbsp;</p>

<c:if test="${isUserSessionValid}">
  <br/>
  isUserSessionValid -&gt; TRUE
</c:if>

<p>&nbsp;</p>
