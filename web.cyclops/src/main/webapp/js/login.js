$(document).ready(function() {

  $('#login-password').keypress(function(e) {
    if (e.which == 13) {
      $('#login-btn').click();
      return false;
    }
  });

  // -- login --
  $("#login-btn").click(function() {
    var endpointUrl = 'webapi/Authentication/login';
    var method = 'POST';
    var requestObj = {
      "username" : $("#login-username").val(),
      "password" : $("#login-password").val()
    };
    var requestJson = JSON.stringify(requestObj);
    $.ajax({
      url : endpointUrl,
      data : requestJson,
      contentType : 'application/json; charset=UTF-8',
      dataType : 'json',
      method : method,
      success : function(data, textStatus, jqXHR) {
        var msg = 'Login: HTTP.' + jqXHR.status + ' -> ' + jqXHR.statusText;
        showSuccessAlert(msg);
        var opVal = getParameterByName('op');
        if (! opVal || 0 === opVal.lengt)
          opVal = 'instruments'
          var rootUrl = $('#root-url-prefix').val() + '?op=' + opVal;
        window.location.replace(rootUrl);
      },
      error : function(jqXHR, textStatus, errorThrown) {
        var msg = 'Login: HTTP.' + jqXHR.status + ' -> ' + jqXHR.statusText;
        showErrorAlert(msg);
      }
    });
  }); // login

  // // -- logout --
  // $("#logout-btn").click(function() {
  // var endpointUrl = 'webapi/Authentication/logout';
  // var method = 'GET';
  // $.ajax({
  // url: endpointUrl,
  // dataType: 'json',
  // method: method,
  // success: function(data, textStatus, jqXHR) {
  // var msg = 'Logout: HTTP.' + jqXHR.status + ' -> ' + jqXHR.statusText;
  // showSuccessAlert(msg);
  //
  // var rootUrl = $('#root-url-prefix').val();
  // window.location.replace(rootUrl);
  // },
  // error: function(jqXHR, textStatus, errorThrown) {
  // var msg = 'Logout: HTTP.' + jqXHR.status + ' -> ' + jqXHR.statusText;
  // showErrorAlert(msg);
  // }
  // });
  //  }); // logout

});
