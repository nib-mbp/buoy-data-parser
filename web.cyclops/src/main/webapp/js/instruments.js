$(function () {
  var MAX_VALID_DATA_AGE_sec = 3600;    // how old data are still considered valid [seconds]
  var STATE_OK = 0;           // OK / Normal operation
  var STATE_FAIL = 1;         // FAIL / Stopped / service mode
  var STATE_INITIALIZING = 2; // Initializing
  var STATE_NA = 3;           // N/A
  var statesArray = ["OK", "Error / Stopped", "Initializing", "N/A"];
  var iconsArray = ["glyphicon-ok-sign", "glyphicon-exclamation-sign", "glyphicon-question-sign", "glyphicon-repeat"];

  var fetchingData = false;

  function updateInstrument(instrument, dt, status, error, leftArr, rightArr, maxDataAge) {
    // -- check data age -> if older than 1 hour, override the status to red --
    if (typeof(maxDataAge) == undefined) {
      var maxDataAge = 0;
    } else if (maxDataAge > 0) {
      var nowDate = new Date();
      var nowDt = nowDate.valueOf() / 1000 + nowDate.getTimezoneOffset() * 60;
      var dtDiffSec = nowDt.valueOf() - (new Date(dt.replace(/-/g, "/"))).valueOf() / 1000;
      if (dtDiffSec > maxDataAge)
        status = STATE_FAIL;
    }

    // -- bar color --
    if (error == 0 && status == STATE_OK)
      $('#' + instrument).removeClass('orange red').addClass('green');
    else if (error == 0 && status == STATE_INITIALIZING)
      $('#' + instrument).removeClass('green red').addClass('orange');
    else if (error != 0 || status == STATE_FAIL)
      $('#' + instrument).removeClass('green orange').addClass('red');
    else
      $('#' + instrument).removeClass('green orange red').addClass('');

    // -- icon --
    $('#' + instrument + '-icon').removeClass(iconsArray.join(' ')).addClass(iconsArray[status]);

    // -- data --
    $('#' + instrument + '-status').text(statesArray[status]);
    $('#' + instrument + '-dt').text(dt);
    $('#' + instrument + '-err').text(error);
    for (var i = 0; i < leftArr.length; ++i) {
      $('#' + instrument + '-l' + i).text(leftArr[i]);
    }
    for (var i = 0; i < rightArr.length; ++i) {
      $('#' + instrument + '-r' + i).text(rightArr[i]);
    }
    return;
  } // updateInstrument()

  // Convert UNIX time into something nicer.
  function timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['jan', 'feb', 'mar', 'apr', 'maj', 'jun', 'jul', 'avg', 'sep', 'okt', 'nov', 'dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate() + 'tralalal';
    var hour = (a.getHours() < 10 ? '0' + a.getHours() : a.getHours());
    var min = (a.getMinutes() < 10 ? '0' + a.getMinutes() : a.getMinutes());
    var sec = (a.getSeconds() < 10 ? '0' + a.getSeconds() : a.getSeconds());
    var time = date + '-' + month + '-' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
  }

  function getMeasurements() {
    $.getJSON("webapi/Data/get")
      .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log("Request Failed: " + err);
      })
      .done(function (data) {

      // -- wind --
      updateInstrument(
        instrument = "wind",
        dt = data["inst"].wind['dt'],
        status = data["inst"].wind['status'],
        error = data["inst"].wind['err'],
        leftArr = [
          data["inst"].wind['u'], data["inst"].wind['v'], data["inst"].wind['w'], data["inst"].wind['vspd'], data["inst"].wind['vdir']
        ],
        rightArr = [
          data["inst"].wind['currMin'], data["inst"].wind['currMax'], data["inst"].wind['currAvg'], data["inst"].wind['currDt']
        ],
        MAX_VALID_DATA_AGE_sec
      );

      // -- air (humidity) --
      updateInstrument(
        instrument = "air",
        dt = data["inst"].air['dt'],
        status = data["inst"].air['status'],
        error = data["inst"].air['err'],
        leftArr = [
          data["inst"].air['temp'], data["inst"].air['humid']
        ],
        rightArr = [
          data["inst"].air['currMin'], data["inst"].air['currMax'], data["inst"].air['currAvg'], data["inst"].air['currDt']
        ],
        MAX_VALID_DATA_AGE_sec
      );

      // -- compass --
      updateInstrument(
        instrument = "compass",
        dt = data["inst"].compass['dt'],
        status = data["inst"].compass['status'],
        error = data["inst"].compass['err'],
        leftArr = [
          data["inst"].compass['heading'], data["inst"].compass['pitch'], data["inst"].compass['roll']
        ],
        rightArr = [
          data["inst"].compass['currMin'], data["inst"].compass['currMax'], data["inst"].compass['currAvg'], data["inst"].compass['currDt']
        ],
        MAX_VALID_DATA_AGE_sec
      );

      // -- salinity (SeaBird) --
      updateInstrument(
        instrument = "seaWater",
        dt = data["inst"].seaWater['dt'],
        status = data["inst"].seaWater['status'],
        error = data["inst"].seaWater['err'],
        leftArr = [
          data["inst"].seaWater['temp'], data["inst"].seaWater['cond'], data["inst"].seaWater['salinity'], data["inst"].seaWater['chla']
        ],
        rightArr = [
          data["inst"].seaWater['currMin'], data["inst"].seaWater['currMax'], data["inst"].seaWater['currAvg'], data["inst"].seaWater['currDt']
        ],
        MAX_VALID_DATA_AGE_sec
      );

      // -- oxygen --
      updateInstrument(
        instrument = "o2",
        dt = data["inst"].o2['dt'],
        status = data["inst"].o2['status'],
        error = data["inst"].o2['err'],
        leftArr = [
          data["inst"].o2['conc']
        ],
        rightArr = [
          data["inst"].o2['currMin'], data["inst"].o2['currMax'], data["inst"].o2['currAvg'], data["inst"].o2['currDt']
        ],
        MAX_VALID_DATA_AGE_sec
      );

      // -- awac --
      updateInstrument(
        instrument = "awac",
        dt = data["inst"].awac['dt'],
        status = data["inst"].awac['status'],
        error = data["inst"].awac['err'],
        leftArr = [
          data["inst"].awac['temp'], data["inst"].awac["pressure"]
        ],
        rightArr = [
          data["inst"].awac['currMin'], data["inst"].awac['currMax'], data["inst"].awac['currAvg'], data["inst"].awac['currDt']
        ],
        MAX_VALID_DATA_AGE_sec
      );

      // -- par --
      updateInstrument(
        instrument = "par",
        dt = data["inst"].par['dt'],
        status = data["inst"].par['status'],
        error = data["inst"].par['err'],
        leftArr = [
          data["inst"].par['value_raw']
        ],
        rightArr = [
          data["inst"].par['currMin'], data["inst"].par['currMax'], data["inst"].par['currAvg'], data["inst"].par['currDt']
        ],
        MAX_VALID_DATA_AGE_sec
      );

      fetchingData = false;

    });

  }

  // Get measurements on page load.
  getMeasurements();

  // Get measurements every 5s.
  setInterval(function () {
    if (!fetchingData) {
      fetchingData = true;
      getMeasurements();
    }
  }, 5000);

});
