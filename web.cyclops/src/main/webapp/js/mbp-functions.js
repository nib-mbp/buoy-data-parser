

function getCookie(cookieName){
  var cookieArray = document.cookie.split(';');
  for(var i=0; i<cookieArray.length; i++){
    var cookie = cookieArray[i];
    while (cookie.charAt(0)==' '){
      cookie = cookie.substring(1);
    }
    cookieHalves = cookie.split('=');
    if(cookieHalves[0]== cookieName){
      return cookieHalves[1];
    }
  }
  return "";
} // getCookie()

function createTOC() {
  var ToC =
    "<nav role='navigation' class='table-of-contents'>" +
      "<ul>";

  var newLine, el, title, link;
  $("body ul h3").each(function() {
    el = $(this);
    title = el.text();
    link = "#" + el.attr("id");

    newLine =
      "<li>" +
        "<a href='" + link + "'>" +
          title +
        "</a>" +
      "</li>";

    ToC += newLine;
  });

  ToC +=
    "</ul>" +
   "</nav>";

  return ToC;
} // createTOC()