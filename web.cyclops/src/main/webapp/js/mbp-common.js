function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function showSuccessAlert(msg) {
    if (msg === '') {
        msg = "Success";
    }
    
    var html = $('<div id="successPopup" class="alert alert-success popup" role="alert" data-dismiss="alert" aria-label="Close">success</div>');
    html.html(msg);
    html.fadeTo(5000, 500).slideUp(500, function () {
        $(this).alert('close');
    });
    $('body').prepend(html).alert();
}

function showErrorAlert(msg) {
  if (msg === '') {
      msg = "Error";
  }

  var html = $('<div id="errorPopup" class="alert alert-danger popup" role="alert" data-dismiss="alert" aria-label="Close">error</div>');
  html.html(msg);
  html.fadeTo(5000, 500).slideUp(500, function () {
      $(this).alert('close');
  });
  $('body').prepend(html).alert();
}
