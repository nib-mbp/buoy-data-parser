$(function() {
  var MAX_VALID_DATA_AGE_sec = 3600; // how old data are still considered valid
                                      // [seconds]
  var DEFAULT_DT = '1970-01-01 00:00:00';
  var STATE_OK = 0; // OK / Normal operation
  var STATE_FAIL = 1; // FAIL / Stopped / service mode
  // var STATE_INITIALIZING = 2; // Initializing
  var STATE_SERVICE_MODE = 2; // Service mode
  var STATE_NA = 3; // N/A
  // var statesArray = ["OK", "Error / Stopped / Service", "Initializing",
  // "N/A"];
  var statesArray = [ "OK", "Alarm", "Service / Initializing", "N/A" ];
  var iconsArray = [ "glyphicon-ok-sign", "glyphicon-exclamation-sign",
      "glyphicon-question-sign", "glyphicon-repeat" ];

  var fetchingData = false;
  /*
   * var status = ['ni aktivna', 'aktivna'];
   * 
   */

  function updateSensor(instrument, dt, status, error, dataArr, maxDataAge) {
    // -- check data age -> if older than 1 hour, override the status to red --
    if (typeof (maxDataAge) == undefined) {
      var maxDataAge = 0;
    } else if (maxDataAge > 0) {
      var nowDate = new Date();
      var nowDt = nowDate.valueOf() / 1000 + nowDate.getTimezoneOffset() * 60;
      var dtDiffSec = nowDt.valueOf()
          - (new Date(dt.replace(/-/g, "/"))).valueOf() / 1000;
      if (dtDiffSec > maxDataAge)
        status = STATE_FAIL;
    }

    // -- bar color --
    if (error == 0 && status == STATE_OK)
      $('#' + instrument).removeClass('orange red').addClass('green');
    else if (error == 0 && status == STATE_SERVICE_MODE)
      $('#' + instrument).removeClass('green red').addClass('orange');
    else if (error != 0 || status == STATE_FAIL)
      $('#' + instrument).removeClass('green orange').addClass('red');
    else
      $('#' + instrument).removeClass('green orange red').addClass('');

    // -- icon --
    $('#' + instrument + '-icon').removeClass(iconsArray.join(' ')).addClass(
        iconsArray[status]);

    // -- data --
    // $('#' + instrument + '-status').text(statesArray[status]);
    $('#' + instrument + '-dt').text(dt);
    // $('#' + instrument + '-err').text(error);
    for (var i = 0; i < dataArr.length; ++i) {
      $('#' + instrument + '-d' + i).text(dataArr[i]);
    }
    return;
  } // updateSensor()

  // Update the sensor status
  function updateStatus(sensorTag, data) {
    if (data["status"] == STATE_OK) {
      $('#' + sensorTag).removeClass('orange red').addClass('green');
    } else if (data["status"] == STATE_FAIL) {
      $('#' + sensorTag).removeClass('green orange').addClass('red');
    } else if (data["status"] == STATE_SERVICE_MODE) {
      $('#' + sensorTag).removeClass('green red').addClass('orange');
    } else {
      $('#' + sensorTag).removeClass('green orange red').addClass('');
    }
    // -- icon --
    $('#' + sensorTag + '-icon').removeClass(iconsArray.join(' ')).addClass(
        iconsArray[data["status"]]);
    // -- text status and dt --
    $('#' + sensorTag + '-dt').text(data["dt"]);
    $('#' + sensorTag + '-status').text(statesArray[data["status"]]);
  }

  function getStatus() {
    $.getJSON("webapi/Data/get")
      .fail(function(jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request Failed: " + err);
      })
      .done(
        function(data) {
          updateStatus("door", data.status.door);
          updateStatus("co2", data.status.co2);
          updateStatus("energy", data.status.energy);
          updateStatus("wifi", data.status.wifi);
          updateStatus("waterCmn", data.status.waterCmn);
          updateStatus("camCmn", data.status.camCmn);
          updateStatus("gps", data.status.gps);
          updateStatus("wifi24g", data.status.wifi24g);
          updateStatus("wifi5g", data.status.wifi5g);
          updateStatus("waterBody", data.status.waterBody);
          updateStatus("waterCam", data.status.waterCam);
          updateStatus("cam1", data.status.cam1);
          updateStatus("cam2", data.status.cam2);
          updateStatus("cam3", data.status.cam3);
          updateStatus("cam4", data.status.cam4);
          updateStatus("uwCam", data.status.uwCam);

          // -- co2 sensor --
          var co2State = (data["inst"].bCo2['dt'] == DEFAULT_DT) ? STATE_NA
              : STATE_OK;
          updateSensor(instrument = "bCo2", dt = data["inst"].bCo2['dt'],
              status = co2State, error = 0, // data["inst"].wind['err'],
              dataArr = [ statesArray[status],
                  data["inst"].bCo2['concentration'] ], maxDataAge = 0);
          // $("#co2-concentration").text(data.inst["bCo2"].concentration + " ("
          // + data.inst["bCo2"].dt + ")");

          // -- buoy interior --
          updateSensor(instrument = "bInterior", dt = data["inst"].wind['dt'],
              status = STATE_OK, // data["inst"].wind['status'],
              error = 0, // data["inst"].wind['err'],
              dataArr = [ data["inst"].bInterior['temp'],
                  data["inst"].bInterior['humid'] ],
              maxDataAge = MAX_VALID_DATA_AGE_sec);
          fetchingData = false;
        });

  }

  // Get measurements on page load.
  getStatus();

  // Get measurements every 5s.
  setInterval(function() {
    if (!fetchingData) {
      fetchingData = true;
      getStatus();
    }
  }, 5000);

});
