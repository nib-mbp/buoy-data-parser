$(function() {
  var MAX_VALID_DATA_AGE_sec = 3600; // how old data are still considered valid
                                      // [seconds]
  var STATE_OK = 0; // OK / Normal operation
  var STATE_FAIL = 1;
  var STATE_ALARM = STATE_FAIL; // FAIL / Stopped / service mode
  var STATE_INITIALIZING = 2;
  var STATE_WARNING = STATE_INITIALIZING; // Initializing
  var STATE_NA = 3; // N/A
  var statesArray = [ "OK", "Alarm", "Warning", "N/A" ];
  var statesBatInfo = [ "NAPAJA", "NAPOLNJEN", "SE POLNI", "OPOZORILO", "ALARM", "neznano" ];
  var fuelStatusesArray = [ 'izključena', 'vključena' ];
  var iconsArray = [ "glyphicon-ok-sign", "glyphicon-exclamation-sign",
      "glyphicon-question-sign", "glyphicon-repeat" ];

  function updatePowerSensor(instrument, dt, status, dataArr, maxDataAge, statusInfoText) {
    // -- check data age -> if older than 1 hour, override the status to red --
    if (typeof (maxDataAge) === "undefined") {
      var maxDataAge = 0;
    } else if (maxDataAge > 0) {
      var nowDate = new Date();
      var nowDt = nowDate.valueOf() / 1000 + nowDate.getTimezoneOffset() * 60;
      var dtDiffSec = nowDt.valueOf()
          - (new Date(dt.replace(/-/g, "/"))).valueOf() / 1000;
      if (dtDiffSec > maxDataAge)
        status = STATE_FAIL;
    }

    // -- bar color --
    if (status == STATE_OK)
      $('#' + instrument).removeClass('orange red').addClass('green');
    else if (status == STATE_WARNING)
      $('#' + instrument).removeClass('green red').addClass('orange');
    else if (status == STATE_ALARM)
      $('#' + instrument).removeClass('green orange').addClass('red');
    else
      $('#' + instrument).removeClass('green orange red').addClass('');

    // -- icon --
    $('#' + instrument + '-icon').removeClass(iconsArray.join(' ')).addClass(
        iconsArray[status]);

    // -- data --
    if (typeof (statusInfoText) === "undefined") {
      $('#' + instrument + '-status').text(statesArray[status]);
    } else {
      $('#' + instrument + '-status').text(statusInfoText);
    }
    $('#' + instrument + '-dt').text(dt);
    for (var i = 0; i < dataArr.length; ++i) {
      $('#' + instrument + '-d' + i).text(dataArr[i]);
    }
    return;
  } // updatePowerSensor()

  var fetchingData = false;
  /*
   * var status = ['izključena', 'se prazni', 'se polni', 'v rezervi', 'v
   * okvari'];
   */
  function getSensors() {
    $.getJSON("webapi/Data/get")
      .fail(function(jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request Failed: " + err);
      })
      .done(
        function(data) {
          // -- system voltage (napetost sistema) --
          var voltage = data["power"].cmnVoltage["voltage"];
          var voltageStatus = STATE_ALARM;
          if (voltage > 12.2)
            // color: green
            voltageStatus = STATE_OK;
          else if (voltage > 11.9)
            // color: orange
            voltageStatus = STATE_WARNING;
          else
            // color: red
            voltageStatus = STATE_ALARM;
          updatePowerSensor(instrument = "voltage",
              dt = data["power"].cmnVoltage["dt"], status = voltageStatus,
              dataArr = [ data["power"].cmnVoltage["voltage"] ],
              MAX_VALID_DATA_AGE_sec);

          // -- system voltage (napetost sistema) --
          updatePowerSensor(instrument = "cpe", dt = data["inst"].bBoard["dt"],
              status = STATE_OK, dataArr = [ data["inst"].bBoard["temp"],
                  data["inst"].bBoard["humid"], data["inst"].bBoard["curr"] ],
              MAX_VALID_DATA_AGE_sec);

          // -- battery pack 1 --
          updatePowerSensor(instrument = "bat1", dt = data["power"].bat["dt"],
              status = data["status"].bat1["status"],
              dataArr = [
                  data["power"].bat["volMin"][0],
                  [ data["power"].bat["currMin"][0],
                      data["power"].bat["currMax"][0],
                      data["power"].bat["currAvg"][0] ].join(" / "),
                  [ data["power"].bat["currMin"][1],
                      data["power"].bat["currMax"][1],
                      data["power"].bat["currAvg"][1] ].join(" / "),
                  statesBatInfo[ data["status"].bat1["statusInfo"]],
                  data["status"].bat1["dt"] ], MAX_VALID_DATA_AGE_sec
          );

          // -- battery pack 2 --
          updatePowerSensor(instrument = "bat2", dt = data["power"].bat["dt"],
              status = data["status"].bat2["status"],
              dataArr = [
                  data["power"].bat["volMin"][1],
                  [ data["power"].bat["currMin"][2],
                      data["power"].bat["currMax"][2],
                      data["power"].bat["currAvg"][2] ].join(" / "),
                  [ data["power"].bat["currMin"][3],
                      data["power"].bat["currMax"][3],
                      data["power"].bat["currAvg"][3] ].join(" / "),
                  statesBatInfo[ data["status"].bat2["statusInfo"]],
                  data["status"].bat2["dt"] ], MAX_VALID_DATA_AGE_sec
          );

          // -- El.Current loop 1 --
          var idxCurrLoop = 0;
          updatePowerSensor(instrument = "elCurr1",
              dt = data["power"].currentLoops[idxCurrLoop]["dt"],
              status = STATE_OK,
              dataArr = [ data["power"].currentLoops[idxCurrLoop]["current"] ],
              MAX_VALID_DATA_AGE_sec);
          // -- El.Current loop 2 --
          idxCurrLoop = 1;
          updatePowerSensor(instrument = "elCurr2",
              dt = data["power"].currentLoops[idxCurrLoop]["dt"],
              status = STATE_OK,
              dataArr = [ data["power"].currentLoops[idxCurrLoop]["current"] ],
              MAX_VALID_DATA_AGE_sec);
          // -- El.Current loop 3 --
          idxCurrLoop = 2;
          updatePowerSensor(instrument = "elCurr3",
              dt = data["power"].currentLoops[idxCurrLoop]["dt"],
              status = STATE_OK,
              dataArr = [ data["power"].currentLoops[idxCurrLoop]["current"] ],
              MAX_VALID_DATA_AGE_sec);

          // -- Solarni paneli (mppt) --
          updatePowerSensor(instrument = "mppt", dt = data["power"].mppt["dt"],
              status = STATE_OK, dataArr = [ data["power"].mppt["inU"],
                  data["power"].mppt["inI"], data["power"].mppt["outU"],
                  data["power"].mppt["outI"] ], MAX_VALID_DATA_AGE_sec);

          // -- Fuel cells --
          updatePowerSensor(instrument = "fuel",
              dt = data["power"].fuelCell["dt"], status = STATE_OK, dataArr = [
                  fuelStatusesArray[data["power"].fuelCell["status"]],
                  data["power"].fuelCell["current"] ], MAX_VALID_DATA_AGE_sec);

          fetchingData = false;

        });

  }

  // Get measurements on page load.
  getSensors();

  // Get measurements every 5s.
  setInterval(function() {
    if (!fetchingData) {
      fetchingData = true;
      getSensors();
    }
  }, 5000);

});
