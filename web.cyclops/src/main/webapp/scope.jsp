<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
   <head>
      <title>Select a Scope</title>
   </head>

   <body>
      <form action='show_scope_attributes.jsp'>
         Select a scope:
         <select name='scope'>
            <option value='page'>page</option>
            <option value='request'>request</option>
            <option value='session'>session</option>
            <option value='application'>application</option>
         </select>

         <p><input type='submit' value='Show Scope Attributes'/>
      </form>
   </body>
</html> 