<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%@ include file="_header.jsp" %>

<div id="nav">
  <tags:nav menuChoice="LOGIN" showLoginMenu="true" isLoggedIn="${isUserSessionValid}" hasAdminRights="false" />
</div>

<div id="container" class="container-fluid" style="min-height: 100%; position: relative;">
  <div class="form-signin" style="margin-top: 30px; border-radius: 0;">
    <h2 class="form-signin-heading">Prijavi se</h2>
    <input id="login-username" type="text" class="form-control" placeholder="Uporabniško ime" name="uname" value="${pageContext.request.remoteUser}" disabled="disabled" />
    <input id="login-password" type="password" class="form-control" placeholder="Geslo" name="passwd"/>
    <button id="login-btn" class="btn btn-primary" type="submit" name="login">Prijava</button>
  </div>
</div>

<script src="js/nav.js"></script>
<script src="js/login.js"></script>

<%@ include file="_footer.jsp" %>
