<!DOCTYPE html>
<%@ page import="com.sun.xml.internal.txw2.Document"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%@ page import = "java.util.Map" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "java.util.Enumeration" %>
<%@ page import="java.sql.*, javax.sql.DataSource, javax.naming.*"%>

<html lang="en">
<head>
<head>
  <meta charset="utf-8">
  <title>Project Buoy - DBG page</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="DEZO.org">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <!-- Optional theme -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

  <link href="//fonts.googleapis.com/css?family=Roboto:400,600,300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">

  <link href="css/style.css" rel="stylesheet">

  <link rel="shortcut icon" href="ico/favicon.png">
</head>

<body>
<h1>Project Buoy - DBG Test Page</h1>

<hr/>
<div>
  <h2>HTTP Headers</h2>
  <div>
    <table>
      <thead>
        <tr>
          <th>HTTP Header</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach items="${headerValues}" var="entry">
          <tr>
            <td>${entry.key}</td>
            <td>
              <c:forEach items="${entry.value}" var="headerValue">
                <code>${headerValue}</code>
              </c:forEach>
            </td>
          </tr>
        </c:forEach>
      </tbody>
    </table>
  </div>
</div>

<hr/>
<div>
  <h2>pageContext.request</h2>
  <div>
    <table>
      <thead>
        <tr>
          <th>name</th>
          <th>value</th>
        </tr>
      </thead>
      <tbody>
        <tags:dbg-echo-tblRow name="remoteAddr" val="${pageContext.request.remoteAddr}"/>
        <tags:dbg-echo-tblRow name="remoteHost" val="${pageContext.request.remoteHost}"/>
        <tags:dbg-echo-tblRow name="protocol" val="${pageContext.request.protocol}"/>
        <tags:dbg-echo-tblRow name="scheme" val="${pageContext.request.scheme}"/>
        <tags:dbg-echo-tblRow name="secure" val="${pageContext.request.secure}"/>
        <tags:dbg-echo-tblRow name="authType" val="${pageContext.request.authType}"/>
        <tags:dbg-echo-tblRow name="method" val="${pageContext.request.method}"/>
        <tags:dbg-echo-tblRow name="queryString" val="${pageContext.request.queryString}"/>
        <tags:dbg-echo-tblRow name="remoteUser" val="${pageContext.request.remoteUser}"/>
        <tags:dbg-echo-tblRow name="requestURI" val="${pageContext.request.requestURI}"/>
        <tags:dbg-echo-tblRow name="servletPath" val="${pageContext.request.servletPath}"/>
        <tags:dbg-echo-tblRow name="userPrincipal" val="${pageContext.request.userPrincipal}"/>
        <c:forEach items="${pageContext.request.attributeNames}" var="attrName">
          <%
            String attrName = (String)pageContext.findAttribute("attrName");
            request.setAttribute("attrVal", request.getAttribute(attrName));
          %>
          <tags:dbg-echo-tblRow name="${attrName}" val="${attrVal}"/>
        </c:forEach>
        <c:forEach items="${pageContext.request.parameterMap}" var="parName">
          <c:forEach items="${parName.value}" var="currItem" varStatus="stat">
            <c:set var="listValue" value="${stat.first ? '' : listValue} ${currItem}" />
          </c:forEach>
          <tags:dbg-echo-tblRow name="parameter['${parName.key}']" val="${listValue}"/>
        </c:forEach>
      </tbody>
    </table>
  </div>
</div>

<hr/>
<div>
  <h2>Custom tag - in/out variables</h2>
  <div>
    <h3>Input (before tag call)</h3>
    <tags:dbg-varTest inVar="Input value" />
    <h3>Output (after tag call)</h3>
    <pre>
Echo (inVar):  '${inVar}'
Echo (outVar): '${outVar}'
</pre>
  </div>
</div>

<hr/>
<div>
  <h2>EL and JSP DB queries</h2>

  <div>
    <sql:query dataSource="jdbc/SCADA" var="result" maxRows="1">
      SELECT TRUE AS id;
    </sql:query>
    <c:choose>
      <c:when test="${result.rowCount == 1}">
        <c:set var="elSqlQueryRes" value="${result.rows[0].id}"></c:set>
      </c:when>
      <c:otherwise>
        <c:set var="elSqlQueryRes" value="false"></c:set>
      </c:otherwise>
    </c:choose>
<%
  boolean isSessionValid = false;
  
  DataSource ds = null;
  Connection conn = null;
  ResultSet result = null;
  Statement stmt = null;
  ResultSetMetaData rsmd = null;
  try {
      Context context = new InitialContext();
      Context envCtx = (Context) context.lookup("java:comp/env");
      ds =  (DataSource)envCtx.lookup("jdbc/SCADA");
      if (ds != null) {
          conn = ds.getConnection();
          stmt = conn.createStatement();
          result = stmt.executeQuery("SELECT 1");
          if (result.next()) {
              isSessionValid = result.getBoolean(1);
          }
      }
      request.setAttribute("jspSqlQueryRes", isSessionValid);
  } catch (SQLException e) {
      System.out.println("<pre>DB connection error occurred " + e + "</pre>");
  }
%>
    <h3>Query results</h3>
    <pre>
EL SQL query result:  ${elSqlQueryRes}
JSP SQL query result: ${jspSqlQueryRes}
</pre>
  </div>
</div>

<hr/>
<div>
  <h2>Retrieve parameter from web.xml</h2>
  <div>
    <h3>EL result:</h3>
    <pre>
X-Mbp-ProxySignature: '<c:out
      value="${(not empty initParam['X-Mbp-ProxySignature'])
               ? initParam['X-Mbp-ProxySignature']
               : '<null or empty>'}"
      />'
</pre>
  </div>
  <br/>
  <div>
    <h3>JSP result:</h3>
    <pre>
X-Mbp-ProxySignature: '<%
  ServletContext sCtx = pageContext.getServletContext();
  String xMbpProxySignature = sCtx.getInitParameter("X-Mbp-ProxySignature");
  if ((xMbpProxySignature != null) && (xMbpProxySignature.length() > 0)) {
    out.print(xMbpProxySignature);
  } else {
    out.print("&lt;null or empty&gt;");
  }
%>'
</pre>
  </div>
</div>

<hr/>
<div>
  isProxyAuthenticated = ${isProxyAuthenticated} <br/>
  isUserCertAuthenticated = ${isUserCertAuthenticated} <br/>
  isUserSessionValid = ${isUserSessionValid} <br/>
  pageContext.request.remoteUser = ${pageContext.request.remoteUser} <br/>
  sessionDetails = ${sessionDetails} <br/>
  priimek = ${sessionDetails.surname} <br/>
  op = ${param.op} <br/>
  rootUrlPrefix = ${rootUrlPrefix eq null ? "null" : "not null"}
</div>

  <!-- Latest compiled and minified JavaScript -->
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </body>
</html>

<%
  Cookie cookie = new Cookie("test-name", "My Value");
  response.addCookie(cookie);
%>
