package org.dezo.mbp.buoy.cyclops.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.dezo.mbp.buoy.cyclops.core.AuthService;
import org.dezo.mbp.buoy.cyclops.core.MbpException;
import org.dezo.mbp.buoy.cyclops.core.MbpNotAuthorizedException;
import org.dezo.mbp.buoy.cyclops.dao.api.IAuthenticationDao;
import org.dezo.mbp.buoy.cyclops.model.beans.misc.GenericResponseMessage;
import org.dezo.mbp.buoy.cyclops.model.beans.misc.UserAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/Authentication")
public class AuthenticationRest {
    final Logger logger = LoggerFactory.getLogger(AuthenticationRest.class);

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response login(@Context HttpServletRequest httpRequest, @Context HttpHeaders headers,
            UserAuthentication authData)
                    throws WebApplicationException
    {
        if (authData == null) {
            throw new MbpException("Bad request");
        } else if ((authData.username == null) || (authData.password == null)) {
            throw new MbpException("Bad request");
        }

        List<NewCookie> cookies = null;
        AuthService authService = new AuthService();
        cookies = authService.authenticate(httpRequest.getContextPath(), authData.username, authData.password);

        if (cookies.size() != 2) {
            throw new MbpNotAuthorizedException("Bad username/password combination");
        }

        GenericResponseMessage message = new GenericResponseMessage();
        message.code = Response.Status.OK.getStatusCode();
        message.message = "OK - Authenticated";
        Response response = Response
                .status(message.code)
                .cookie(cookies.toArray(new NewCookie[cookies.size()]))
                .entity(message)
                .build();
        return response;
    } // login()

    @GET
    @Path("/logout")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response logout(@Context HttpServletRequest httpRequest, @Context HttpHeaders headers,
            @CookieParam(value = IAuthenticationDao.cookieAuthUsername) String username,
            @CookieParam(value = IAuthenticationDao.cookieAuthSessionId) String sessionToken)
                    throws WebApplicationException
    {
        List<NewCookie> cookies = null;
        AuthService authService = new AuthService();
        cookies = authService.logout(httpRequest.getContextPath(), username, sessionToken);

        GenericResponseMessage message = new GenericResponseMessage();
        message.code = Response.Status.OK.getStatusCode();
        message.message = "OK - Logged off";
        Response response = Response
                .status(message.code)
                .cookie(cookies.toArray(new NewCookie[cookies.size()]))
                .entity(message)
                .build();
        return response;
    } // logout()
} // class AuthenticationRest
