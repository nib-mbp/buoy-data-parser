package org.dezo.mbp.buoy.cyclops.servlet;

public enum NavMenu {
    CUSTOM_MENU, LOGIN, INSTRUMENTS, ENERGY, STATUS, ADMIN;
}
