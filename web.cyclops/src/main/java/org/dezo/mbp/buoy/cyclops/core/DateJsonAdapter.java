package org.dezo.mbp.buoy.cyclops.core;

import java.lang.reflect.Type;
import java.sql.Date;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class DateJsonAdapter implements JsonDeserializer<Date>, JsonSerializer<Date> {
    final Logger logger = LoggerFactory.getLogger(DateJsonAdapter.class);

    @Override
    public Date deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        String date = element.getAsString();

        try {
            return Date.valueOf(date);
        } catch (IllegalArgumentException e) {
            logger.error("Failed to parse Date ...", e);
            return null;
        }
    }



    @Override
    public JsonElement serialize(Date element, Type type, JsonSerializationContext context) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return element == null ? null : new JsonPrimitive(df.format(element));
    }

}
