package org.dezo.mbp.buoy.cyclops.dao;

/*
 * Rest WADL contract:
 *  - http://localhost:8080/wera-data-exchange.rest/webapi/application.wadl?detail=true
 *
 * Help URL-s:
 *  - JDBC - Quick Guide:
 *    http://www.tutorialspoint.com/jdbc/jdbc-quick-guide.htm
 *  - The DAO Design Pattern:
 *    http://tutorials.jenkov.com/java-persistence/dao-design-pattern.html
 *  - Data Access Object Pattern:
 *    http://www.tutorialspoint.com/design_pattern/data_access_object_pattern.htm
 *  - Core J2EE Patterns - Data Access Object (DAO) by Oracle:
 *    http://www.oracle.com/technetwork/java/dataaccessobject-138824.html
 *  - RESTful Web Services API using Java and MySQL:
 *    http://www.9lessons.info/2012/09/restful-web-services-api-using-java-and.html
 */

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class DaoDbConnection {
    public static final String DB = "scada_data";

    final Logger logger = LoggerFactory.getLogger(DaoDbConnection.class);
    protected Connection connection = null;

    protected DaoDbConnection() {
    }

    protected boolean connect() {
        /*
	  try {
      Class.forName(DRIVER);
    } catch (ClassNotFoundException e) {
      logger.error("MySQL JDBC Driver is missing", e);
      return false;
    }
         */
        try {
            InitialContext initialContext = new InitialContext();
            Context envContext = (Context)initialContext.lookup("java:/comp/env");
            DataSource datasource = (DataSource)envContext.lookup("jdbc/SCADA");
            connection = datasource.getConnection();
        }
        catch (Exception e) {
            logger.error("Failed establishing DB connection", e);
        }

        return true;
    }
} // class DaoDbConnection
