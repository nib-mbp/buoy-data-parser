package org.dezo.mbp.buoy.cyclops.servlet.filter;

/*
 * Reference:
 *  http://viralpatel.net/blogs/http-session-handling-tutorial-using-servlet-filters-session-error-filter-servlet-filter/
 */

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dezo.mbp.buoy.cyclops.dao.AuthenticationDaoImpl;
import org.dezo.mbp.buoy.cyclops.dao.api.IAuthenticationDao;
import org.dezo.mbp.buoy.cyclops.model.beans.misc.UserSessionDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mysql.jdbc.StringUtils;

public class SessionFilter implements javax.servlet.Filter {
    public static final String proxyAuthHeader = "X-Mbp-ProxySignature";
    public static final String certAuthHeader = "X-Mbp-CertAuth";
    public static final String rootUrlPrefixHeader = "X-Mbp-RootUrlPrefix";
    public static final String loginPageParameter = "login-page";
    public static final String errorPageParameter = "error-page"; 

    final Logger logger = LoggerFactory.getLogger(SessionFilter.class);

    private String proxySignature = null;

    private ArrayList<String> urlList;
    private String loginPage = null;
    private String errorPage = null;

    public void destroy() {
    }
 
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        //request.setCharacterEncoding("UTF-8");

        HttpServletResponse response = (HttpServletResponse) res;
        String requestedUrl = request.getServletPath();
        String redirectUrl = null;
        boolean doRedirect = false;
        boolean isUrlAuthFree = false;

        request.setAttribute("isProxyAuthenticated", false);
        request.setAttribute("isUserCertAuthenticated", false);
        request.setAttribute("rootUrlPrefix", null);
        request.setAttribute("isUserSessionValid", false);
        request.setAttribute("userFriendlyName", "");
        request.setAttribute("userLoginName", "");

        do {
            // Check web service root url
            String rootUrlPrefix = request.getHeader(SessionFilter.rootUrlPrefixHeader);
            if (rootUrlPrefix != null) {
                request.setAttribute("rootUrlPrefix", rootUrlPrefix);
            } else {
                logger.warn(MessageFormat.format("Missing HTTP header ''{0}''", SessionFilter.rootUrlPrefixHeader));
                redirectUrl = this.errorPage;
                doRedirect = true;
                break;
            }

            // Check proxy signature header validity - if invalid, redirect to an error page
            String proxySignature = request.getHeader(SessionFilter.proxyAuthHeader);
            if (proxySignature == null) {
                logger.warn(MessageFormat.format("Missing HTTP header ''{0}''", SessionFilter.proxyAuthHeader));
                redirectUrl = this.errorPage;
                doRedirect = true;
                break;
            }

            if (this.proxySignature.equals(proxySignature)) {
                request.setAttribute("isProxyAuthenticated", true);
            } else {
                logger.warn(MessageFormat.format("Wrong proxy signature - redirecting to ''{0}'' ...", this.errorPage));
                redirectUrl = this.errorPage;
                doRedirect = true;
                break;
            }

            // Parse a request header indicating the user was authenticated by certificate - actually
            // not really needed here, as the header can be retrieved in JSP and EL as well, but still,
            // let's keep it in one place 
            String certAuthHeader = request.getHeader(SessionFilter.certAuthHeader);
            boolean isCertAuthenticated = Boolean.parseBoolean(certAuthHeader);
            request.setAttribute("isUserCertAuthenticated", isCertAuthenticated);

            // Check if the requested URL is listed as one not requiring authentication
            if (urlList.contains(requestedUrl)) {
                isUrlAuthFree = true;
            }

            // Check the session cookie validity
            Cookie[] cookies = request.getCookies();
            if (cookies == null) {
                cookies = new Cookie[0];
            }
            String sessionToken = null;
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(IAuthenticationDao.cookieAuthSessionId)) {
                    sessionToken = cookie.getValue();
                    break;
                }
            }
            if (! StringUtils.isNullOrEmpty(sessionToken)) {
                UserSessionDetails sessionDetails = AuthenticationDaoImpl.getSessionDetailsStatic(request.getRemoteUser(), sessionToken);
                logger.info(sessionDetails.toString());
                request.setAttribute("sessionDetails", sessionDetails);
                if (isUrlAuthFree || sessionDetails.isSessionValid) {
                    request.setAttribute("isUserSessionValid", true);
                    break;
                }
            }

            if (! isUrlAuthFree) {
                redirectUrl = this.loginPage;
                doRedirect = true;
                break;
            }
        } while (false);

logger.debug(MessageFormat.format("doRedirect={0}", doRedirect));
        if (doRedirect) {
            if (! requestedUrl.equals("/" + this.errorPage)) {
logger.debug(MessageFormat.format("redirectUrl=''{0}''", redirectUrl));
                response.sendRedirect(redirectUrl);
                return;
            } else {
                logger.info(MessageFormat.format("Requested the error page url (''{0}'') - skip filtering it ...", requestedUrl));
            }
        }

        chain.doFilter(req, res);
        return;
    } // doFilter()


    public void init(FilterConfig config) throws ServletException {
        String urls = config.getInitParameter("avoid-urls");
        StringTokenizer token = new StringTokenizer(urls, ",");

        logger.info("Initializing ");

        this.errorPage = config.getInitParameter(SessionFilter.errorPageParameter);
        if ((this.errorPage == null) || this.errorPage.isEmpty()) {
            String msg = MessageFormat.format("Missing config parameter '{0}'!", SessionFilter.errorPageParameter);
            logger.error(msg);
            throw new ServletException(msg);
        }

        this.loginPage = config.getInitParameter("login-page");
        if ((this.loginPage == null) || this.loginPage.isEmpty()) {
            String msg = MessageFormat.format("Missing config parameter '{0}'!", SessionFilter.loginPageParameter);
            logger.error(msg);
            throw new ServletException(msg);
        }

        this.proxySignature = config.getServletContext().getInitParameter(SessionFilter.proxyAuthHeader);
        if ((this.proxySignature == null) || (this.proxySignature.isEmpty())) {
            String msg = MessageFormat.format("Missing config parameter '{0}'", "Missing config parameter '" + SessionFilter.proxyAuthHeader);
            logger.error(msg);
            throw new ServletException(msg);
        }
        urlList = new ArrayList<String>();

        while (token.hasMoreTokens()) {
            urlList.add(token.nextToken());
        }
        logger.info(MessageFormat.format("URL filter initialized - skip filter list: {0}", urlList.toString()));
    }
}
