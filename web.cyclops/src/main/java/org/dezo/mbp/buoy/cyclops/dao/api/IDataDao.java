package org.dezo.mbp.buoy.cyclops.dao.api;

import javax.ws.rs.WebApplicationException;

public interface IDataDao {
    public static final String cookieAuthUsername = "mbp-username";
    public static final String cookieAuthSessionId = "mbp-session";

    String getData() throws WebApplicationException;
}
