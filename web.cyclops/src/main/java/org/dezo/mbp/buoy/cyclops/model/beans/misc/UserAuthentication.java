package org.dezo.mbp.buoy.cyclops.model.beans.misc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserAuthentication {
    @XmlElement
    public String username;

    @XmlElement
    public String password;

    public UserAuthentication() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    public String toString() {
        return "UserAuthentication [username=" + username + ", password=" + password + "]";
    }
}
