package org.dezo.mbp.buoy.cyclops.model.beans.misc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.dezo.mbp.buoy.cyclops.model.beans.misc.enums.AccountRole;

@XmlRootElement
public class UserSessionDetails {
    @XmlElement
    public String displayName;

    @XmlElement
    public String firstname;

    @XmlElement
    public String surname;

    @XmlElement
    public String username;

    @XmlElement
    public boolean isSessionValid;

    @XmlElement
    public boolean isCertAuthenticated;

    @XmlElement
    public AccountRole role;

    public UserSessionDetails() {
        super();
        isSessionValid = false;
        isCertAuthenticated = false;
    }

    public String getDisplayName() {
        return displayName;
    }



    public String getFirstname() {
        return firstname;
    }



    public String getSurname() {
        return surname;
    }



    public String getUsername() {
        return username;
    }



    public boolean isSessionValid() {
        return isSessionValid;
    }



    public boolean isCertAuthenticated() {
        return isCertAuthenticated;
    }



    public AccountRole getRole() {
        return role;
    }



    @Override
    public String toString() {
        return "UserSessionDetails ["
                + "displayName=" + displayName
                + ", firstname=" + firstname + ", surname=" + surname
                + ", username=" + username
                + ", isSessionValid=" + isSessionValid + ", isCertAuthenticated=" + isCertAuthenticated
                + ", role=" + role
                + "]";
    }
}
