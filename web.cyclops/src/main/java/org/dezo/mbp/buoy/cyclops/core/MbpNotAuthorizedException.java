package org.dezo.mbp.buoy.cyclops.core;

import javax.ws.rs.core.Response.Status;

public class MbpNotAuthorizedException extends MbpException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
    * Create a HTTP 404 (Not Found) exception.
    */
    public MbpNotAuthorizedException() {
        super(Status.UNAUTHORIZED);
    }

    /**
    * Create a HTTP 404 (Not Found) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public MbpNotAuthorizedException(String message) {
        super(Status.UNAUTHORIZED, message);
    }
}
