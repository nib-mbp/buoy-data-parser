package org.dezo.mbp.buoy.cyclops.core;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class MbpException extends WebApplicationException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
    * Create a HTTP 400 (Bad Request) exception.
    */
    public MbpException() {
        super(Response.status(Status.BAD_REQUEST).build());
    }

    /**
    * Create a HTTP 400 (Bad Request) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public MbpException(String message) {
      super(
              Response.status(Status.BAD_REQUEST)
              .entity(message).type("text/plain").build());
    }

    /**
    * Create a custom exception with specified status.
    */
    public MbpException(Status status) {
        super(Response.status(status).build());
    }

    /**
    * Create a custom exception with specified status.
    */
    public MbpException(Status status, String message) {
        super(
                Response.status(status)
                .entity(message).type("text/plain").build());
    }
}
