package org.dezo.mbp.buoy.cyclops.model.beans.misc.enums;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum(String.class)
public enum AccountRole {
    ROLE_NA(0), ROLE_READER(1), ROLE_WRITER(2), ROLE_ADMIN(3), ROLE_SUPERADMIN(4);

    private final int role;

    AccountRole(int role) {
        this.role = role;
    }

    public int getInt() {
        return this.role;
    }

    /*
     * ATTENTION: Not really used -> DB stored procedure is being used for this
     */
    boolean checkAccess(AccountRole role) {
        if (this.role >= role.getInt()) {
            return true;
        } else {
            return false;
        }
    } // checkPermission()

    public static AccountRole fromInt(int val) {
        switch(val) {
        case 0:
            return AccountRole.ROLE_NA;
        case 1:
            return AccountRole.ROLE_READER;
        case 2:
            return AccountRole.ROLE_WRITER;
        case 3:
            return AccountRole.ROLE_ADMIN;
        case 4:
            return AccountRole.ROLE_SUPERADMIN;
        }
        return null;
    }
}
