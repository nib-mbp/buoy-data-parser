package org.dezo.mbp.buoy.cyclops.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.WebApplicationException;

import org.dezo.mbp.buoy.cyclops.core.MbpException;
import org.dezo.mbp.buoy.cyclops.core.MbpNotAuthorizedException;
import org.dezo.mbp.buoy.cyclops.core.MbpNotFoundException;
import org.dezo.mbp.buoy.cyclops.dao.api.IDataDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataDaoImpl extends DaoDbConnection implements IDataDao {
    final Logger logger = LoggerFactory.getLogger(DataDaoImpl.class);


    @Override
    public String getData()
            throws WebApplicationException
    {
        if (!connect()) {
            throw new MbpException("Failed connecting to DB");
        }

        String item = null;
        try {
            String query = "";
            query += "SELECT `" + DaoDbConnection.DB + "`.`f_getScadaData`() AS `scada_data`";
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                item = rs.getString("scada_data");
                rs.close();
            } else {
                throw new MbpNotFoundException("No next element available");
            }
        } catch (SQLException e) {
            logger.error("Failed retrieving data", e);
            throw new MbpNotAuthorizedException("Authentication failed");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Failed closing DB connection", e);
        }

        return item;
    } // getData()
}
