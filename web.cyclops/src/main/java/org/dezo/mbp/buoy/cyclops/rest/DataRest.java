package org.dezo.mbp.buoy.cyclops.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.dezo.mbp.buoy.cyclops.dao.DataDaoImpl;
import org.dezo.mbp.buoy.cyclops.dao.api.IAuthenticationDao;
import org.dezo.mbp.buoy.cyclops.dao.api.IDataDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/Data")
public class DataRest {
    @SuppressWarnings("unused")
    private URI restUri = null;
    final Logger logger = LoggerFactory.getLogger(DataRest.class);

    public DataRest(@Context UriInfo uriInfo) {
        super();

        try {
            this.restUri = new URI(uriInfo.getBaseUri().toString() + "Data/");
        } catch (URISyntaxException e) {
            restUri = uriInfo.getBaseUri();
            logger.error("Failed initialising '" + this.getClass().getName() + "' class URI - failing back to original uri", e);
        }
    }

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getData(@Context HttpServletRequest httpRequest, @Context HttpHeaders headers,
            @CookieParam(value = IAuthenticationDao.cookieAuthUsername) String username,
            @CookieParam(value = IAuthenticationDao.cookieAuthSessionId) String sessionToken)
                    throws WebApplicationException
    {
        //AuthService authService = new AuthService();
        //authService.checkAccess(username, sessionToken, AccountRole.ROLE_READER);

        IDataDao dao = new DataDaoImpl();
        String item = dao.getData();
        Response response = Response.ok().entity(item).build();

        return response;
    } // logout()
} // class AuthenticationRest
