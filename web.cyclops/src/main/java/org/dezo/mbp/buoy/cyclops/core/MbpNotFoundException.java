package org.dezo.mbp.buoy.cyclops.core;

import javax.ws.rs.core.Response.Status;

public class MbpNotFoundException extends MbpException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
    * Create a HTTP 404 (Not Found) exception.
    */
    public MbpNotFoundException() {
        super(Status.NOT_FOUND);
    }

    /**
    * Create a HTTP 404 (Not Found) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public MbpNotFoundException(String message) {
      super(Status.NOT_FOUND, message);
    }
}
