package org.dezo.mbp.buoy.cyclops.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.WebApplicationException;

import org.dezo.mbp.buoy.cyclops.core.MbpException;
import org.dezo.mbp.buoy.cyclops.core.MbpNotAuthorizedException;
import org.dezo.mbp.buoy.cyclops.dao.api.IAuthenticationDao;
import org.dezo.mbp.buoy.cyclops.model.beans.misc.UserSessionDetails;
import org.dezo.mbp.buoy.cyclops.model.beans.misc.enums.AccountRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthenticationDaoImpl extends DaoDbConnection implements IAuthenticationDao {
    final Logger logger = LoggerFactory.getLogger(AuthenticationDaoImpl.class);


    @Override
    public String authenticate(String username, String password) throws WebApplicationException {
        String sessionToken = null;

        if (!connect()) {
            throw new MbpException("Failed connecting to DB");
        }

        try {
            String query = "";
            query += "SELECT `" + DaoDbConnection.DB + "`.`f_authenticateUser`(?, ?) AS `session_token`";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            rs.next();

            sessionToken = rs.getString("session_token");
        } catch (SQLException e) {
            logger.error("Login failed", e);
            throw new MbpNotAuthorizedException("Authentication failed");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Failed closing DB connection", e);
        }

        return sessionToken;
    } // authenticate()

    @Override
    public void updateSession(String username, String sessionToken) throws WebApplicationException {
        if (!connect()) {
            throw new MbpException("Failed connecting to DB");
        }

        try {
            String query = "";
            query += "CALL `" + DaoDbConnection.DB + "`.`p_updateSession`(?, ?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, sessionToken);

            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error("Session refresh failed", e);
            throw new MbpException("Session refresh failed");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Failed closing DB connection", e);
        }
    } // updateSession()


    @Override
    public void logout(String username, String sessionToken) throws WebApplicationException {
        if (!connect()) {
            throw new MbpException("Failed connecting to DB");
        }

        try {
            String query = "";
            query += "CALL `" + DaoDbConnection.DB + "`.`p_logoutUser`(?, ?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, sessionToken);

            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error("Logout failed", e);
            throw new MbpNotAuthorizedException("Logout failed");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Failed closing DB connection", e);
        }
    } // logout()

    @Override
    public void checkAccess(String username, String sessionToken, AccountRole requiredAccessRole) throws WebApplicationException {
        if (!connect()) {
            throw new MbpException("Failed connecting to DB");
        }

        boolean hasAccess = false;
        try {
            String query = "";
            query += "SELECT `mbp_wera_auth`.`f_hasUserEnoughGrants`(?, ?, ?) AS hasAccess";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, sessionToken);
            ps.setInt(3, requiredAccessRole.getInt());
            ps.executeQuery();

            ResultSet rs = ps.executeQuery();
            rs.next();
            hasAccess = rs.getBoolean("hasAccess");
        } catch (SQLException e) {
            logger.error("Failed retrieving user details", e);
            throw new MbpException("Failed retrieving user details");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Failed closing DB connection", e);
        }

        if (! hasAccess) {
            throw new MbpNotAuthorizedException("The specified user has no enough access rights (requested=" + requiredAccessRole + ")");
        }

        return;
    } // checkAccess()


    @Override
    public boolean isSessionValid(String username, String sessionToken)
            throws WebApplicationException
    {
        if (!connect()) {
            throw new MbpException("Failed connecting to DB");
        }

        boolean isSessionValid = false;
        try {
            String query = "";
            query += "SELECT `" + DaoDbConnection.DB + "`.`f_hasUserEnoughGrants`(?, ?, ?) AS hasAccess";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, sessionToken);
            ps.setInt(3, AccountRole.ROLE_READER.getInt());
            ps.executeQuery();

            ResultSet rs = ps.executeQuery();
            rs.next();
            isSessionValid = rs.getBoolean("hasAccess");
        } catch (SQLException e) {
            logger.error("Failed retrieving user details", e);
            throw new MbpException("Failed retrieving user details");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Failed closing DB connection", e);
        }

        return isSessionValid;
    } // isSessionValid()


    @Override
    public UserSessionDetails getSessionDetails(String username, String sessionToken)
            throws WebApplicationException
    {
        if (!connect()) {
            throw new MbpException("Failed connecting to DB");
        }

        UserSessionDetails sessionDetails = new UserSessionDetails();
        sessionDetails.username = username;
        sessionDetails.isSessionValid = false;
        try {
            String query = "";
            query += "CALL `" + DaoDbConnection.DB + "`.`p_getUserData`(?, ?, @isSessionValid, @isCertAuthenticated, @firstName, @lastName, @email, @roleId, @roleName)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, sessionDetails.username);
            ps.setString(2, sessionToken);
            ps.executeUpdate();

            query = "";
            query += "SELECT";
            query += "  TRIM(CONCAT(@firstName, ' ', @lastName)) AS `displayName`,";
            query += "  @firstName AS `firstName`, @lastName AS `lastName`,";
            query += "  @isSessionValid AS `isSessionValid`, @isCertAuthenticated AS `isCertAuthenticated`,";
            query += "  @roleId AS `roleId`";
            ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            rs.next();
            sessionDetails.isSessionValid = rs.getBoolean("isSessionValid");
            if (sessionDetails.isSessionValid) {
                sessionDetails.displayName = rs.getString("displayName");
                sessionDetails.firstname = rs.getString("firstName");
                sessionDetails.surname = rs.getString("lastName");
                sessionDetails.isCertAuthenticated = rs.getBoolean("isCertAuthenticated");
                sessionDetails.role = AccountRole.fromInt(rs.getInt("roleId"));
            }
        } catch (SQLException e) {
            logger.error("Failed retrieving user details", e);
            throw new MbpException("Failed retrieving user details");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Failed closing DB connection", e);
        }

        return sessionDetails;
    }

    public static boolean isSessionValidStatic(String username, String sessionToken) {
        Logger logger = LoggerFactory.getLogger(AuthenticationDaoImpl.class);
        IAuthenticationDao authDao = new AuthenticationDaoImpl();
        boolean isSessionValid = false;
        try {
            isSessionValid = authDao.isSessionValid(username, sessionToken);
        } catch (WebApplicationException e) {
            logger.error("Failed checking session validity", e);
        }
        return isSessionValid;
    } // isSessionValidStatic()


    public static UserSessionDetails getSessionDetailsStatic(String username, String sessionToken) {
        Logger logger = LoggerFactory.getLogger(AuthenticationDaoImpl.class);
        IAuthenticationDao authDao = new AuthenticationDaoImpl();
        UserSessionDetails sessionDetails = new UserSessionDetails();
        try {
            sessionDetails = authDao.getSessionDetails(username, sessionToken);
        } catch (WebApplicationException e) {
            logger.error("Failed retrieving session details", e);
        }
        return sessionDetails;
    }
}
