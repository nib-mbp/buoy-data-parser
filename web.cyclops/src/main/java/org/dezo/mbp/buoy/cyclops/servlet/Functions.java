package org.dezo.mbp.buoy.cyclops.servlet;

import javax.ws.rs.WebApplicationException;

import org.dezo.mbp.buoy.cyclops.dao.AuthenticationDaoImpl;
import org.dezo.mbp.buoy.cyclops.dao.api.IAuthenticationDao;

public class Functions {
    public static String hello(String name) {
        return "Hiya, " + name + ".";
    }
    
    public static boolean isHello(String str) {
        return false;
    }

    public static boolean isSessionValidStatic(String sessionStr) throws WebApplicationException {
        IAuthenticationDao authDao = new AuthenticationDaoImpl();
        return authDao.isSessionValid("username", sessionStr);
        //return false;
    }
}
