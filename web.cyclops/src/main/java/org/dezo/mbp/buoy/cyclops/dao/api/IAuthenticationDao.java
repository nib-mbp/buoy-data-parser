package org.dezo.mbp.buoy.cyclops.dao.api;

import javax.ws.rs.WebApplicationException;

import org.dezo.mbp.buoy.cyclops.model.beans.misc.UserSessionDetails;
import org.dezo.mbp.buoy.cyclops.model.beans.misc.enums.AccountRole;

public interface IAuthenticationDao {
    public static final String cookieAuthUsername = "mbp-username";
    public static final String cookieAuthSessionId = "mbp-session";

    /*
     * in: username in: password out: sessionToken
     *
     * Return values: Session token string if successful, otherwise null.
     */
    String authenticate(String username, String password) throws WebApplicationException;

    /*
     * @param username username
     *
     * @param sessionToken 24 bytes session token string
     */
    void updateSession(String username, String sessionToken) throws WebApplicationException;

    /*
     * This function should fill private variables to be used for getters and
     * setters
     *
     * @param username (in)username
     *
     * @param sessionToken (in) 24 bytes string
     *
     * @return Returns true if user exists and the operation succeed, otherwise
     * false
     */
    //AccountDetails getAccountDetails(String username, String sessionToken) throws WebApplicationException;

    /*
     * in: username in: sessionToken (24 bytes string)
     */
    void logout(String username, String sessionToken) throws WebApplicationException;

    /*
     * @param username Username
     *
     * @param sessionToken user session token
     *
     * @param requiredAccessRole The required access role as per enum jada jada
     */
    void checkAccess(String username, String sessionToken, AccountRole requiredAccessRole) throws WebApplicationException;

    UserSessionDetails getSessionDetails(String username, String sessionToken) throws WebApplicationException;

    boolean isSessionValid(String username, String sessionToken) throws WebApplicationException;
}
