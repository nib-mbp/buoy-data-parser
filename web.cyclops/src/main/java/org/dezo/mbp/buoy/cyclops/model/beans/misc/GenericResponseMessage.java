package org.dezo.mbp.buoy.cyclops.model.beans.misc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GenericResponseMessage {
    @XmlElement
    public int code;

    @XmlElement
    public String message;

    public GenericResponseMessage() {
        super();
        this.code = 200;
        this.message = "";
    }

    @Override
    public String toString() {
        return "GenericResponse [code=" + code + ", message=" + message + "]";
    }
}
