package org.dezo.mbp.buoy.cyclops.core;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.NewCookie;

import org.dezo.mbp.buoy.cyclops.dao.AuthenticationDaoImpl;
import org.dezo.mbp.buoy.cyclops.dao.api.IAuthenticationDao;
import org.dezo.mbp.buoy.cyclops.model.beans.misc.enums.AccountRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthService {
    final Logger logger = LoggerFactory.getLogger(AuthService.class);

    public AuthService() {
        super();
    }

    public List<NewCookie> authenticate(String cookiePath, String username, String password) throws WebApplicationException {
        List<NewCookie> cookies = new ArrayList<NewCookie>();
        IAuthenticationDao dao = new AuthenticationDaoImpl();

        String sessionToken = dao.authenticate(username, password);

        if (sessionToken != null) {
            NewCookie cookie = new NewCookie("mbp-session", sessionToken, cookiePath, null, 1, "Session token", -1, false);
            cookies.add(cookie);

            cookie = new NewCookie("mbp-username", username, cookiePath, null, 1, "Authentication username", -1, false);
            cookies.add(cookie);
        }

        return cookies;
    } // authenticate()

    public List<NewCookie> logout(String cookiePath, String username, String sessionToken) throws WebApplicationException {
        List<NewCookie> cookies = new ArrayList<NewCookie>();
        IAuthenticationDao dao = new AuthenticationDaoImpl();

        dao.logout(username, sessionToken);

        NewCookie cookie = new NewCookie("mbp-session", "", cookiePath, null, 1, "Session token", 0, false);
        cookies.add(cookie);

        cookie = new NewCookie("mbp-username", "", cookiePath, null, 1, "Authentication username", 0, false);
        cookies.add(cookie);

        return cookies;
    } // logout()

    public void checkAccess(String username, String sessionToken, AccountRole requiredAccessRole) throws WebApplicationException {
        IAuthenticationDao dao = new AuthenticationDaoImpl();

        dao.checkAccess(username, sessionToken, requiredAccessRole);

        return;
    }
}
