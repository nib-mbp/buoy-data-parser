DROP VIEW IF EXISTS `v_wind_curr_waves`;
CREATE VIEW `v_wind_curr_waves` AS (
  SELECT
    p.`pid` AS `pid`,
    p.`datestart` AS `datestart`,
    p.`period_length` AS `period_length`,
    w.`vmspd` AS `vmspd`, w.`vmdir` AS `vmdir` ,
    w.`maxspd` AS `maxspd`, w.`maxdir` AS `maxdir`,
    a.`pressure` AS `pressure`, a.`temperature` AS `temp`,
    aw.`wave_height` AS `wave_height`, aw.`mean_direction` AS `mean_direction`, aw.`mean_period` AS `mean_period`,
    aw.`wave_height_peak10` AS `wave_height_peak10`, aw.`wave_height_peak33` AS `wave_height_peak33`, aw.`wave_height_max` AS `wave_height_max`,
    ROUND(f_get_awacSpd(p.pid, 0),1) AS `currSpd_2m`, ROUND(f_get_awacE(p.pid, 0),1) AS `currE_2m`, ROUND(f_get_awacN(p.pid, 0),1) AS `currN_2m`, ROUND(f_get_awacW(p.pid, 0),1) AS `currW_2m`,
    ROUND(f_get_awacSpd(p.pid, 3),1) AS `currSpd_5m`, ROUND(f_get_awacE(p.pid, 3),1) AS `currE_5m`, ROUND(f_get_awacN(p.pid, 3),1) AS `currN_5m`, ROUND(f_get_awacW(p.pid, 3),1) AS `currW_5m`,
    ROUND(f_get_awacSpd(p.pid, 7),1) AS `currSpd_9m`, ROUND(f_get_awacE(p.pid, 7),1) AS `currE_9m`, ROUND(f_get_awacN(p.pid, 7),1) AS `currN_9m`, ROUND(f_get_awacW(p.pid, 7),1) AS `currW_9m`,
    ROUND(f_get_awacSpd(p.pid, 11),1) AS `currSpd_13m`, ROUND(f_get_awacE(p.pid, 11),1) AS `currE_13m`, ROUND(f_get_awacN(p.pid, 11),1) AS `currN_13m`, ROUND(f_get_awacW(p.pid, 11),1) AS `currW_13m`,
    ROUND(f_get_awacSpd(p.pid, 15),1) AS `currSpd_17m`, ROUND(f_get_awacE(p.pid, 15),1) AS `currE_17m`, ROUND(f_get_awacN(p.pid, 15),1) AS `currN_17m`, ROUND(f_get_awacW(p.pid, 15),1) AS `currW_17m`,
    ROUND(f_get_awacSpd(p.pid, 17),1) AS `currSpd_19m`, ROUND(f_get_awacE(p.pid, 17),1) AS `currE_19m`, ROUND(f_get_awacN(p.pid, 17),1) AS `currN_19m`, ROUND(f_get_awacW(p.pid, 17),1) AS `currW_19m`,
    ROUND(f_get_awacSpd(p.pid, 19),1) AS `currSpd_21m`, ROUND(f_get_awacE(p.pid, 19),1) AS `currE_21m`, ROUND(f_get_awacN(p.pid, 19),1) AS `currN_21m`, ROUND(f_get_awacW(p.pid, 19),1) AS `currW_21m`,
    ROUND(f_get_awacSpd(p.pid, 20),1) AS `currSpd_22m`, ROUND(f_get_awacE(p.pid, 20),1) AS `currE_22m`, ROUND(f_get_awacN(p.pid, 20),1) AS `currN_22m`, ROUND(f_get_awacW(p.pid, 20),1) AS `currW_22m`
  FROM `profile` p
    LEFT JOIN `wind` w ON p.pid=w.pid
    LEFT JOIN `awac_sensors` a ON p.pid=a.pid
    LEFT JOIN `awac_waves` aw ON p.pid=aw.pid
  WHERE
    p.period_length='30'
);


DROP VIEW IF EXISTS `v_wind_curr_waves15`;
CREATE VIEW `v_wind_curr_waves15` AS (
  SELECT
    p.`pid` AS `pid`,
    p.`datestart` AS `datestart`,
    p.`period_length` AS `period_length`,
    w.`vmspd` AS `vmspd`, w.`vmdir` AS `vmdir` ,
    w.`maxspd` AS `maxspd`, w.`maxdir` AS `maxdir`,
    a.`pressure` AS `pressure`, a.`temperature` AS `temp`,
    aw.`wave_height` AS `wave_height`, aw.`mean_direction` AS `mean_direction`, aw.`mean_period` AS `mean_period`,
    aw.`wave_height_peak10` AS `wave_height_peak10`, aw.`wave_height_peak33` AS `wave_height_peak33`, aw.`wave_height_max` AS `wave_height_max`,
    ROUND(f_get_awacSpd(p.pid, 0),1) AS `currSpd_2m`, ROUND(f_get_awacE(p.pid, 0),1) AS `currE_2m`, ROUND(f_get_awacN(p.pid, 0),1) AS `currN_2m`, ROUND(f_get_awacW(p.pid, 0),1) AS `currW_2m`,
    ROUND(f_get_awacSpd(p.pid, 3),1) AS `currSpd_5m`, ROUND(f_get_awacE(p.pid, 3),1) AS `currE_5m`, ROUND(f_get_awacN(p.pid, 3),1) AS `currN_5m`, ROUND(f_get_awacW(p.pid, 3),1) AS `currW_5m`,
    ROUND(f_get_awacSpd(p.pid, 7),1) AS `currSpd_9m`, ROUND(f_get_awacE(p.pid, 7),1) AS `currE_9m`, ROUND(f_get_awacN(p.pid, 7),1) AS `currN_9m`, ROUND(f_get_awacW(p.pid, 7),1) AS `currW_9m`,
    ROUND(f_get_awacSpd(p.pid, 11),1) AS `currSpd_13m`, ROUND(f_get_awacE(p.pid, 11),1) AS `currE_13m`, ROUND(f_get_awacN(p.pid, 11),1) AS `currN_13m`, ROUND(f_get_awacW(p.pid, 11),1) AS `currW_13m`,
    ROUND(f_get_awacSpd(p.pid, 15),1) AS `currSpd_17m`, ROUND(f_get_awacE(p.pid, 15),1) AS `currE_17m`, ROUND(f_get_awacN(p.pid, 15),1) AS `currN_17m`, ROUND(f_get_awacW(p.pid, 15),1) AS `currW_17m`,
    ROUND(f_get_awacSpd(p.pid, 17),1) AS `currSpd_19m`, ROUND(f_get_awacE(p.pid, 17),1) AS `currE_19m`, ROUND(f_get_awacN(p.pid, 17),1) AS `currN_19m`, ROUND(f_get_awacW(p.pid, 17),1) AS `currW_19m`,
    ROUND(f_get_awacSpd(p.pid, 19),1) AS `currSpd_21m`, ROUND(f_get_awacE(p.pid, 19),1) AS `currE_21m`, ROUND(f_get_awacN(p.pid, 19),1) AS `currN_21m`, ROUND(f_get_awacW(p.pid, 19),1) AS `currW_21m`,
    ROUND(f_get_awacSpd(p.pid, 20),1) AS `currSpd_22m`, ROUND(f_get_awacE(p.pid, 20),1) AS `currE_22m`, ROUND(f_get_awacN(p.pid, 20),1) AS `currN_22m`, ROUND(f_get_awacW(p.pid, 20),1) AS `currW_22m`
  FROM `profile` p
    LEFT JOIN `wind` w ON p.pid=w.pid
    LEFT JOIN `awac_sensors` a ON p.pid=a.pid
    LEFT JOIN `awac_waves` aw ON p.pid=aw.pid
  ORDER BY p.`datestart`,p.`pid`
);

SELECT * FROM v_wind_curr_waves15 ORDER BY datestart DESC LIMIT 10;
SELECT * FROM v_wind_curr_waves ORDER BY datestart DESC LIMIT 10;

DROP FUNCTION IF EXISTS `f_get_awacSpd`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_get_awacSpd`(
  i_pid INT UNSIGNED,
  i_cell tinyint UNSIGNED
) RETURNS float
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Returns AWAC current E component for the given cell'
BEGIN
  DECLARE _rv FLOAT;
  SELECT SQRT(POW(ac.current_E,2) + POW(ac.current_N,2) + POW(ac.current_W,2)) INTO _rv FROM awac_currents ac WHERE ac.pid=i_pid AND ac.cell_no=i_cell LIMIT 1;
  RETURN _rv;
END ;;
DELIMITER ;





SET TIME_ZONE='+00:00';
SET character_set_client = utf8;


-- awacHwConf (0xA505) - acquireDt=2017-07-20 10:36:53 [hSize=24 (48B) / instrument (sys s/n=WPR 2921, prolog s/n=1781, prolog ver=4.15) /
--                       config (recorder=0, compass=0) / hFrequency=8826 / nPICversion=0 / nHWrevision=2 / hRecSize=60368 / status (vrange=0) /
--                       hFWversion=2026710375465168435 / hChecksum=0x0de3]


DROP TABLE IF EXISTS `awac2_hw_config`;
CREATE TABLE `awac2_hw_config` (
  `time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Buoy data acquisition time (when it was downloaded to the buoy)',
  `packet_id` int(10) unsigned NOT NULL,
  `sys_sn` char(8) NOT NULL COMMENT 'AWAC system S/N',
  `prolog_sn` smallint unsigned NOT NULL COMMENT 'Prolog S/N',
  `prolog_ver` char(4) NOT NULL COMMENT 'Prolog version',
  `config_hex` char(4) NOT NULL COMMENT 'AWAC hardware config bits stored as 2 bytes HEX ASCII value',
  `board_frequency` smallint unsigned NOT NULL COMMENT 'AWAC board frequency [kHz]',
  `pic_code` smallint unsigned NOT NULL COMMENT 'PIC code version number',
  `hw_revision` smallint unsigned NOT NULL COMMENT 'Hardware revision',
  `rec_size` smallint unsigned NOT NULL COMMENT 'SD card (recorder) size expressed in 64kB units',
  `status_hex` char(4) NOT NULL COMMENT 'AWAC status bits',
  `firmware_ver` int(10) unsigned NOT NULL COMMENT 'Firmware version',
  PRIMARY KEY(`time`),
  KEY `fk_awac2_hw_config_profile` (`packet_id`),
  CONSTRAINT `fk_awac2_hw_config_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC hardware config (0xA505)';


-- awacHeadConf (0xA504) - acquireDt=2017-07-20 10:36:52 [hSize=112 (224B) / config (pressure sens.=1, magnetometer=1, tilt sens.=1, tilt orient.=down) /
--                         hHeadFrequency=600 / hHeadType=3 / s/n=WAV 7149 / nBeams=3 / hChecksum=0x4647 / systemData=(hex)<
-- 00000: 19 00 19 00 19 00 00 00 3D 19 60 F3 60 F3 00 00 24 EA DC 15 E2 05 E2 05 E2 05 8B 9A 43 02 48 32 AF FF 74 9A 03 F9 9F 32 FD 00 32 00 01 00 00 00
-- 00048: 00 00 00 00 01 00 00 00 00 00 00 00 01 00 01 00 00 00 00 00 00 00 FF FF 00 00 00 00 00 00 FF FF 00 00 01 00 01 00 00 00 00 00 01 00 FF FF 00 00
-- 00096: 00 00 00 00 C1 43 CA 22 C9 05 03 01 D8 1B 6D 2A C0 9B B4 FF B8 33 9A FF D0 9D E6 05 36 33 7B FF 17 7B 91 FA FB FF 64 FD 16 7D 1B FF CA FF 2A FF
-- 00144: FF 7F E3 FF EE FF FA FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 AA 0A 10 0E 10 0E 10 27
-- >]
DROP TABLE IF EXISTS `awac2_head_config`;
CREATE TABLE `awac2_head_config` (
  `time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Buoy data acquisition time (when it was downloaded to the buoy)',
  `packet_id` int(10) unsigned NOT NULL,
  `config_hex` char(4) NOT NULL COMMENT 'AWAC head configuration bits stored as 2 bytes HEX ASCII value',
  `frequency` smallint unsigned NOT NULL COMMENT 'AWAC head frequency [kHz]',
  `head_type` smallint unsigned NOT NULL COMMENT 'AWAC head type',
  `head_sn` char(12) NOT NULL COMMENT 'AWAC head S/N',
  `n_beams` smallint unsigned NOT NULL COMMENT 'number of beams',
  `system_data_bin` binary(176) NOT NULL COMMENT 'system data',
  PRIMARY KEY  (`time`),
  KEY `fk_awac2_head_config_profile` (`packet_id`),
  CONSTRAINT `fk_awac2_head_config_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC Head Configuration (0xA504)';



-- awacUserConf (0xA500) - acquireDt=2017-07-20 10:36:50 [hSize=256 (512B) / T1=55 / T2=97 / T3=14 / T4=4095 / T5=512 / nPings=1 /
--                         avgInterval=600 / nBeams=3 / timCtrlReg (profile=single, mode=burst, synchout=middle, sampleOnSync=enabled,
--                         startOnSync=enabled) hex: 0x0000 / pwrCtrlReg hex: 0x0000 / tA1=0 / tB0=0 / tB1=0 / compassUpdRate=1800 /
--                         coordSystem=0 / nBins=21 / binLength=3534 / measInterval=1800 / deployName=VIDA / wrapMode=0 /
--                         clockDeploy=2017-07-20 11:00:00 / diagInterval=1800 / mode (userSoundSpd=0, diagWaveMode=1, analogOutput=0,
--                         outputFormat=Vector, scaling=1 mm, serialOutput=1, stage=0, outPower=1) - hex: 0x0122 / adjSoundSpeed=16657 /
--                         nSampDiag=1 / nBeamsCellDiag=0 / nPingsDiag=2 / modeTest (dspFilter=0, filterDataOutput=0) - hex: 0x4004 /
--                         anaInAddr=1 / swVersion=14705 / waveMode (dataRate=0, waveCellPos=dynamic, dynamicPosType=1) - hex: 0x0006 /
--                         dynPercPos=29490 / wT1=132 / wT2=866 / wT3=8482 / nSamp=1024 / wA1=100 / wB0=8 / wB1=446 / anaOutScale=0 /
--                         corrThresh=0 / tiLag2=6 / hChecksum=0xae1b]
DROP TABLE IF EXISTS `awac2_user_config`;
CREATE TABLE `awac2_user_config` (
  `time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Buoy data acquisition time (when it was downloaded to the buoy)',
--  `user_config_id` int(10) unsigned NOT NULL auto_increment,
  `packet_id` int(10) unsigned NOT NULL,
  `T1` smallint unsigned NOT NULL COMMENT 'Transmit pulse length [counts]',
  `T2` smallint unsigned NOT NULL COMMENT 'Blanking distance [counts]',
  `T3` smallint unsigned NOT NULL COMMENT 'Receive length [counts]',
  `T4` smallint unsigned NOT NULL COMMENT 'Time between pings [counts]',
  `T5` smallint unsigned NOT NULL COMMENT 'Time between burst sequences [counts]',
  `n_pings` smallint unsigned NOT NULL COMMENT 'Number of beam sequences per burst',
  `avg_interval` smallint unsigned NOT NULL COMMENT 'Average interval [seconds]',
  `n_beams` smallint unsigned NOT NULL COMMENT 'Number of beams',
  `timing_ctrl_reg_hex` char(4) NOT NULL COMMENT 'Timing controller register stored as 2 bytes HEX ASCII value',
  `power_ctrl_reg_hex` char(4) NOT NULL COMMENT 'Power controller register stored as 2 bytes HEX ASCII value',
  `compass_update` smallint unsigned NOT NULL COMMENT 'Compass update rate',
  `coord_system` smallint unsigned NOT NULL COMMENT 'Coordinate system',
  `n_cells` smallint unsigned NOT NULL COMMENT 'Number of bins (cells)',  
  `cell_size` float NOT NULL COMMENT 'Bin (cell) size [m]',
  `meas_interval` smallint unsigned NOT NULL COMMENT 'Measurement interval [s]',
  `deploy_name` char(6) NOT NULL COMMENT 'Recorder deployment name',
  `wrap_mode` smallint unsigned NOT NULL COMMENT 'Recorder wrap mode (0=NO WRAP, 1=WRAP WHEN FULL)',
  `deployment_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Deployment time - config validity start time (check if correct)',
  `diagnostic` int(10) unsigned NOT NULL COMMENT 'Dumber of seconds between diagnostics measurements [s]',
  `mode_status_hex` char(4) NOT NULL COMMENT 'Mode status bits stored as 2 bytes HEX ASCII value',
  `adj_sound_speed` smallint unsigned NOT NULL COMMENT 'Adjustment factor for sound speed',
  `n_samp_diag` smallint unsigned NOT NULL COMMENT '# samples in diagnostic mode',
  `n_beamscell_diag` smallint unsigned NOT NULL COMMENT '# beams / cell number to measure in diagnostics mode',
  `n_pings_diag` smallint unsigned NOT NULL COMMENT '# pings in diagnostics/wave mode',
  `test_mode_status_hex` char(4) NOT NULL COMMENT 'Test mode status bits stored as 2 bytes HEX ASCII value',
  `ana_in_adddr` smallint unsigned NOT NULL COMMENT 'Analog input address',
  `sw_version` smallint unsigned NOT NULL COMMENT 'Software version [(ver/10000).(ver/100)%100.(ver%100)]',
  `vel_adj_table_bin` binary(180) NOT NULL COMMENT 'Velocity adjustment table',
  `file_comments` binary(180) NOT NULL COMMENT 'File comments',
  `wave_mode_hex` char(4) NOT NULL COMMENT 'Wave measurement mode bits stored as 2 bytes HEX ASCII value',
  `dyn_perc_pos` smallint unsigned NOT NULL COMMENT 'Percentage for wave cell positioning',
  `wT1` smallint unsigned NOT NULL COMMENT 'Wave transmit pulse',
  `wT2` smallint unsigned NOT NULL COMMENT 'Fixed wave blanking distance [counts]',
  `wT3` smallint unsigned NOT NULL COMMENT 'Wave measurement cell size',
  `n_samples` smallint unsigned NOT NULL COMMENT 'Number of diagnostics/wave samples',
  `ana_out_scale` float NOT NULL COMMENT 'Analog output scale factor (16384=1.0, max=4.0)',
  `corr_treshold` smallint unsigned NOT NULL COMMENT 'Correlation threshold for resolving ambiguities',
  `ti_lag2` smallint unsigned NOT NULL COMMENT 'Transmit pulse length (counts) second lag [counts]',
  `qual_const_bin` binary(16) NOT NULL COMMENT 'Stage match filter constants (EZQ)',
  PRIMARY KEY  (`time`),
  KEY `fk_awac2_user_config_profile` (`packet_id`),
  CONSTRAINT `fk_awac2_user_config_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC User Configuration (0xA500)';



-- awacProf (0xA520) - acquireDt=2017-07-20 11:28:10 [hSize=155 (310B) / clock=2017-07-20 11:00:00 / hError=0 / hAnaIn1=1536 /
--                     hBattery=146 / hSoundspeed=15225 / hHeading=1800 / hPitch=7 / hRoll=-6 / pressure=21826 / cStatus=0 /
--                     hTemperature=1959 / velocity and amplitude=<
--  cell #1. vel [E=-0.026, N=-0.005, U=-0.021], amp [amp1=133, amp2=127, amp3=130]
--  ...
--  cell #21. vel [E=0.389, N=-0.064, U=-0.028], amp [amp1=130, amp2=134, amp3=125]
-- > / hChecksum=0xf8cf]

DROP TABLE IF EXISTS `awac2_currents`;
DROP TABLE IF EXISTS `awac2_currents_header`;
CREATE TABLE `awac2_currents_header` (
  `currents_hdr_id` int(10) unsigned NOT NULL auto_increment,
  `time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Buoy data acquisition time (when it was downloaded to the buoy)',
  `packet_id` int(10) unsigned NOT NULL,
  `profile_start_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Computation profile start time (rounded to 30 minutes)',
  `awac_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'AWAC internal time - measurement start time',
  `error_code` smallint NOT NULL COMMENT 'Error code',
  `ana_in` smallint unsigned NOT NULL COMMENT 'Analog input 1',
  `battery` float NOT NULL COMMENT 'Battery voltage [V]',
  `sound_speed` float NOT NULL COMMENT 'Speed of sound [m/s]',
  `heading` float NOT NULL COMMENT 'Compass heading [°]',
  `pitch` float NOT NULL COMMENT 'Compass pitch [°]',
  `roll` float NOT NULL COMMENT 'Compass roll [°]',
  `pressure` float NOT NULL COMMENT 'Pressure [dBar]',
  `temperature` float NOT NULL COMMENT 'Temperatura [°C]',
  `status` tinyint NOT NULL COMMENT 'Status',
  PRIMARY KEY  (`currents_hdr_id`),
  UNIQUE KEY (`time`),
  KEY `fk_awac2_currents_header_profile` (`packet_id`),
  CONSTRAINT `fk_awac2_currents_header_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC Velocity Profile - header (0xA520)';

CREATE TABLE `awac2_currents` (
  `currents_hdr_id` int(10) unsigned NOT NULL COMMENT 'Foreign link to awac2_currents_header',
  `cell_no` tinyint unsigned NOT NULL COMMENT 'Cell number',
  `current_E` float NOT NULL COMMENT 'Current speed: East component [cm/s]',
  `current_N` float NOT NULL COMMENT 'Current speed: North component [cm/s]',
  `current_W` float NOT NULL COMMENT 'Current speed: Up component [cm/s]',
  `amp_E` tinyint unsigned NOT NULL COMMENT 'Response amplitude of the 1st beam jet',
  `amp_N` tinyint unsigned NOT NULL COMMENT 'Response amplitude of the 2nd beam jet',
  `amp_W` tinyint unsigned NOT NULL COMMENT 'Response amplitude of the 3rd beam jet',
  PRIMARY KEY  (`currents_hdr_id`,`cell_no`),
--  KEY `fk_awac2_currents_data_hdr` (`currents_hdr_id`),
  CONSTRAINT `fk_awac2_currents_data_hdr` FOREIGN KEY (`currents_hdr_id`) REFERENCES `awac2_currents_header` (`currents_hdr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC Velocity Profile - data (0xA520)';


-- awacWaveParData (0xA560) - acquireDt=2017-07-20 11:28:10 [hSize=40 (80B) / clock=2017-07-20 11:10:01 / cSpectrumType=3 /
--                            cProcMethod=4 / hHm0=158 / hH3=138 / hH10=171 / hHmax=242 / hTm02=212 / hTp=245 / hTz=231 /
--                            hDirTp=26982 / hSprTp=8100 / hDirMean=32534 / hUI=38397 / lPressureMean=21784 / hNumNoDet=33 /
--                            hNumBadDet=28114 / hCurSpeedMean=128 / hCurDirMean=0 / lError=21364 / hChecksum0x5970]

DROP TABLE IF EXISTS `awac2_waves`;
CREATE TABLE `awac2_waves` (
  `time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Buoy data acquisition time (when it was downloaded to the buoy)',
  `packet_id` int(10) unsigned NOT NULL,
  `profile_start_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Computation profile start time (rounded to 30 minutes)',
  `awac_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'AWAC internal time - measurement start time',
  `spectrum_type` tinyint unsigned NOT NULL COMMENT 'Spectrum used for calculation',
  `proc_method` tinyint unsigned NOT NULL COMMENT 'Processing method used in actual calculation',
  `Hm0` float NOT NULL COMMENT 'Spectral significant wave height [m]',
  `H3` float NOT NULL COMMENT 'Significant wave height (mean of largest 1/3, i.e. 33%) [m]',
  `H10` float NOT NULL COMMENT 'Wave height(mean of largest 1/10, i.e. 10%) [m]',
  `H_max` float NOT NULL COMMENT 'Max wave height in wave ensemble [m]',
  `Tm02` float NOT NULL COMMENT 'Mean period spectrum based [s]',
  `Tp` float NOT NULL COMMENT 'Peak period [s]',
  `Tz` float NOT NULL COMMENT 'Mean zero-crossing period [s]',
  `dir_Tp` float NOT NULL COMMENT 'Direction at Tp [°]',
  `spr_Tp` float NOT NULL COMMENT 'Spreading at Tp [°]',
  `dir_mean` float NOT NULL COMMENT 'Mean wave direction [°]',
  `UI` float NOT NULL COMMENT 'Unidirectivity index [1/65535]',
  `pressure_mean` float NOT NULL COMMENT 'Mean pressure during burst [dBar]',
  `no_detect` smallint unsigned NOT NULL COMMENT 'Number of ST No detects [#]',
  `bad_detect` smallint unsigned NOT NULL COMMENT 'Number of ST Bad detects [#]',
  `mean_current_speed` float NOT NULL COMMENT 'Mean current speed - wave cells [cm/sec]',
  `mean_current_direction` float NOT NULL COMMENT 'Mean current direction - wave cells [°]',
  `error_code` int(10) unsigned NOT NULL COMMENT 'Error Code for bad data',
  PRIMARY KEY  (`time`),
  KEY `TIME` (`time`),
  KEY `fk_awac2_waves_profile` (`packet_id`),
  CONSTRAINT `fk_awac2_waves_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC Wave Parameter Data (0xA560)';


-- awacWaveBandData (0xA561) - acquireDt=2017-07-20 11:28:11 [hSize=24 (48B) / clock=2017-07-20 11:10:01 / cSpectrumType=3 /
--                             cProcMethod=4 / hLowFrequency=20 / hHighFrequency=200 / hHm0=30 / hTm02=677 / hTp=568 /
--                             hDirTp=5110 / hDirMean=32843 / hSprTp=8095 / lError=0 / hChecksum0x0006]
-- awacWaveBandData (0xA561) - acquireDt=2017-07-20 11:28:11 [hSize=24 (48B) / clock=2017-07-20 11:10:01 / cSpectrumType=3 /
--                             cProcMethod=4 / hLowFrequency=210 / hHighFrequency=490 / hHm0=129 / hTm02=268 / hTp=245 /
--                             hDirTp=4500 / hDirMean=33046 / hSprTp=0 / lError=0 / hChecksum0x0023]

DROP TABLE IF EXISTS `awac2_wave_bands`;
CREATE TABLE `awac2_wave_bands` (
  `wave_bands_id` int(10) unsigned NOT NULL auto_increment,
  `time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Buoy data acquisition time (when it was downloaded to the buoy)',
  `packet_id` int(10) unsigned NOT NULL,
  `profile_start_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Computation profile start time (rounded to 30 minutes)',
  `awac_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'AWAC internal time - measurement start time',
  `spectrum_type` tinyint unsigned NOT NULL COMMENT 'Spectrum used for calculation',
  `proc_method` tinyint unsigned NOT NULL COMMENT 'Processing method used in actual calculation',
  `low_freq` float NOT NULL COMMENT 'Low frequency in [Hz]',
  `hi_freq` float NOT NULL COMMENT 'High frequency in [Hz]',
  `Hm0` float NOT NULL COMMENT 'Spectral significant wave height [m]',
  `Tm02` float NOT NULL COMMENT 'Mean period spectrum based [s]',
  `Tp` float NOT NULL COMMENT 'Peak period [s]',
  `dir_Tp` float NOT NULL COMMENT 'Direction at Tp [°]',
  `dir_mean` float NOT NULL COMMENT 'Mean wave direction [°]',
  `spr_Tp` float NOT NULL COMMENT 'Spreading at Tp [°]',
  `error_code` int(10) unsigned NOT NULL COMMENT 'Error code',
  PRIMARY KEY (`wave_bands_id`),
  KEY  (`time`),
  KEY `fk_awac2_wave_bands_profile` (`packet_id`),
  CONSTRAINT `fk_awac2_wave_bands_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC Wave Band data (0xA561)';



-- awacWaveSpectrumData (0xA562) - acquireDt=2017-07-20 11:28:11 [hSize=120 (240B) / clock=2017-07-20 11:10:01 / cSpectrumType=3 /
--                                 hNumSpectrum=98 / hLowFrequency=20 / hHighFrequency=990 / hStepFrequency=10 /
--                                 lEnergyMultiplier=116251746656845877 / hEnergy=<
--  energy #1 -> 335
--  ...
--  energy #96 -> 5426
-- > / hChecksum=0x905e]
DROP TABLE IF EXISTS `awac2_wave_spectrum_data`;
DROP TABLE IF EXISTS `awac2_wave_spectrum_header`;
CREATE TABLE `awac2_wave_spectrum_header` (
  `wave_spectrum_hdr_id` int(10) unsigned NOT NULL auto_increment,
  `time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Buoy data acquisition time (when it was downloaded to the buoy)',
  `packet_id` int(10) unsigned NOT NULL,
  `profile_start_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Computation profile start time (rounded to 30 minutes)',
  `awac_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'AWAC internal time - measurement start time',
  `spectrum_type` tinyint unsigned NOT NULL COMMENT 'Spectrum used for calculation',
  `n_spectrum` smallint unsigned NOT NULL COMMENT 'Number of spectral bins',
  `low_freq` float NOT NULL COMMENT 'Low frequency [Hz]',
  `hi_freq` float NOT NULL COMMENT 'High frequency [Hz]',
  `step_freq` float NOT NULL COMMENT 'Frequency step [Hz]',
  `energy_mult` float NOT NULL COMMENT 'Energy spectrum multiplier [cm^2/Hz]',
  PRIMARY KEY  (`wave_spectrum_hdr_id`),
  UNIQUE KEY (`time`),
  KEY `fk_awac2_wave_spectrum_header_profile` (`packet_id`),
  CONSTRAINT `fk_awac2_wave_spectrum_header_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC Wave Spectrum - header (0xA562)';

CREATE TABLE `awac2_wave_spectrum_data` (
  `wave_spectrum_hdr_id` int(10) unsigned NOT NULL,
  `idx` smallint unsigned NOT NULL COMMENT 'Wave spectrum data energy index',
  `energy` float NOT NULL COMMENT 'Spectra [0 - 1/65535]',
  PRIMARY KEY  (`wave_spectrum_hdr_id`,`idx`),
  CONSTRAINT `fk_awac2_wave_spectrum_hdr` FOREIGN KEY (`wave_spectrum_hdr_id`) REFERENCES `awac2_wave_spectrum_header` (`wave_spectrum_hdr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC Wave Spectrum - data (0xA562)';


-- awacWaveFourierCoeff (0xA563) - acquireDt=2017-07-20 11:28:11 [hSize=208 (416B) / clock=2017-07-20 11:10:01 / cProcMethod=4/
--                                 hNumSpectrum=48 / hLowFrequency=20 / hHighFrequency=490 / hStepFrequency=10 / fourier coeffs=<
--  coeff #1. [hA1=239, hB1=64, hA2=10, hB2=7]
--  ...
--  coeff #48. [hA1=-32768, hB1=-32768, hA2=-32768, hB2=-32768]
-- > / hChecksum=0x5d95]
DROP TABLE IF EXISTS `awac2_wave_fourier_data`;
DROP TABLE IF EXISTS `awac2_wave_fourier_header`;
CREATE TABLE `awac2_wave_fourier_header` (
  `wave_fourier_hdr_id` int(10) unsigned NOT NULL auto_increment,
  `time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Buoy data acquisition time (when it was downloaded to the buoy)',
  `packet_id` int(10) unsigned NOT NULL,
  `profile_start_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'Computation profile start time (rounded to 30 minutes)',
  `awac_time` timestamp NOT NULL default '1970-01-01 00:00:01' COMMENT 'AWAC internal time - measurement start time',
  `proc_method` tinyint unsigned NOT NULL COMMENT 'Processing method used in actual calculation',
  `n_spectrum` smallint unsigned NOT NULL COMMENT 'Number of spectral bins (default 49)',
  `low_freq` float NOT NULL COMMENT 'Low frequency [Hz]',
  `hi_freq` float NOT NULL COMMENT 'High frequency [Hz]',
  `step_freq` float NOT NULL COMMENT 'Frequency step [Hz]',
  PRIMARY KEY  (`wave_fourier_hdr_id`),
  UNIQUE KEY (`time`),
  KEY `fk_awac2_wave_fourier_header_profile` (`packet_id`),
  CONSTRAINT `fk_awac2_wave_fourier_header_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC Wave Fourier Coefficients - header (0xA563)';

CREATE TABLE `awac2_wave_fourier_data` (
  `wave_fourier_hdr_id` int(10) unsigned NOT NULL,
  `idx` smallint unsigned NOT NULL COMMENT 'Wave spectrum data energy index',
  `A1` float default NULL COMMENT 'Fourier coefficients [+/- 1/32767]',
  `B1` float default NULL COMMENT 'Fourier coefficients [+/- 1/32767]',
  `A2` float default NULL COMMENT 'Fourier coefficients [+/- 1/32767]',
  `B2` float default NULL COMMENT 'Fourier coefficients [+/- 1/32767]',
  PRIMARY KEY  (`wave_fourier_hdr_id`,`idx`),
  CONSTRAINT `fk_awac2_wave_fourier_hdr` FOREIGN KEY (`wave_fourier_hdr_id`) REFERENCES `awac2_wave_fourier_header` (`wave_fourier_hdr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AWAC Wave Fourier Coefficients - data (0xA563)';
