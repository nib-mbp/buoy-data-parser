USE `scada_data`;
SET time_zone="+00:00";


-- -----------------
-- SCADA Auth Tables
-- -----------------
DROP TABLE IF EXISTS `account_role`;
DROP TABLE IF EXISTS `account`;
DROP TABLE IF EXISTS `role`;
DROP TABLE IF EXISTS `account_session`;

CREATE TABLE `account` (
  `account_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `is_cert_user` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'If 1, it indicates the usernme is a certificate full dn, i.e. the user is a x509 certificate one',
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `unique_login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `role` (
  `role_id` smallint(5) NOT NULL,
  `role_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `account_role` (
  `account_id` smallint(5) unsigned NOT NULL,
  `role_id` smallint(5) NOT NULL,
  PRIMARY KEY (`account_id`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `account_role_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON UPDATE CASCADE,
  CONSTRAINT `account_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `account_session` (
  `session_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` smallint(5) unsigned NOT NULL,
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `session` binary(24) NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session` (`session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP VIEW IF EXISTS `v_tomcat_roles`;
CREATE VIEW `v_tomcat_roles` AS SELECT `a`.`login` AS `user_name`,`r`.`role_name` AS `role_name` from ((`account_role` `ar` left join `account` `a` on((`ar`.`account_id` = `a`.`account_id`))) left join `role` `r` on((`ar`.`role_id` = `r`.`role_id`))) where (`a`.`is_enabled` = 1);

DROP VIEW IF EXISTS `v_tomcat_users`;
CREATE VIEW `v_tomcat_users` AS select `a`.`login` AS `user_name`,`a`.`password` AS `user_pass` from `account` `a` where (`a`.`is_enabled` = 1);


-- -------------
-- Add user data
-- -------------
INSERT INTO `role` VALUES (0,'ROLE_NA'),(1,'ROLE_READER'),(2,'ROLE_WRITER'),(3,'ROLE_ADMIN'),(4,'ROLE_SUPERADMIN');

DROP FUNCTION IF EXISTS `f_authenticateUser`;
DROP FUNCTION IF EXISTS `f_hasUserEnoughGrants`;
DROP PROCEDURE IF EXISTS `p_getUserData`;
DROP PROCEDURE IF EXISTS `p_logoutUser`;
DROP PROCEDURE IF EXISTS `p_updateSession`;

DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_authenticateUser`(
    a_username VARCHAR(200),
    a_password VARCHAR(64)) RETURNS char(24) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
    COMMENT 'Authenticate user'
BEGIN
  DECLARE _num INT;
  DECLARE _sessionToken CHAR(24);
  DECLARE _accountId SMALLINT UNSIGNED;

  DELETE FROM account_session WHERE dt<DATE_ADD(NOW(), INTERVAL -2 HOUR);

  SELECT COUNT(*) AS num, `account_id` INTO _num, _accountId FROM `account` WHERE BINARY `login`=a_username AND BINARY `password`=a_password;
  IF _num > 0 THEN
    SELECT CONCAT(LEFT(UUID(), 8), LEFT(UUID(), 8), LEFT(UUID(), 8)) INTO _sessionToken;
    INSERT INTO `account_session` (`account_id`, `session`) VALUES (_accountId, _sessionToken);
    RETURN _sessionToken;
  ELSE
    RETURN NULL;
  END IF;
END ;;

CREATE DEFINER=`root`@`localhost` FUNCTION `f_hasUserEnoughGrants`(
    a_username VARCHAR(200),
    a_sessionToken CHAR(24),
    a_requestedRole SMALLINT UNSIGNED) RETURNS tinyint(1)
    READS SQL DATA
    COMMENT 'Check if the user has enough privilegas to carry out the operation specified by the role - ATTENTION: max permited role for non-cert user is ROLE_READER'
BEGIN
  DECLARE _rv BOOLEAN;
  DECLARE _isCertUser BOOLEAN;
  DECLARE _num INT;
  DECLARE _roleId INT;

  DELETE FROM account_session WHERE dt<DATE_ADD(NOW(), INTERVAL -2 HOUR);

  SELECT COUNT(*) AS num, ar.`role_id`, a.`is_cert_user`
      INTO _num, _roleId, _isCertUser
    FROM `account_session` s
      LEFT JOIN `account` a ON s.`account_id`=a.`account_id`
      LEFT JOIN `account_role` ar ON a.`account_id`=ar.`account_id`
    WHERE
      BINARY a.`login`=a_username AND s.`session`=a_sessionToken
      AND ar.`role_id` >= a_requestedRole
      AND s.`dt`>=DATE_ADD(NOW(), INTERVAL -2 HOUR)
    ORDER BY `dt` DESC
    LIMIT 1;

  IF _num < 1 THEN
    RETURN FALSE;
  ELSE
    IF _isCertUser = 0 AND a_requestedRole > 1 THEN
      -- if user isn't cert authenticated and requested role > ROLE_READER,
      -- return false
      RETURN FALSE;
    END IF;
    RETURN TRUE;
  END IF;
END ;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getUserData`(
    IN a_username VARCHAR(200),
    IN a_sessionToken CHAR(24),
    OUT a_isValid BOOL,
    OUT a_isCertAuth BOOL,
    OUT a_firstName VARCHAR(100),
    OUT a_lastName VARCHAR(100),
    OUT a_email VARCHAR(100),
    OUT a_roleId SMALLINT UNSIGNED,
    OUT a_role VARCHAR(100))
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Returns user details for the provided session if it is still valid'
BEGIN
  SELECT COUNT(*) AS num, a.`is_cert_user`, a.`first_name`, a.`last_name`, a.`email`, ar.`role_id`
      INTO a_isValid, a_isCertAuth, a_firstName, a_lastName, a_email, a_roleId
    FROM `account_session` s
      LEFT JOIN `account` a ON s.`account_id`=a.`account_id`
      LEFT JOIN `account_role` ar ON a.`account_id`=ar.`account_id`
    WHERE BINARY a.`login`=a_username AND s.`session`=a_sessionToken
    ORDER BY `dt` DESC
    LIMIT 1;
  DELETE FROM account_session WHERE dt<DATE_ADD(NOW(), INTERVAL -2 HOUR);

  IF a_roleId IS NULL THEN
    SET a_roleId = 0;
  END IF;
  IF a_isCertAuth = 0 THEN
    IF a_roleId > 1 THEN
      SET a_roleId = 1;
    END IF;
  END IF;

  SELECT r.`role_name` INTO a_role FROM `role` r WHERE r.`role_id`=a_roleId LIMIT 1;
END ;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `p_logoutUser`(
    IN a_username VARCHAR(200),
    IN a_sessionToken CHAR(24))
    MODIFIES SQL DATA
    DETERMINISTIC
    COMMENT 'Logs out the specified user/session if they exist'
BEGIN
  DELETE a1
    FROM `account_session` a1
      LEFT JOIN `account` a2
        ON a1.`account_id`=a2.`account_id`
    WHERE 
      BINARY a2.`login`=a_username AND a1.`session`=a_sessionToken;
END ;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateSession`(
    IN a_username VARCHAR(200),
    IN a_sessionToken CHAR(24))
    MODIFIES SQL DATA
    COMMENT 'Update the user session timestamp'
BEGIN
  UPDATE `account_session` s LEFT JOIN `account` a ON s.`account_id`=a.`account_id` SET s.`dt`=NOW() WHERE BINARY a.`login`=a_username AND s.`session`=a_sessionToken;
  DELETE FROM account_session WHERE dt<DATE_ADD(NOW(), INTERVAL -2 HOUR);
END ;;
DELIMITER ;




-- ---------------------------
-- -- SCADA JSON Data table --
-- ---------------------------
DROP TABLE IF EXISTS `scada_data`;
CREATE TABLE `scada_data` (
  `id` int(10) unsigned NOT NULL,
  `data` varchar(4000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '{}',
  PRIMARY KEY (`id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Buoy runtime data';


-- -------------------------------
-- -- SCADA Buoy Commands table --
-- -------------------------------
DROP TABLE IF EXISTS `scada_commands`;
CREATE TABLE `scada_commands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `instrument` SET('humidity', 'salinity', 'wind', 'compass', 'awac', 'oxygen1', 'oxygen2', 'oxygen3', 'par', 'co2') NOT NULL COMMENT 'Instrument - only one should be selected',
  `password` VARCHAR(64) NOT NULL COMMENT 'password / signature for the command',
  `cmd` TINYINT UNSIGNED NOT NULL COMMENT 'Command - 0: stop,  1: start',
  `cmd_dt` TIMESTAMP NOT NULL DEFAULT '1970-01-01T00:00:01' COMMENT 'Timestamp when the command was received',
  `retr_no` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'How many times the command was retrieved using the get*() stored procedure',
  `retr_update_dt` TIMESTAMP NOT NULL DEFAULT '1970-01-01T00:00:01' COMMENT 'Last time the retrival count was updated',
--  `cmd_exec_status` TINYINT UNSIGNED NOT NULL COMMENT 'Command execution status - 0: new cmd,  1: retrieved one time,  2: retrieved two times, 3: retrieved three times; note: when retrieving the command for the 4th time or it is older than 1 hour, it will be removed',
  `exec_status` TINYINT UNSIGNED NOT NULL COMMENT 'Exec status - 0: new cmd (did not received any ACK from buoy yet),  1: cmd being processed (got received ACK from buoy),  2: command sent too many times, but no ACK received - stopped,  3: command cannot be delivered or failed',
--  `cmd_status_dt` TIMESTAMP NOT NULL DEFAULT '1970-01-01T00:00:01' COMMENT 'Timestamp of the last cmd_exec_status update (either insert of command, or when last retrieved',
  `status_update_dt` TIMESTAMP NOT NULL DEFAULT '1970-01-01T00:00:01' COMMENT 'Last time the command status changed',
  PRIMARY KEY (`id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Buoy commands - instruments manipulation';


-- --------------------------
-- -- SCADA Data Functions --
-- --------------------------
DROP FUNCTION IF EXISTS `f_getScadaData`;
DROP PROCEDURE IF EXISTS `p_setScadaData`;

DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_getScadaData`() RETURNS varchar(4000) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
    COMMENT 'Retrieves SCADA'
BEGIN
  DECLARE _rv VARCHAR(4000);

  SELECT `data` INTO _rv FROM `scada_data` ORDER BY id DESC LIMIT 1;

  IF _rv IS NULL THEN
    SET _rv = "{}";
  END IF;

  RETURN _rv;
END ;;


CREATE DEFINER=`root`@`localhost` PROCEDURE `p_setScadaData`(
    IN data VARCHAR(4000))
    MODIFIES SQL DATA
    COMMENT 'Updates SCADA data'
BEGIN
  DECLARE _num INT;

  SELECT COUNT(*) INTO @num FROM `scada_data`;

  SET _num = @num;

  IF _num > 1 THEN
    DELETE FROM `scada_data`;
    SET _num = 0;
  END IF;
  IF _num = 0 THEN
    INSERT INTO `scada_data` (`data`) VALUES (data);
  ELSE
    UPDATE `scada_data` SET `data`=data;
  END IF;
END ;;
DELIMITER ;



-- ----------------------------------
-- -- SCADA Buoy Command Functions --
-- ----------------------------------
DROP PROCEDURE IF EXISTS `p_setScadaCmd`;
DROP PROCEDURE IF EXISTS `p_getScadaCmd`;
DROP PROCEDURE IF EXISTS `p_updateScadaCmdStatus`;

DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_setScadaCmd`(
  IN inInstrument SET('humidity', 'salinity', 'wind', 'compass', 'awac', 'oxygen1', 'oxygen2', 'oxygen3', 'par', 'co2'),
  IN inPassword VARCHAR(64),
  IN inNewCommand TINYINT UNSIGNED
  )
    LANGUAGE SQL
    NOT DETERMINISTIC
    MODIFIES SQL DATA
    SQL SECURITY DEFINER
    COMMENT 'Add new buoy command and delete all previous commands for the same instrument'
proc_label:BEGIN
  DECLARE _errText VARCHAR(255);
  DECLARE _sigInvalidInstrument CONDITION FOR SQLSTATE '45001';
  DECLARE _sigExecPipelineLocked CONDITION FOR SQLSTATE '45002';

  DECLARE _instrument SET('humidity', 'salinity', 'wind', 'compass', 'awac', 'oxygen1', 'oxygen2', 'oxygen3', 'par', 'co2');

  DECLARE _isOk BOOLEAN;
  SET _isOk = FALSE;
  IF LENGTH(inInstrument) > 0 THEN
    SELECT MOD(LOG2(inInstrument), 1) INTO @modRv;  -- find full numbers
    IF @modRv = 0 THEN
      SET _isOk = TRUE;
    END IF;
  END IF;

  IF _isOk = FALSE THEN
    SET _errText = CONCAT("Invalid instrument '", inInstrument, "' (", (inInstrument+0), ") specified!");
    SIGNAL _sigInvalidInstrument SET MESSAGE_TEXT = _errText;
  END IF;

  SELECT `instrument` INTO _instrument
    FROM `scada_commands`
    WHERE `exec_status`<>0
    ORDER BY `cmd_dt` ASC
    LIMIT 1;
  IF _instrument IS NOT NULL THEN
    SET _errText = CONCAT ("Execution pipeline locked by command for instrument '", _instrument, "' (", (_instrument+0), ")!");
    SIGNAL _sigExecPipelineLocked SET MESSAGE_TEXT = _errText;
  END IF;
  INSERT INTO `scada_commands`
    (`instrument`, `password`, `cmd`, `cmd_dt`, `retr_no`, `retr_update_dt`, `exec_status`, `status_update_dt`)
    VALUES (inInstrument, inPassword, inNewCommand, NOW(), 0, NOW(), 0, NOW());
  SELECT LAST_INSERT_ID() INTO @lastId;
  DELETE FROM `scada_commands` WHERE `instrument`=inInstrument AND `id`<>@lastId;
END ;;


CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getScadaCmd`(
  OUT outInstrument SET('humidity', 'salinity', 'wind', 'compass', 'awac', 'oxygen1', 'oxygen2', 'oxygen3', 'par', 'co2'),
  OUT outPassword VARCHAR(64),
  OUT outCmd INT,
  OUT outCmdDt TIMESTAMP,
  OUT outRetrNo INT,
  OUT outRetrUpdateDt TIMESTAMP,
  OUT outExecStatus INT,
  OUT outStatusUpdateDt TIMESTAMP
  )
    LANGUAGE SQL
    NOT DETERMINISTIC
    MODIFIES SQL DATA
    SQL SECURITY DEFINER
    COMMENT 'Find the oldest command, retrieve its newest status, and remove all its old variants'
proc_label:BEGIN
  DECLARE _errText VARCHAR(255);
  DECLARE specialty CONDITION FOR SQLSTATE '45000';

  DECLARE _instrument SET('humidity', 'salinity', 'wind', 'compass', 'awac', 'oxygen1', 'oxygen2', 'oxygen3', 'par', 'co2');

  SET outInstrument = "";
  SET outPassword = "";
  SET outCmd = -1;
  SET outCmdDt = FROM_UNIXTIME(1);
  SET outRetrNo = -1;
  SET outRetrUpdateDt = FROM_UNIXTIME(1);
  SET outExecStatus = -1;
  SET outStatusUpdateDt = FROM_UNIXTIME(1);

  SELECT COUNT(*) INTO @num FROM `scada_commands`;
  IF @num = 0 THEN
    LEAVE proc_label;
  END IF;

  SELECT `instrument` INTO _instrument FROM `scada_commands` WHERE `exec_status`<>0 ORDER BY `status_update_dt` ASC;
  IF _instrument IS NULL THEN
    SELECT `instrument` INTO _instrument
      FROM `scada_commands`
      ORDER BY `cmd_dt` ASC
      LIMIT 1;
  END IF;
  SELECT `id`, `password`, `cmd`, `cmd_dt`, `retr_no`, `retr_update_dt`, `exec_status`, `status_update_dt`
    INTO @id, @password, @cmd, @cmdDt, @retrNo, @retrUpdateDt, @execStatus, @statusUpdateDt
    FROM `scada_commands`
    WHERE `instrument`=_instrument
    LIMIT 1;

  IF @execStatus<0 OR @execStatus>3 OR @cmd<0 OR @cmd>1 THEN
    SET _errText = CONCAT("Invalid command or status encountered [cmd=", @cmd, " / status=", @execStatus, "]");
    SIGNAL specialty SET MESSAGE_TEXT = _errText;
  ELSEIF @execStatus=0 THEN
    IF @retrNo>=3 OR @retrUpdateDt<DATE_SUB(NOW(), INTERVAL 1 HOUR) THEN
      -- If command was retrieved 3+ times or last retrival is older than 1 hour and
      -- the command reception wasn't ever confirmed by the buoy service program ...
      UPDATE `scada_commands`
        SET `exec_status`=3, `status_update_dt`=NOW()
        WHERE `id`=@id;
    ELSE
      -- The command wasn't retrieved 3 times yet and the command reception wasn't yet
      -- confirmed by the buoy service program
      UPDATE `scada_commands`
        SET `retr_no`=`retr_no`+1, `retr_update_dt`=NOW()
        WHERE `id`=@id;
    END IF;
  END IF;
  -- NOTE: @execStatus=1 OR @execStatus=2 -> pass through


  SET outInstrument = _instrument;
  SET outPassword = @password;
  SET outCmd = @cmd;
  SET outCmdDt = @cmdDt;
  SET outRetrNo = @retrNo;
  SET outRetrUpdateDt = @retrUpdateDt;
  SET outExecStatus = @execStatus;
  SET outStatusUpdateDt = @statusUpdateDt;
END ;;


DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateScadaCmdStatus`(
  IN inInstrument SET('humidity', 'salinity', 'wind', 'compass', 'awac', 'oxygen1', 'oxygen2', 'oxygen3', 'par', 'co2'),
  IN inStatus TINYINT)
    LANGUAGE SQL
    NOT DETERMINISTIC
    MODIFIES SQL DATA
    SQL SECURITY DEFINER
    COMMENT 'Update the status of the command for the specified instrument'
proc_label:BEGIN
  DECLARE _id INT UNSIGNED;
  DECLARE _errText VARCHAR(255);
  DECLARE _sigInvalidInstrument CONDITION FOR SQLSTATE '45001';
  DECLARE _sigExecPipelineLocked CONDITION FOR SQLSTATE '45002';

  DECLARE _isOk BOOLEAN;
  SET _isOk = FALSE;
  IF LENGTH(inInstrument) > 0 THEN
    SELECT MOD(LOG2(inInstrument), 1) INTO @modRv;  -- find full numbers
    IF @modRv = 0 THEN
      SET _isOk = TRUE;
    END IF;
  END IF;

  IF _isOk = FALSE THEN
    SET _errText = CONCAT("Invalid instrument '", inInstrument, "' (", (inInstrument+0), ") specified!");
    SIGNAL _sigInvalidInstrument SET MESSAGE_TEXT = _errText;
  END IF;

  SELECT `id` INTO _id
    FROM `scada_commands`
    WHERE `instrument`=inInstrument AND `retr_no`>=1
    ORDER BY `cmd_dt` ASC
    LIMIT 1;
  IF _id IS NULL THEN
    SET _errText = CONCAT ("No command for instrument '", inInstrument, "' (", (inInstrument+0), ") is in progress!");
    SIGNAL _sigExecPipelineLocked SET MESSAGE_TEXT = _errText;
  END IF;

  IF inStatus=0 THEN
    -- command execution completed successfully
    DELETE FROM `scada_commands` WHERE `id`=_id;
  ELSEIF inStatus=1 or inStatus=2 THEN
    -- buoy processor ACK the reception of the command
    UPDATE `scada_commands` SET `exec_status`=1 WHERE `id`=_id;
  -- ELSEIF inStatus=2 THEN
  --   pass through -> command is being executed
  ELSE
    UPDATE `scada_commands` SET `exec_status`=3 WHERE `id`=_id;
  END IF;
END ;;





DELIMITER ;


-- ------------
-- -- Grants --
-- ------------
-- GRANT USAGE ON *.* TO 'buoy_cmd_test'@'localhost' IDENTIFIED BY 'buoy';
-- GRANT USAGE ON *.* TO 'buoy_cmd_test'@'%' IDENTIFIED BY 'buoy';
-- GRANT USAGE ON *.* TO 'scada_runtime_rw'@'192.168.123.%' IDENTIFIED BY 'BuoyRuntimeRW';
-- GRANT USAGE ON *.* TO 'cyclops_mbss_si'@'192.168.123.%' IDENTIFIED BY 'JFW9fjiw0239dasE';
GRANT USAGE ON *.* TO 'scada_runtime_rw'@'192.168.0.%' IDENTIFIED BY 'BuoyRuntimeRW';
-- GRANT USAGE ON *.* TO 'cyclops_mbss_si'@'192.168.0.%' IDENTIFIED BY 'JFW9fjiw0239dasE';
GRANT USAGE ON *.* TO 'scada_runtime_rw'@'buoylink.nib.sql' IDENTIFIED BY 'BuoyRuntimeRW';

-- GRANT SELECT ON `cyclops_mbss_si`.* TO 'cyclops_mbss_si'@'192.168.123.%';
-- GRANT SELECT ON `cyclops_mbss_si`.* TO 'cyclops_mbss_si'@'192.168.0.%';

-- GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'buoy_cmd_test'@'localhost';
-- GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getScadaCmd` TO 'buoy_cmd_test'@'localhost';
-- GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'buoy_cmd_test'@'%';
-- GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getScadaCmd` TO 'buoy_cmd_test'@'%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'scada_runtime_rw'@'192.168.123.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getScadaCmd` TO 'scada_runtime_rw'@'192.168.123.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_updateScadaCmdStatus` TO 'scada_runtime_rw'@'192.168.123.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'scada_runtime_rw'@'192.168.0.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getScadaCmd` TO 'scada_runtime_rw'@'192.168.0.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_updateScadaCmdStatus` TO 'scada_runtime_rw'@'192.168.0.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'scada_runtime_rw'@'buoylink.nib.sql';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getScadaCmd` TO 'scada_runtime_rw'@'buoylink.nib.sql';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_updateScadaCmdStatus` TO 'scada_runtime_rw'@'buoylink.nib.sql';
-- GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'scada_runtime_rw'@'%';
-- GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getScadaCmd` TO 'scada_runtime_rw'@'%';

GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'scada_runtime_rw'@'192.168.123.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaData` TO 'scada_runtime_rw'@'192.168.123.%';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'scada_runtime_rw'@'192.168.0.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaData` TO 'scada_runtime_rw'@'192.168.0.%';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'scada_runtime_rw'@'buoylink.nib.sql';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaData` TO 'scada_runtime_rw'@'buoylink.nib.sql';
-- GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'cyclops_mbss_si'@'194.249.52.164';
-- GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaData` TO 'cyclops_mbss_si'@'194.249.52.164';
-- GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'cyclops_mbss_si'@'192.168.123.%';
-- GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaData` TO 'cyclops_mbss_si'@'192.168.0.%';
-- GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'cyclops_mbss_si'@'192.168.0.%';

-- ----------------------
-- SCADA J2EE web user --
-- ----------------------
DELETE FROM `mysql`.`procs_priv` WHERE User='scada_web_user';
DELETE FROM `mysql`.`db` WHERE User='scada_web_user';
DELETE FROM `mysql`.`tables_priv` WHERE User='scada_web_user';
DELETE FROM `mysql`.`user` WHERE User='scada_web_user';
DELETE FROM `mysql`.`db` WHERE User='scada_http_auth';
DELETE FROM `mysql`.`tables_priv` WHERE User='scada_http_auth';
DELETE FROM `mysql`.`user` WHERE User='scada_http_auth';
FLUSH PRIVILEGES;

GRANT USAGE ON *.* TO 'scada_web_user'@'192.168.0.%' IDENTIFIED BY 'JFW9fjiw0239dasE';
GRANT USAGE ON *.* TO 'scada_web_user'@'192.168.123.%' IDENTIFIED BY 'JFW9fjiw0239dasE';
GRANT USAGE ON *.* TO 'scada_web_user'@'webserver.nib.si' IDENTIFIED BY 'JFW9fjiw0239dasE';
GRANT USAGE ON *.* TO 'scada_web_user'@'www.nib.si' IDENTIFIED BY 'JFW9fjiw0239dasE';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'scada_web_user'@'webserver.nib.si';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'scada_web_user'@'www.nib.si';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'scada_web_user'@'192.168.0.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_setScadaCmd` TO 'scada_web_user'@'192.168.123.%';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'scada_web_user'@'webserver.nib.si';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'scada_web_user'@'www.nib.si';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'scada_web_user'@'192.168.0.%';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_getScadaData` TO 'scada_web_user'@'192.168.123.%';

GRANT EXECUTE ON FUNCTION `scada_data`.`f_authenticateUser` TO 'scada_web_user'@'webserver.nib.si';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_authenticateUser` TO 'scada_web_user'@'www.nib.si';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_authenticateUser` TO 'scada_web_user'@'192.168.0.%';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_authenticateUser` TO 'scada_web_user'@'192.168.123.%';

GRANT EXECUTE ON FUNCTION `scada_data`.`f_hasUserEnoughGrants` TO 'scada_web_user'@'webserver.nib.si';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_hasUserEnoughGrants` TO 'scada_web_user'@'www.nib.si';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_hasUserEnoughGrants` TO 'scada_web_user'@'192.168.0.%';
GRANT EXECUTE ON FUNCTION `scada_data`.`f_hasUserEnoughGrants` TO 'scada_web_user'@'192.168.123.%';

GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getUserData` TO 'scada_web_user'@'webserver.nib.si';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getUserData` TO 'scada_web_user'@'www.nib.si';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getUserData` TO 'scada_web_user'@'192.168.0.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_getUserData` TO 'scada_web_user'@'192.168.123.%';

GRANT EXECUTE ON PROCEDURE `scada_data`.`p_logoutUser` TO 'scada_web_user'@'webserver.nib.si';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_logoutUser` TO 'scada_web_user'@'www.nib.si';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_logoutUser` TO 'scada_web_user'@'192.168.0.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_logoutUser` TO 'scada_web_user'@'192.168.123.%';

GRANT EXECUTE ON PROCEDURE `scada_data`.`p_updateSession` TO 'scada_web_user'@'webserver.nib.si';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_updateSession` TO 'scada_web_user'@'www.nib.si';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_updateSession` TO 'scada_web_user'@'192.168.0.%';
GRANT EXECUTE ON PROCEDURE `scada_data`.`p_updateSession` TO 'scada_web_user'@'192.168.123.%';


-- GRANT SELECT ON `scada_data`.* TO 'scada_http_auth'@'www.nib.si' IDENTIFIED BY 'httpAuth';



SELECT `scada_data`.`f_getScadaData`() AS JsonData;
CALL `scada_data`.`p_setScadaData`('{"test": "n/a"}');
CALL `scada_data`.`p_setScadaData`('
{"inst": { "bInterior": { "dt": "2016-04-03 15:01:50", "temp": 16.39, "humid": 73}, "bBoard": { "dt": "2016-04-03 15:01:40", "temp": 25.4, "humid": 40, "curr": 0.222}, "bCo2": { "dt": "2016-03-16 18:59:32", "concentration": 100}, "wind": { "dt": "2016-04-03 15:01:49", "u": 3.922, "v": -2.109, "w": 0.116, "vspd": 4.4546, "vdir": 208.269, "err": 0, "status": 0, "currDt": "2016-04-03 15:01:36", "currMin": 0.162, "currMax": 0.201, "currAvg": 0.1749}, "air": { "dt": "2016-04-03 15:01:40", "temp": 12.7, "humid": 83, "err": 0, "status": 0, "currDt": "2016-04-03 15:01:36", "currMin": 0.099, "currMax": 0.114, "currAvg": 0.108}, "compass": { "dt": "2016-04-03 15:01:48", "roll": -3.869, "pitch": -0.175216, "heading": 12.5131, "err": 0, "status": 0, "currDt": "2016-04-03 15:01:44", "currMin": 0.129, "currMax": 0.165, "currAvg": 0.1449}, "seaWater": { "dt": "2016-04-03 15:00:06", "temp": 12.7658, "cond": 4.355, "salinity": 37.715, "chla": 427, "err": 0, "status": 0, "currDt": "2016-04-03 15:01:44", "currMin": 0.021, "currMax": 0.033, "currAvg": 0.0264}, "awac": { "dt": "2016-04-03 14:57:09", "temp": 11.79,"pressure": 21.814, "err": 0, "status": 0, "currDt": "2016-04-03 15:01:34", "currMin": 0.075, "currMax": 2.151, "currAvg": 0.1983}, "o2": { "dt": "2016-04-03 15:01:37", "conc": 0.296965, "err": 0, "status": 0, "currDt": "2016-04-03 15:01:17", "currMin": 0.03, "currMax": 0.216, "currAvg": 0.0387}, "par": { "dt": "2016-04-03 15:01:53", "value_raw": 2176, "err": 0, "status": 0, "currDt": "2016-04-03 15:01:17", "currMin": 0.057, "currMax": 0.075, "currAvg": 0.0645}}, "power":{ "cmnVoltage": { "dt": "2016-04-03 15:00:50", "voltage": 12.6238}, "bat": { "dt": "2016-04-03 15:00:50", "volMin": [6.226, 6.222], "volMax": [6.226, 6.222], "volAvg": [6.226, 6.222], "currMin": [-0.53125, -0.570312, -0.476562, -0.609375], "currMax": [-0.140625, -0.171875, -0.0625, -0.148438], "currAvg": [-0.185938, -0.215625, -0.109375, -0.202344]}, "fuelCell": { "dt": "2016-04-03 15:00:50", "status": 1, "current": -6.03809}, "currentLoops": [{ "dt": "2016-04-03 15:00:50", "current": 1.19355}, { "dt": "2016-04-03 15:00:50", "current": 0.706348}, { "dt": "2016-04-03 15:00:50", "current": 0.0206055}], "mppt": { "dt": "2016-04-03 15:00:50", "inI": 1.09, "inU": 16.74, "outI": 1.37, "outU": 12.65} }, "status":{ "door": { "dt": "2016-03-16 13:01:57", "status": 0}, "co2": { "dt": "1970-01-01 00:00:00", "status": 3}, "energy": { "dt": "2016-03-16 19:04:50", "status": 0},"wifi": { "dt": "2016-03-16 19:07:17", "status": 2},"wifi24g": { "dt": "2016-03-16 13:05:26", "status": 1},"wifi5g": { "dt": "2016-04-02 10:32:20", "status": 0},"waterCmn": { "dt": "2016-03-16 19:10:53", "status": 1},"waterBody": { "dt": "2016-03-16 19:22:38", "status": 1},"waterCam": { "dt": "2016-03-16 19:22:38", "status": 1},"camCmn": { "dt": "2016-03-16 19:15:29", "status": 0},"cam1": { "dt": "2016-03-16 19:17:19", "status": 0},"cam2": { "dt": "2016-03-16 19:17:19", "status": 0},"cam3": { "dt": "2016-03-16 19:17:19", "status": 0},"cam4": { "dt": "2016-03-16 19:17:19", "status": 0},"uwCam": { "dt": "2016-03-16 19:28:48", "status": 0}, "gps": { "dt": "2016-03-16 12:58:49", "status": 0},"bat1": { "dt": "1970-01-01 00:00:00", "status": 3},"bat2": { "dt": "1970-01-01 00:00:00", "status": 3}}}');
SELECT `scada_data`.`f_getScadaData`() AS JsonData;

-- CALL p_setScadaCmd('', '', 3, 1);

-- CALL p_setScadaCmd('compass,salinity', '', 3, 1);

-- CALL p_setScadaCmd('compass', 'TestPassword', 1);

-- CALL p_getScadaCmd(@instrument, @password, @cmd, @cmd_dt, @retrNo, @retrUpdateDt, @execStatus, @statusUpdateDt);
-- SELECT @instrument, @password, @cmd, @cmd_dt, @retrNo, @retrUpdateDt, @execStatus, @statusUpdateDt;
