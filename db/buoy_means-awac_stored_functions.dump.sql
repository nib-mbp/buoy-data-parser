SET TIME_ZONE='+00:00';
SET character_set_client = utf8;

DELIMITER ;;

DROP FUNCTION IF EXISTS `buoy_means`.`f_get_awacE`;;
CREATE DEFINER=`root`@`localhost` FUNCTION `buoy_means`.`f_get_awacE`(
  i_pid INT UNSIGNED,
  i_cell tinyint UNSIGNED
) RETURNS float
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Returns AWAC current E component for the given cell [cm/s]'
BEGIN
  DECLARE _rv FLOAT;
  SELECT ac.current_E INTO _rv FROM awac_currents ac WHERE ac.pid=i_pid AND ac.cell_no=i_cell LIMIT 1;
  RETURN _rv;
END;;

DROP FUNCTION IF EXISTS `buoy_means`.`f_get_awacN`;;
CREATE DEFINER=`root`@`localhost` FUNCTION `buoy_means`.`f_get_awacN`(
  i_pid INT UNSIGNED,
  i_cell tinyint UNSIGNED
) RETURNS float
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Returns AWAC current N component for the given cell [cm/s]' 
BEGIN
  DECLARE _rv FLOAT;
  SELECT ac.current_N INTO _rv FROM awac_currents ac WHERE ac.pid=i_pid AND ac.cell_no=i_cell LIMIT 1;
  RETURN _rv;
END;;

DROP FUNCTION IF EXISTS `buoy_means`.`f_get_awacW`;;
CREATE DEFINER=`root`@`localhost` FUNCTION `buoy_means`.`f_get_awacW`(
  i_pid INT UNSIGNED,
  i_cell tinyint UNSIGNED
) RETURNS float
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Returns AWAC current W component for the given cell [cm/s]'
BEGIN
  DECLARE _rv FLOAT;
  SELECT ac.current_W INTO _rv FROM awac_currents ac WHERE ac.pid=i_pid AND ac.cell_no=i_cell LIMIT 1;
  RETURN _rv;
END;;

DELIMITER ;
