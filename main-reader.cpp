#include <iostream>
#include <string>
#include <cstring>

#include <syslog.h>

#include <boost/shared_array.hpp>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>
#include <boost/timer/timer.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "buoy_shared_array.hpp"
#include "buoy_utils.hpp"
#include "buoy_udp_connector.hpp"
#include "buoy_data.hpp"
#include "buoy_db.hpp"
#include "generated/version.hpp"

#define SLEEP_TIME_ms   (200)
#define BUF_SIZE_mb     (1)
#define BUF_LEN_b       (BUF_SIZE_mb * 1024 * 1024)

#define DB_ALLOWED_DUPLICATES_TIME_PERIOD_s     (600)   // a time period to look for duplicates


// -- Global variables --
static bool g_doLoop = true;    // global as it is updated by signal handler
static int g_mainRv = 0;


// -- Signal action handler (handling -HUP and -TERM
static void signalHdl (int sig, siginfo_t *siginfo, void *context)  {
    std::ostringstream sstr;
    sstr << "Got signal ("
         << "signal=" << sig << " / "
         << "sender PID=" << (long)siginfo->si_pid
         << " / UID=" << (long)siginfo->si_uid << ")";
    L_(warning) << sstr.str();
    g_mainRv = SIGTERM;
    switch (sig) {
        case SIGTERM:
            sstr.str("");
            sstr << "Buoy reader stopping (got SIGTERM) @ "
                 << mbp::buoy::PtimeToStr(boost::posix_time::second_clock::universal_time());
            L_(warning) << sstr.str();
            syslog(LOG_NOTICE | LOG_USER, "%s", sstr.str().c_str());
            g_mainRv = SIGTERM;
            g_mainRv = 0;
            g_doLoop = false;
            break;
        case SIGKILL:
            sstr.str("");
            sstr << "Buoy reader stopping (got SIGKILL) @ "
                 << mbp::buoy::PtimeToStr(boost::posix_time::second_clock::universal_time());
            L_(warning) << sstr.str();
            syslog(LOG_NOTICE | LOG_USER, "%s", sstr.str().c_str());
            g_doLoop = false;
            break;
        case SIGINT:
        case SIGQUIT:
            sstr.str("");
            sstr << "Buoy reader stopping (got signal " << sig << ") @ "
                 << mbp::buoy::PtimeToStr(boost::posix_time::second_clock::universal_time());
            L_(warning) << sstr.str();
            syslog(LOG_NOTICE | LOG_USER, "%s", sstr.str().c_str());
            g_doLoop = false;
            break;
        default:
            sstr.str("");
            sstr << "Buoy reader got signal " << sig << " @ "
                 << mbp::buoy::PtimeToStr(boost::posix_time::second_clock::universal_time())
                 << ". Ignoring ...";
            L_(warning) << sstr.str();
            g_doLoop = true;
            g_mainRv = 0;
    } // switch (sig)
    return;
} // signalHdl()


// -- Main program --
int main(int argc, char *argv[]) {
    BOOST_LOG_FUNCTION();
    struct sigaction signalAct;
    int iRv;
    uint32_t numberOfDbDuplicates = 0;
    boost::posix_time::ptime dbDuplicatesCountStartTime = boost::posix_time::second_clock::universal_time();
    std::ostringstream sstr;

    if (!mbp::buoy::g_options.Parse(argc, argv, mbp::buoy::CONFIG_READER_e)) {
        return 1;
    }

    if ( mbp::buoy::g_options.getDoDaemonize() == true ) {
        // x. Check if a daemon is already running

        // 1. Close all open file descriptors except standard input, output, and error (i.e. the
        //    first three file descriptors 0, 1, 2), etc. -> just log is open

        // 2. Reset all signal handlers to their default. This is best done by iterating through the
        //    available signals up to the limit of _NSIG and resetting them to SIG_DFL.
        L_(trace) << "Resetting all signals to their defaults ...";
        for (int i=0; i < _NSIG; ++i) {
            signal(i, SIG_DFL);
        }

        // x. Register signal handling for SIGTERM and SIGHUP
        L_(trace) << "Set signal handling for SIGTERM and SIGHUP ...";
        memset(&signalAct, '\0', sizeof(signalAct));
        signalAct.sa_sigaction = &signalHdl;
        signalAct.sa_flags = SA_SIGINFO;
        if (
                (sigaction(SIGTERM, &signalAct, NULL) < 0)
                || (sigaction(SIGHUP, &signalAct, NULL) < 0)
                ) {
            L_(fatal) << "Error occured when registering sigaction (" << std::strerror(errno) << ")!";
            exit(1);
        }

        // 3. Reset the signal mask using sigprocmask().
        sigset_t mask;
        sigemptyset(&mask);
        sigprocmask(SIG_SETMASK, &mask, NULL);

        mbp::buoy::DaemonizeOnRequest(mbp::buoy::g_options.getDoDaemonize());
    }


    // -- log startup of the reader --
    sstr.str("");
    sstr << "Buoy reader, version: '" << mbp::buoy::GetProgramVersion() << "', started @ "
         << mbp::buoy::PtimeToStr(boost::posix_time::second_clock::universal_time())
         << " (daemon = " << mbp::buoy::g_options.getDoDaemonize() << ")";
    L_(warning) << sstr.str();
    syslog(LOG_NOTICE | LOG_USER, "%s", sstr.str().c_str());

    buoy_udp_pkt_t* udpPacket_p = NULL;
    mysql_packet_data_t* mysqlReply_p = NULL;

    mbp::BuoySharedArray<uint8_t> buoyArray_sp(new uint8_t [BUF_LEN_b], BUF_LEN_b, 0, 0);
    mbp::BuoyUdpConnector buoyConnector(mbp::buoy::g_options.getServerIp(), mbp::buoy::g_options.getServerPort());

    mbp::BuoyDb sql(
            mbp::buoy::g_options.getDbUser(),
            mbp::buoy::g_options.getDbPasswd(),
            mbp::buoy::g_options.getDbDbPrefix(),
            mbp::buoy::g_options.getDbMarkerTable(),
            mbp::buoy::g_options.getDbHost());

    mbp::DbDataMarker marker;
    try {
        buoyConnector.Init();
        sql.Connect();
    } catch (const mysqlpp::ConnectionFailed& e) {
        L_(fatal) << "ERROR (mysqlpp::ConnectionFailed): " << e.what();
        return 1;
    } catch (...) {
        L_(fatal) << "ERROR: Unknown exception occured!";
        return 1;
    }

    buoyArray_sp.ClearData();

    // bool doLoop = true;
    g_doLoop = true;
    int idleCounter = 0;

    // -- measuring time spend in function (this is wallclock AND cpu time) --
    int loopCounter = 0;
    boost::timer::cpu_timer timer;
    while (g_doLoop) {
        ++loopCounter;
        if (   (mbp::buoy::g_options.getDbgDoSqlDump())
            || (! mbp::buoy::g_options.getDbgDoDbInsert())
           )
        {
            g_doLoop = false;
        }
        try {
            marker = sql.getPacketMarker();
            L_(trace) << "DB Data Marker (from DB) [" << marker.getDateStr() << " / " << marker.getPacketId() << "]";

            marker.incMarker();
            L_(trace) << "DB Data Marker (incremented) [" << marker.getDateStr() << " / " << marker.getPacketId() << "]";
            boost::gregorian::date markerDate = marker.getDate();
            int32_t reqPacket = marker.getPacketId();
            iRv = buoyConnector.QueryMysql(buoyArray_sp, markerDate.year(), markerDate.month(), markerDate.day(), reqPacket);

            if (iRv != 0) {
                L_(warning) << "Query RAW.mysql data FAILED - rv=" << iRv;
                // -- there aren't new packages for the day, but the day isn't finished yet --
                if ( (idleCounter % 5) == 0 ) {
                    std::cout << ". " << std::flush;
                }
                boost::this_thread::sleep_for(boost::chrono::milliseconds(SLEEP_TIME_ms));
                ++idleCounter;
                continue;
            } else {
                //std::cout << std::endl;
                idleCounter = 0;
            }
            udpPacket_p = reinterpret_cast<buoy_udp_pkt_t *>(buoyArray_sp.get());
            mysqlReply_p = reinterpret_cast<mysql_packet_data_t *>(&udpPacket_p->data[0]);
            L_(trace)
                << "mysql (reqPacket = " << reqPacket << ")"
                << " [cmd = " << udpPacket_p->hdr.command
                << " / udp.len = " << buoyArray_sp.getTotalLength()
                << " / pkt.full_len = " << (udpPacket_p->hdr.data_len + BUOY_UDP_PKT_HEADER_SIZE)
                << " / pkt.data_only_len = " << udpPacket_p->hdr.data_len
                << " / pkt.packet = " << (uint32_t) udpPacket_p->hdr.packet
                << " / date = " << boost::format("%4u-%02d-%02d")
                        % (uint32_t)mysqlReply_p->hdr.year % (int32_t)mysqlReply_p->hdr.month % (int32_t)mysqlReply_p->hdr.day
                << " / pkt.# = " << (int32_t)mysqlReply_p->hdr.packet_no
                << " / data.len = " << mysqlReply_p->len
                << "]";

            sql.StartTsx();
            if ( mysqlReply_p->len > 0 ) {
                mbp::BuoyData buoyData(buoyArray_sp);
                buoyData.ParseBuffer(mbp::PARSING_TYPE_MYSQL_e, NULL);

                sql.setData(marker, buoyData.getTables(), buoyData.getRows());

                if (mbp::buoy::g_options.getDbgDoSqlDump()) {
                    sql.DumpData();
                }

                if (mbp::buoy::g_options.getDbgDoDbInsert()) {
                    sql.CommitData();
                }
            }

            // -- store the data marker --
            if ( mysqlReply_p->hdr.packet_no == -marker.getPacketId() ) {
                // -- found last packet of the day --
                marker.setPacketId(mysqlReply_p->hdr.packet_no);
            }
            sql.setPacketMarker(marker);

            sql.CommitTsx();
        }
        catch (std::runtime_error& e) {
            L_(fatal) << "Runtime exception: " << e.what();
            exit(1);
        }
        catch (std::string& strE) {
            L_(fatal) << "Got exception: " << strE;
            sql.RollbackTsx();

            // check if the less
            if (strE.find("DB err: Duplicate entry") != std::string::npos) {
                boost::posix_time::ptime now = boost::posix_time::second_clock::universal_time();
                boost::posix_time::time_duration timeDiff = now - dbDuplicatesCountStartTime;

                if (numberOfDbDuplicates < mbp::buoy::g_options.getNumIgnoreDuplicates() ) {
                    if (timeDiff.total_seconds() > DB_ALLOWED_DUPLICATES_TIME_PERIOD_s) {
                        dbDuplicatesCountStartTime = boost::posix_time::second_clock::universal_time();
                        numberOfDbDuplicates = 0;
                    }

                    ++numberOfDbDuplicates;

                    L_(warning) << "Found a duplicate #" << numberOfDbDuplicates << " within " << DB_ALLOWED_DUPLICATES_TIME_PERIOD_s << " seconds. Try to recover ...";

                    // -- Increase the marker --
                    sql.StartTsx();
                    if ( mysqlReply_p->hdr.packet_no == -marker.getPacketId() ) {
                        // -- found last packet of the day --
                        marker.setPacketId(mysqlReply_p->hdr.packet_no);
                    }
                    sql.setPacketMarker(marker);
                    sql.CommitTsx();
                    
                    continue;
                } else {
                    L_(error) << "Maximum number of duplicates " << mbp::buoy::g_options.getNumIgnoreDuplicates() << " exceeded within the allowed period of " << DB_ALLOWED_DUPLICATES_TIME_PERIOD_s << " seconds! Exiting ...";
                }
            }

            // throw strE;
            exit(2);
        }
        catch (std::exception& e) {
            sstr.str("");
            sstr << "Exception: " << e.what();
            syslog(LOG_DAEMON | LOG_EMERG, "%s\n", sstr.str().c_str());
            printf("%s\n", sstr.str().c_str());
            fprintf(stderr, "%s\n", sstr.str().c_str());
            L_(fatal) << "Exception: " << e.what();
            exit(3);
        }
        catch (...) {
            L_(fatal) << "UNKNOWN Exception raised!!!";
            sql.RollbackTsx();
            //throw std::string("UNKNOWN Exception raised!!!");
            exit(99);
        }

        if (loopCounter % 5000 == 0) {
            boost::timer::cpu_times elapsed = timer.elapsed();
            L_(debug)
                    << " marker (date=" << marker.getDateStr() << " / packet=" << marker.getPacketId() << ");"
                    << " cpu ("
                    << "time=" << (elapsed.user + elapsed.system) / 1e9 << " sec / "
                    << " wall-clock=" << elapsed.wall / 1e9 << " sec)";
            loopCounter = 0;
            timer.start();
        }
    } // while (g_doLoop)

    return g_mainRv;
} // main()
