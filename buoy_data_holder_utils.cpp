#include <list>
#include <map>
#include <string>
#include <sstream>
#include <iostream>

#include <boost/shared_ptr.hpp>
#include <boost/variant.hpp>

#include "buoy_data_holder_utils.hpp"
#include "buoy_datatypes.hpp"
#include "awac/nortek_awac.hpp"

namespace mbp {
namespace buoy {


TableRow::TableRow(
        const mbp::buoy::table_e& a_table,
        const mbp::buoy::row_spt& a_row_sp)
:
    m_table(a_table),
    m_row_sp(a_row_sp)
{
    return;
} // TableRow::TableRow()


TableRow::~TableRow()
{
    return;
} // TableRow::~TableRow()


mbp::buoy::row_spt TableRow::getRow(void) const
{
    return m_row_sp;
} // TableRow::getRow()


mbp::buoy::table_e TableRow::getTable(void) const
{
    return m_table;
} // TableRow::getTable()




const char *DataHolder::SEQ_VAR = "@ID";


DataHolder::DataHolder()
:
    m_isRawSql(false),
    m_isVar(false),
    m_isNull(false),
    m_varId(0),
    m_rawSql("")
{
    return;
} // DataHolder::DataHolder()


DataHolder::DataHolder(
        uint32_t a_varId)
:
    m_isRawSql(false),
    m_isVar(true),
    m_isNull(false),
    m_varId(a_varId),
    m_rawSql("")
{
    return;
} // DataHolder::DataHolder()


DataHolder::DataHolder(
        const std::string a_rawSql)
:
    m_isRawSql(true),
    m_isVar(false),
    m_isNull(false),
    m_varId(0),
    m_rawSql(a_rawSql)
{
    return;
} // DataHolder::DataHolder()


std::string DataHolder::getVar(void)
{
    if (! m_isVar) {
        throw std::string("ERROR: Invalid DataHolder usage!");
    }
    std::ostringstream sstr("");
    sstr << std::string("@ID") << m_varId;
    
    return sstr.str();
} // DataHolder::getVar()


std::string DataHolder::getRawSql(void)
{
    return m_rawSql;
} // DataHolder::getRawSql()


bool DataHolder::isRawSql(void)
{
    return m_isRawSql;
} // DataHolder::isRawSql()


bool DataHolder::isVar(void)
{
    return m_isVar;
} // DataHolder::isVar()


void DataHolder::setNull(void)
{
    m_isNull = true;
    return;
} // DataHolder::setNull()


bool DataHolder::isNull(void)
{
    return m_isNull;
} // DataHolder::isNull()


}; // namespace buoy
}; // namespace mbp
