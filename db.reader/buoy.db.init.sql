SET time_zone="+00:00";
DROP DATABASE IF EXISTS `buoy_markers`;
CREATE DATABASE `buoy_markers` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

DROP DATABASE IF EXISTS `buoy16`;
CREATE DATABASE `buoy16` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

GRANT USAGE ON *.* TO 'buoy'@'localhost' IDENTIFIED BY 'id489D1d';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE ON `buoy16`.* TO 'buoy'@'localhost';
GRANT SELECT, INSERT, DELETE ON `buoy_markers`.* TO 'buoy'@'localhost';



USE `buoy_markers`;

DROP TABLE IF EXISTS `raw_data_import`;
CREATE TABLE `raw_data_import` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `markerdate` date NOT NULL DEFAULT '2013-11-20' COMMENT 'Date of the last raw packet alterady inserted',
  `day_packet_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Last inserted day packet ID (a negative value means last pkt of the day)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Points to the last data already imported into the DB';

INSERT INTO `raw_data_import` VALUES (1,'2016-01-01',-1);




DROP TABLE IF EXISTS `raw_data_test_import`;
CREATE TABLE `raw_data_test_import` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `markerdate` date NOT NULL DEFAULT '2013-11-20' COMMENT 'Date of the last raw packet alterady inserted',
  `day_packet_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Last inserted day packet ID (a negative value means last pkt of the day)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Points to the last data already imported into the DB';
INSERT INTO `raw_data_test_import` VALUES (1047242,'2015-01-25',76664);




USE `buoy16`;


-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: buoy16
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu7-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `air`
--

DROP TABLE IF EXISTS `air`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `air` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `humidity` float NOT NULL COMMENT 'Fixed point, 0 decimal places, %',
  `temperature` float NOT NULL COMMENT 'Fixed point, 1 decimal place, degrees Celsius',
  `error_code` smallint(5) unsigned NOT NULL COMMENT 'Data quality',
  PRIMARY KEY (`time`),
  KEY `fk_air_profile` (`packet_id`),
  CONSTRAINT `fk_air_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `air_co2`
--

DROP TABLE IF EXISTS `air_co2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `air_co2` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `co2_filtered` float NOT NULL COMMENT 'Fixed point, 1 decimal place [ppm]',
  `co2_raw` float NOT NULL COMMENT 'Fixed point, 1 decimal place [ppm]',
  `temperature` float NOT NULL COMMENT 'Fixed point, 2 decimal places [degrees Celsius]',
  `instrument_code` smallint(5) unsigned NOT NULL,
  `error_code` smallint(5) unsigned NOT NULL COMMENT 'Data quality',
  PRIMARY KEY (`time`),
  KEY `fk_air_co2_profile` (`packet_id`),
  CONSTRAINT `fk_air_co2_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alarms`
--

DROP TABLE IF EXISTS `alarms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alarms` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `msec` smallint(5) unsigned NOT NULL,
  `packet_id` int(10) unsigned NOT NULL,
  `alarm_code` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`time`,`msec`,`alarm_code`),
  KEY `fk_alarms_profile` (`packet_id`),
  CONSTRAINT `fk_alarms_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_config`
--

DROP TABLE IF EXISTS `awac_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_config` (
  `aconfig_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `packet_id` int(10) unsigned NOT NULL,
  `dt_start_profile30` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'start time of an awac 30 min profile',
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'datum in cas od katerega dalje velja',
  `ack_time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'datum in cas zajema',
  `instr_type` smallint(5) unsigned NOT NULL COMMENT 'vrsta instrumenta',
  `serial_no` int(10) unsigned NOT NULL COMMENT 'vrsta instrumenta',
  `n_beams` smallint(5) unsigned NOT NULL COMMENT 'stevilo zarkov',
  `FFT_size` smallint(5) unsigned NOT NULL COMMENT 'stevilo posnetkov',
  `blank_dist` float NOT NULL COMMENT 'blanking distance (m)',
  `cell_size` float NOT NULL COMMENT 'one cell size (m)',
  `avg_interval` smallint(5) unsigned NOT NULL COMMENT 'average time in s',
  `meas_load` float NOT NULL COMMENT '???',
  `coord_syst` tinyint(3) unsigned NOT NULL COMMENT '???',
  `frequency` smallint(5) unsigned NOT NULL COMMENT '???',
  `power` smallint(5) unsigned NOT NULL COMMENT '???',
  `samp_freq` smallint(5) unsigned NOT NULL COMMENT '???',
  `nST_samp` smallint(5) unsigned NOT NULL COMMENT '???',
  `min_press` float NOT NULL COMMENT '???',
  `max_press` float NOT NULL COMMENT '???',
  `AST_size` smallint(5) unsigned NOT NULL COMMENT '???',
  `n_cells` smallint(5) unsigned NOT NULL COMMENT 'number of cells',
  PRIMARY KEY (`aconfig_id`),
  KEY `TIME` (`time`),
  KEY `fk_awac_config_profile` (`packet_id`),
  CONSTRAINT `fk_awac_config_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='konfiguracija AWAC-a (chk if it is ok)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_configdata`
--

DROP TABLE IF EXISTS `awac_configdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_configdata` (
  `aconfig_id` int(10) unsigned NOT NULL,
  `beam` smallint(5) unsigned NOT NULL COMMENT 'stevilo',
  `noise` tinyint(4) NOT NULL COMMENT '???',
  `noise_correlation` float DEFAULT NULL COMMENT '???',
  PRIMARY KEY (`aconfig_id`,`beam`),
  KEY `fk_aconfigdata_aconfig` (`aconfig_id`),
  CONSTRAINT `fk_aconfigdata_aconfig` FOREIGN KEY (`aconfig_id`) REFERENCES `awac_config` (`aconfig_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='AWAC dodatni podatki o meritvah';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_currents`
--

DROP TABLE IF EXISTS `awac_currents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_currents` (
  `acurrents_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'awac_currents ID',
  `packet_id` int(10) unsigned NOT NULL COMMENT 'packet id',
  `dt_start_profile30` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'start time of an awac 30 min profile',
  `cell_no` tinyint(3) unsigned NOT NULL COMMENT 'cell number ',
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `ack_time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `current_E` float NOT NULL COMMENT 'current speed (East component) - m/s',
  `current_N` float NOT NULL COMMENT 'current speed (North component) - m/s',
  `current_W` float NOT NULL COMMENT 'current speed (Up component) - m/s',
  `amplitude_E` tinyint(3) unsigned NOT NULL COMMENT 'jakost odziva 1. curka',
  `amplitude_N` tinyint(3) unsigned NOT NULL COMMENT 'jakost odziva 2. curka',
  `amplitude_W` tinyint(3) unsigned NOT NULL COMMENT 'jakost odziva 3. curka',
  `error_code` smallint(5) unsigned NOT NULL COMMENT 'koda napake',
  PRIMARY KEY (`acurrents_id`),
  KEY `fk_acurrents_profile` (`packet_id`),
  CONSTRAINT `awac_currents_ibfk_1` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='AWAC izmerjeni podatki o morskih tokovih';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_sensors`
--

DROP TABLE IF EXISTS `awac_sensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_sensors` (
  `asensors_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'awac_sensors ID',
  `packet_id` int(10) unsigned NOT NULL COMMENT 'packet id',
  `dt_start_profile30` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'start time of an awac 30 min profile',
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT '??? datum in cas meritve',
  `ack_time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT '??? datum in cas zajema',
  `heading` float NOT NULL COMMENT 'smer - v stopinjah',
  `pitch` float NOT NULL COMMENT 'naklon - v stopinjah',
  `roll` float NOT NULL COMMENT 'naklon - v stopinjah',
  `temperature` float NOT NULL COMMENT 'temperatura - v stopinjah Celzija',
  `pressure` float NOT NULL COMMENT 'tlak - v barih',
  `sound_speed` float NOT NULL COMMENT 'hitrost zvoka - v m/s',
  `voltage` float NOT NULL COMMENT 'napetost napajanja - v voltih',
  `error_code` smallint(5) unsigned NOT NULL COMMENT 'koda napake',
  PRIMARY KEY (`asensors_id`),
  KEY `TIME` (`time`),
  KEY `fk_asensors_profile` (`packet_id`),
  CONSTRAINT `awac_sensors_ibfk_1` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='Tabela vsebuje headerje posameznih profilov ADP-ja';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_wave_stspectrum_data`
--

DROP TABLE IF EXISTS `awac_wave_stspectrum_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_wave_stspectrum_data` (
  `aws_hdr_id` int(10) unsigned NOT NULL COMMENT 'awac_wavespectrum_header ID',
  `index` smallint(5) unsigned NOT NULL COMMENT 'stevilo',
  `ST_energy` float NOT NULL COMMENT '???',
  PRIMARY KEY (`aws_hdr_id`,`index`),
  CONSTRAINT `awac_wave_stspectrum_data_ibfk_1` FOREIGN KEY (`aws_hdr_id`) REFERENCES `awac_wavespectrum_header` (`aws_hdr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='AWAC podatki o ST spektrih';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_wavebands_data`
--

DROP TABLE IF EXISTS `awac_wavebands_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_wavebands_data` (
  `awb_hdr_id` int(10) unsigned NOT NULL COMMENT 'awac_wavebands_header ID',
  `band` smallint(5) unsigned NOT NULL COMMENT 'stevilo',
  `low_freq` float NOT NULL COMMENT '???',
  `hi_freq` float NOT NULL COMMENT '???',
  `mean_wave_height` float NOT NULL COMMENT '???',
  `mean_period` float NOT NULL COMMENT 'razprsenost valov pri vrsni periodi v stopinjah',
  `peak_period` float NOT NULL COMMENT 'srednja smer valov v stopinjah',
  `direction` float NOT NULL COMMENT 'smerni indeks',
  `mean_direction` float NOT NULL COMMENT '??? visina valov (iz spektra) v m',
  `spreading` float NOT NULL COMMENT 'ST znacilna visina valov (33 %)',
  `mean_spreading` float NOT NULL COMMENT 'ST znacilna perioda valov (33%)',
  `error_code` int(10) unsigned NOT NULL COMMENT 'koda napake',
  PRIMARY KEY (`awb_hdr_id`,`band`),
  CONSTRAINT `awac_wavebands_data_ibfk_1` FOREIGN KEY (`awb_hdr_id`) REFERENCES `awac_wavebands_header` (`awb_hdr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='AWAC podatki o spektralnih pasovih';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_wavebands_header`
--

DROP TABLE IF EXISTS `awac_wavebands_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_wavebands_header` (
  `awb_hdr_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'awac_wavebands_header ID',
  `packet_id` int(10) unsigned NOT NULL COMMENT 'packet id',
  `dt_start_profile30` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'start time of an awac 30 min profile',
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'UTC date and time when data were captured in IO unit',
  `acq_time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'data acqusition time (in device)',
  `valid` smallint(5) unsigned NOT NULL COMMENT '??? which data columns are present / valid',
  `n_bands` smallint(5) unsigned NOT NULL COMMENT 'number of spectral bands',
  `spectrum_type` smallint(5) unsigned NOT NULL COMMENT 'spectrum type',
  PRIMARY KEY (`awb_hdr_id`),
  KEY `TIME` (`time`),
  KEY `fk_awb_hdr_profile` (`packet_id`),
  CONSTRAINT `awac_wavebands_header_ibfk_1` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='AWAC glava sprektralnih pasov';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_waves`
--

DROP TABLE IF EXISTS `awac_waves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_waves` (
  `acurrents_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'awac_waves ID',
  `packet_id` int(10) unsigned NOT NULL COMMENT 'packet id',
  `dt_start_profile30` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'start time of an awac 30 min profile',
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'datum in cas meritve',
  `acq_time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'datum in cas zajema meritve',
  `valid` smallint(5) unsigned NOT NULL COMMENT 'kateri podatki so veljavni',
  `mean_period` float NOT NULL COMMENT 'srednja vrednost periode valov',
  `peak_period` float NOT NULL COMMENT 'vrsna vrednost periode v s',
  `direc_peak_period` float NOT NULL COMMENT 'smer valov pri vrsni periodi v stopinjah',
  `spread_peak_period` float NOT NULL COMMENT 'razprsenost valov pri vrsni periodi v stopinjah',
  `mean_direction` float NOT NULL COMMENT 'srednja smer valov v stopinjah',
  `UI` float NOT NULL COMMENT 'smerni indeks',
  `wave_height` float NOT NULL COMMENT 'visina valov (iz spektra) v m',
  `wave_height_33` float NOT NULL COMMENT 'ST znacilna visina valov (33 %)',
  `wave_period_33` float NOT NULL COMMENT 'ST znacilna perioda valov (33%)',
  `wave_height_10` float NOT NULL COMMENT 'ST visina valov (zgornjih 10 %)',
  `wave_period_10` float NOT NULL COMMENT 'ST perioda valov (zgornjih 10%)',
  `wave_height_max` float NOT NULL COMMENT 'maksimalna visina valov',
  `wave_period_max` float NOT NULL COMMENT 'maksimalna perioda valov',
  `mzcp` float NOT NULL COMMENT 'srednja vrednost periode prehajanja cez 0',
  `mean_pressure` float NOT NULL COMMENT 'srednja vrednost tlaka med pinganjem',
  `mean_distance` float NOT NULL COMMENT 'srednja globina (razdalja do povrsja)',
  `mean_current_speed` float NOT NULL COMMENT 'srednja hitrost tokov',
  `mean_current_direction` float NOT NULL COMMENT 'srednja smer tokov',
  `bad_detect` smallint(5) unsigned NOT NULL COMMENT 'stevilo slabih zaznav',
  `error_code` int(10) unsigned NOT NULL COMMENT 'koda napake',
  PRIMARY KEY (`acurrents_id`),
  KEY `TIME` (`time`),
  KEY `fk_awaves_profile` (`packet_id`),
  CONSTRAINT `awac_waves_ibfk_1` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='AWAC izmerjeni podatki o morskih valovih';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_wavespectrum_data`
--

DROP TABLE IF EXISTS `awac_wavespectrum_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_wavespectrum_data` (
  `aws_hdr_id` int(10) unsigned NOT NULL COMMENT 'awac_wavespectrum_header ID',
  `index` smallint(5) unsigned NOT NULL,
  `dir_spectrum` float NOT NULL COMMENT 'razprsenost valov pri vrsni periodi v stopinjah',
  `spread_dir_spectrum` float NOT NULL COMMENT 'srednja smer valov v stopinjah',
  `A1` float DEFAULT NULL COMMENT 'smerni indeks',
  `B1` float DEFAULT NULL COMMENT 'visina valov (iz spektra) v m',
  `A2` float DEFAULT NULL COMMENT 'ST znacilna visina valov (33 %)',
  `B2` float DEFAULT NULL COMMENT 'ST znacilna perioda valov (33%)',
  PRIMARY KEY (`aws_hdr_id`,`index`),
  CONSTRAINT `awac_wavespectrum_data_ibfk_1` FOREIGN KEY (`aws_hdr_id`) REFERENCES `awac_wavespectrum_header` (`aws_hdr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='AWAC podatki o spektrih';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `awac_wavespectrum_header`
--

DROP TABLE IF EXISTS `awac_wavespectrum_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awac_wavespectrum_header` (
  `aws_hdr_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'awac_wavespectrum_header ID',
  `packet_id` int(10) unsigned NOT NULL COMMENT 'packet id',
  `dt_start_profile30` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'start time of an awac 30 min profile',
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'datum in cas meritve',
  `acq_time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'datum in cas zajema meritve',
  `valid` smallint(5) unsigned NOT NULL COMMENT 'kateri podatki so veljavni',
  `n_values` smallint(5) unsigned NOT NULL COMMENT 'stevilo',
  `low_freq` float NOT NULL COMMENT '???',
  `hi_freq` float NOT NULL COMMENT '???',
  `n_values_ST` smallint(5) unsigned NOT NULL COMMENT 'stevilo',
  `low_freq_ST` float NOT NULL COMMENT '???',
  `hi_freq_ST` float NOT NULL COMMENT '???',
  `ST_energy_mult` int(11) NOT NULL COMMENT '??? stevilo',
  PRIMARY KEY (`aws_hdr_id`),
  KEY `TIME` (`time`),
  KEY `fk_awavespecth_profile` (`packet_id`),
  CONSTRAINT `awac_wavespectrum_header_ibfk_1` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='AWAC glava sprektra';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bat_voltage`
--

DROP TABLE IF EXISTS `bat_voltage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bat_voltage` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `min_voltage` float NOT NULL,
  `max_voltage` float NOT NULL,
  `avg_voltage` float NOT NULL,
  PRIMARY KEY (`time`),
  KEY `fk_bat_voltage_profile` (`packet_id`),
  CONSTRAINT `fk_bat_voltage_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `batteries`
--

DROP TABLE IF EXISTS `batteries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batteries` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `channel` tinyint(3) unsigned NOT NULL,
  `min_current` float NOT NULL,
  `max_current` float NOT NULL,
  `avg_current` float NOT NULL,
  `accumulated_charge` double NOT NULL,
  PRIMARY KEY (`time`,`channel`),
  KEY `fk_batteries_profile` (`packet_id`),
  CONSTRAINT `fk_batteries_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `buoy_interior`
--

DROP TABLE IF EXISTS `buoy_interior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buoy_interior` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `temperature` float(4,1) NOT NULL COMMENT 'Temperature in 0.1 degrees',
  `humidity` float(4,1) NOT NULL COMMENT 'Relative humidity in %',
  PRIMARY KEY (`time`),
  KEY `fk_buoy_interior_profile` (`packet_id`),
  CONSTRAINT `fk_buoy_interior_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Buoy interior sensor SHT21';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `co2`
--

DROP TABLE IF EXISTS `co2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `co2` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `value` float NOT NULL,
  PRIMARY KEY (`time`),
  KEY `fk_co2_profile` (`packet_id`),
  CONSTRAINT `fk_co2_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `compass`
--

DROP TABLE IF EXISTS `compass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compass` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `msec` smallint(5) unsigned NOT NULL,
  `packet_id` int(10) unsigned NOT NULL,
  `accx` float NOT NULL COMMENT 'Float, acceleration on X axis, m/s^2',
  `accy` float NOT NULL COMMENT 'Float, acceleration on Y axis, m/s^2',
  `accz` float NOT NULL COMMENT 'Float, acceleration on Z axis, m/s^2',
  `gyrx` float NOT NULL COMMENT 'Float, angular velocity on X axis, rad/s',
  `gyry` float NOT NULL COMMENT 'Float, angular velocity on Y axis, rad/s',
  `gyrz` float NOT NULL COMMENT 'Float, angular velocity on Z axis, rad/s',
  `magx` float NOT NULL COMMENT 'Float, strength of magnetic field on X axis, \nunits normalized to earth field strength\n',
  `magy` float NOT NULL COMMENT 'Float, strength of magnetic field on Y axis, \nunits normalized to earth field strength\n',
  `magz` float NOT NULL COMMENT 'Float, strength of magnetic field on Z axis, \nunits normalized to earth field strength\n',
  `roll` float NOT NULL COMMENT 'Float, angle of rotation, degrees\n',
  `pitch` float NOT NULL COMMENT 'Float, angle of rotation, degrees',
  `heading` float NOT NULL COMMENT 'Float, angle of rotation from N, degrees',
  `error_code` smallint(5) unsigned NOT NULL COMMENT 'Data quality',
  PRIMARY KEY (`time`,`msec`),
  KEY `fk_compass_profile` (`packet_id`),
  CONSTRAINT `fk_compass_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comstat_main_io`
--

DROP TABLE IF EXISTS `comstat_main_io`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comstat_main_io` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `board` smallint(5) unsigned NOT NULL,
  `present` tinyint(1) NOT NULL,
  `initialized` tinyint(1) NOT NULL,
  `packet_errors` int(10) unsigned NOT NULL,
  `multiple_errors` int(10) unsigned NOT NULL,
  `soft_errors` int(10) unsigned NOT NULL,
  PRIMARY KEY (`time`,`board`),
  KEY `fk_comstat_main_io_profile` (`packet_id`),
  CONSTRAINT `fk_comstat_main_io_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='Communication status main board <--> IO boards';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fuel_cell_status`
--

DROP TABLE IF EXISTS `fuel_cell_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fuel_cell_status` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `msec` smallint(5) unsigned NOT NULL,
  `packet_id` int(10) unsigned NOT NULL,
  `switched_on` tinyint(1) NOT NULL,
  `charging` tinyint(1) NOT NULL,
  `temperature_overrange` tinyint(1) NOT NULL,
  `fuel_low` tinyint(1) NOT NULL,
  `no_fuel` tinyint(1) NOT NULL,
  PRIMARY KEY (`time`,`msec`),
  KEY `fk_fuel_cell_status_profile` (`packet_id`),
  CONSTRAINT `fk_fuel_cell_status_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gps_position`
--

DROP TABLE IF EXISTS `gps_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gps_position` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `longitude` double NOT NULL COMMENT '[degrees] - <val_i> + (<val_f>/10^7)',
  `latitude` double NOT NULL COMMENT '[degrees] - <val_i> + (<val_f>/10^7)',
  `altitude` float NOT NULL COMMENT 'mnm',
  `satelites` tinyint(3) unsigned NOT NULL COMMENT 'Active satelites during reception',
  PRIMARY KEY (`time`),
  KEY `fk_gps_position_profile` (`packet_id`),
  CONSTRAINT `fk_gps_position_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gps_status`
--

DROP TABLE IF EXISTS `gps_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gps_status` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL COMMENT 'GPS status',
  `antenna` tinyint(3) unsigned NOT NULL COMMENT 'Antenna status',
  `battery` tinyint(3) unsigned NOT NULL COMMENT 'Power status',
  `almanac` tinyint(3) unsigned NOT NULL COMMENT 'RTC, almanac status',
  PRIMARY KEY (`time`),
  KEY `fk_gps_status_profile` (`packet_id`),
  CONSTRAINT `fk_gps_status_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gps_sync`
--

DROP TABLE IF EXISTS `gps_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gps_sync` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `time_difference` int(11) NOT NULL COMMENT 'GPS status',
  PRIMARY KEY (`time`),
  KEY `fk_gp_sync_profile` (`packet_id`),
  CONSTRAINT `fk_gp_sync_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `io_alarms`
--

DROP TABLE IF EXISTS `io_alarms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `io_alarms` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `msec` smallint(5) unsigned NOT NULL,
  `packet_id` int(10) unsigned NOT NULL,
  `board` tinyint(3) unsigned NOT NULL,
  `device` tinyint(3) unsigned NOT NULL,
  `error_code` smallint(5) unsigned NOT NULL DEFAULT '0',
  `device_mode` tinyint(3) unsigned NOT NULL,
  `device_state` tinyint(3) unsigned NOT NULL,
  `device_name` varchar(16) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`time`,`msec`,`board`,`device`),
  KEY `fk_io_alarms_profile` (`packet_id`),
  CONSTRAINT `fk_io_alarms_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `io_power`
--

DROP TABLE IF EXISTS `io_power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `io_power` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `board` tinyint(3) unsigned NOT NULL,
  `port` tinyint(3) unsigned NOT NULL,
  `min_current` float NOT NULL,
  `max_current` float NOT NULL,
  `avg_current` float NOT NULL,
  PRIMARY KEY (`time`,`board`,`port`),
  KEY `fk_io_power_profile` (`packet_id`),
  CONSTRAINT `fk_io_power_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='Power consumption of IO boards';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_board`
--

DROP TABLE IF EXISTS `main_board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_board` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `current` float NOT NULL COMMENT '(2500.0/4096.0) * (<val_current> - <val_voltage> + 14.0) / 1000.0',
  `temperature` float NOT NULL COMMENT '[degree C] - (<val>/10.0)',
  `humidity` float NOT NULL COMMENT '[%] - (<val>/4096.0)*100.0',
  PRIMARY KEY (`time`),
  KEY `fk_main_board_profile` (`packet_id`),
  CONSTRAINT `fk_main_board_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mppt`
--

DROP TABLE IF EXISTS `mppt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mppt` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `Uin_min` float NOT NULL COMMENT 'Fixed point, 1 decimal place [V]',
  `Uin_max` float NOT NULL COMMENT 'Fixed point, 1 decimal place [V]',
  `Uin_avg` float NOT NULL COMMENT 'Fixed point, 2 decimal places [V]',
  `Iin_min` float NOT NULL COMMENT 'Fixed point, 1 decimal place [A]',
  `Iin_max` float NOT NULL COMMENT 'Fixed point, 1 decimal place [A]',
  `Iin_avg` float NOT NULL COMMENT 'Fixed point, 2 decimal places [A]',
  `Ubat_min` float NOT NULL COMMENT 'Fixed point, 1 decimal place [V]',
  `Ubat_max` float NOT NULL COMMENT 'Fixed point, 1 decimal place [V]',
  `Ubat_avg` float NOT NULL COMMENT 'Fixed point, 2 decimal places [V]',
  `Iout_min` float NOT NULL COMMENT 'Fixed point, 1 decimal place [A]',
  `Iout_max` float NOT NULL COMMENT 'Fixed point, 1 decimal place [A]',
  `Iout_avg` float NOT NULL COMMENT 'Fixed point, 2 decimal place [A]',
  `output_charge` double NOT NULL COMMENT 'Fixed point, 4 decimal places [As]',
  `remaining_eq_time` smallint(5) unsigned NOT NULL COMMENT '[minute]',
  `days_last_full_charge` tinyint(3) unsigned NOT NULL COMMENT '[day]',
  `days_last_equalize` tinyint(3) unsigned NOT NULL COMMENT '[day]',
  `total_charge` float NOT NULL COMMENT '[Ah]',
  PRIMARY KEY (`time`),
  KEY `fk_mppt_profile` (`packet_id`),
  CONSTRAINT `fk_mppt_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mppt_status`
--

DROP TABLE IF EXISTS `mppt_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mppt_status` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `msec` smallint(5) unsigned NOT NULL,
  `packet_id` int(10) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `flags` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`time`,`msec`),
  KEY `fk_mppt_status_profile` (`packet_id`),
  CONSTRAINT `fk_mppt_status_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oxygen`
--

DROP TABLE IF EXISTS `oxygen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oxygen` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `device_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'O2 (ID - 0: o3835 (2m under buoy with cleaner), 1: o4835 (sea floor), 2: o4835(2m under buoy w/o cleaner)',
  `concentration` float NOT NULL COMMENT 'Fixed point, 5 decimal places [mol/m^3]',
  `saturation` float NOT NULL COMMENT 'Fixed point, 2 decimal places',
  `temperature` float NOT NULL COMMENT 'Fixed point, 2 decimal places',
  `error_code` smallint(5) unsigned NOT NULL COMMENT 'Data quality',
  PRIMARY KEY (`time`,`device_id`),
  KEY `fk_oxygen_profile` (`packet_id`),
  CONSTRAINT `fk_oxygen_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `par`
--

DROP TABLE IF EXISTS `par`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `par` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `msec` smallint(5) unsigned NOT NULL,
  `packet_id` int(10) unsigned NOT NULL,
  `value_raw` int(11) NOT NULL,
  `error_code` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`time`,`msec`),
  KEY `fk_par_profile` (`packet_id`),
  CONSTRAINT `fk_par_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Photosynthetically Active Radiation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `power_sections`
--

DROP TABLE IF EXISTS `power_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `power_sections` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `channel` tinyint(3) unsigned NOT NULL,
  `min_current` float NOT NULL,
  `max_current` float NOT NULL,
  `avg_current` float NOT NULL,
  `accumulated_charge` double NOT NULL,
  PRIMARY KEY (`time`,`channel`),
  KEY `fk_power_sections_profile` (`packet_id`),
  CONSTRAINT `fk_power_sections_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `packet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestart` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'Time tag for data packet (start of interval time)',
  `dataset_date` date NOT NULL DEFAULT '1970-01-01' COMMENT 'Current packet date (to be used in conjunction with day_packet_id)',
  `dataset_packet_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Day packet ID (to be used in conjunction with dataset_marker)',
  `srv_end` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01' COMMENT 'Time when packet was successfully entered into database',
  PRIMARY KEY (`packet_id`),
  UNIQUE KEY `dt_pkt_id` (`dataset_date`,`dataset_packet_id`),
  KEY `datestart` (`datestart`),
  KEY `dateset_packet_id` (`dataset_packet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sea_co2`
--

DROP TABLE IF EXISTS `sea_co2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sea_co2` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `instr_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `temperature` float NOT NULL,
  `d434` float NOT NULL,
  `d620` float NOT NULL,
  `d740` float NOT NULL,
  `i434` float NOT NULL,
  `i620` float NOT NULL,
  `i740` float NOT NULL,
  `stddev434` float NOT NULL,
  `stddev620` float NOT NULL,
  `stddev740` float NOT NULL,
  `k434` float NOT NULL,
  `k620` float NOT NULL,
  `bat_voltage1` float NOT NULL,
  `bat_voltage2` float NOT NULL,
  `instrument_code` smallint(5) unsigned NOT NULL,
  `error_code` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`time`),
  KEY `fk_sea_co2_profile` (`packet_id`),
  CONSTRAINT `fk_sea_co2_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='Sea water co2 sensor.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sea_water`
--

DROP TABLE IF EXISTS `sea_water`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sea_water` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `temperature` float NOT NULL COMMENT 'Fixed point, 4 decimal places, degrees Celsius',
  `conductivity` float NOT NULL COMMENT 'Fixed point, 5 decimal places, S/m',
  `salinity` float NOT NULL COMMENT 'Fixed point, 4 decimal places, PSU',
  `chlorophile` smallint(5) unsigned NOT NULL COMMENT 'Chlorophile concentration, raw ADC measurement (will be post processed in mean* tables',
  `acquisition_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Time reported from Seabird',
  `error_code` smallint(5) unsigned NOT NULL COMMENT 'Data quality',
  PRIMARY KEY (`time`),
  KEY `fk_sea_water_profile` (`packet_id`),
  CONSTRAINT `fk_sea_water_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_sync_io`
--

DROP TABLE IF EXISTS `time_sync_io`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_sync_io` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `packet_id` int(10) unsigned NOT NULL,
  `board` tinyint(3) unsigned NOT NULL,
  `time_difference` int(11) NOT NULL,
  `old_counter` int(10) unsigned NOT NULL,
  `new_counter` int(10) unsigned NOT NULL,
  PRIMARY KEY (`time`,`board`),
  KEY `fk_time_sync_io_profile` (`packet_id`),
  CONSTRAINT `fk_time_sync_io_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wind`
--

DROP TABLE IF EXISTS `wind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wind` (
  `time` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
  `msec` smallint(5) unsigned NOT NULL,
  `packet_id` int(10) unsigned NOT NULL,
  `U` float NOT NULL COMMENT 'Fixed point, 3 decimal places, m/s',
  `V` float NOT NULL COMMENT 'Fixed point, 3 decimal places, m/s',
  `W` float NOT NULL COMMENT 'Fixed point, 3 decimal places, m/s',
  `sound_speed` float NOT NULL COMMENT 'Fixed point, 2 decimal places, m/s',
  `sonic_temperature` float NOT NULL COMMENT 'Fixed point, 2 decimal places, degrees Celsius',
  `device_error_code` smallint(5) unsigned NOT NULL COMMENT 'Error code reported by WindMaster',
  `error_code` smallint(5) unsigned NOT NULL COMMENT 'Data quality',
  PRIMARY KEY (`time`,`msec`),
  KEY `fk_wind_profile` (`packet_id`),
  CONSTRAINT `fk_wind_profile` FOREIGN KEY (`packet_id`) REFERENCES `profile` (`packet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `writter_log`
--

DROP TABLE IF EXISTS `writter_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `writter_log` (
  `wlog_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `packet_id` int(10) unsigned NOT NULL,
  `log` text COLLATE utf8_slovenian_ci NOT NULL,
  `filename` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`wlog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci COMMENT='Stores writter log messages (e.g. time out of range, etc.).';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'buoy16'
--
/*!50003 DROP FUNCTION IF EXISTS `f_getBatteryCurrentAvg` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_getBatteryCurrentAvg`(
  i_time TIMESTAMP,
  i_channel tinyint UNSIGNED
) RETURNS float
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Returns buoy battery current data for the given current cycle'
BEGIN
  DECLARE _rv FLOAT;
  SELECT b.avg_current INTO _rv FROM batteries b WHERE channel=(i_channel+4) AND time=i_time;
  RETURN _rv;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `f_getBatteryVoltageAvg` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_getBatteryVoltageAvg`(
  i_time TIMESTAMP,
  i_channel tinyint UNSIGNED
) RETURNS float
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Returns buoy battery voltage data for the given current cycle'
BEGIN
  DECLARE _rv FLOAT;
  SELECT b.avg_current INTO _rv FROM batteries b WHERE channel=(i_channel) AND time=i_time;
  RETURN _rv;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `f_get_windDir` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_get_windDir`() RETURNS float
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Calculate current wind direction'
BEGIN
  DECLARE _rv FLOAT;
  SELECT (ATAN2(U, V) + (PI()/2)) INTO _rv FROM wind ORDER BY time DESC LIMIT 1;
  RETURN _rv;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `f_get_windSpd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_get_windSpd`() RETURNS float
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Calculate current wind speed'
BEGIN
  DECLARE _rv FLOAT;
  SELECT SQRT((U*U) + (V*V)) INTO _rv FROM wind ORDER BY time DESC LIMIT 1;
  RETURN _rv;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-11 21:29:51
