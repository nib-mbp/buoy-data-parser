#ifndef _BUOY_DATA_HOLDER_UTILS_HPP
#define _BUOY_DATA_HOLDER_UTILS_HPP

#include <list>
#include <map>
#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/variant.hpp>

#include "buoy_datatypes.hpp"
#include "awac/nortek_awac.hpp"

namespace mbp {
namespace buoy {


// -- Enums --
typedef enum {
    SQL_VAR_AWAC2_CURRENTS_HDR      = 1,
    SQL_VAR_AWAC2_WAVESPECTRUM_e    = 2,
    SQL_VAR_AWAC2_FOURIERCOEFF_e    = 3,
    SQL_VAR_AWAC_CONFIG_e           = 4,
    SQL_VAR_AWAC_WAVEBANDS_e        = 5,
    SQL_VAR_AWAC_WAVESPECTRUM_e     = 6
} sql_var_e;


class DataHolder;
typedef boost::variant<
    std::string,
    bool,
    double,
    uint16_t,
    uint32_t,
    int16_t,
    int32_t,
    DataHolder> row_types_t;

typedef std::list<std::string>                  field_names_lt;   // colum names list
typedef field_names_lt::iterator                field_names_it;
typedef std::map<table_e, field_names_lt>       tbls_mt;
typedef std::pair<table_e,field_names_lt>       tbls_pt;
typedef boost::shared_ptr<tbls_mt>              tbls_msp;

typedef std::list<row_types_t>                  row_fields_lt;
typedef row_fields_lt::iterator                 row_fields_it;
typedef boost::shared_ptr<row_fields_lt>        row_spt;

class TableRow;
typedef std::list<TableRow>                     rows_lt;
typedef rows_lt::iterator                       rows_it;
typedef boost::shared_ptr<rows_lt>              rows_spt;


class TableRow {
private:
    mbp::buoy::table_e  m_table;
    mbp::buoy::row_spt  m_row_sp;
public:
    TableRow(const mbp::buoy::table_e& a_table, const mbp::buoy::row_spt& a_row_sp);
    ~TableRow();

    mbp::buoy::row_spt getRow(void) const;
    mbp::buoy::table_e getTable(void) const;
}; // class TableRow


class DataHolder {
private:
    bool m_isRawSql;
    bool m_isVar;
    bool m_isNull;
    uint32_t m_varId;
    std::string m_rawSql;

public:
    const static char *SEQ_VAR;
    DataHolder();
    DataHolder(uint32_t a_varId);
    DataHolder(const std::string a_rawSql);
    std::string getVar(void);
    std::string getRawSql(void);
    bool isRawSql(void);
    bool isVar(void);
    void setNull(void);
    bool isNull(void);
}; // class DataHolder
} // namespace buoy

}; // namespace mbp

#endif // _BUOY_DATA_HOLDER_UTILS_HPP
