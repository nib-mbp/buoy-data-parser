#ifndef _BUOY_LOG_HPP
#define _BUOY_LOG_HPP

#include <boost/log/common.hpp>

#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/attributes/named_scope.hpp>

// -- Defines and Macros --
#define L_(_lvl) \
    BOOST_LOG_SEV(mbp::g_lg, mbp::_lvl)


// -- Samples --
// Boost logging tag definition:
//   BOOST_LOG_SCOPED_LOGGER_TAG(mbp::g_lg, "Tag", "To sem pac jaz");
//
// Boost function scope definition:
//   BOOST_LOG_FUNCTION()
//


namespace mbp {

// -- Enums and Typedefs --
typedef enum { //loglevel_e {
    trace,
    debug,
    info,
    warning,
    error,
    fatal
} loglevel_e;


// -- Generic functions --
template< typename CharT, typename TraitsT >
inline std::basic_ostream< CharT, TraitsT >& operator<< (std::basic_ostream< CharT, TraitsT >& strm, mbp::loglevel_e lvl) {
    static const char* const str[] = {
            "trace",
            "debug",
            "info",
            "warning",
            "error",
            "fatal"
    };
    if (static_cast< std::size_t >(lvl) < (sizeof(str) / sizeof(*str)))
        strm << str[lvl];
    else
        strm << static_cast< int >(lvl);
    return strm;
} // mbp::operator<<()



// -- Global variables --
extern boost::log::sources::severity_logger< mbp::loglevel_e > g_lg;
extern mbp::loglevel_e g_loglevel;


// -- Function declarations --
void mbp_LogInit(bool a_doExtraLogging, const std::string& a_filename="-");
void mbp_SetLogLevel(mbp::loglevel_e a_level);
bool mbp_IsEnabled(mbp::loglevel_e a_level);

}; // namespace mbp

#endif // _BUOY_LOG_HPP
