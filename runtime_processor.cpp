#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

#include <boost/shared_array.hpp>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/chrono/duration.hpp>


#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <boost/log/common.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/support/date_time.hpp>
#if (BOOST_VERSION == 105500)
    #include <boost/archive/shared_ptr_helper.hpp>
#elif (BOOST_VERSION > 105500)
    #include <boost/core/null_deleter.hpp>
#else
    #include <boost/log/utility/empty_deleter.hpp>
#endif

#include <boost/format/group.hpp>

#include <mysql++/mysql++.h>

#include "buoy_shared_array.hpp"
#include "buoy_utils.hpp"
#include "buoy_udp_connector.hpp"
#include "buoy_data.hpp"
#include "buoy_datatypes.hpp"
#include "buoy_log.hpp"
#include "runtime_data.hpp"

#include "runtime_processor.hpp"
#include "buoy_data_holder_utils.hpp"

#include "awac/nortek_awac.hpp"

#define DB_MAX_CONNECTION_ATTEMPTS      10

#define BUOY_INSTRUMENT_CMD_PASSWORD    "TestPassword"

#define SLEEP_TIME_ms   (2000)
#define BUF_SIZE_mb     (1)
#define BUF_LEN_b       (BUF_SIZE_mb * 1024 * 1024)

mbp::buoy::RuntimeProcessor::RuntimeProcessor()
{
    BOOST_LOG_FUNCTION();

    //
    // -- Initialize the data points for data queries --
    //

    // -- instruments data + door sensors + water sensors + cameras --
    int vectNum = 0;
    m_dataPoints_v[vectNum].clear();
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_CO2_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_WIND_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_HUMIDITY_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_COMPASS_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_SEA_WATER_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_OXYGEN4835_1_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_PAR_e, 0));

    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(0xff00 + mbp::buoy::EXT_DATA_AWAC2_e, Nortek::PD_CURRENTS_VELOCITY_PROFILE));

    // buoy door
    for (int idx=1; idx<=4; ++idx) {
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx));
    }
    // water sensors
    for (int idx=10; idx<=15; ++idx) {
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx));
    }

    // -- instruments statuses - see technical doc., table no.19 --
    vectNum = 1;
    m_dataPoints_v[vectNum].clear();
    // -----------------------------------------------------------
    // Request instrument statuses by calling IO_ALARMS_e endpoint
    //  - endpoint bases:
    //    - 500    -> connected (slo: priključen + ime)
    //    - 600    -> start in auto mode
    //    - 700    -> stop
    //    - 800    -> start in manual mode
    //  - instrument offsets
    //    - 2      -> wind
    //    - 3      -> humidity
    //    - 4      -> compass
    //    - 5      -> salinity
    //    - 6      -> AWAC
    //    - 7      -> oxygen 1
    //    - 8      -> PAR
    //    - 9      -> oxygen 2
    //    - 10     -> CO2
    //    - 11     -> oxygen 3
    // -----------------------------------------------------------
    for (int base=500; base<=800; base+=100) {
        // for (int offset=2; offset<=11; ++offset) {
        for (int offset=2; offset<=9; ++offset) {
            if (offset == 7) {
                // skip oxygen 1, as it isn't present any more
                continue;
            }
            m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(IO_ALARMS_e, (base+offset)));
        }
    }

    // -- buoy status - see technical doc., table no.19 --
    vectNum=2;
    m_dataPoints_v[vectNum].clear();

    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::AD_ENERGY_00_07_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::AD_ENERGY_08_15_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::BUOY_INTERIOR_SHT21_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::MAIN_BOARD_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 1));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 2));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 3));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 4));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 5));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::MPPT_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::MAIN_BOARD_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 0));

    // cameras - control ones
    for (int idx=50; idx<=59; ++idx) {
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx));
    }
    // camera - uw1
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 60));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 62));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 64));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 66));

// -- Signal light - As per 2016-02-28 --
//    it isn't connected to the buoy and so it was REMOVED from queries
/*
    for (int idx=20; idx<=21; ++idx) {
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx));
    }
*/
    // buoy indor CO2 concentration
    for (int idx=30; idx<=33; ++idx) {
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx));
    }
    // WiFI radios
    for (int idx=40; idx<=45; ++idx) {
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx));
    }
    // gps
    for (int idx=80; idx<=83; ++idx) {
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx));
    }
    // power section
    for (int idx=90; idx<=93; ++idx) {
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx));
    }

    // batteries (all 4 packs)
    for (int idx=2000; idx<=2030; idx+=10) {
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx+0));
        m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, idx+1));
    }
    // other batterie alarms
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 2050));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 2051));
    // misc
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 2100));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 2150));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 2200));


    if (mbp_IsEnabled(trace)) {
        for (int i = 0; i < 3; ++i) {
            L_(trace) << "m_dataPoints_v[" << i << "].size() = " << m_dataPoints_v[i].size() << std::endl;
        }
    }

/*
    // TODO / FIXME -> debugging only

    m_dataPoints_v[0].clear();
    //m_dataPoints_v[0].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::MAIN_BOARD_e, 0));
    vectNum = 0;
//    //m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_WIND_e, 0));
//    //m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 10));
//    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 11));
//    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 12));
//    //m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 13));
//    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 14));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::MAIN_BOARD_e, 0));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 1));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 2));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 3));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 4));
    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_ADC_e, 5));
    //m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(ALARM_DATA_e, 15));
//    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_HUMIDITY_e, 0));
//    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::DATA_COMPASS_e, 0));
//    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::BUOY_INTERIOR_SHT21_e, 0));
//    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::BUOY_INSTRUMENT_WIND_e, 0));
//    m_dataPoints_v[vectNum].push_back(mbp::buoy::runtime_hdr_pair_t(mbp::buoy::BUOY_INSTRUMENT_COMPASS_e, 0));
*/

    return;
} // mbp::buoy::RuntimeProcessor::RuntimeProcessor()



mbp::buoy::RuntimeProcessor::~RuntimeProcessor()
{
    BOOST_LOG_FUNCTION();
}




void mbp::buoy::RuntimeProcessor::run(const bool *a_doLoop_p)
{
    BOOST_LOG_FUNCTION();
    // -- initialize data points --
    //buoy_udp_pkt_t* udpPacket_p = NULL;

    mbp::BuoySharedArray<uint8_t> buoyArray_sp(new uint8_t [BUF_LEN_b], BUF_LEN_b, 0, 0);
    buoyArray_sp.ClearData();
    mbp::BuoyData buoyData(buoyArray_sp);

    mbp::BuoyUdpConnector buoyConnector(g_options.getServerIp(), g_options.getServerPort());
    RuntimeData runtimeData;
    try {
        buoyConnector.Init();
        runtimeData.Init();
    } catch (const std::invalid_argument &e) {
        std::ostringstream sstr("");
        sstr << "Got exception: " << e.what();
        L_(fatal) << sstr.str();
        exit(1);
    }


    int loopCounter = 0;
    std::vector<runtime_hdr_pair_t> dataPoints_v;
    int dbConnAttempt = 0;
    while (*a_doLoop_p == true) {
        // -- mysql --
        mysqlpp::Connection dbConn(mysqlpp::use_exceptions);
        dbConn.set_option(new mysqlpp::MultiStatementsOption(true));
        std::ostringstream sstrQuery;
        try {
            dbConn.connect(NULL, g_options.getDbHost().c_str(), g_options.getDbUser().c_str(), g_options.getDbPasswd().c_str());
            dbConnAttempt = 0;
        } catch (const mysqlpp::Exception &e) {
            std::ostringstream sstr;
            sstr.str("");
            sstr << "DB connection ERROR: " << e.what();
            L_(error) << sstr.str();
            if (dbConnAttempt >= DB_MAX_CONNECTION_ATTEMPTS) {
                L_(fatal) << "DB ERROR: Too many non succesful connection attempts! Exiting ...";
                exit(1);
            }
            boost::this_thread::sleep_for(boost::chrono::milliseconds(SLEEP_TIME_ms));
            continue;
        }
        mysqlpp::Query dbQuery(&dbConn, true);
        sstrQuery.str("");
        sstrQuery << "SET time_zone='+00:00'";
        try {
            dbQuery.exec(sstrQuery.str().c_str());
            dbQuery.reset();
        } catch (const mysqlpp::Exception &e) {
            std::ostringstream sstr("");
            sstr << "DB ERROR: Initialization failed (timezone setup failed) - " << e.what();
            L_(fatal) << sstr.str();
            exit(1);
        }

        try {
            for (size_t vectNum=0; vectNum < NUM_OF_RUNTIME_DP_VECTORS; ++vectNum) {
// std::cout << "-> vector #" << (vectNum+1) << ":" << std::endl;
                buoyArray_sp.setPosition(0);
                buoyArray_sp.setUsableDataLength(0);
                buoyConnector.QueryRuntime(buoyArray_sp, m_dataPoints_v[vectNum]);
                buoyData.ParseBuffer(mbp::PARSING_TYPE_RUNTIME_e, &runtimeData);
// std::cout << buoyArray_sp.getDebugDetails() << std::endl << "----" << std::endl << std::endl;
            }   // for ( ... vectNum < NUM_OF_RUNTIME_DP_VECTORS ... )
        }
        catch (const std::invalid_argument &e) {
            std::ostringstream sstr("");
            sstr << "Got exception: " << e.what();
            L_(fatal) << sstr.str();
            exit(1);
        }
        catch (const std::string &strE) {
            std::ostringstream sstr("");
            sstr << "Got exception: " << strE;
            L_(fatal) << sstr.str();
            exit(1);
        }
        catch (std::exception &e) {
            L_(fatal) << "Exception: " << e.what();
            exit(2);
        }
        catch (...) {
            L_(fatal) << "UNKNOWN Exception raised!!!";
//            sql.RollbackTsx();
            //throw std::string("UNKNOWN Exception raised!!!");
            exit(3);
        }



        try {
            std::string jsonData(runtimeData.getJson());
/*
            std::cout
                << std::endl << std::endl
                << "JSON ["
                << jsonData
                << "]"
                << std::endl << std::endl;
*/

            // -- Process JSON --
//            std::cout << "..." << std::endl;
            sstrQuery.str("");
            sstrQuery
                << "CALL scada_data.p_setScadaData('"
                << jsonData
                << "')";
            try {
                dbQuery.store(sstrQuery.str().c_str());
                dbQuery.reset();
            }
            catch (const mysqlpp::Exception &e) {
                std::ostringstream sstr("");
                sstr << "DB ERROR: Updating instruments data - " << e.what();
                L_(fatal) << sstr.str();
                exit(1);
            }

// -- Enable SCADA commands processing --
//boost::this_thread::sleep_for(boost::chrono::milliseconds(SLEEP_TIME_ms));
//++loopCounter;
//continue;

            // -- Check for next pending buoy command --
            try {
                sstrQuery.str("");
                sstrQuery
                    << "CALL scada_data.p_getScadaCmd(@instrument, @password, @cmd, @cmdDt, @retrNo, @retrUpdateDt, @execStatus, @statusUpdateDt); "
                    << "SELECT @instrument, @password, @cmd, @cmdDt, @retrNo, @retrUpdateDt, @execStatus, @statusUpdateDt;";
                mysqlpp::StoreQueryResult res = dbQuery.store(sstrQuery.str().c_str());
                res = dbQuery.store_next();
                if (res.size() == 1) {
                    mysqlpp::Row row = res.at(0);
                    const std::string instrument(row[0]);
                    const std::string password(row[1]);
                    int cmdReq = row[2];
                    const std::string cmdDt(row[3]);
                    int retrNo = row[4];
                    const std::string retrUpdateDt(row[5]);
                    int execStatus = row[6];
                    const std::string statusUpdateDt(row[7]);
                    dbQuery.reset();

                    // -- set the buoy command --
                    mbp::buoy::instrument_command_e cmd = BUOY_INSTRUMENT_CMD_NA_e;
                    switch (cmdReq) {
                        case 0:
                            cmd = mbp::buoy::BUOY_INSTRUMENT_CMD_STOP_e;
                            break;
                        case 1:
                            cmd = mbp::buoy::BUOY_INSTRUMENT_CMD_START_e;
                            break;
                        default:
                            // TODO -> throw an exception
                            break;
                    }
                    // if the buoy command processor already acknowledged the recipient of
                    // the command, change the cmd to 'GET_STATUS' one
                    if (execStatus == 1)
                        cmd = mbp::buoy::BUOY_INSTRUMENT_CMD_EXEC_STATUS_e;
                    else if (execStatus > 1)
                        // something is wrong with the command processor - just skip the command
                        cmd = mbp::buoy::BUOY_INSTRUMENT_CMD_SKIP_e;


L_(trace)
    << "sql-rv"
    " [inst=" << instrument
    << " / pass=" << password
    << " / cmd=" << cmd
    << " / cmd-dt=" << cmdDt
    << " / retr-no=" << retrNo
    << " / retr-update-dt=" << retrUpdateDt
    << " / status=" << execStatus
    << " / status-dt=" << statusUpdateDt
    << "]";

                    // -- Check the given password --
                    if (
                           (cmd != mbp::buoy::BUOY_INSTRUMENT_CMD_SKIP_e)
                        && (cmd != mbp::buoy::BUOY_INSTRUMENT_CMD_NA_e)
                       ) {
                        if (password.compare(BUOY_INSTRUMENT_CMD_PASSWORD) != 0) {
                            L_(error) << "WARN (Buoy CMD): Wrong password received - skipping command ...";
                        } else {
                            boost::this_thread::sleep_for(boost::chrono::milliseconds(300));
                            buoyArray_sp.setPosition(0);
                            buoyArray_sp.setUsableDataLength(0);
L_(trace) << buoyArray_sp.getDebugDetails() << std::endl << "----" << std::endl;
                            buoyConnector.RuntimeInstrumentControl(buoyArray_sp, instrument, cmd);

                            buoy_udp_pkt_t *reply_p = reinterpret_cast<buoy_udp_pkt_t *>(buoyArray_sp.get());
                            mbp::buoy::instrument_command_reply_t *cmdReply_p =
                                    reinterpret_cast<mbp::buoy::instrument_command_reply_t *>(reply_p->data);
                            mbp::buoy::instruments_index_e instrument_e =
                                    buoyConnector.getInstrument(cmdReply_p->cmd.instrument.board, cmdReply_p->cmd.instrument.port);
                            if (instrument_e != mbp::buoy::BUOY_INSTRUMENT_NA_e) {
                                switch (cmdReply_p->status) {
                                    case 0:
                                        // uspesen zakljucek izvajanja
                                        break;
                                    case 1:
                                        // sprejem ukaza
                                        break;
                                    case 2:
                                        // ukaz v izvajanju
                                        break;
                                    case 3:
                                        // izvajanje neuspesno
                                        break;
                                    default:
                                        // all other errors
                                        break;
                                } // switch (cmdReply_p->status)
                                sstrQuery.str("");
                                sstrQuery
                                    << "CALL `scada_data`.`p_updateScadaCmdStatus`('"
                                    << buoyConnector.getInstrument(instrument_e)
                                    << "', " << cmdReply_p->status << ")";
/*
std::cout  << "query: '" << sstrQuery.str() << "'" << std::endl;
*/
                                try {
                                    bool bRv = dbQuery.exec(sstrQuery.str().c_str());
                                    std::cout << "p_updateScadaCmdStatus() -> bRv = " << bRv << std::endl;
                                } catch (const mysqlpp::BadQuery &e) {
                                    std::ostringstream sstr("");
                                    sstr << "SCADA Cmd: Instrument update error (err.no=" << e.errnum() << ") - " << e.what();
                                    std::cout << sstr.str() << std::endl;
                                    exit(1);
                                }
                            }

L_(info)
    << "SCADA CMD reply"
    << " [udp.cmd = " << reply_p->hdr.command
    << " / udp.len = " << buoyArray_sp.getTotalLength()
    << " / pkt.full_len = " << (reply_p->hdr.data_len + BUOY_UDP_PKT_HEADER_SIZE)
    << " / pkt.data_only_len = " << reply_p->hdr.data_len
    << " / pkt.packet = " << (uint32_t) reply_p->hdr.packet
    << " / instrument = " << buoyConnector.getInstrument(buoyConnector.getInstrument(cmdReply_p->cmd.instrument.board, cmdReply_p->cmd.instrument.port)) << " (" << (uint32_t)cmdReply_p->cmd.instrument.board << ":" << (uint32_t)cmdReply_p->cmd.instrument.port << ")"
    << " / instrument cmd = " << (uint32_t)cmdReply_p->cmd.command
    << " / execution status = " << cmdReply_p->status
    << "]";

/*
                            udpPacket_p = reinterpret_cast<buoy_udp_pkt_t *>(buoyArray_sp.get());
                            std::cout
                                << "flag = " << udpPacket_p->hdr.flag
                                << " / udp.cmd = " << udpPacket_p->hdr.command
                                << " / len = " << udpPacket_p->hdr.data_len
                                << " / epoch = " << udpPacket_p->hdr.utc_secs
                                << " / usec = " << udpPacket_p->hdr.utc_usecs
                                << " / packet = " << udpPacket_p->hdr.packet
                                << " / crc = " << udpPacket_p->hdr.crc
                                << std::endl;
*/
                        }
                    }
                }
            }
            catch (const std::invalid_argument &e) {
                std::ostringstream sstr("");
                sstr << "ARG ERROR: Updating instruments data - invalid arg - " << e.what();
                L_(fatal) << sstr.str();
                exit(1);
            }
            catch (const mysqlpp::Exception &e) {
                std::ostringstream sstr("");
                sstr << "DB ERROR: Updating instruments data - " << e.what();
                L_(fatal) << sstr.str();
                exit(1);
            }
            catch (std::exception& e) {
                std::ostringstream sstr("");
                sstr << "General ERROR: Unknown DB exception";
                L_(fatal) << "Exception: " << sstr.str();
            }

            if ( (loopCounter % 5) == 0 ) {
//                std::cout << ". " << std::flush;
            }
            boost::this_thread::sleep_for(boost::chrono::milliseconds(SLEEP_TIME_ms));
            ++loopCounter;
        }
        catch (std::string& strE) {
            L_(fatal) << "Got exception: " << strE;
//            sql.RollbackTsx();
            // throw strE;
            exit(1);
        }
        catch (std::exception& e) {
            L_(fatal) << "Exception: " << e.what();
            exit(2);
        }
        catch (...) {
            L_(fatal) << "UNKNOWN Exception raised!!!";
//            sql.RollbackTsx();
            //throw std::string("UNKNOWN Exception raised!!!");
            exit(3);
        }
    } // while (doLoop)
    L_(warning) << "Runtime loop interrupted (doLoop=" << *a_doLoop_p << ")";

    return;
} // mbp::buoy::RuntimeProcessor::run()
