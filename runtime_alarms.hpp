#ifndef _RUNTIME_ALARMS_HPP
#define _RUNTIME_ALARMS_HPP

#include <boost/cstdint.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "runtime_datatypes.hpp"

#define OLD_INSTRUMENT_DATA_ALARM_TRIGGER_AGE   (60*60)   // seconds -> 1 hour

namespace mbp {
namespace buoy {
typedef enum {
    ALARM_STATUS_OK                         = 0,    // Status  OK
    ALARM_STATUS_OPERATION_NORMAL,                  // Normal operation  - NOTE: it isn't applicable for all data points
    ALARM_STATUS_OPERATION_SERVICE_MODE,            // Service operation - NOTE: it isn't applicable for all data points
    ALARM_STATUS_FAIL,                              // Status  ALARM

    IO_ALARM_INSTRUMENT_INITIALIZING,               // Instrument is being initialized
    IO_ALARM_INSTRUMENT_STOPPED,                    // Instrument is stopped

    ALARM_STATUS_OLD_DATA,                          // Instrument data is older than 

    ALARM_BAT_STATUS_IN_USE,                        // Special battery pack status -> "NAPAJA"
    ALARM_BAT_STATUS_FULL,                          // Special battery pack status -> "NAPOLNJEN"
    ALARM_BAT_STATUS_CHARGING,                      // Special battery pack status -> "SE POLNI"
    ALARM_BAT_STATUS_WARNING,                       // Special battery pack status -> "OPOZORILO"
                                                    // Additionally, the ALARM_STATUS_FAIL is used for battery pack status "ALARM"

    ALARM_STATUS_NA                                 // Status unknown  (initialization value)
} alarm_status_e;


// -- class definition --
class RuntimeAlarms {
private:
    boost::posix_time::ptime    m_pTime[_SENSOR__SENTINEL_e];
    mbp::buoy::alarm_status_e   m_status[_SENSOR__SENTINEL_e];
    uint16_t                    m_code[_SENSOR__SENTINEL_e];


public:
    RuntimeAlarms();
    ~RuntimeAlarms() {};
    void Init() throw(std::invalid_argument);


    // IoAlarms - instruments status:
    //  mode: 0 – ustavljen, 1 – deluje
    //  state:  0 – inicializacija še ni izvedena,
    //          1 – postopek inicializacije zaključen (to ne pomeni, da instrument dela in pošilja podatke, ampak samo da 
    //              se je zaključila rutina za inicializacijo
    //  code: tabela 17 v navodilih
    //
    // Return value:
    //  0 -> if no state update processed
    //  >0 -> instrument Idx for which the state was updated
    mbp::buoy::buoy_sensor_e setIoAlarm(
            const boost::posix_time::ptime& a_recPTime,
            uint16_t a_board,
            uint16_t a_device,
            uint16_t a_mode,
            uint16_t a_state,
            uint16_t a_code) throw(std::invalid_argument);
    void setAlarm(boost::posix_time::ptime a_recPTime, uint16_t a_code);

    mbp::buoy::alarm_status_e getSensorStatus(mbp::buoy::buoy_sensor_e sensorIdx) const { return m_status[sensorIdx]; };

    void getStatusJson(std::ostringstream& sstr) const;
}; // class RuntimeData

std::ostream& operator<<(std::ostream& a_os, const mbp::buoy::RuntimeAlarms& a_runtimeAlarms);


} // namespace buoy
} // namespace mbp

#endif // _RUNTIME_ALARMS_HPP
