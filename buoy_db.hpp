#ifndef _BUOY_DB_HPP
#define _BUOY_DB_HPP

#include <iostream>
#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/cstdint.hpp>
#include <boost/date_time/date.hpp>
#include <boost/date_time/gregorian_calendar.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

#include <mysql++/mysql++.h>
#include <mysql++/transaction.h>

#include "buoy_data.hpp"
#include "buoy_data_holder_utils.hpp"
#include "buoy_utils.hpp"

namespace mbp {

class DbDataMarker {
private:
    boost::gregorian::date m_date;
    int32_t m_dayPacketId;

    void incPacketId() { ++m_dayPacketId; };
    void incDate() { m_date += boost::gregorian::days(1); }; 

public:
    DbDataMarker(
            const boost::gregorian::date& a_date=boost::gregorian::date(),
            int32_t a_dayPacketId=1)
        :
            m_date(a_date),
            m_dayPacketId(a_dayPacketId)
    {
        return;
    };

    DbDataMarker(
            const DbDataMarker& a_other)
        :
            m_date(a_other.m_date),
            m_dayPacketId(a_other.m_dayPacketId)
    {
        return;
    }

    ~DbDataMarker() {};

    DbDataMarker& operator=(const DbDataMarker& a_right)
    {
        m_date = a_right.m_date;
        m_dayPacketId = a_right.m_dayPacketId;
        return *this;
    };

    int32_t getPacketId() const { return m_dayPacketId; };
    void setPacketId(int32_t a_dayPacketId) { m_dayPacketId = a_dayPacketId; };

    boost::gregorian::date getDate() const { return m_date; };
    std::string getDateStr() const { return boost::gregorian::to_iso_extended_string(m_date); };

    void incMarker()
    {
        if (m_dayPacketId < 0) {
            incDate();
            m_dayPacketId = 1;
        } else {
            incPacketId();
        }
        return;
    }
}; // class DbDataMarker

std::ostream& operator<<(std::ostream& os, const DbDataMarker& obj);

class BuoyDb {
private:
    const std::string m_user;
    const std::string m_passwd;
    const std::string m_dbPrefix;
    const std::string m_dbMarkerTable;
    const std::string m_host;
    const uint32_t m_maxTsxPerConnection;
    uint32_t m_packetId;
    bool m_isDataSet;
    bool m_isConnected;
    bool m_isTsxInitialized;
    boost::shared_ptr<mysqlpp::Connection> m_db_sp;
    boost::shared_ptr<mysqlpp::Transaction> m_tsx_sp;
    boost::shared_ptr<mysqlpp::Query> m_query_sp;
    mbp::buoy::tbls_msp m_tblFields;
    mbp::buoy::rows_spt m_rows;
    uint32_t m_numDuplicates;
    std::vector<std::string> m_logQueries_v;
    mbp::DbDataMarker m_datasetMarker;
    uint32_t m_numOfProcessedTsx;

    void _InsertProfileRow(void);
    const std::string _MkRowQuery(const mbp::buoy::TableRow& a_row) const;
    bool _IncMsec(const mbp::buoy::TableRow& a_row);
    void _AddLogEntry(const std::string& a_logMessage);
    void _AddLogEntry(const mbp::buoy::TableRow& a_row);

public:
    BuoyDb(
            const std::string& a_user,
            const std::string& a_passwd,
            const std::string& a_dbPrefix,
            const std::string& a_dbMarkerTable,
            const std::string& a_host="localhost");
    ~BuoyDb();

    void setData(
            const mbp::DbDataMarker& a_datasetMarker,
            const mbp::buoy::tbls_msp& a_tblFields,
            const mbp::buoy::rows_spt& a_rows);
    void clearData(void);
    DbDataMarker getPacketMarker(void) const;
    void setPacketMarker(const DbDataMarker& a_marker);

    void Connect(void) throw(mysqlpp::ConnectionFailed);
    void Disconnect(void);

    void CommitData(void);
    void DumpData(void);
    void CommitLogs(void);
    void StartTsx(void);
    void CommitTsx(bool a_doCommit=true);
    void RollbackTsx(void);

    void test(void);
}; // class BuoyDb

}; // namespace mbp
#endif // _BUOY_DB_HPP
