-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: buoy_data_4_web
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu7-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `runtime_data`
--

DROP TABLE IF EXISTS `runtime_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runtime_data` (
  `id` int(10) unsigned NOT NULL,
  `instruments_data` varchar(2048) COLLATE utf8_unicode_ci NOT NULL DEFAULT '{}',
  `sensors_data` varchar(2048) COLLATE utf8_unicode_ci NOT NULL DEFAULT '{}',
  PRIMARY KEY (`id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Buoy runtime data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'buoy_data_4_web'
--
/*!50003 DROP FUNCTION IF EXISTS `f_getRuntimeInstrumentsData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_getRuntimeInstrumentsData`() RETURNS varchar(2048) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
    COMMENT 'Retrieves instruments runtime data'
BEGIN
  DECLARE _rv VARCHAR(2048);

  SELECT `instruments_data` INTO _rv FROM runtime_data ORDER BY id DESC LIMIT 1;

  IF _rv IS NULL THEN
    SET _rv = "{}";
  END IF;

  RETURN _rv;
END ;;
DELIMITER ;

/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `f_getRuntimeSensorsData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_getRuntimeSensorsData`() RETURNS varchar(2048) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
    COMMENT 'Retrieves sensors runtime data'
BEGIN
  DECLARE _rv VARCHAR(2048);

  SELECT `sensors_data` INTO _rv FROM runtime_data ORDER BY id DESC LIMIT 1;

  IF _rv IS NULL THEN
    SET _rv = "{}";
  END IF;

  RETURN _rv;
END ;;
DELIMITER ;

/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_setRuntimeInstrumentsData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_setRuntimeInstrumentsData`(
    IN instrumentsJson VARCHAR(2048))
    MODIFIES SQL DATA
    COMMENT 'Updates instruments runtime data'
BEGIN
  DECLARE _num INT;

  SELECT COUNT(*) INTO @num FROM runtime_data;

  SET _num = @num;

  IF _num > 1 THEN
    DELETE FROM runtime_data;
    SET _num = 0;
  END IF;
  IF _num = 0 THEN
    INSERT INTO `runtime_data` (`instruments_data`) VALUES (instrumentsJson);
  ELSE
    UPDATE `runtime_data` SET `instruments_data`=instrumentsJson;
  END IF;
END ;;
DELIMITER ;

/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `p_setRuntimeSensorsData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_setRuntimeSensorsData`(
    IN sensorsJson VARCHAR(2048))
    MODIFIES SQL DATA
    COMMENT 'Updates sensors runtime data'
BEGIN
  DECLARE _num INT;

  SELECT COUNT(*) INTO @num FROM runtime_data;

  SET _num = @num;

  IF _num > 1 THEN
    DELETE FROM runtime_data;
    SET _num = 0;
  END IF;
  IF _num = 0 THEN
    INSERT INTO `runtime_data` (`sensors_data`) VALUES (sensorsJson);
  ELSE
    UPDATE `runtime_data` SET `sensors_data`=sensorsJson;
  END IF;
END ;;
DELIMITER ;

/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-09 21:56:15
