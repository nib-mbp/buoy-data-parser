#ifndef _BUOY_AWAC_DATA_HPP
#define _BUOY_AWAC_DATA_HPP

#include <list>
#include <map>
#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/variant.hpp>

#include "buoy_data_holder_utils.hpp"
#include "buoy_datatypes.hpp"
#include "runtime_data.hpp"

#include "awac/nortek_awac.hpp"
#include "awac/awac_data.hpp"

namespace mbp {

#define ADD_AWAC_DATA_OR_NULL(_list, _awacVar, _varName, _value) \
        do { \
            /* L_(debug) << "Checking var '" << _varName << "' for NULL value:"; */ \
            if (_awacVar->IsVarSet(_varName)) { \
                /* L_(debug) << " - val: " << (_value); */ \
                _list->push_back(_value); \
            } else { \
                /* L_(debug) << " - val: NULL"; */ \
                mbp::buoy::DataHolder dh; \
                dh.setNull(); \
                _list->push_back(dh); \
            } \
        } while (0)

#define ADD_AWAC_DATA_OR_DEFAULT(_list, _awacVar, _varName, _value, _default) \
        do { \
            /* L_(debug) << "Checking var '" << _varName << "' for NULL value:"; */ \
            if ((_awacVar)->IsVarSet(_varName)) { \
                /* L_(debug) << " - val: " << (_value); */ \
                (_list)->push_back(_value); \
            } else { \
                /* L_(debug) << " - val (default): " << _default; */ \
                (_list)->push_back(_default); \
            } \
        } while (0)

class BuoyAwacData {
private:
    mbp::buoy::rows_spt         m_rows;

public:
    BuoyAwacData(mbp::buoy::rows_spt a_rows_sp);
    ~BuoyAwacData();
    void Reset(mbp::buoy::rows_spt a_rows_sp);

    void ProcessDataRecord(mbp::buoy::awac_data_header_t *a_awac_p, size_t a_currDataStrucSize, const mbp::parsing_type_e& a_parsingType, mbp::RuntimeData *a_runtimeData_p) throw (std::runtime_error);
}; // class BuoyData

}; // namespace mbp

#endif // _BUOY_AWAC_DATA_HPP
