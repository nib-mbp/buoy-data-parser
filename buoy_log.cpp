#include <iostream>
#include <string>
#include <fstream>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <boost/log/common.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/support/date_time.hpp>

#if BOOST_VERSION == 105500
    #include <boost/serialization/shared_ptr.hpp>
    #include <boost/utility/empty_deleter.hpp>
#elif (BOOST_VERSION > 105500)
    #include <boost/core/null_deleter.hpp>
#else
    #include <boost/log/utility/empty_deleter.hpp>
#endif

#include "buoy_log.hpp"


namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;
namespace attrs = boost::log::attributes;
namespace expr = boost::log::expressions;


// -- Enums and Typedefs --
typedef sinks::synchronous_sink< sinks::text_ostream_backend > text_sink;


// -- Global variables --
boost::log::sources::severity_logger< mbp::loglevel_e > mbp::g_lg;
mbp::loglevel_e mbp::g_loglevel;



void mbp::mbp_LogInit(
        bool a_doExtraLogging,
        const std::string& a_filename) {
    boost::shared_ptr< text_sink > sink = boost::make_shared< text_sink >();

    if (a_filename.compare("-") == 0) {
#if BOOST_VERSION == 105500
        boost::shared_ptr< std::ostream > stream(&std::clog, boost::empty_deleter());
#elif (BOOST_VERSION > 105500)
        boost::shared_ptr< std::ostream > stream(&std::clog, boost::null_deleter());
#else
        boost::shared_ptr< std::ostream > stream(&std::clog, logging::empty_deleter());
#endif
        sink->locked_backend()->add_stream(stream);
    } else {
        sink->locked_backend()->auto_flush(true);
        sink->locked_backend()->add_stream(boost::make_shared< std::ofstream >(a_filename.c_str(), std::ios::out | std::ios::app));
    }

    boost::shared_ptr< logging::core > core = logging::core::get();
    core->add_sink(sink);

    if (a_doExtraLogging) {
        sink->set_formatter(expr::stream
            << "[" << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%d.%m.%Y %H:%M:%S")
            << "] [" << expr::attr< mbp::loglevel_e >("Severity")
            << "] "
            << expr::if_(expr::has_attr("Tag"))
               [
                    expr::stream
                    << "[" << expr::attr< std::string >("Tag") << "] "
               ]
            << "<" << expr::format_named_scope("Scope", keywords::format = "%n", keywords::iteration = expr::reverse) << ">: "
            << expr::smessage);
    } else {
        sink->set_formatter(expr::stream
            << expr::smessage);
    }

    //logging::core::get()->add_global_attribute("auto_flush")
    logging::core::get()->add_global_attribute("TimeStamp", attrs::utc_clock());

    attrs::named_scope Scope;
    logging::core::get()->add_thread_attribute("Scope", Scope);

    BOOST_LOG_FUNCTION();

    return;
} // mbp::mbp_LogInit()


void mbp::mbp_SetLogLevel(mbp::loglevel_e a_level) {
    mbp::g_loglevel = a_level;

    logging::core::get()->set_filter(expr::attr< mbp::loglevel_e >("Severity") >= mbp::g_loglevel);

    return;
} // mbp::mbp_SetLogLevel()


bool mbp::mbp_IsEnabled(
        mbp::loglevel_e a_level)
{
    if (g_loglevel <= a_level) {
        return true;
    }
    return false;
} // mbp::mbp_IsEnabled()
