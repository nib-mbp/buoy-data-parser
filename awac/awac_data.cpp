#include <iostream>
#include <string>
#include <map>
#include <list>
#include <sstream>
#include <iomanip>

#include "awac_data.hpp"
#include "nortek_awac.hpp"

using namespace std;
using namespace Nortek;
using namespace mbp::buoy;

void AwacData::AlignPtr(void)
{
    size_t ptrDiff = m_data_p - m_orgData_p;
    if ((ptrDiff % 2) != 0) {
        ++m_data_p;
    }
    return;
} // AwacData::AlignPtr()


AwacData::AwacData(
        uint8_t* a_data_p)
:
    m_data_p(a_data_p),
    m_orgData_p(a_data_p),
    m_setVariables()
{
    // TODO: check sync char!!!
    return;
} // AwacData::AwacData()


void AwacData::SetVar(
        const std::string &m_varName)
{
    m_setVariables[m_varName] = true;
    return;
} // AwacData::SetVar()


bool AwacData::IsVarSet(
        const std::string& a_varName) const
{
    std::map<std::string, bool>::const_iterator iter =
        m_setVariables.find(a_varName);
    if (iter != m_setVariables.end()) {
        return true;
    } else {
        return false;
    }
} // AwacData::IsVarSet()



std::list<std::string> AwacData::GetVarsList(void)
{
    std::list<std::string> list;
    list.clear();
    for (std::map<std::string, bool>::const_iterator iter=m_setVariables.begin();
         iter != m_setVariables.end();
         ++iter)
    {
        list.push_back(std::string(iter->first));
    }
    return list;
} // AwacData::GetVarsList()






/*
 * Used by AWAC
 */
uint8_t mbp::buoy::_BcdToBin(
        uint8_t val)
{
    return((val >> 4) * 10 + (val & 15));
} // mbp::buoy::_BcdToBin()

std::string mbp::buoy::PdClock2String(
        const Nortek::PdClock& clock)
{
    std::ostringstream sstr("");
    sstr << std::setfill('0') <<
        (uint32_t(mbp::buoy::_BcdToBin(clock.cYear))+2000) << "-" << std::setw(2) << uint32_t(mbp::buoy::_BcdToBin(clock.cMonth)) << "-" << std::setw(2) << uint32_t(mbp::buoy::_BcdToBin(clock.cDay)) <<
        " " <<
        std::setw(2) << uint32_t(mbp::buoy::_BcdToBin(clock.cHour)) << ":" << std::setw(2) << uint32_t(mbp::buoy::_BcdToBin(clock.cMinute)) << ":" << std::setw(2) << uint32_t(mbp::buoy::_BcdToBin(clock.cSecond));
    return sstr.str();
} // AwacData::PdClock2String()


std::string mbp::buoy::PdClock2ProfileDtStartString(
        const Nortek::PdClock& clock)
{
    std::ostringstream sstr("");
    sstr << std::setfill('0')
        << (uint32_t(mbp::buoy::_BcdToBin(clock.cYear))+2000) << "-"
        << std::setw(2) << uint32_t(mbp::buoy::_BcdToBin(clock.cMonth)) << "-"
        << std::setw(2) << uint32_t(mbp::buoy::_BcdToBin(clock.cDay)) <<
        " " <<
        std::setw(2) << uint32_t(mbp::buoy::_BcdToBin(clock.cHour)) << ":";
    if (uint32_t(mbp::buoy::_BcdToBin(clock.cMinute)) < 30) {
        sstr << "00:00";
    } else {
        sstr << "30:00";
    }
    return sstr.str();
} // mbp::buoy::PdClock2ProfileDtStartString()

