BYTE bcdtobin(BYTE val)
{
  return((val >> 4) * 10 + (val & 15));
}


void expand_sensor_data(unsigned char *srcData, PdSensorData *data)
{
	unsigned char *srcPtr = srcData;
	data->header.cSync = *srcPtr;
	++srcPtr;
	data->header.cID = *srcPtr;
	++srcPtr;
	data->header.hSize = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	data->header.hDataFormat = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	for ( int b = 0; b < 16; b++)
	{
		if ( (data->header.hDataFormat & 1 << b) == 0)
		{
			continue;
		}
		switch ( b )
		{
			case PD_DF_SENSOR_COUNTER :
				data->hCounter = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_CLOCK	:
				data->clock = *(PdClock *)srcPtr;
				srcPtr += sizeof(PdClock);
				break;
			case PD_DF_SENSOR_MSECONDS :
				data->hMilliSeconds = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_ERROR	:
				data->hError = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_STATUS :
				data->hStatus = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_TEMPERATURE :
				data->hTemperature = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_PRESSURE :
				data->hPressure = *(int *)srcPtr;
				srcPtr += sizeof(int);
				break;
			case PD_DF_SENSOR_HEADING :
				data->hHeading = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_PITCH	:
				data->hPitch = *(short *)srcPtr;
				srcPtr += sizeof(short);
				break;
			case PD_DF_SENSOR_ROLL :
				data->hRoll = *(short *)srcPtr;
				srcPtr += sizeof(short);
				break;
			case PD_DF_SENSOR_BATTERY :
				data->hBattery = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_SOUNDSPEED :
				data->hSoundSpeed = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_ANALOG1 :
				data->hAnaIn1 = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_ANALOG2 :
				data->hAnaIn2 = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_SENSOR_EXT_FORMAT :
				data->hExtDataFormat = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
		}
	}
	data->AcqTime = *(unsigned int *)srcPtr;
}

void expand_profile_data(unsigned char *srcData, PdProfileData *data)
{
	unsigned char *srcPtr = srcData;
	data->header.cSync = *srcPtr;
	++srcPtr;
	data->header.cID = *srcPtr;
	++srcPtr;
	data->header.hSize = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	data->header.hDataFormat = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	for ( int b = 0; b < 16; b++)
	{
		if ( (data->header.hDataFormat & 1 << b) == 0)
		{
			continue;
		}
		switch ( b )
		{
			case PD_DF_PROFILE_COUNTER :
				data->hCounter = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_PROFILE_CLOCK :
				data->clock = *(PdClock *)srcPtr;
				srcPtr += sizeof(PdClock);
				break;
			case PD_DF_PROFILE_MSECONDS :
				data->hMilliSeconds = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_PROFILE_ERROR :
				data->hError = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_PROFILE_STATUS :
				data->hStatus = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_PROFILE_NBEAMS_NCELLS :
				data->nBeams = *(unsigned char *)srcPtr;
				srcPtr += sizeof(unsigned char);
				data->nCells = *(unsigned char *)srcPtr;
				srcPtr += sizeof(unsigned char);
				break;
			case PD_DF_PROFILE_BEAM_SELECTION :
				data->cBeams = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_PROFILE_CELL_SELECTION :
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_NBEAMS_NCELLS) )
				{
					return; // PD_DATA_CONVERSION_ERROR_MISSING_DATA;
				
				}
				memset(data->cCells,0,sizeof(data->cCells));
				if (data->nCells == 0) 
					break;
				memcpy(data->cCells,srcPtr,(data->nCells-1)/8+1);
				srcPtr += (data->nCells-1)/8+1;
				break;
			case PD_DF_PROFILE_VELOCITY :
				{
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_NBEAMS_NCELLS) )
				{
					return;// PD_DATA_CONVERSION_ERROR_MISSING_DATA;
				}

				bool doBeamCheck;
				bool doCellCheck;
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_BEAM_SELECTION) )
				{
					doBeamCheck = true;
				} 
				else 
				{
					doBeamCheck = false;
				}
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_CELL_SELECTION) )
				{
					doCellCheck = true;
				}
				else 
				{
					doCellCheck = false;
				}

				for ( int b = 0; b < data->nBeams; b++)
				{
					if ( doBeamCheck )
					{
						if ( ( data->cBeams & (1 << b)) == 0)
						{
							continue;
						}
					}
					for ( int c = 0; c < data->nCells; c++)
					{
						if ( doCellCheck )
						{
							if ( ( data->cCells[c/8] & (1 << (c % 8))) == 0)
							{
								continue;
							}
						}
						data->hVel[ b ][ c ] = *(unsigned short *)srcPtr;
						srcPtr += sizeof(unsigned short);
					}
				}
				}
				break;
			case PD_DF_PROFILE_AMPLITUDE :
				{
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_NBEAMS_NCELLS) )
				{
					return; // PD_DATA_CONVERSION_ERROR_MISSING_DATA;
				}

				bool doBeamCheck;
				bool doCellCheck;
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_BEAM_SELECTION) )
				{
					doBeamCheck = true;
				} 
				else 
				{
					doBeamCheck = false;
				}
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_CELL_SELECTION) )
				{
					doCellCheck = true;
				} 
				else
				{
					doCellCheck = false;
				}

				for ( int b = 0; b < data->nBeams; b++)
				{
					if ( doBeamCheck )
					{
						if ( ( data->cBeams & (1 << b)) == 0)
						{
							continue;
						}
					}
					for ( int c = 0; c < data->nCells; c++)
					{
						if ( doCellCheck )
						{
							if ( ( data->cCells[c/8] & (1 << (c % 8))) == 0)
							{
								continue;
							}
						}
						data->cAmp[ b ][ c ] = *(unsigned char *)srcPtr;
						srcPtr += sizeof(unsigned char);
					}
				}
				}
				break;
			case PD_DF_PROFILE_CORRELATION :
				{
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_NBEAMS_NCELLS) )
				{
					return; // PD_DATA_CONVERSION_ERROR_MISSING_DATA;
				}

				bool doBeamCheck;
				bool doCellCheck;
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_BEAM_SELECTION) )
				{
					doBeamCheck = true;
				}
				else
				{
					doBeamCheck = false;
				}
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_PROFILE_CELL_SELECTION) )
				{
					doCellCheck = true;
				} 
				else 
				{
					doCellCheck = false;
				}

				for ( int b = 0; b < data->nBeams; b++)
				{
					if ( doBeamCheck )
					{
						if ( ( data->cBeams & (1 << b)) == 0)
						{
							continue;
						}
					}
					for ( int c = 0; c < data->nCells; c++)
					{
						if ( doCellCheck )
						{
							if ( ( data->cCells[c/8] & (1 << c)) == 0)
							{
								continue;
							}
						}
						data->cCorr[ b ][ c ] = *(unsigned char *)srcPtr;
						srcPtr += sizeof(unsigned char);
					}
				}
				}
				break;
			case PD_DF_PROFILE_EXT_FORMAT :
				data->hExtDataFormat = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
		}
		if ( (srcPtr-srcData) % 2 != 0)
		{
			srcPtr += sizeof(unsigned char);
		}
	}
	data->AcqTime = *(unsigned int *)srcPtr;
}

void expand_header(unsigned char *srcData, PdHeaderData *data)
{
	unsigned char *srcPtr = srcData;
	data->header.cSync = *srcPtr;
	++srcPtr;
	data->header.cID = *srcPtr;
	++srcPtr;
	data->header.hSize = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	data->header.hDataFormat = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	for ( int b = 0; b < 32; b++)
	{
		if ( b < 16 )
		{
			if ( (data->header.hDataFormat & ( 1 << b )) == 0)
			{
				continue;
			}
		} 
		else 
		{
			if ( (data->hExtDataFormat & ( 1 << (b & 0xf)) ) == 0)
			{
				continue;
			}
		}
		switch ( b )
		{
			case PD_DF_HEADER_COUNTER :
				data->hCounter = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_CLOCK	:
				data->clock = *(PdClock *)srcPtr;
				srcPtr += sizeof(PdClock);
				break;
			case PD_DF_HEADER_MSECONDS :
				data->hMilliSeconds = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_DATA_TYPE :
				data->hDataType = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_INSTRUMENT_TYPE :
				data->hInstrumentType = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_INSTRUMENT_SERIAL :
				data->hInstrumentSerial = *(unsigned int *)srcPtr;
				srcPtr += sizeof(unsigned int);
				break;
			case PD_DF_HEADER_N_BEAMS :
				data->nBeams = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_N_RECORDS :
				data->nRecords = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_BEAM_MATRIX :
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_HEADER_N_BEAMS) )
				{
					for ( int b1 = 0; b1 < data->nBeams; b1++)
					{
						for ( int b2 = 0; b2 < data->nBeams; b2++)
						{
							data->hBeamMatrix[ b1 ][ b2 ] = *(short *)srcPtr;
							srcPtr += sizeof(short);
						}
					}
				}
				break;
			case PD_DF_HEADER_BLANKING :
				data->hBlankingDistance = *(int *)srcPtr;
				srcPtr += sizeof(int);
				break;
			case PD_DF_HEADER_CELLSIZE :
				data->hCellSize = *(int *)srcPtr;
				srcPtr += sizeof(int);
				break;
			case PD_DF_HEADER_AVERAGE_INTERVAL :
				data->hAverageInterval = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_MEASUREMENT_LOAD :
				data->hMeasurementLoad = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_COORDINATE_SYSTEM :
				data->hCoordinateSystem = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_FREQUENCY :
				data->hFrequency = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_EXT_FORMAT :
				data->hExtDataFormat = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_POWERLEVEL :
				data->hPowerLevel = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_NOISE	:
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_HEADER_N_BEAMS) )
				{
					for ( int b = 0; b < data->nBeams; b++)
					{
						data->cNoise[ b ] = *(unsigned char *)srcPtr;
						srcPtr += sizeof(unsigned char);
					}
				}
				break;
			case PD_DF_HEADER_NOISE_CORRELATION :
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_HEADER_N_BEAMS) )
				{
					for ( int b = 0; b < data->nBeams; b++)
					{
						data->cNoiseCorrelation[ b ] = *(unsigned char *)srcPtr;
						srcPtr += sizeof(unsigned char);
					}
				}
				break;
			case PD_DF_HEADER_SAMPLE_FREQ :
				data->hSampleFrequency = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_DATA_COLL_METHOD :
				data->hDataCollectionMethod = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_MIN_PRESSURE :
				data->hMinPressure = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_MAX_PRESSURE :
				data->hMaxPressure = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_AST_WND_SIZE :
				data->hASTWindowsSize = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_HEADER_EXT_EXT_FORMAT :
				break;
		}
		if ( (srcPtr-srcData) % 2 != 0)
		{
			srcPtr += sizeof(unsigned char);
		}
	}
	data->AcqTime = *(unsigned int *)srcPtr;
}

void expand_wave_data(unsigned char *srcData, PdWaveData *data)
{
	unsigned char *srcPtr = srcData;
	data->header.cSync = *srcPtr;
	++srcPtr;
	data->header.cID = *srcPtr;
	++srcPtr;
	data->header.hSize = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	data->header.hDataFormat = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	for ( int b = 0; b < 32; b++)
	{
		if ( b < 16 )
		{
			if ( (data->header.hDataFormat & ( 1 << b )) == 0)
			{
				continue;
			}
		} 
		else 
		{
			if ( (data->hExtDataFormat & ( 1 << (b & 0xf)) ) == 0)
			{
				continue;
			}
		}
		switch ( b )
		{
			case PD_DF_WAVE_COUNTER	:
				data->hCounter = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_CLOCK :
				data->clock = *(PdClock *)srcPtr;
				srcPtr += sizeof(PdClock);
				break;
			case PD_DF_WAVE_MSECONDS :
				data->hMilliSeconds = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_SPECTRUM_TYPE :
				data->hSpectrumType = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_TM02 :
				data->hTm02= *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_TP :
				data->hTp = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_DIR_TP :
				data->hDirTp = *(unsigned short *)srcPtr;
				srcPtr += sizeof(short);
				break;
			case PD_DF_WAVE_SPR_TP :
				data->hSprTp = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_DIR_MEAN :
				data->hDirMean = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_UI :
				data->hUI = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_HM0 :
				data->hHm0 = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_H3 :
				data->hH3 = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_T3 :
				data->hT3 = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_H10 :
				data->hH10 = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_T10 :
				data->hT10 = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_EXT_FORMAT :
				data->hExtDataFormat = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_HMAX :
				data->hHmax = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_TMAX :
				data->hTmax = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_TZ :
				data->hTz = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_PRESSURE_MEAN :
				data->hPressureMean = *(int *)srcPtr;
				srcPtr += sizeof(int);
				break;
			case PD_DF_WAVE_AST_MEAN :
				data->hSTMean = *(int *)srcPtr;
				srcPtr += sizeof(int);
				break;
			case PD_DF_WAVE_CURRENT_SPEED_MEAN :
				data->hCurSpeedMean = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_CURRENT_DIRECTION_MEAN :
	 			data->hCurDirMean = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_N_BAD_DETECTS :
				data->hNumBadDet = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_ERROR :
				data->hError = *(unsigned int *)srcPtr;
				srcPtr += sizeof(unsigned int);
				break;
			case PD_DF_WAVE_EXT_EXT_FORMAT :
				break;
			default	:
				break;
		}
		if ( (srcPtr-srcData) % 2 != 0)
		{
			srcPtr += sizeof(unsigned char);
		}
	}
	data->AcqTime = *(unsigned int *)srcPtr;
}

void expand_waveband_data(unsigned char *srcData, PdWaveBandsData *data)
{
	unsigned int band;
	
	unsigned char *srcPtr = srcData;
	data->header.cSync = *srcPtr;
	++srcPtr;
	data->header.cID = *srcPtr;
	++srcPtr;
	data->header.hSize = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	data->header.hDataFormat = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	for ( int b = 0; b < 32; b++)
	{
		if ( b < 16 )
		{
			if ( (data->header.hDataFormat & ( 1 << b )) == 0)
			{
				continue;
			}
		}
		else
		{
			if ( (data->hExtDataFormat & ( 1 << (b & 0xf)) ) == 0)
			{
				continue;
			}
		}
		switch ( b )
		{
			case PD_DF_WAVE_BAND_COUNTER :
				data->hCounter = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_BAND_CLOCK :
				data->clock = *(PdClock *)srcPtr;
				srcPtr += sizeof(PdClock);
				break;
			case PD_DF_WAVE_BAND_MSECONDS :
				data->hMilliSeconds = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_BAND_N_BANDS:
				data->nBands= *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_BAND_SPECTRUM_TYPE :
				data->hSpectrumType= *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_BAND_FREQUENCY_INTERVAL	:
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_BAND_N_BANDS))
					break;
				for (band = 0; band < data->nBands; band++)
				{
					data->hLowFrequency[ band ] = *(unsigned short *)srcPtr;
					srcPtr += sizeof(unsigned short);
				}
				for (band = 0; band < data->nBands; band++)
				{
					data->hHighFrequency[ band ] = *(unsigned short *)srcPtr;
					srcPtr += sizeof(unsigned short);
				}
				break;
			case PD_DF_WAVE_BAND_HM0 :
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_BAND_N_BANDS))
					break;
				for (band = 0; band < data->nBands; band++)
				{
					data->hHm0[ band ] = *(unsigned short *)srcPtr;
					srcPtr += sizeof(unsigned short);
				}
				break;
			case PD_DF_WAVE_BAND_TM02 :
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_BAND_N_BANDS))
					break;
				for (band = 0; band < data->nBands; band++)
				{
					data->hTm02[ band ] = *(unsigned short *)srcPtr;
					srcPtr += sizeof(unsigned short);
				}
				break;
			case PD_DF_WAVE_BAND_TP	:
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_BAND_N_BANDS))
					break;
				for (band = 0; band < data->nBands; band++)
				{
					data->hTp[ band ] = *(unsigned short *)srcPtr;
					srcPtr += sizeof(unsigned short);
				}
				break;
			case PD_DF_WAVE_BAND_DIR_TP :
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_BAND_N_BANDS))
					break;
				for (band = 0; band < data->nBands; band++)
				{
					data->hDirTp[ band ] = *(unsigned short *)srcPtr;
					srcPtr += sizeof(unsigned short);
				}
				break;
			case PD_DF_WAVE_BAND_DIR_MEAN :
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_BAND_N_BANDS))
					break;
				for (band = 0; band < data->nBands; band++)
				{
					data->hDirMean[ band ] = *(unsigned short *)srcPtr;
					srcPtr += sizeof(unsigned short);
				}
				break;
			case PD_DF_WAVE_BAND_SPREAD_TP :
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_BAND_N_BANDS))
					break;
				for (band = 0; band < data->nBands; band++)
				{
					data->hSprTp[ band ] = *(unsigned short *)srcPtr;
					srcPtr += sizeof(unsigned short);
				}
				break;
			case PD_DF_WAVE_BAND_SPREAD_MEAN :
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_BAND_N_BANDS))
					break;
				for (band = 0; band < data->nBands; band++)
				{
					data->hSprMean[ band ] = *(unsigned short *)srcPtr;
					srcPtr += sizeof(unsigned short);
				}
				break;
			case PD_DF_WAVE_BAND_ERROR :
				if ( ! IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_BAND_N_BANDS))
					break;
				for (band = 0; band < data->nBands; band++)
				{
					data->hError[ band ] = *(unsigned int *)srcPtr;
					srcPtr += sizeof(unsigned int);
				}
				break;
			default:
				break;
		}
		if ( (srcPtr-srcData) % 2 != 0)
		{
			srcPtr += sizeof(unsigned char);
		}
	}
}

void expand_wave_spectrum(unsigned char *srcData, PdWaveSpectrumData *data)
{
	unsigned char *srcPtr = srcData;
	data->header.cSync = *srcPtr;
	++srcPtr;
	data->header.cID = *srcPtr;
	++srcPtr;
	data->header.hSize = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	data->header.hDataFormat = *(unsigned short*)srcPtr;
	srcPtr += sizeof(unsigned short);
	for (int b = 0; b < 32; b++)
	{
		if ( b < 16 )
		{
			if ( (data->header.hDataFormat & ( 1 << b )) == 0)
			{
				continue;
			}
		} 
		else 
		{
			if ( (data->hExtDataFormat & ( 1 << (b & 0xf)) ) == 0)
			{
				continue;
			}
		}
		switch ( b )
		{
			case PD_DF_WAVE_SPECTRUM_COUNTER :
				data->hCounter = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_SPECTRUM_CLOCK :
				data->clock = *(PdClock *)srcPtr;
				srcPtr += sizeof(PdClock);
				break;
			case PD_DF_WAVE_SPECTRUM_MSECONDS :
				data->hMilliSeconds = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_SPECTRUM_N_VALUES :
				data->nValues = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_SPECTRUM_FREQUENCY_INTERVAL :
				data->hLowFrequency = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				data->hHighFrequency = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_SPECTRUM_N_VALUES_ST :
				data->nValuesST = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_SPECTRUM_FREQUENCY_INTERVAL_ST :
				data->hLowFrequencyST = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				data->hHighFrequencyST = *(unsigned short *)srcPtr;
				srcPtr += sizeof(unsigned short);
				break;
			case PD_DF_WAVE_SPECTRUM_FREQUENSIES :
				break;
			case PD_DF_WAVE_SPECTRUM_ST_ENERGY :
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_SPECTRUM_N_VALUES_ST) )
				{
					data->hSTEnergyMultiplier = *(unsigned int *)srcPtr;
					srcPtr += sizeof(unsigned int);
					memcpy(data->hSTEnergy,srcPtr,sizeof(unsigned short)*data->nValuesST);
					srcPtr += sizeof(unsigned short)*data->nValuesST;
				}
				break;
			case PD_DF_WAVE_SPECTRUM_PRES_ENERGY :
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_SPECTRUM_N_VALUES) )
				{
					data->hPressureEnergyMultiplier = *(unsigned int *)srcPtr;
					srcPtr += sizeof(unsigned int);
					memcpy(data->hPresEnergy,srcPtr,sizeof(unsigned short)*data->nValues);
					srcPtr += sizeof(unsigned short)*data->nValues;
				}
				break;
			case PD_DF_WAVE_SPECTRUM_VEL_ENERGY :
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_SPECTRUM_N_VALUES) )
				{
					data->hVelEnergyMultiplier = *(unsigned int *)srcPtr;
					srcPtr += sizeof(unsigned int);
					memcpy(data->hVelEnergy,srcPtr,sizeof(unsigned short)*data->nValues);
					srcPtr += sizeof(unsigned short)*data->nValues;
				}
				break;
			case PD_DF_WAVE_SPECTRUM_DIRECTION :
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_SPECTRUM_N_VALUES) )
				{
					memcpy(data->hDirSpec,srcPtr,sizeof(unsigned char)*data->nValues);
					srcPtr += sizeof(unsigned char)*data->nValues;
				}
				break;
			case PD_DF_WAVE_SPECTRUM_SPREAD	:
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_SPECTRUM_N_VALUES) )
				{
					memcpy(data->hSpreadDirSpec,srcPtr,sizeof(unsigned char)*data->nValues);
					srcPtr += sizeof(unsigned char)*data->nValues;
				}
				break;
			case PD_DF_WAVE_SPECTRUM_FOURIER_COEFS :
				if ( IS_BIT_SET(data->header.hDataFormat,PD_DF_WAVE_SPECTRUM_N_VALUES) )
				{
					memcpy(data->hA1,srcPtr,sizeof(short)*data->nValues);
					srcPtr += sizeof(short)*data->nValues;
					memcpy(data->hB1,srcPtr,sizeof(short)*data->nValues);
					srcPtr += sizeof(short)*data->nValues;
					memcpy(data->hA2,srcPtr,sizeof(short)*data->nValues);
					srcPtr += sizeof(short)*data->nValues;
					memcpy(data->hB2,srcPtr,sizeof(short)*data->nValues);
					srcPtr += sizeof(short)*data->nValues;
				}
				break;
			default:
				break;
		}
		if ( (srcPtr-srcData) % 2 != 0)
		{
			srcPtr += sizeof(unsigned char);
		}
	}
}
