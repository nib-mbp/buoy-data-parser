#ifndef NORTEK_DATA_DEF
#define NORTEK_DATA_DEF

/******************************************************************************
Nortek Data Formats

	This file describes the Nortek data formats. All the dataformats are 
dynamic, thus there are not static structures. The flexibility allows for
more compressed data as well as the possibility to extend the formats in the
future.
	All dataformats have a general structure like:
	unsigned char	cSync;			// A5 (hex)
	unsigned char	cID;			
	unsigned short	hSize;			// size in words
	unsigned short	hDataFormat; 	// Specifies which of the field below that are present
	PAYLOAD							// Payload data
	unsigned short	hChecksum;		// checksum

Thus to detect a datastructure successfully the following operation should be made:
1 - look for cSync (0xA5)
2 - read datatype
3 - read size (this is in multiple of 2 bytes)
4 - use size to read last two bytes of structure
5 - calculate checksum of data
6 - match checksum with checkusm in data
7 - if calculated checksum matches checksum in datastructure a data package is
    successfully located.
    
When a package is found the id tells what kind of dataformat that is present.
The hDataFormat describes the payload data. Each bit in the hDataFormats tells 
wheather a specific datafield is present in the data. The defines specified 
which bit correspands to what data. The meaning of each bit in hDataFormat will
be different for different datatypes ( specified by cID field).

Example of bitfield definition is below (for sensor data):

enum PdSensorDataFields{
	PD_DF_SENSOR_EXT_FORMAT,	// 2
	PD_DF_SENSOR_COUNTER,		// 2
	PD_DF_SENSOR_CLOCK,			// 6
	PD_DF_SENSOR_MSECONDS,		// 2
	PD_DF_SENSOR_ERROR,			// 2
	PD_DF_SENSOR_STATUS,		// 2
	PD_DF_SENSOR_TEMPERATURE,	// 4
	PD_DF_SENSOR_PRESSURE,		// 4
	PD_DF_SENSOR_HEADING,		// 2
	PD_DF_SENSOR_PITCH,			// 2
	PD_DF_SENSOR_ROLL,			// 2
	PD_DF_SENSOR_BATTERY,		// 2
	PD_DF_SENSOR_SOUNDSPEED,	// 2
	PD_DF_SENSOR_ANALOG1,		// 2
	PD_DF_SENSOR_ANALOG2		// 2
};

The size of each bitfield is specified in the structure definition:

typedef struct {
	unsigned char	cSync;			// A5 (hex)
	unsigned char	cId;			// A3 (hex)
	unsigned short	hSize;			// size in words
	unsigned short	hDataFormat;	// Specifies which of the field below that are present
	unsigned short	hExtDataFormat;		// BIT  0 : extended data format
	unsigned short	hCounter;			// BIT  1 : data package counter
	PdClock			clock;				// BIT  2 : date and time
	unsigned short	hMilliSeconds;		// BIT  3 : milliseconds
	unsigned short	hError;				// BIT  4 : error code
	unsigned short	hStatus;			// BIT  5 : status
	long			hTemperature;		// BIT  6 : temperature [0.01 deg C]
    long			hPressure;			// BIT  7 : [mm]
	unsigned short	hHeading;			// BIT  8 : compass heading [0.01 deg]
	short			hPitch;				// BIT  9 : pitch [0.01 deg]
	short			hRoll;				// BIT 10 : roll [0.01 deg]
	unsigned short	hBattery;			// BIT 11 : battery voltage [0.01 V] 
	unsigned short	hSoundSpeed;		// BIT 12 : speed of sound [0.1 m/s]
	unsigned short	hAnaIn1;			// BIT 13 : analog input 1 [counts]
	unsigned short	hAnaIn2;			// BIT 14 : analog input 2 [counts]
	short			hChecksum;		// checksum
} PdSensorData;

So if the hDataFormat is specified as 0b0000 0100 0011 1010 (counting bits from the right)
the payload data will look like:
	PdClock			clock;				// BIT  1 : date and time
	unsigned short	hError;				// BIT  3 : error code
	unsigned short	hStatus;			// BIT  4 : status
	long			hTemperature;		// BIT  5 : temperature [0.01 deg C]
	unsigned short	hBattery;			// BIT 10 : battery voltage [0.01 V] 

In some cases the 16bit hDataFormat field will not be sufficient. In this case
the dataformat can be extended with an extended data format. The first bit of
the hDataFormat will then be set (BIT 0) and the 16bit hExtDataFormat will be
present in the data, and thus specifying additional 16bit, thus a total of 32bit
and thus 32 different datafields in total.

In this way we do not need any speare bytes to accomodate for future changes since
we can just specify the data content for an unused bit. It will also give the
possibility to use make use of compact datastructures when data is not present.
If on for example have not enabled analog input you neither have any analog input
data in your reported structure, thus saving bandwith and data storage.

In the case where decoding seems difficult you can always treat the datastructure
as static if you know your configuration.

******************************************************************************/

#define PD_MAX_BEAMS		4
#define PD_MAX_BINS		32
#define PD_MAX_STRING_SIZE	1000
#define PD_MAX_BEAM_MATRIX	PD_MAX_BEAMS*PD_MAX_BEAMS
#define PD_MAX_CELL_CELECTION	PD_MAX_BINS / 8
#define PD_MAX_SPECTRUM 	400
#define PD_MAX_TIME_SERIES 	4096
#define PD_MAX_BANDS		10

enum PdDataHeaderType
{
	PD_DATATYPE_STANDARD,
	PD_DATATYPE_PROFILE,
	PD_DATATYPE_WAVE
};

enum PdDataCollectionMethod
{
	PD_DATA_COLLECTION_METHOD_MLM,
	PD_DATA_COLLECTION_METHOD_SUV,
};

/*enum PdDataCoordinateSystem
{
	PD_DATA_COORDINATE_SYSTEM_ENU = PD_COORDSYS_ENU,
	PD_DATA_COORDINATE_SYSTEM_XYZ = PD_COORDSYS_XYZ,
	PD_DATA_COORDINATE_SYSTEM_BEAM= PD_COORDSYS_BEAM
};*/

enum PdInstrumentType
{
	PD_INSTRUMENTTYPE_UNKNOWN,
	PD_INSTRUMENTTYPE_AQD,	//Aquadopp
	PD_INSTRUMENTTYPE_AQP,	//Aquaprofile
	PD_INSTRUMENTTYPE_WAV,	//AWAC
};

enum PdDataFormats
{
	PD_DATA_ERROR					=		0xA0,
	PD_DATA_EVENT					=		0xA1,
	PD_DATA_HEADER					=		0xA2,
	PD_DATA_SENSOR					=		0xA3,
	PD_DATA_BEAM					=		0xA4,
	PD_DATA_PROFILE					=		0xA5,
	PD_DATA_WAVE					=		0xA6,
	PD_DATA_WAVE_BANDS				=		0xA7,
	PD_DATA_WAVE_SPECTRUM				=		0xA8,
	PD_DATA_TIME_SERIES				=		0xA9,
	PD_DATA_WAVE_PROC_PARAM				=		0xAA
};

enum PdHeaderDataFields
{
	PD_DF_HEADER_COUNTER,			// 2
	PD_DF_HEADER_CLOCK,			// 6
	PD_DF_HEADER_MSECONDS,			// 2
	PD_DF_HEADER_DATA_TYPE,			// 2
	PD_DF_HEADER_INSTRUMENT_TYPE,		// 2
	PD_DF_HEADER_INSTRUMENT_SERIAL,		// 4
	PD_DF_HEADER_N_BEAMS,			// 2
	PD_DF_HEADER_N_RECORDS,			// 2
	PD_DF_HEADER_BEAM_MATRIX,		// 2 * nBeams * nBeams
	PD_DF_HEADER_BLANKING,			// 4
	PD_DF_HEADER_CELLSIZE,			// 4
	PD_DF_HEADER_AVERAGE_INTERVAL,		// 2
	PD_DF_HEADER_MEASUREMENT_LOAD,		// 2
	PD_DF_HEADER_COORDINATE_SYSTEM,		// 2
	PD_DF_HEADER_FREQUENCY,			// 2
	PD_DF_HEADER_EXT_FORMAT,		// 2
	PD_DF_HEADER_POWERLEVEL,		// 2
	PD_DF_HEADER_NOISE,			// 1 * nBeams + ( nBeams % 2)
	PD_DF_HEADER_NOISE_CORRELATION,		// 1 * nBeams + ( nBeams % 2)
	PD_DF_HEADER_SAMPLE_FREQ,		// 2
	PD_DF_HEADER_DATA_COLL_METHOD,		// 2
	PD_DF_HEADER_MIN_PRESSURE,		// 4
	PD_DF_HEADER_MAX_PRESSURE,		// 4
	PD_DF_HEADER_AST_WND_SIZE,		// 2
	PD_DF_HEADER_EXT_EXT_FORMAT = 31,	// 2
};

enum PdSensorDataFields
{
	PD_DF_SENSOR_COUNTER,			// 2
	PD_DF_SENSOR_CLOCK,			// 6
	PD_DF_SENSOR_MSECONDS,			// 2
	PD_DF_SENSOR_ERROR,			// 2
	PD_DF_SENSOR_STATUS,			// 2
	PD_DF_SENSOR_TEMPERATURE,		// 2
	PD_DF_SENSOR_PRESSURE,			// 4
	PD_DF_SENSOR_HEADING,			// 2
	PD_DF_SENSOR_PITCH,			// 2
	PD_DF_SENSOR_ROLL,			// 2
	PD_DF_SENSOR_BATTERY,			// 2
	PD_DF_SENSOR_SOUNDSPEED,		// 2
	PD_DF_SENSOR_ANALOG1,			// 2
	PD_DF_SENSOR_ANALOG2,			// 2
	PD_DF_SENSOR_EXT_FORMAT = 15		// 2
};

enum PdProfileDataFields
{
	PD_DF_PROFILE_COUNTER,			// 2
	PD_DF_PROFILE_CLOCK,			// 6
	PD_DF_PROFILE_MSECONDS,			// 2
	PD_DF_PROFILE_ERROR,			// 2
	PD_DF_PROFILE_STATUS,			// 2
	PD_DF_PROFILE_NBEAMS_NCELLS,		// 2
	PD_DF_PROFILE_BEAM_SELECTION,		// 2
	PD_DF_PROFILE_CELL_SELECTION,		// ((nCells-1)/8 + 1) + ((nCells-1)/8 + 1) % 2
	PD_DF_PROFILE_VELOCITY,			// 2 * nCells or 2 * nHighBits(cCells)
	PD_DF_PROFILE_AMPLITUDE,		// 1 * nCells ( 1 * nCells )% 2 or 1 * nHighBits(cCells) + ( 1 * nHighBits(cCells) ) % 2
	PD_DF_PROFILE_CORRELATION,		// 1 * nCells ( 1 * nCells )% 2 or 1 * nHighBits(cCells) + ( 1 * nHighBits(cCells) ) % 2
	PD_DF_PROFILE_EXT_FORMAT = 15		// 2
};

enum PdWaveDataFields
{
	PD_DF_WAVE_COUNTER,			// 2
	PD_DF_WAVE_CLOCK,			// 6
	PD_DF_WAVE_MSECONDS,			// 2
	PD_DF_WAVE_SPECTRUM_TYPE,		// 2
	PD_DF_WAVE_TM02,			// 2
	PD_DF_WAVE_TP,				// 2
	PD_DF_WAVE_DIR_TP,			// 2
	PD_DF_WAVE_SPR_TP,			// 2
	PD_DF_WAVE_DIR_MEAN,			// 2
	PD_DF_WAVE_UI,				// 2
	PD_DF_WAVE_HM0,				// 2
	PD_DF_WAVE_H3,				// 2
	PD_DF_WAVE_T3,				// 2
	PD_DF_WAVE_H10,				// 2
	PD_DF_WAVE_T10,				// 2
	PD_DF_WAVE_EXT_FORMAT,			// 2
	PD_DF_WAVE_HMAX,			// 2
	PD_DF_WAVE_TMAX,			// 2
	PD_DF_WAVE_TZ,				// 2
	PD_DF_WAVE_PRESSURE_MEAN,		// 4
	PD_DF_WAVE_AST_MEAN,			// 4
	PD_DF_WAVE_CURRENT_SPEED_MEAN,		// 2
	PD_DF_WAVE_CURRENT_DIRECTION_MEAN,	// 2
	PD_DF_WAVE_N_BAD_DETECTS,		// 2
	PD_DF_WAVE_ERROR,			// 4
	PD_DF_WAVE_EXT_EXT_FORMAT = 31		// 2
};

enum PdWaveBandsDataFields
{
	PD_DF_WAVE_BAND_COUNTER,		// 2
	PD_DF_WAVE_BAND_CLOCK,			// 6
	PD_DF_WAVE_BAND_MSECONDS,		// 2
	PD_DF_WAVE_BAND_N_BANDS,		// 2
	PD_DF_WAVE_BAND_SPECTRUM_TYPE,		// 2
	PD_DF_WAVE_BAND_FREQUENCY_INTERVAL,	// 2 * 2 * nBands
	PD_DF_WAVE_BAND_HM0,			// 2 * nBands
	PD_DF_WAVE_BAND_TM02,			// 2 * nBands
	PD_DF_WAVE_BAND_TP,			// 2 * nBands
	PD_DF_WAVE_BAND_DIR_TP,			// 2 * nBands
	PD_DF_WAVE_BAND_DIR_MEAN,		// 2 * nBands
	PD_DF_WAVE_BAND_SPREAD_TP,		// 2 * nBands
	PD_DF_WAVE_BAND_SPREAD_MEAN,		// 2 * nBands
	PD_DF_WAVE_BAND_ERROR,			// 4 * nBands
	PD_DF_WAVE_BAND_EXT_FORMAT = 15		// 2
};

enum PdWaveSpectrumDataFields
{
	PD_DF_WAVE_SPECTRUM_COUNTER,		// 2
	PD_DF_WAVE_SPECTRUM_CLOCK,		// 6
	PD_DF_WAVE_SPECTRUM_MSECONDS,		// 2
	PD_DF_WAVE_SPECTRUM_N_VALUES,		// 2
	PD_DF_WAVE_SPECTRUM_FREQUENCY_INTERVAL,	// 2 * 2
	PD_DF_WAVE_SPECTRUM_N_VALUES_ST,		// 2
	PD_DF_WAVE_SPECTRUM_FREQUENCY_INTERVAL_ST,	// 2 * 2
	PD_DF_WAVE_SPECTRUM_FREQUENSIES,		// 2 * max(nValues,nValuesSt)
	PD_DF_WAVE_SPECTRUM_ST_ENERGY,			// 4 + 2 * nValuesSt
	PD_DF_WAVE_SPECTRUM_PRES_ENERGY,		// 4 + 2 * nValues
	PD_DF_WAVE_SPECTRUM_VEL_ENERGY,			// 4 + 2 * nValues
	PD_DF_WAVE_SPECTRUM_DIRECTION,			// 1 * nValues
	PD_DF_WAVE_SPECTRUM_SPREAD,			// 1 * nValues
	PD_DF_WAVE_SPECTRUM_FOURIER_COEFS,		// 8 * nValues
	PD_DF_WAVE_SPECTRUM_EXT_FORMAT = 15,		// 2
};

typedef struct
{
	unsigned char	cSync;			// A5 (hex)
	unsigned char	cID;			// ??
	unsigned short	hSize;			// size in words
	unsigned short	hDataFormat;		// Specifies which of the field below that are present
} PdGenericHeader;

typedef struct
{
   unsigned char  cMinute;       // minute
   unsigned char  cSecond;       // second
   unsigned char  cDay;          // day
   unsigned char  cHour;         // hour
   unsigned char  cYear;         // year
   unsigned char  cMonth;        // month
} PdClock;

typedef struct
{
	PdGenericHeader	header;			// header (cID = A2 (hex))
	unsigned short	hCounter;		// BIT  0 : data package counter
	PdClock		clock;			// BIT  1 : date and time
	unsigned short	hMilliSeconds;		// BIT  2 : milliseconds
	unsigned short	hDataType;		// BIT  3 : what data type is this header for
	unsigned short	hInstrumentType;	// BIT  4 : instrument type
	unsigned int	hInstrumentSerial;	// BIT  5 : instrument serial no.
	unsigned short	nBeams;              	// BIT  6 : number of beams [#]
	unsigned short	nRecords;		// BIT  7 : number of records collected [#]
	short		hBeamMatrix[PD_MAX_BEAMS][PD_MAX_BEAMS];	// BIT  8 : beam to xyz matrix (nBeams*nBeams elements) [1/32767];
	int		hBlankingDistance;				// BIT  9 : blanking distance in [mm]
	int		hCellSize;					// BIT 10 : cell size in [mm]
	unsigned short	hAverageInterval;				// BIT 11 : average interval used for data collection [sec]
	unsigned short	hMeasurementLoad;				// BIT 12 : [1/65536]
	unsigned short	hCoordinateSystem;				// BIT 13 : what coordinate system the data is represented in (beam, xyz, enu)
	unsigned short	hFrequency;					// BIT 14 : [kHz]
	unsigned short	hExtDataFormat;					// BIT 15 : extended data format
	unsigned short	hPowerLevel;					// BIT 16 : [counts]
	unsigned char	cNoise[PD_MAX_BEAMS];				// BIT 17 : noise for each beam [counts]
	unsigned char	cNoiseCorrelation[PD_MAX_BEAMS];		// BIT 18 : noise correlation for each beam [1/256]
	unsigned short	hSampleFrequency;				// BIT 19 : sample frequency [Hz]
	unsigned short	hDataCollectionMethod;				// BIT 20 : number of samples in AST window [mm]
	int		hMinPressure;					// BIT 21 : minimum pressure [mm]
	int		hMaxPressure;					// BIT 22 : minimum pressure [mm]
	unsigned short 	hASTWindowsSize;				// BIT 23 : number of samples in AST window [#]
	unsigned short	hExtExtDataFormat;				// BIT 31 : extended data format
	int		AcqTime;		// Time data was collected by I/O processor
} PdHeaderData;

typedef struct 
{
	PdGenericHeader	header;			// header (cID = A3 (hex))
	unsigned short	hCounter;		// BIT  0 : data package counter
	PdClock		clock;			// BIT  1 : date and time
	unsigned short	hMilliSeconds;		// BIT  2 : milliseconds
	unsigned short	hError;			// BIT  3 : error code
	unsigned short	hStatus;		// BIT  4 : status
	short		hTemperature;		// BIT  5 : temperature [0.01 deg C]
	int		hPressure;		// BIT  6 : [mm]
	unsigned short	hHeading;		// BIT  7 : compass heading [0.01 deg]
	short		hPitch;			// BIT  8 : pitch [0.01 deg]
	short		hRoll;			// BIT  9 : roll [0.01 deg]
	unsigned short	hBattery;		// BIT 10 : battery voltage [0.01 V] 
	unsigned short	hSoundSpeed;		// BIT 11 : speed of sound [0.1 m/s]
	unsigned short	hAnaIn1;		// BIT 12 : analog input 1 [counts]
	unsigned short	hAnaIn2;		// BIT 13 : analog input 2 [counts]
	unsigned short	hExtDataFormat;		// BIT 15 : extended data format
	int		AcqTime;		// Time data was collected by I/O processor
} PdSensorData;

typedef struct 
{
	PdGenericHeader	header;			// header (cID = A5 (hex))
	unsigned short	hCounter;		// BIT  0 : data package counter
	PdClock		clock;			// BIT  1 : date and time
	unsigned short	hMilliSeconds;		// BIT  2 : milliseconds
	unsigned short	hError;			// BIT  3 : error code
	unsigned short	hStatus;		// BIT  4 : status
	unsigned char	nBeams;			// BIT  5 : Number of beams [#]
	unsigned char	nCells;			//        : Maximum number of cells
	unsigned short  cBeams;			// BIT  6 : Bitfield that specifies which beams that are present
	unsigned char	cCells[PD_MAX_CELL_CELECTION];		// BIT  7 : Bitfield that specifies which cells that are present, size is nCells/8, padded to even bytes
	short		hVel[PD_MAX_BEAMS][PD_MAX_BINS];	// BIT  8 : velocity [mm/s] or [0.1mm/s] depending on status
	unsigned char	cAmp[PD_MAX_BEAMS][PD_MAX_BINS];	// BIT  9 : amplitude, padded to even bytes [counts]
	unsigned char	cCorr[PD_MAX_BEAMS][PD_MAX_BINS];	// BIT 10 : Standard deviation, padded to even bytes
	unsigned short	hExtDataFormat;				// BIT 15 : extended data format
	int		AcqTime;		// Time data was collected by I/O processor
} PdProfileData;

typedef struct 
{
	PdGenericHeader	header;			// header (cID = A6 (hex))
	unsigned short	hCounter;		// BIT  0 : data package counter
	PdClock		clock;			// BIT  1 : date and time
	unsigned short	hMilliSeconds;		// BIT  2 : milliseconds
	unsigned short	hSpectrumType;		// BIT  3 : spectrum used for calculation
	unsigned short	hTm02;			// BIT  4 : Mean period spectrum based [0.01 sec]
	unsigned short	hTp;			// BIT  5 : Peak period [0.01 sec]
	unsigned short	hDirTp;			// BIT  6 : Direction at Tp [0.01 deg]
	unsigned short	hSprTp;			// BIT  7 : Spreading at Tp [0.01 deg]
	unsigned short	hDirMean;		// BIT  8 : Mean wave direction [0.01 deg]
	unsigned short	hUI;			// BIT  9 : Unidirectivity index [1/32767]
	unsigned short	hHm0;			// BIT 10 : Spectral significant wave height [mm]
	unsigned short	hH3;			// BIT 11 : ST significant (33%) wave height [mm]
	unsigned short	hT3;			// BIT 12 : ST significant (33%) wave period [0.01 sec]
	unsigned short	hH10;			// BIT 13 : ST wave height, top 10% [mm]
	unsigned short	hT10;			// BIT 14 : ST wave period, top 10% [0.01 sec]
	unsigned short	hExtDataFormat;		// BIT 15 : extended data format
	unsigned short	hHmax;			// BIT 16 : ST max wave height in wave ensemble [mm]
	unsigned short	hTmax;			// BIT 17 : ST max period in wave ensemble [0.01 sec]
	unsigned short	hTz;			// BIT 18 : ST mean zero-crossing period [0.01 sec]
	int 		hPressureMean;		// BIT 19 : Mean pressure during burst [mm]
	int 		hSTMean;		// BIT 20 : Mean ST distance to surface during burst [mm]
	unsigned short	hCurSpeedMean;		// BIT 21 : Mean current speed - wave cells [mm/sec]
	unsigned short	hCurDirMean;		// BIT 22 : Mean current direction - wave cells [0.1 deg]
	unsigned short	hNumBadDet;		// BIT 23 : Number of ST Bad detects [#]
	unsigned int	hError;			// BIT 24 : Error Code for bad data
	unsigned short	hExtExtDataFormat;	// BIT 31 : extended data format
	int		AcqTime;		// Time data was collected by I/O processor
} PdWaveData;

typedef struct 
{						
	PdGenericHeader	header;			// header (cID = A7 (hex))
	unsigned short	hCounter;		// BIT  0 : data package counter
	PdClock 	clock;			// BIT  1 : date and time
	unsigned short	hMilliSeconds;		// BIT  2 : milliseconds
	unsigned short	nBands;			// BIT  3 : number of wave bands in data
	unsigned short	hSpectrumType;		// BIT  4 : spectrum used for calculation
	unsigned short	hLowFrequency[PD_MAX_BANDS];	// BIT  5 : low frequency in [0.001 Hz]
	unsigned short	hHighFrequency[PD_MAX_BANDS];	//        : high frequency in [0.001 Hz]
	unsigned short	hHm0[PD_MAX_BANDS];		// BIT  6 : Spectral significant wave height [mm]
	unsigned short	hTm02[PD_MAX_BANDS];		// BIT  7 : Mean period spectrum based [0.01 sec]
	unsigned short	hTp[PD_MAX_BANDS];		// BIT  8 : Peak period [0.01 sec]
	unsigned short	hDirTp[PD_MAX_BANDS];		// BIT  9 : Direction at Tp [0.01 deg]
	unsigned short	hDirMean[PD_MAX_BANDS];		// BIT 10 : Mean wave direction [0.01 deg]
	unsigned short	hSprTp[PD_MAX_BANDS];		// BIT 11 : Spreading at Tp [0.01 deg]
	unsigned short	hSprMean[PD_MAX_BANDS];		// BIT 12 : Mean spread [0.01 deg]
	unsigned int	hError[PD_MAX_BANDS];		// BIT 13 : Error Code for bad data
	unsigned short	hExtDataFormat;			// BIT 15 : extended data format
	int		AcqTime;		// Time data was collected by I/O processor
} PdWaveBandsData;

typedef struct 
{
	PdGenericHeader	header;			// header (cID = A8 (hex))
	unsigned short	hCounter;		// BIT  0 : data package counter
	PdClock		clock;			// BIT  1 : date and time
	unsigned short	hMilliSeconds;		// BIT  2 : milliseconds
	unsigned short	nValues;		// BIT  3 : number of spectrum values
	unsigned short	hLowFrequency;		// BIT  4 : low frequency in [0.001 Hz]
	unsigned short	hHighFrequency;		//        : high frequency in [0.001 Hz]
	unsigned short	nValuesST;		// BIT  5 : number of spectrum values for ST
	unsigned short	hLowFrequencyST;	// BIT  6 : low frequency for ST in [0.001 Hz]
	unsigned short	hHighFrequencyST;	//        : high frequency for ST in [0.001 Hz]
	unsigned short	hFrequencies[PD_MAX_SPECTRUM];	// BIT  7 : Frequency values
	unsigned int	hSTEnergyMultiplier;		// BIT  8 : ST energy spectrum multiplier
	unsigned short	hSTEnergy[PD_MAX_SPECTRUM];	//			ST Spectra [cm^2/Hz]
	unsigned int	hPressureEnergyMultiplier;	// BIT  9 : Pressure energy spectrum multiplier
	unsigned short	hPresEnergy[PD_MAX_SPECTRUM];	//			Pressure spectra [cm^2/Hz]
	unsigned int	hVelEnergyMultiplier;		// BIT 10 : Velocity energy spectrum multiplier
	unsigned short	hVelEnergy[PD_MAX_SPECTRUM];	//			Velocity spectra [cm^2/Hz]
	unsigned char	hDirSpec[PD_MAX_SPECTRUM];	// BIT 11 : direction spectrum in [0.01 deg]
	unsigned char	hSpreadDirSpec[PD_MAX_SPECTRUM];// BIT 12 : spread direction spectrum in [0.01 deg]
	short		hA1[PD_MAX_SPECTRUM];		// BIT 13 : Fourier coefficients in [1/32767]
	short		hB1[PD_MAX_SPECTRUM];
	short		hA2[PD_MAX_SPECTRUM];
	short		hB2[PD_MAX_SPECTRUM];
	unsigned short	hExtDataFormat;		// BIT 15 : extended data format
	int		AcqTime;		// Time data was collected by I/O processor
} PdWaveSpectrumData;

#endif
