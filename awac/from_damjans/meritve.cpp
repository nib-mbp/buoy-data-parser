PdHeaderData Aheader;
PdSensorData Asens;
PdProfileData Acur;
PdWaveData Awaves;
PdWaveBandsData Aband;
PdWaveSpectrumData Aspec;

	do
	{

        bytesr = fread(&buffer[ 0 ],1,1,fd);

		switch (buffer[ 0 ])
		{

        case 0xA5:
          bytesr = fread(&buffer[ 1 ],1,3,fd);
          adp_len = buffer[ 3 ];
          adp_len <<= 8;
          adp_len += buffer[ 2 ];
          adp_len <<= 1;
          pos += bytesr;
          bytesr = fread(&buffer[ 4 ],1,adp_len,fd);
          
          switch (buffer[ 1 ])
          {
            case 0xA2:
            case 0xA3:
            case 0xA5:
            case 0xA6:
            case 0xA7:
            case 0xA8:
              break;
            default:
              error_message("Napacen podatek tokomera");
              return(false);
          }

          switch (buffer[ 1 ])
          {
            case 0xA2:
              memset(&Aheader,0,sizeof(Aheader));
              expand_header(&buffer[ 0 ],&Aheader);
              t = Asens.AcqTime;
              gmtime_r(&t,&tm);
              sprintf(qbuf,"update %s.awacconfig set datetime = '%04d-%02d-%02d %02d:%02d:%02d',datetimea = '%04d-%02d-%02d %02d:%02d:%02d',\
                instrType = %u,serialNo = %u,nBeams = %u,FFTSize = %u,blankDist = %7.3f,cellSize = %7.3f,\
                avgInterval = %u,measLoad = %7.4f,coordSyst = %u,frequency = %u,power = %u,sampFreq = %u,nSTsamp = %u,\
                minPress = %7.3f,maxPress = %7.3f,ASTSize = %u where acnfid = %s",
                database2,2000+bcdtobin(Aheader.clock.cYear),bcdtobin(Aheader.clock.cMonth),
                bcdtobin(Aheader.clock.cDay),bcdtobin(Aheader.clock.cHour),
                bcdtobin(Aheader.clock.cMinute),bcdtobin(Aheader.clock.cSecond),
                tm.tm_year+1900,tm.tm_mon+1,tm.tm_mday,tm.tm_hour,tm.tm_min,tm.tm_sec,
                Aheader.hInstrumentType,Aheader.hInstrumentSerial,Aheader.nBeams,Aheader.nRecords,
                ((double)Aheader.hBlankingDistance)/1000.0,
                ((double)Aheader.hCellSize)/1000.0,
                Aheader.hAverageInterval,
                ((double)Aheader.hMeasurementLoad)/65536.0,
                Aheader.hCoordinateSystem,Aheader.hFrequency,
                Aheader.hPowerLevel,Aheader.hSampleFrequency,Aheader.hDataCollectionMethod,
                ((double)Aheader.hMinPressure)/1000.0,
                ((double)Aheader.hMaxPressure)/1000.0,
                Aheader.hASTWindowsSize,acnfid);
              if (!execute_query())
                return(false);
              for (i = 0;i < Aheader.nBeams;++i)
              {
                sprintf(qbuf,"insert into %s.awacconfigdata (acnfid,beam,noise,noisecorr) values (%s,%u,%u,%7.4f)",
                  database2,acnfid,i,Aheader.cNoise[ i ],
                  ((double)Aheader.cNoiseCorrelation[ i ])/256.0);
                if (!execute_query())
                  return(false);
              }
              break;
            case 0xA3:
              memset(&Asens,0,sizeof(Asens));
              expand_sensor_data(&buffer[ 0 ],&Asens);
              t = Asens.AcqTime;
              gmtime_r(&t,&tm);
              sprintf(qbuf,"insert into %s.awacsensors (pid,acnfid,datetime,datetimea,heading,pitch,roll,temperature,pressure,sndspeed,voltage,errcode) values (%s,%s,'%04d-%02d-%02d %02d:%02d:%02d','%04d-%02d-%02d %02d:%02d:%02d',%4.1f,%4.1f,%4.1f,%5.2f,%7.2f,%7.1f,%4.1f,%u)",
                database2,pid2,acnfid,2000+bcdtobin(Asens.clock.cYear),bcdtobin(Asens.clock.cMonth),
                bcdtobin(Asens.clock.cDay),bcdtobin(Asens.clock.cHour),
                bcdtobin(Asens.clock.cMinute),bcdtobin(Asens.clock.cSecond),
                tm.tm_year+1900,tm.tm_mon+1,tm.tm_mday,tm.tm_hour,tm.tm_min,tm.tm_sec,
                ((double)Asens.hHeading)/100.0,
                ((double)Asens.hPitch)/100.0,
                ((double)Asens.hRoll)/100.0,
                ((double)Asens.hTemperature)/100.0,
                ((double)Asens.hPressure)/1000.0, // je v m globine
                ((double)Asens.hSoundSpeed)/10.0,
                ((double)Asens.hBattery)/100.0,
                Asens.hError);
              if (!execute_query())
                return(false);

              break;
            case 0xA5:
              memset(&Acur,0,sizeof(Acur));
              expand_profile_data(&buffer[ 0 ],&Acur);
              for (i = 0;i < Acur.nCells;++i)
              {
                sprintf(qbuf,"insert into %s.awaccurrents (pid,acnfid,cellno,curE,curN,curW,ampE,ampN,ampW,stdE,stdN,stdW,errcode) values (%s,%s,%d,%7.1f,%7.1f,%7.1f,%u,%u,%u,%5.2f,%5.2f,%5.2f,%u)",
                  database2,pid2,acnfid,i,
                  ((double)Acur.hVel[ 0 ][ i ])/10.0,
                  ((double)Acur.hVel[ 1 ][ i ])/10.0,
                  ((double)Acur.hVel[ 2 ][ i ])/10.0, // dobimo v m/s
                  Acur.cAmp[ 0 ][ i ],
                  Acur.cAmp[ 1 ][ i ],
                  Acur.cAmp[ 2 ][ i ],
                  ((double)Acur.cCorr[ 0 ][ i ]),
                  ((double)Acur.cCorr[ 1 ][ i ]),
                  ((double)Acur.cCorr[ 2 ][ i ]),
                  Acur.hError);
                if (!execute_query())
                  return(false);
                sprintf(qbuf,"update %s.awacconfig set nCells = %u where acnfid = %s",database2,Acur.nCells,acnfid);
                if (!execute_query())
                  return(false);
              }
              break;
            case 0xA6:
              memset(&Awaves,0,sizeof(Awaves));
              expand_wave_data(&buffer[ 0 ],&Awaves);
              t = Awaves.AcqTime;
              gmtime_r(&t,&tm);
              sprintf(qbuf,"insert into %s.awacwaves (pid,acnfid,valid,datetime,datetimea,meanp,peakp,dirpp,spreadpp,meandir,UI,wheight,wh33,wp33,wh10,wp10,whmax,wpmax,mzcp,meanpress,meandist,meancurspeed,meancurdir,baddet,errcode) \
                values (%s,%s,%u,'%04d-%02d-%02d %02d:%02d:%02d','%04d-%02d-%02d %02d:%02d:%02d',%7.2f,%7.2f,%7.2f,%7.2f,%7.2f,%7.5f,%7.3f,%7.3f,%7.2f,%7.3f,%7.2f,%7.3f,%7.2f,%7.2f,%7.3f,%7.3f,%7.1f,%7.2f,%u,%u)",
                database2,pid2,acnfid,Awaves.header.hDataFormat,
                    2000+bcdtobin(Awaves.clock.cYear),bcdtobin(Awaves.clock.cMonth),
                    bcdtobin(Awaves.clock.cDay),bcdtobin(Awaves.clock.cHour),
                    bcdtobin(Awaves.clock.cMinute),bcdtobin(Awaves.clock.cSecond),
                tm.tm_year+1900,tm.tm_mon+1,tm.tm_mday,tm.tm_hour,tm.tm_min,tm.tm_sec,
                ((double)Awaves.hTm02)/100.0,
                ((double)Awaves.hTp)/100.0,
                ((double)Awaves.hDirTp)/100.0,
                ((double)Awaves.hSprTp)/100.0,
                ((double)Awaves.hDirMean)/100.0,
                ((double)Awaves.hUI)/32767.0,
                ((double)Awaves.hHm0)/1000.0,
                ((double)Awaves.hH3)/1000.0,
                ((double)Awaves.hT3)/100.0,
                ((double)Awaves.hH10)/1000.0,
                ((double)Awaves.hT10)/100.0,
                ((double)Awaves.hHmax)/1000.0,
                ((double)Awaves.hTmax)/100.0,
                ((double)Awaves.hTz)/100.0,
                ((double)Awaves.hPressureMean)/1000.0,
                ((double)Awaves.hSTMean)/1000.0,
                ((double)Awaves.hCurSpeedMean)/10.0, // v cm/s
                ((double)Awaves.hCurDirMean)/100.0,
                Awaves.hNumBadDet,
                Awaves.hError);
              if (!execute_query())
                return(false);
              break;
            case 0xA7:
              memset(&Aband,0,sizeof(Aband));
              expand_waveband_data(&buffer[ 0 ],&Aband);
              t = Awaves.AcqTime;
              gmtime_r(&t,&tm);
              sprintf(qbuf,"insert into %s.awacbandsheader (pid,acnfid,valid,datetime,datetimea,nbands,spectrumtype) values (%s,%s,%u,'%04d-%02d-%02d %02d:%02d:%02d','%04d-%02d-%02d %02d:%02d:%02d',%u,%u)",
                database2,pid2,acnfid,Aband.header.hDataFormat,2000+bcdtobin(Aband.clock.cYear),bcdtobin(Aband.clock.cMonth),
                bcdtobin(Aband.clock.cDay),bcdtobin(Aband.clock.cHour),
                bcdtobin(Aband.clock.cMinute),bcdtobin(Aband.clock.cSecond),
                tm.tm_year+1900,tm.tm_mon+1,tm.tm_mday,tm.tm_hour,tm.tm_min,tm.tm_sec,
                Aband.nBands,Aband.hSpectrumType);
              if (!execute_query())
                return(false);
              sprintf(qbuf,"select max(abhid) from %s.awacbandsheader",database2);
              if (!execute_query())
                return(false);
              if (!store_result())
                return(false);
              row = mysql_fetch_row(res);
              if ((row != NULL) && (row[ 0 ] != NULL))
                strcpy(abhid,row[ 0 ]);
              else
              {
                error_message("Sistemska SQL napaka: prazna vrstica iz tabele awacbandsheader", SQL_ERROR);
                mysql_free_result(res);
                return(false);
              }
              mysql_free_result(res);
              
              for (i = 0;i < Aband.nBands;++i)
              {
                sprintf(qbuf,"insert into %s.awacbandsdata (abhid,band,lowfreq,hifreq,meanwh,meanp,peakp,dir,meandir,spr,meanspr,errcode) values (%s,%d,%7.3f,%7.3f,%7.3f,%7.2f,%7.2f,%7.2f,%7.2f,%7.2f,%7.2f,%u)",
                  database2,abhid,i,
                  ((double)Aband.hLowFrequency[ i ])/1000.0,
                  ((double)Aband.hHighFrequency[ i ])/1000.0,
                  ((double)Aband.hHm0[ i ])/1000.0,
                  ((double)Aband.hTm02[ i ])/100.0,
                  ((double)Aband.hTp[ i ])/100.0,
                  ((double)Aband.hDirTp[ i ])/100.0,
                  ((double)Aband.hDirMean[ i ])/100.0,
                  ((double)Aband.hSprTp[ i ])/100.0,
                  ((double)Aband.hSprMean[ i ])/100.0,
                  Aband.hError[ i ]);
                if (!execute_query())
                  return(false);
              }
              break;
            case 0xA8:
              memset(&Aspec,0,sizeof(Aspec));
              expand_wave_spectrum(&buffer[ 0 ],&Aspec);
              t = Awaves.AcqTime;
              gmtime_r(&t,&tm);
              sprintf(qbuf,"insert into %s.awacspectrumheader (pid,acnfid,valid,datetime,datetimea,nvalues,lowfreq,hifreq,nvaluesST,lowfreqST,hifreqST,STenergymult,pressenergymult,velenergymult) \
                values (%s,%s,%u,'%04d-%02d-%02d %02d:%02d:%02d','%04d-%02d-%02d %02d:%02d:%02d',%u,%7.3f,%7.3f,%u,%7.3f,%7.3f,%u,%u,%u)",
                database2,pid2,acnfid,Aspec.header.hDataFormat,
                2000+bcdtobin(Aspec.clock.cYear),bcdtobin(Aspec.clock.cMonth),
                bcdtobin(Aspec.clock.cDay),bcdtobin(Aspec.clock.cHour),
                bcdtobin(Aspec.clock.cMinute),bcdtobin(Aspec.clock.cSecond),
                tm.tm_year+1900,tm.tm_mon+1,tm.tm_mday,tm.tm_hour,tm.tm_min,tm.tm_sec,
                Aspec.nValues,
                ((double)Aspec.hLowFrequency)/1000.0,
                ((double)Aspec.hHighFrequency)/1000.0,
                Aspec.nValuesST,
                ((double)Aspec.hLowFrequencyST)/1000.0,
                ((double)Aspec.hHighFrequencyST)/1000.0,
                Aspec.hSTEnergyMultiplier,
                Aspec.hPressureEnergyMultiplier,
                Aspec.hVelEnergyMultiplier);
              if (!execute_query())
                return(false);
              sprintf(qbuf,"select max(ashid) from %s.awacspectrumheader",database2);
              if (!execute_query())
                return(false);
              if (!store_result())
                return(false);
              row = mysql_fetch_row(res);
              if ((row != NULL) && (row[ 0 ] != NULL))
                strcpy(ashid,row[ 0 ]);
              else
              {
                error_message("Sistemska SQL napaka: prazna vrstica iz tabele awacspectrumheader", SQL_ERROR);
                mysql_free_result(res);
                return(false);
              }
              mysql_free_result(res);
              
              for (i = 0;i < Aspec.nValues;++i)
              {
                sprintf(qbuf,"insert into %s.awacspectrumdata (ashid,ndx,frequency,pressenergy,velenergy,dirspec,spreaddirspec,A1,B1,A2,B2) \
                  values (%s,%u,%7.3f,%10.7E,%10.7E,%10.7E,%10.7E,%10.7E,%10.7E,%10.7E,%10.7E)",
                  database2,ashid,i,
                  ((double)Aspec.hFrequencies[ i ])/1000.0,
                  (double)Aspec.hPresEnergy[ i ] * (double)Aspec.hPressureEnergyMultiplier / 100000.0 / 65536.0,
                  (double)Aspec.hVelEnergy[ i ] * (double)Aspec.hVelEnergyMultiplier / 100000.0 / 65536.0,
                  (double)Aspec.hDirSpec[ i ] / 256.0 * 360.0,
                  (double)Aspec.hSpreadDirSpec[ i ] / 256.0 * 82.0,
                  (double)Aspec.hA1[ i ] / 32767.0,
                  (double)Aspec.hB1[ i ] / 32767.0,
                  (double)Aspec.hA2[ i ] / 32767.0,
                  (double)Aspec.hB2[ i ] / 32767.0);
                if (!execute_query())
                  return(false);
              }
              if (IS_BIT_SET(Aspec.header.hDataFormat, PD_DF_WAVE_SPECTRUM_ST_ENERGY))
              {
                for (i = 0;i < Aspec.nValuesST;++i)
                {
                  sprintf(qbuf,"insert into %s.awacSTspectrumdata (ashid,ndx,STenergy) values (%s,%u,%10.7E)",
                    database2,ashid,i,
                    (double)Aspec.hSTEnergy[ i ] * (double)Aspec.hSTEnergyMultiplier / 100000.0 / 65536.0);
                  if (!execute_query())
                    return(false);
                }
              }
              break;
          }
          break;
      }
      pos += bytesr;
    }
  } while (bytesr > 0);
  