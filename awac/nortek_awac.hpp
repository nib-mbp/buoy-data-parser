/*
 * Nortek data structures.
 */
#ifndef _NORTEK_AWAC_HPP
#define _NORTEK_AWAC_HPP

#include <boost/cstdint.hpp>

/*
#ifndef WIN32
    //#include <stdint.h>
#include <cstdint>
#endif

#if !defined(__uint32_t_defined) && !defined(_STDINT_H_)
    #define __uint32_t_defined
    typedef unsigned __int32 uint32_t;
    typedef unsigned __int16 uint16_t;
    typedef unsigned __int8  uint8_t;
#endif // __uint32_t_defined
#ifndef WIN32
    typedef uint32_t UINT32;
    typedef uint16_t UINT16;
    typedef uint8_t  UINT8;
#endif // WIN32
*/
namespace Nortek {

#define PD_NO_BEAMS             (3)

#define PD_MAX_BEAMS            (3)
#define PD_MAX_BINS             (128)
#define PD_MAX_STAGECELLS       (1024)



// TODO: TO BE REMOVED !!!!
#define PD_MAX_WAVEFREQ         (3)
#define PD_MAX_WAVEFREQST       (3)


/*
#define PD_MAX_BEAMS            (4)
#define PD_MAX_BINS             (1024)

#define PD_MAX_STRING_SIZE      1000
#define PD_MAX_BEAM_MATRIX      PD_MAX_BEAMS*PD_MAX_BEAMS
#define PD_MAX_CELL_CELECTION   PD_MAX_BINS / 8
#define PD_MAX_SPECTRUM         400
#define PD_MAX_TIME_SERIES      4096
#define PD_MAX_BANDS            10

enum PdDataHeaderType {
    PD_DATATYPE_STANDARD,
    PD_DATATYPE_PROFILE,
    PD_DATATYPE_WAVE
};

enum PdDataCollectionMethod {
    PD_DATA_COLLECTION_METHOD_MLM,
    PD_DATA_COLLECTION_METHOD_SUV,
};

enum PdInstrumentType {
    PD_INSTRUMENTTYPE_UNKNOWN,
    PD_INSTRUMENTTYPE_AQD,  //Aquadopp
    PD_INSTRUMENTTYPE_AQP,  //Aquaprofile
    PD_INSTRUMENTTYPE_WAV,  //AWAC
};

enum PdDataFormats {
    PD_DATA_ERROR               =       0xA0,
    PD_DATA_EVENT               =       0xA1,
    PD_DATA_HEADER              =       0xA2,
    PD_DATA_SENSOR              =       0xA3,
    PD_DATA_BEAM                =       0xA4,
    PD_DATA_PROFILE             =       0xA5,
    PD_DATA_WAVE                =       0xA6,
    PD_DATA_WAVE_BANDS          =       0xA7,
    PD_DATA_WAVE_SPECTRUM       =       0xA8,
    PD_DATA_TIME_SERIES         =       0xA9,
    PD_DATA_WAVE_PROC_PARAM     =       0xAA
};

enum PdErrorDataFields {
    PD_DF_ERROR_COUNTER,            // 2
    PD_DF_ERROR_CLOCK,              // 6
    PD_DF_ERROR_MSECONDS,           // 2
    PD_DF_ERROR_SOURCE,             // 2
    PD_DF_ERROR_CODE,               // 4
    PD_DF_ERROR_MESSAGE,            // 2 + hErrorStrSize + ( hErrorStrSize % 2);
    PD_DF_ERROR_EXT_FORMAT  = 15,   // 2
};

enum PdEventDataFields {
    PD_DF_EVENT_COUNTER,            // 2
    PD_DF_EVENT_CLOCK,              // 6
    PD_DF_EVENT_MSECONDS,           // 2
    PD_DF_EVENT_ID,                 // 4
    PD_DF_EVENT_MESSAGE,            // 2 + hEventStrSize + ( hEventStrSize % 2 );
    PD_DF_EVENT_EXT_FORMAT = 15,    // 2
};

enum PdHeaderDataFields {
    PD_DF_HEADER_COUNTER,           // 2
    PD_DF_HEADER_CLOCK,             // 6
    PD_DF_HEADER_MSECONDS,          // 2
    PD_DF_HEADER_DATA_TYPE,         // 2
    PD_DF_HEADER_INSTRUMENT_TYPE,   // 2
    PD_DF_HEADER_INSTRUMENT_SERIAL, // 4
    PD_DF_HEADER_N_BEAMS,           // 2
    PD_DF_HEADER_N_RECORDS,         // 2
    PD_DF_HEADER_BEAM_MATRIX,       // 2 * nBeams * nBeams
    PD_DF_HEADER_BLANKING,          // 4
    PD_DF_HEADER_CELLSIZE,          // 4
    PD_DF_HEADER_AVERAGE_INTERVAL,  // 2
    PD_DF_HEADER_MEASUREMENT_LOAD,  // 2
    PD_DF_HEADER_COORDINATE_SYSTEM, // 2
    PD_DF_HEADER_FREQUENCY,         // 2
    PD_DF_HEADER_EXT_FORMAT,        // 2
    PD_DF_HEADER_POWERLEVEL,        // 2
    PD_DF_HEADER_NOISE,             // 1 * nBeams + ( nBeams % 2) 
    PD_DF_HEADER_NOISE_CORRELATION, // 1 * nBeams + ( nBeams % 2)
    PD_DF_HEADER_SAMPLE_FREQ,       // 2
    PD_DF_HEADER_DATA_COLL_METHOD,  // 2
    PD_DF_HEADER_MIN_PRESSURE,      // 4
    PD_DF_HEADER_MAX_PRESSURE,      // 4
    PD_DF_HEADER_AST_WND_SIZE,      // 2
    PD_DF_HEADER_EXT_EXT_FORMAT = 31,   // 2
};

enum PdSensorDataFields {
    PD_DF_SENSOR_COUNTER,           // 2
    PD_DF_SENSOR_CLOCK,             // 6
    PD_DF_SENSOR_MSECONDS,          // 2
    PD_DF_SENSOR_ERROR,             // 2
    PD_DF_SENSOR_STATUS,            // 2
    PD_DF_SENSOR_TEMPERATURE,       // 2
    PD_DF_SENSOR_PRESSURE,          // 4
    PD_DF_SENSOR_HEADING,           // 2
    PD_DF_SENSOR_PITCH,             // 2
    PD_DF_SENSOR_ROLL,              // 2
    PD_DF_SENSOR_BATTERY,           // 2
    PD_DF_SENSOR_SOUNDSPEED,        // 2
    PD_DF_SENSOR_ANALOG1,           // 2
    PD_DF_SENSOR_ANALOG2,           // 2
    PD_DF_SENSOR_EXT_FORMAT = 15    // 2
};

enum PdBeamDataFields {
    PD_DF_BEAM_COUNTER,             // 2
    PD_DF_BEAM_CLOCK,               // 6
    PD_DF_BEAM_MSECONDS,            // 2
    PD_DF_BEAM_NBEAMS,              // 2
    PD_DF_BEAM_VELOCITY,            // 2 * nBeams
    PD_DF_BEAM_DISTANCE,            // 4 * nBeams
    PD_DF_BEAM_NOISE,               // nBeams + ( nBeams % 2 )
    PD_DF_BEAM_CORELATION,          // nBeams + ( nBeams % 2 )
    PD_DF_BEAM_QUALITY,             // nBeams + ( nBeams % 2 )
    PD_DF_BEAM_EXT_FORMAT = 15      // 2
};

enum PdProfileDataFields {
    PD_DF_PROFILE_COUNTER,          // 2
    PD_DF_PROFILE_CLOCK,            // 6
    PD_DF_PROFILE_MSECONDS,         // 2
    PD_DF_PROFILE_ERROR,            // 2
    PD_DF_PROFILE_STATUS,           // 2
    PD_DF_PROFILE_NBEAMS_NCELLS,    // 2
    PD_DF_PROFILE_BEAM_SELECTION,   // 2
    PD_DF_PROFILE_CELL_SELECTION,   // ((nCells-1)/8+1)+((nCells-1)/8+1)%2
    PD_DF_PROFILE_VELOCITY,         // 2 * nCells or 2 * nHighBits(cCells)
    PD_DF_PROFILE_AMPLITUDE,        // 1*nCells+(1*nCells)%2 or 1*nHighBits(cCells)+(1*nHighBits(cCells))%2
    PD_DF_PROFILE_CORRELATION,      // 1*nCells+(1*nCells)%2 or 1*nHighBits(cCells)+(1*nHighBits(cCells))%2
    PD_DF_PROFILE_EXT_FORMAT = 15   // 2
};

enum PdWaveDataFields {
    PD_DF_WAVE_COUNTER,             // 2
    PD_DF_WAVE_CLOCK,               // 6
    PD_DF_WAVE_MSECONDS,            // 2
    PD_DF_WAVE_SPECTRUM_TYPE,       // 2
    PD_DF_WAVE_TM02,                // 2
    PD_DF_WAVE_TP,                  // 2
    PD_DF_WAVE_DIR_TP,              // 2
    PD_DF_WAVE_SPR_TP,              // 2
    PD_DF_WAVE_DIR_MEAN,            // 2
    PD_DF_WAVE_UI,                  // 2
    PD_DF_WAVE_HM0,                 // 2
    PD_DF_WAVE_H3,                  // 2
    PD_DF_WAVE_T3,                  // 2
    PD_DF_WAVE_H10,                 // 2
    PD_DF_WAVE_T10,                 // 2
    PD_DF_WAVE_EXT_FORMAT,          // 2
    PD_DF_WAVE_HMAX,                // 2
    PD_DF_WAVE_TMAX,                // 2
    PD_DF_WAVE_TZ,                  // 2
    PD_DF_WAVE_PRESSURE_MEAN,       // 4
    PD_DF_WAVE_AST_MEAN,            // 4
    PD_DF_WAVE_CURRENT_SPEED_MEAN,  // 2
    PD_DF_WAVE_CURRENT_DIRECTION_MEAN,  // 2
    PD_DF_WAVE_N_BAD_DETECTS,       // 2
    PD_DF_WAVE_ERROR,               // 4
    PD_DF_WAVE_EXT_EXT_FORMAT = 31  // 2
};

enum PdWaveBandsDataFields {
    PD_DF_WAVE_BAND_COUNTER,        // 2
    PD_DF_WAVE_BAND_CLOCK,          // 6
    PD_DF_WAVE_BAND_MSECONDS,       // 2
    PD_DF_WAVE_BAND_N_BANDS,        // 2
    PD_DF_WAVE_BAND_SPECTRUM_TYPE,  // 2
    PD_DF_WAVE_BAND_FREQUENCY_INTERVAL, // 2 * 2 * nBands
    PD_DF_WAVE_BAND_HM0,            // 2 * nBands
    PD_DF_WAVE_BAND_TM02,           // 2 * nBands
    PD_DF_WAVE_BAND_TP,             // 2 * nBands
    PD_DF_WAVE_BAND_DIR_TP,         // 2 * nBands
    PD_DF_WAVE_BAND_DIR_MEAN,       // 2 * nBands
    PD_DF_WAVE_BAND_SPREAD_TP,      // 2 * nBands
    PD_DF_WAVE_BAND_SPREAD_MEAN,    // 2 * nBands
    PD_DF_WAVE_BAND_ERROR,          // 4 * nBands
    PD_DF_WAVE_BAND_EXT_FORMAT = 15 // 2
};

enum PdWaveSpectrumDataFields {
    PD_DF_WAVE_SPECTRUM_COUNTER,                // 2
    PD_DF_WAVE_SPECTRUM_CLOCK,                  // 6
    PD_DF_WAVE_SPECTRUM_MSECONDS,               // 2
    PD_DF_WAVE_SPECTRUM_N_VALUES,               // 2
    PD_DF_WAVE_SPECTRUM_FREQUENCY_INTERVAL,     // 2 * 2
    PD_DF_WAVE_SPECTRUM_N_VALUES_ST,            // 2
    PD_DF_WAVE_SPECTRUM_FREQUENCY_INTERVAL_ST,  // 2 * 2
    PD_DF_WAVE_SPECTRUM_FREQUENSIES,            // 2*max(nValues,nValuesSt)
    PD_DF_WAVE_SPECTRUM_ST_ENERGY,              // 4 + 2 * nValuesSt
    PD_DF_WAVE_SPECTRUM_PRES_ENERGY,            // 4 + 2 * nValues
    PD_DF_WAVE_SPECTRUM_VEL_ENERGY,             // 4 + 2 * nValues
    PD_DF_WAVE_SPECTRUM_DIRECTION,              // 1 * nValues
    PD_DF_WAVE_SPECTRUM_SPREAD,                 // 1 * nValues
    PD_DF_WAVE_SPECTRUM_FOURIER_COEFS,          // 8 * nValues
    PD_DF_WAVE_SPECTRUM_EXT_FORMAT = 15,        // 2
};

enum PdTimeSeriesDataFields {
    PD_DF_TIME_SERIES_COUNTER,                  // 2
    PD_DF_TIME_SERIES_CLOCK,                    // 6
    PD_DF_TIME_SERIES_MSECONDS,                 // 2
    PD_DF_TIME_SERIES_N_VALUES,                 // 2
    PD_DF_TIME_SERIES_AST,                      // 4 * nValues
    PD_DF_TIME_SERIES_EXT_FORMAT = 15,          // 2
};

enum PdWaveProcParamDataFields {
    PD_DF_WAVE_PROC_PAR_COUNTER,                // 2
    PD_DF_WAVE_PROC_PAR_CLOCK,                  // 6
    PD_DF_WAVE_PROC_PAR_MSECONDS,               // 2
    PD_DF_WAVE_PROC_PAR_PROCESSING_METHOD,      // 2
    PD_DF_WAVE_PROC_PAR_FREQUENCY_INTERVAL,     // 2
    PD_DF_WAVE_PROC_PAR_MOUNTING_HEIGHT,        // 2
    PD_DF_WAVE_PROC_PAR_DIRECTION_OFFSET,       // 2
    PD_DF_WAVE_PROC_PAR_PRESSURE_OFFSET,        // 2
    PD_DF_WAVE_PROC_PAR_DIRECTION_SMOOTHING,    // 2
    PD_DF_WAVE_PROC_PAR_FREQUENCY_SMOOTHING,    // 2
    PD_DF_WAVE_PROC_PAR_SPECTRUM_TYPE,          // 2
    PD_DF_WAVE_PROC_PAR_N_DIRECTIONS,           // 2
    PD_DF_WAVE_PROC_PAR_BEAM_SELECTION,         // 2
    PD_DF_WAVE_PROC_PAR_EXT_FORMAT = 15,        // 2
};

*/

enum PdDataFormats {
    PD_USER_CONFIGURATION           = 0x00,
    PD_HEAD_CONFIGURATION           = 0x04,
    PD_HARDWARE_CONFIGURATION       = 0x05,
    PD_CURRENTS_VELOCITY_PROFILE    = 0x20,
    PD_WAVE_PARAMETER_DATA          = 0x60,
    PD_WAVE_BAND_DATA               = 0x61,
    PD_WAVE_SPECTRUM_DATA           = 0x62,
    PD_WAVE_FOURIER_COEFF           = 0x63
};


#pragma pack(push)
#pragma pack(1)
typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A0 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;
} PdGenericHeader;

typedef struct {
   unsigned char  cMinute;                      // minute
   unsigned char  cSecond;                      // second
   unsigned char  cDay;                         // day
   unsigned char  cHour;                        // hour
   unsigned char  cYear;                        // year
   unsigned char  cMonth;                       // month
} PdClock;

/*
typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A0 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned short  hErrorSource;               // BIT  3 : what reported the error
    unsigned long   hErrorCode;                 // BIT  4 : the error code
    unsigned short  hErrorStrSize;              // BIT  5 : size of error message
    char        strErrorMessage[PD_MAX_STRING_SIZE];    //        : error string ZT
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    unsigned short  hChecksum;                  // checksum
} PdErrorData;

typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A1 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;                // Specifies which of the field below that are present
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned long   hEventId;                   // BIT  3 : what reported the error
    unsigned short  hEventStrSize;              // BIT  4 : size of event message
    char        strEventMessage[PD_MAX_STRING_SIZE];    //        : event string ZT
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    unsigned short  hChecksum;                  // checksum
} PdEventData;

typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A2 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;                // Specifies which of the field below that are present
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned short  hDataType;                  // BIT  3 : what data type is this header for
    unsigned short  hInstrumentType;            // BIT  4 : instrument type
    unsigned long   hInstrumentSerial;          // BIT  5 : instrument serial no.
    unsigned short  nBeams;                     // BIT  6 : number of beams [#]
    unsigned short  nRecords;                   // BIT  7 : number of records collected [#]
    short       hBeamMatrix[PD_MAX_BEAMS][PD_MAX_BEAMS];    // BIT  8 : beam to xyz matrix (nBeams*nBeams elements) [1/32767];
    long        hBlankingDistance;              // BIT  9 : blanking distance in [mm]
    long            hCellSize;                  // BIT 10 : cell size in [mm]
    unsigned short  hAverageInterval;           // BIT 11 : average interval used for data collection [sec]
    unsigned short  hMeasurementLoad;           // BIT 12 : [1/65536]
    unsigned short  hCoordinateSystem;          // BIT 13 : what coordinate system the data is represented in (beam, xyz, enu)
    unsigned short  hFrequency;                 // BIT 14 : [kHz]
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    unsigned short  hPowerLevel;                // BIT 16 : [counts]
    unsigned char   cNoise[PD_MAX_BEAMS];       // BIT 17 : noise for each beam [counts]
    unsigned char   cNoiseCorrelation[PD_MAX_BEAMS];        // BIT 18 : noise correlation for each beam [1/256]
    unsigned short  hSampleFrequency;           // BIT 19 : sample frequency [Hz]
    unsigned short  hDataCollectionMethod;      // BIT 20 : number of samples in AST window [mm]
    long            hMinPressure;               // BIT 21 : minimum pressure [mm]
    long            hMaxPressure;               // BIT 22 : minimum pressure [mm]
    unsigned short  hASTWindowsSize;            // BIT 23 : number of samples in AST window [#]
    unsigned short  hExtExtDataFormat;          // BIT 31 : extended data format
    unsigned short  hChecksum;                  // checksum
} PdHeaderData;

typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A3 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned short  hError;                     // BIT  3 : error code
    unsigned short  hStatus;                    // BIT  4 : status
    short           hTemperature;               // BIT  5 : temperature [0.01 deg C]
    long            hPressure;                  // BIT  6 : [mm]
    unsigned short  hHeading;                   // BIT  7 : compass heading [0.01 deg]
    short           hPitch;                     // BIT  8 : pitch [0.01 deg]
    short           hRoll;                      // BIT  9 : roll [0.01 deg]
    unsigned short  hBattery;                   // BIT 10 : battery voltage [0.01 V] 
    unsigned short  hSoundSpeed;                // BIT 11 : speed of sound [0.1 m/s]
    unsigned short  hAnaIn1;                    // BIT 12 : analog input 1 [counts]
    unsigned short  hAnaIn2;                    // BIT 13 : analog input 2 [counts]
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    short           hChecksum;                  // checksum
} PdSensorData;

typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A4 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;                // Specifies which of the field below that are present
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned short  nBeams;                     // BIT  3 : number of beams [#]
    short       hVelocity[PD_MAX_BEAMS];        // BIT  4 : velocity [mm/s]
    long        hDistance[PD_MAX_BEAMS];        // BIT  5 : distance in [mm]
    unsigned char   hNoise[PD_MAX_BEAMS];       // BIT  6 : noise, padded to even bytes [counts]
    unsigned char   hCorrelation[PD_MAX_BEAMS]; // BIT  7 : correlation between beams, padded to even bytes [1/256]
    unsigned char   hQuality[PD_MAX_BEAMS];     // BIT  8 : quality for distance data [counts]
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    unsigned short  hChecksum;                  // checksum
} PdBeamData;

typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A5 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;                // Specifies which of the field below that are present
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned short  hError;                     // BIT  3 : error code
    unsigned short  hStatus;                    // BIT  4 : status
    unsigned char   nBeams;                     // BIT  5 : Number of beams [#]
    unsigned char   nCells;                     //        : Maximum number of cells
    unsigned short  cBeams;                     // BIT  6 : Bitfield that specifies which beams that are present
    unsigned char   cCells[PD_MAX_CELL_CELECTION];      // BIT  7 : Bitfield that specifies which cells that are present, size is nCells/8, padded to even bytes
    short           hVel[PD_MAX_BEAMS][PD_MAX_BINS];    // BIT  8 : velocity [mm/s] or [0.1mm/s] depending on status
    unsigned char   cAmp[PD_MAX_BEAMS][PD_MAX_BINS];    // BIT  9 : amplitude, padded to even bytes [counts]
    unsigned char   cCorr[PD_MAX_BEAMS][PD_MAX_BINS];   // BIT 10 : Standard deviation, padded to even bytes
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    unsigned short  hChecksum;                  // checksum
} PdProfileData;

typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A6 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;                // Specifies which of the field below that are present
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned short  hSpectrumType;              // BIT  3 : spectrum used for calculation
    unsigned short  hTm02;                      // BIT  4 : Mean period spectrum based [0.01 sec]
    unsigned short  hTp;                        // BIT  5 : Peak period [0.01 sec]
    unsigned short  hDirTp;                     // BIT  6 : Direction at Tp [0.01 deg]
    unsigned short  hSprTp;                     // BIT  7 : Spreading at Tp [0.01 deg]
    unsigned short  hDirMean;                   // BIT  8 : Mean wave direction [0.01 deg]
    unsigned short  hUI;                        // BIT  9 : Unidirectivity index [1/32767]
    unsigned short  hHm0;                       // BIT 10 : Spectral significant wave height [mm]
    unsigned short  hH3;                        // BIT 11 : ST significant (33%) wave height [mm]
    unsigned short  hT3;                        // BIT 12 : ST significant (33%) wave period [0.01 sec]
    unsigned short  hH10;                       // BIT 13 : ST wave height, top 10% [mm]
    unsigned short  hT10;                       // BIT 14 : ST wave period, top 10% [0.01 sec]
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    unsigned short  hHmax;                      // BIT 16 : ST max wave height in wave ensemble [mm]
    unsigned short  hTmax;                      // BIT 17 : ST max period in wave ensemble [0.01 sec]
    unsigned short  hTz;                        // BIT 18 : ST mean zero-crossing period [0.01 sec]
    long        hPressureMean;                  // BIT 19 : Mean pressure during burst [mm]
    long        hSTMean;                        // BIT 20 : Mean ST distance to surface during burst [mm]
    unsigned short  hCurSpeedMean;              // BIT 21 : Mean current speed - wave cells [mm/sec]
    unsigned short  hCurDirMean;                // BIT 22 : Mean current direction - wave cells [0.1 deg]
    unsigned short  hNumBadDet;                 // BIT 23 : Number of ST Bad detects [#]
    unsigned long   hError;                     // BIT 24 : Error Code for bad data
    unsigned short  hExtExtDataFormat;          // BIT 31 : extended data format
    unsigned short  hChecksum;                  // checksum
} PdWaveData;

typedef struct {                        
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A7 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;                // Specifies which of the field below that are present
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned short  nBands;                     // BIT  3 : number of wave bands in data
    unsigned short  hSpectrumType;              // BIT  4 : spectrum used for calculation
    unsigned short  hLowFrequency[PD_MAX_BANDS];    // BIT  5 : low frequency in [0.001 Hz]
    unsigned short  hHighFrequency[PD_MAX_BANDS];   //        : high frequency in [0.001 Hz]
    unsigned short  hHm0[PD_MAX_BANDS];         // BIT  6 : Spectral significant wave height [mm]
    unsigned short  hTm02[PD_MAX_BANDS];        // BIT  7 : Mean period spectrum based [0.01 sec]
    unsigned short  hTp[PD_MAX_BANDS];          // BIT  8 : Peak period [0.01 sec]
    unsigned short  hDirTp[PD_MAX_BANDS];       // BIT  9 : Direction at Tp [0.01 deg]
    unsigned short  hDirMean[PD_MAX_BANDS];     // BIT 10 : Mean wave direction [0.01 deg]
    unsigned short  hSprTp[PD_MAX_BANDS];       // BIT 11 : Spreading at Tp [0.01 deg]
    unsigned short  hSprMean[PD_MAX_BANDS];     // BIT 12 : Mean spread [0.01 deg]
    unsigned long   hError[PD_MAX_BANDS];       // BIT 13 : Error Code for bad data
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    unsigned short  hChecksum;                  // checksum
} PdWaveBandsData;

typedef struct {                        
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A8 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;                // Specifies which of the field below that are present
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned short  nValues;                    // BIT  3 : number of spectrum values
    unsigned short  hLowFrequency;              // BIT  4 : low frequency in [0.001 Hz]
    unsigned short  hHighFrequency;             //        : high frequency in [0.001 Hz]
    unsigned short  nValuesST;                  // BIT  5 : number of spectrum values for ST
    unsigned short  hLowFrequencyST;            // BIT  6 : low frequency for ST in [0.001 Hz]
    unsigned short  hHighFrequencyST;           //        : high frequency for ST in [0.001 Hz]
    unsigned short  hFrequencies[PD_MAX_SPECTRUM];  // BIT  7 : Frequency values
    unsigned long   hSTEnergyMultiplier;    
// BIT  8 : ST energy specturm multiplier
    unsigned short  hSTEnergy[PD_MAX_SPECTRUM];     //  ST Spectra [cm^2/Hz]
    unsigned long   hPressureEnergyMultiplier;      // BIT  9 : Pressure energy spectrum multiplier
    unsigned short  hPresEnergy[PD_MAX_SPECTRUM];   //  Pressure spectra [cm^2/Hz]
    unsigned long   hVelEnergyMultiplier;           // BIT 10 : Velocity energy spectrum multiplier
    unsigned short  hVelEnergy[PD_MAX_SPECTRUM];    //  Velocity spectra [cm^2/Hz]
    unsigned char   hDirSpec[PD_MAX_SPECTRUM];      // BIT 11 : direction spectrum in [0.01 deg]
    unsigned char   hSpreadDirSpec[PD_MAX_SPECTRUM];// BIT 12 : spread direction spectrum in [0.01 deg]
    short           hA1[PD_MAX_SPECTRUM];           // BIT 13 : Fourier coefficients in [1/32767]
    short           hB1[PD_MAX_SPECTRUM];
    short           hA2[PD_MAX_SPECTRUM];
    short           hB2[PD_MAX_SPECTRUM];
    unsigned short  hExtDataFormat;                 // BIT 15 : extended data format
    unsigned short  hChecksum;                      // checksum
} PdWaveSpectrumData;

typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // A9 (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;                // Specifies which of the field below that are present
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  3 : milliseconds
    unsigned short  nValues;                    // BIT  3 : number of time series values
    long            hAST[PD_MAX_TIME_SERIES];   // BIT  4 : AST time series [mm]
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    unsigned short  hChecksum;                  // checksum
} PdTimeSeriesData;

typedef struct {
    unsigned char   cSync;                      // A5 (hex)
    unsigned char   cID;                        // AA (hex)
    unsigned short  hSize;                      // size in words
    unsigned short  hDataFormat;                // Specifies which of the field below that are present
    unsigned short  hCounter;                   // BIT  0 : data package counter
    PdClock     clock;                          // BIT  1 : date and time
    unsigned short  hMilliSeconds;              // BIT  2 : milliseconds
    unsigned short  hProcessingMethod;          // BIT  3 : processing method
    unsigned short  hLowFrequency;              // BIT  4 : low frequency in [0.001 Hz]
    unsigned short  hHighFrequency;             //        : high frequency in [0.001 Hz]
    unsigned short  hMountingHeight;            // BIT  5 : mounting height from bottom [mm]
    unsigned short  hDirectionOffset;           // BIT  6 : offset in degrees [0.1 deg]
    unsigned short  hPressureOffset;            // BIT  7 : pressure offset [mm]
    unsigned short  nDirSmoothing;              // BIT  8 : Number of bins in FFT to smooth in processing [#]
    unsigned short  nFreqSmoothing;             // BIT  9 : Number of bins in FFT to smooth in processing [#]
    unsigned short  hSpectrumType;              // BIT 10 : spectrum used for calculation
    unsigned short  nDirections;                // BIT 11 : number of directions for full directional spectrum output
    unsigned short  hBeamSelection;             // BIT 12 : beam selection for calculation, bitfield (bit set if beam selected)
    unsigned short  hExtDataFormat;             // BIT 15 : extended data format
    unsigned short  hChecksum;                  // checksum
} PdWaveProcParamData;

*/


//////////////////////////////////////////////////////////////////////////////
//// AWAC Hardware Configuration
typedef struct {
    unsigned char       cSync; // sync = 0xa5
    unsigned char       cId; // identification (0x05)
    unsigned short      hSize; // size of structure (words)
    union {
        unsigned char       raw[14];        // instrument type and serial number
        struct {
            const char          serNo[8];       // serial number text (not \0 terminated)
            unsigned short      prologSerNo;    // Prolog serial number
            const char          prologVer[4];   // Prolog version text (not \0 terminated)
        } data;
    } info;
    union {
        unsigned short      raw;            // board configuration:
                                            //  bit 0: Recorder installed (0=no, 1=yes)
                                            //  bit 1: Compass installed (0=no, 1=yes)
        struct {
            bool                recorder:1;     // is recorder installed
            bool                compass:1;      // is compass installed
            int                 reserved:14;    // -- not used for now --
        } data;
    } config;
    unsigned short      hFrequency;     // board frequency [kHz]
    unsigned short      nPICversion;    // PIC code version number
    unsigned short      nHWrevision;    // Hardware revision
    unsigned short      hRecSize;       // Recorder size (*65536 bytes)
    union {
        unsigned short      raw;            // status: bit 0: Velocity range (0=normal, 1=high)
        struct {
            bool                vRange:1;       // velocity range
            int                 reserved:15;
        } data;
    } status;
    unsigned char       reserved[12];   // spare
    unsigned long       hFWversion;     // firmware version
    unsigned short      hChecksum;      // b58c(hex) + sum of all words in structure
} PdHardwareConfiguration;


//////////////////////////////////////////////////////////////////////////////
//// AWAC Head Configuration
typedef struct {
    uint8_t             cSync;          // sync = 0xa5
    uint8_t             cId;            // identification (0x04)
    uint16_t            hSize;          // size of structure (words)
    union {
        uint16_t            raw;           // head configuration:
                                           //  bit 0: Pressure sensor (0=no, 1=yes)
                                           //  bit 1: Magnetometer sensor (0=no, 1=yes) bit 2: Tilt sensor (0=no, 1=yes)
                                           //  bit 3: Tilt sensor mounting (0=up, 1=down)
        struct {
            bool                pressureSens:1; // pressure sensor
            bool                magnetometer:1; // magnetometer sensor
            bool                tiltSens:1;     // tilt sensor
            uint16_t            tiltOrientation:1;  // tilt sensor mounting (0=up, 1=down)
            uint16_t            reserved:12;
        } data;
    } config;
    uint16_t            hHeadFrequency; // head frequency [kHz]
    uint16_t            hHeadType;      // head type
    const char          headSerNo[12];  // head serial number text (not \0 terminated)
    uint8_t             systemData[176];// system data
    uint8_t             reserved[22];   // spare
    uint16_t            nBeams;         // number of beams
    uint16_t            hChecksum;      // = b58c(hex) + sum of all words in structure
} PdHeadConfiguration;



//////////////////////////////////////////////////////////////////////////////
//// AWAC User Configuration
typedef struct {
    uint8_t             cSync;          // sync = 0xa5
    uint8_t             cId;            // identification (0x00)
    uint16_t            hSize;          // size of structure (words)
    uint16_t            T1;             // transmit pulse length (counts)
    uint16_t            T2;             // blanking distance (counts)
    uint16_t            T3;             // receive length (counts)
    uint16_t            T4;             // time between pings (counts)
    uint16_t            T5;             // time between burst sequences (counts)
    uint16_t            nPings;         // number of beam sequences per burst
    uint16_t            avgInterval;    // average interval in seconds; For Vector AvgInterval = 512/Sampling Rate
    uint16_t            nBeams;         // number of beam
    union {
        uint16_t            raw;            // timing controller mode
                                            //   bit 1: profile (0=single, 1=continuous)
                                            //   bit 2: mode (0=burst, 1=continuous)
                                            //   bit 5: power level (0=1, 1=2, 0=3, 1=4)
                                            //   bit 6: power level (0 0 1 1 )
                                            //   bit 7: synchout position (0=middle of sample, 1=end of sample (Vector))
                                            //   bit 8: sample on synch (0=disabled,1=enabled, rising edge)
                                            //   bit 9: start on synch (0=disabled,1=enabled, rising edge)
        struct {
            uint16_t            profile:1;      // bit 1: profile (0=single, 1=continuous)
            uint16_t            mode:1;         // bit 2: mode (0=burst, 1=continuous)
            uint16_t            unknown1:4;     // ??? error in manual
            uint16_t            synchout:1;     // synchout position (0=middle of sample, 1=end of sample (Vector))
            bool                sampleOnSync:1; // sample on synch (0=disabled,1=enabled, rising edge)
            bool                startOnSync:1;  // start on synch (0=disabled,1=enabled, rising edge)
            uint16_t            reserved:7;
        } data;
    } timCtrlReg;
    union {
        uint16_t            raw;            // power control register
                                            //  bit 5: power level (0=1, 1=2, 0=3, 1=4)
                                            //  bit 6: power level (0 0 1 1 )
    } pwrCtrlReg;
    uint16_t            tA1;            // not used
    uint16_t            tB0;            // not used
    uint16_t            tB1;            // not used
    uint16_t            compassUpdRate; // compass update rate
    uint16_t            coordSystem;    // coordinate system (0=ENU, 1=XYZ, 2=BEAM)
    uint16_t            nBins;          // number of cells
    uint16_t            binLength;      // cell size
    uint16_t            measInterval;   // measurement interval
    const char          deployName[6];  // deploy name text (not \0 terminated)
    uint16_t            wrapMode;       // recorder wrap mode (0=NO WRAP, 1=WRAP WHEN FULL)
    PdClock             clockDeploy;    // deployment start time
    uint32_t            diagInterval;   // number of seconds between diagnostics measurements
    union {
        uint16_t            raw;            // mode
                                            //  bit 0: use user specified sound speed (0=no, 1=yes)
                                            //  bit 1: diagnostics/wave mode 0=disable, 1=enable)
                                            //  bit 2: analog output mode (0=disable, 1=enable)
                                            //  bit 3: output format (0=Vector, 1=ADV) bit 4: scaling (0=1 mm, 1=0.1 mm)
                                            //  bit 5: serial output (0=disable, 1=enable) bit 6: reserved EasyQ
                                            //  bit 7: stage (0=disable, 1=enable)
                                            //  bit 8: output power for analog input (0=disable, 1=enable)
        struct {
            bool                userSoundSpd:1; // bit 0: use user specified sound speed (0=no, 1=yes)
            bool                diagWaveMode:1; // bit 1: diagnostics/wave mode 0=disable, 1=enable)
            bool                analogOutput:1; // bit 2: analog output mode (0=disable, 1=enable)
            uint16_t            outputFormat:1; // bit 3: output format (0=Vector, 1=ADV)
            uint16_t            scaling:1;      // bit 4: scaling (0=1 mm, 1=0.1 mm)
            bool                serialOutput:1; // bit 5: serial output (0=disable, 1=enable)
            uint16_t            reserved1:1;    // bit 6: reserved EasyQ
            bool                stage:1;        // bit 7: stage (0=disable, 1=enable)
            bool                outPower:1;     // bit 8: output power for analog input (0=disable, 1=enable)
            uint16_t            reserved2:7;
        } data;
    } mode;
    uint16_t            adjSoundSpeed;  // user input sound speed adjustment factor
    uint16_t            nSampDiag;      // # samples (AI if EasyQ) in diagnostics mode
    uint16_t            nBeamsCellDiag; // # beams / cell number to measure in diagnostics mode
    uint16_t            nPingsDiag;     // # pings in diagnostics/wave mode
    union {
        uint16_t            raw;            // mode test:
                                            //  bit 0: correct using DSP filter (0=no filter, 1=filter)
                                            //  bit 1: filter data output (0=total corrected velocity,1=only correction part)
        struct {
            uint16_t        dspFilter:1;        // bit 0: correct using DSP filter (0=no filter, 1=filter)
            uint16_t        filterDataOutput:1; // bit 1: filter data output (0=total corrected velocity,1=only correction part)
            uint16_t        reserved:14;
        } data;
    } modeTest;
    uint16_t            anaInAddr;      // analog input address
    uint16_t            swVersion;      // software version [(ver/10000).(ver/100)%100.(ver%100)]
    uint16_t            reserved1;      // spare
    uint8_t             velAdjTable[180];   // velocity adjustment table
    uint8_t             comments[180];  // file comments
    union {
        uint16_t            raw;            // wave measurement mode
                                            //  bit 0: data rate (0=1 Hz, 1=2 Hz)
                                            //  bit 1: wave cell position (0=fixed, 1=dynamic)
                                            //  bit 2: type of dynamic position (0=pct of mean pressure, 1=pct of min re)
        struct {
            uint16_t            dataRate:1;     // bit 0: data rate (0=1 Hz, 1=2 Hz)
            uint16_t            waveCellPos:1;  // bit 1: wave cell position (0=fixed, 1=dynamic)
            uint16_t            dynamicPosType:1;   // bit 2: type of dynamic position (0=pct of mean pressure, 1=pct of min re)
            uint16_t            reserved:13;
        } data;
    } waveMode;
    uint16_t            dynPercPos;     // percentage for wave cell positioning (=32767×#%/100) (# means number of)
    uint16_t            wT1;            // wave transmit pulse
    uint16_t            wT2;            // fixed wave blanking distance (counts)
    uint16_t            wT3;            // wave measurement cell size
    uint16_t            nSamp;          // number of diagnostics/wave samples
    uint16_t            wA1;            // not used
    uint16_t            wB0;            // not used
    uint16_t            wB1;            // not used for most instruments; For Vector it holds Number of Samples Per Burst
    uint16_t            reserved2;      // spare
    uint16_t            anaOutScale;    // analog output scale factor (16384=1.0, max=4.0)
    uint16_t            corrThresh;     // correlation threshold for resolving ambiguities
    uint16_t            reserved3;      // spare
    uint16_t            tiLag2;         // transmit pulse length (counts) second lag
    uint8_t             reserved4[30];  // spare
    uint8_t             qualConst[16];  // stage match filter constants (EZQ)
    uint16_t            hChecksum;      // = b58c(hex) + sum of all words in structuree
} PdUserConfiguration;




//////////////////////////////////////////////////////////////////////////////
//// AWAC velocity profile data (variable length)
typedef struct {
    unsigned char       cSync;          // (0) sync = 0xa5
    unsigned char       cId;            // (1) identification (0x20)
    unsigned short      hSize;          // (2) size of structure (words)
    PdClock             clock;          // (4) date and time
    int16_t             hError;         // (10) error code
    unsigned short      hAnaIn1;        // (12) analog input 1
    unsigned short      hBattery;       // (14) battery voltage (0.1 V) [n * 0.1 = V]
    union {                             // (16)
        unsigned short      hSoundspeed;    // speed of sound (0.1 m/s) [n * 0.1 = m/s]
        unsigned short      hAnaIn2;        // analog input 2
    } u;
    short               hHeading;       // (18) compass heading (0.1 deg) [n * 0.1 = °]
    short               hPitch;         // (20) compass pitch (0.1 deg) [n * 0.1 = °]
    short               hRoll;          // (22) compass roll (0.1 deg) [n * 0.1 = °]
    unsigned char       cPressureMSB;   // (24) pressure MSB [n / 1000.0 = dBar]
    char                cStatus;        // (25) status
    unsigned short      hPressureLSW;   // (26) pressure LSW
    short               hTemperature;   // (28) temperature (0.01 deg C) [n / 100.0 = °C]
    short               hSpare[44];     // (30) actual size of the following = nBeams*nBins*3 + 2
    // ATTENTION:
    //  The rest of the structure comprises of:
    //  ----
    //  int16_t         hVel[ PD_NO_BEAMS ][ number_of_bins ]   // velocity (use the AWAC_PROFILE_GET_NUM_BINS() macro to compute number_of_bins) [n / 10.0 = cm/s]
    //  uint8_t         cAmp[ PD_NO_BEAMS ][ number_of_bins ]   // amplitude ( '' )
    //  uint8_t         cFill;                                  // NOTE: present _only_ if nCells % 2 != 0
    //  int16_t         hChecksum;                              // checksum
    //  ----
    uint8_t             data[1];
} PdProfHeader;
#define AWAC_PROFILE_HEADER_SIZE        (sizeof(Nortek::PdProfHeader)-sizeof(uint8_t))
#define AWAC_PROFILE_GET_NUM_BINS(_awacProfStruct)                                  \
    (uint16_t)                                                                      \
    (                                                                               \
     (                                                                              \
      ((_awacProfStruct)->hSize * 2) - AWAC_PROFILE_HEADER_SIZE - 2 /* hChecksum */ \
     ) / 3 /* no.beams */ / 3 /* sizeof(short) + sizeof(unsigned char) */           \
    )
#define AWAC_PROFILE_GET_VEL_OFFSET(_awacProfStruct)    (0)
#define AWAC_PROFILE_GET_AMP_OFFSET(_awacProfStruct)                                \
    (AWAC_PROFILE_GET_NUM_BINS(_awacProfStruct) * 3 /* no.beams */ * sizeof(short))
#define AWAC_PROFILE_GET_CHECKSUM_OFFSET(_awacProfStruct)                           \
    ((_awacProfStruct)->hSize * 2 - AWAC_PROFILE_HEADER_SIZE - sizeof(short))


//////////////////////////////////////////////////////////////////////////////
// Wave Parameter Data (80 bytes)
typedef struct {
    uint8_t             cSync; // A5 (hex)
    uint8_t             cId; // 60 (hex)
    uint16_t            hSize; // size in words
    PdClock             clock; // date and time
    uint8_t             cSpectrumType; // spectrum used for calculation
    uint8_t             cProcMethod; // processing method used in actual calculation
    uint16_t            hHm0; // Spectral significant wave height [mm]
    uint16_t            hH3; // AST significant wave height (mean of largest 1/3) [mm]
    uint16_t            hH10; // AST wave height(mean of largest 1/10) [mm] [n / 1000.0 = m]
    uint16_t            hHmax; // AST max wave height in wave ensemble [mm] [n / 1000.0 = m]
    uint16_t            hTm02; // Mean period spectrum based [0.01 sec] [n / 100.0 = s]
    uint16_t            hTp; // Peak period [0.01 sec] [n / 100.0 = s]
    uint16_t            hTz; // AST mean zero-crossing period [0.01 sec] [n / 100.0 = s]
    uint16_t            hDirTp; // Direction at Tp [0.01 deg] [n / 100.0 = °]
    uint16_t            hSprTp; // Spreading at Tp [0.01 deg] [n / 100.0 = °]
    uint16_t            hDirMean; // Mean wave direction [0.01 deg] [n / 100.0 = °]
    uint16_t            hUI; // Unidirectivity index [1/65535]
    int32_t             lPressureMean; // Mean pressure during burst [0.001 dbar] [n / 1000.0 = dBar]
    uint16_t            hNumNoDet; // Number of ST No detects [#]
    uint16_t            hNumBadDet; // Number of ST Bad detects [#]
    uint16_t            hCurSpeedMean; // Mean current speed - wave cells [mm/sec] [n / 10.0 = cm/s]
    uint16_t            hCurDirMean; // Mean current direction - wave cells [0.01 deg] [n / 100.0 = °]
    uint32_t            lError; // Error Code for bad data
    uint16_t            hSpares[14];
    uint16_t            hChecksum; // checksum
} PdWaveParData;


//////////////////////////////////////////////////////////////////////////////
// Wave Band data (48 bytes)
typedef struct {
    unsigned char       cSync; // A5 (hex)
    unsigned char       cId; // 61 (hex)
    unsigned short      hSize; // size in words
    PdClock             clock; // date and time
    unsigned char       cSpectrumType; // spectrum used for calculation
    unsigned char       cProcMethod; // processing method used in actual calculation
    unsigned short      hLowFrequency; // low frequency in [0.001 Hz] [n / 1000.0 = Hz]
    unsigned short      hHighFrequency; // high frequency in [0.001 Hz] [n / 1000.0 = Hz]
    unsigned short      hHm0; // Spectral significant wave height [mm] [n / 1000.0 = m]
    unsigned short      hTm02; // Mean period spectrum based [0.01 sec] [n / 100.0 = s]
    unsigned short      hTp; // Peak period [0.01 sec] [n / 100.0 = s]
    unsigned short      hDirTp; // Direction at Tp [0.01 deg] [n / 100.0 = °]
    unsigned short      hDirMean; // Mean wave direction [0.01 deg] [n / 100.0 = °]
    unsigned short      hSprTp; // Spreading at Tp [0.01 deg] [n / 100.0 = °]
    uint32_t            lError; // Error Code for bad data
    unsigned short      hSpares[7];
    unsigned short      hChecksum; // checksum
} PdWaveBandsData;


//////////////////////////////////////////////////////////////////////////////
// Wave Spectrum data (Variable size)
typedef struct {
    unsigned char       cSync; // A5 (hex)
    unsigned char       cId; // 62 (hex)
    unsigned short      hSize; // size in words
    PdClock             clock; // date and time
    unsigned char       cSpectrumType; // spectrum used for calculation
    unsigned char       cSpare;
    unsigned short      hNumSpectrum; // number of spectral bins (default 98)
    unsigned short      hLowFrequency; // low frequency in [0.001 Hz] [n / 1000.0 = Hz]
    unsigned short      hHighFrequency; // high frequency in [0.001 Hz] [n / 1000.0 = Hz]
    unsigned short      hStepFrequency; // frequency step in [0.001 Hz] [n / 1000.0 = Hz]
    unsigned short      hSpare[9];
    uint32_t            lEnergyMultiplier; // AST energy spectrum multiplier [cm^2/Hz]
    // ATTENTION:
    //  The rest of the structure comprises of:
    //  ----
    //  uint16_t        hEnergy[ PD_MAX_WAVEFREQST ]            // AST Spectra [0 - 1/65535] -
    //  uint16_t        hChecksum;                              // checksum
    //  ----
    uint8_t             data[1];
} PdWaveSpectrumHeader; // variable size (hNumSpectrum)
#define AWAC_WAVE_SPECTRUM_HEADER_SIZE      (sizeof(Nortek::PdWaveSpectrumHeader)-sizeof(uint8_t))
#define AWAC_WAVE_SPECTRUM_GET_NUM_FREQ(_awacWaveSpectStruct)                       \
    (uint16_t)                                                                      \
    (                                                                               \
     (                                                                              \
      ((_awacWaveSpectStruct)->hSize * 2) - AWAC_WAVE_SPECTRUM_HEADER_SIZE - 2 /* hChecksum */ \
     ) / 2 /* sizeof(uint16_t) */                                                   \
    )
#define AWAC_WAVE_SPECTRUM_GET_FREQ_OFFSET(_awacWaveSpectStruct)        (0)
#define AWAC_WAVE_SPECTRUM_GET_CHECKSUM_OFFSET(_awacWaveSpectStruct)                \
    ((_awacWaveSpectStruct)->hSize * 2 - AWAC_WAVE_SPECTRUM_HEADER_SIZE - sizeof(uint16_t))


//////////////////////////////////////////////////////////////////////////////
// Wave Fourier Coefficients (Variable size)
typedef struct {
    unsigned char       cSync; // A5 (hex)
    unsigned char       cId; // 63 (hex)
    unsigned short      hSize; // size in words
    PdClock             clock; // date and time
    unsigned char       cSpare;
    unsigned char       cProcMethod; // processing method used in actual calculation
    unsigned short      hNumSpectrum; // number of spectral bins (default 49)
    unsigned short      hLowFrequency; // low frequency in [0.001 Hz] [n / 1000.0 = Hz]
    unsigned short      hHighFrequency; // high frequency in [0.001 Hz] [n / 1000.0 = Hz]
    unsigned short      hStepFrequency; // frequency step in [0.001 Hz] [n / 1000.0 = Hz]
    unsigned short      hSpares[5];
    // ATTENTION:
    //  The rest of the structure comprises of:
    //  ----
    //  int16_t         hA1[PD_MAX_WAVEFREQ]; // Fourier coefficients in [+/- 1/32767]
    //  int16_t         hB1[PD_MAX_WAVEFREQ]; // 0 - hNumSpectrum-1
    //  int16_t         hA2[PD_MAX_WAVEFREQ];
    //  int16_t         hB2[PD_MAX_WAVEFREQ];
    //  uint16_t        hChecksum; // checksum
    //  ----
    uint8_t             data[1];
} PdWaveFourierCoeff;
#define AWAC_WAVE_FOURIER_COEFF_HEADER_SIZE     (sizeof(Nortek::PdWaveFourierCoeff)-sizeof(uint8_t))
#define AWAC_WAVE_FOURIER_COEFF_GET_NUM(_awacWaveFourierCoeff)                      \
    (uint16_t)                                                                      \
    (                                                                               \
     (                                                                              \
      ((_awacWaveFourierCoeff)->hSize * 2) - AWAC_WAVE_FOURIER_COEFF_HEADER_SIZE - 2 /* hChecksum */ \
     ) / (4*2) /* 4*sizeof(int16_t) */                                              \
    )
#define AWAC_WAVE_FOURIER_COEFF_GET_hA1_OFFSET(_awacWaveFourierCoeff)   (0*2)
#define AWAC_WAVE_FOURIER_COEFF_GET_hB1_OFFSET(_awacWaveFourierCoeff)               \
    (AWAC_WAVE_FOURIER_COEFF_GET_NUM(_awacWaveFourierCoeff)) * (1*2)
#define AWAC_WAVE_FOURIER_COEFF_GET_hA2_OFFSET(_awacWaveFourierCoeff)               \
    (AWAC_WAVE_FOURIER_COEFF_GET_NUM(_awacWaveFourierCoeff)) * (2*2)
#define AWAC_WAVE_FOURIER_COEFF_GET_hB2_OFFSET(_awacWaveFourierCoeff)               \
    (AWAC_WAVE_FOURIER_COEFF_GET_NUM(_awacWaveFourierCoeff)) * (3*2)
#define AWAC_WAVE_FOURIER_COEFF_GET_CHECKSUM_OFFSET(_awacWaveFourierCoeff)               \
    ((_awacWaveFourierCoeff)->hSize * 2 - AWAC_WAVE_FOURIER_COEFF_HEADER_SIZE - sizeof(uint16_t))


#pragma pack(pop)
}

#endif // _NORTEK_AWAC_HPP
