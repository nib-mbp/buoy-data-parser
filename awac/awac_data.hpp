#ifndef _AWAC_DATA_HPP
#define _AWAC_DATA_HPP

#include <iostream>
#include <string>
#include <map>
#include <list>

#include <boost/cstdint.hpp>

#include "nortek_awac.hpp"

namespace mbp {
namespace buoy {

class AwacData {
protected:
    uint8_t *m_data_p;
    const uint8_t *m_orgData_p;
    template<typename _T>
    inline _T getValAndIncPtr(void) {
        if (sizeof(_T) >= 2) {
            size_t ptrDiff = m_data_p - m_orgData_p;
            if ((ptrDiff % 2) != 0) {
                ++m_data_p;
            }
        }
        _T rv = *reinterpret_cast<_T *>(m_data_p);
        m_data_p += sizeof(_T);
        return rv;
    } // getVal()
    void AlignPtr(void);

private:
    std::map<std::string, bool> m_setVariables;

public:
    AwacData(uint8_t* a_data_p);

    virtual void Expand(void) = 0;
    void SetVar(const std::string &m_varName);
    bool IsVarSet(const std::string& a_varName) const;
    std::list<std::string> GetVarsList(void);
}; // class AwacData





uint8_t _BcdToBin(uint8_t val);          // Used by AWAC
std::string PdClock2String(const Nortek::PdClock& clock);
std::string PdClock2ProfileDtStartString(const Nortek::PdClock& clock);


}; // namespace buoy
}; // namespace mbp

#endif // _AWAC_DATA_HPP
