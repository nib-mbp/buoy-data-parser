#ifndef _BUOY_DATATYPES_HPP
#define _BUOY_DATATYPES_HPP

#include <boost/cstdint.hpp>
/*
#ifndef WIN32
    //#include <stdint.h>
#include <cstdint>
#endif

#if !defined(__uint32_t_defined) && !defined(_STDINT_H_)
    #define __uint32_t_defined
    typedef unsigned __int32 uint32_t;
    typedef unsigned __int16 uint16_t;
    typedef unsigned __int8  uint8_t;
#endif // __uint32_t_defined
#ifndef WIN32
    typedef uint32_t UINT32;
    typedef uint16_t UINT16;
    typedef uint8_t  UINT8;
#endif // WIN32
*/

namespace mbp {
namespace buoy {

// -- Enums --
typedef enum {
    OXYGEN_DEVICE_ID_3835_e   = 0,
    OXYGEN_DEVICE_ID_4835_1_e = 1,
    OXYGEN_DEVICE_ID_4835_2_e = 2
} oxygen_device_id_e;

typedef enum {
    DATA_NA_e               = 0,
    MPPT_e                  = 1,    //!< solarni paneli
    MPPT_STATUS_e           = 2,    //!< solarni paneli
    AD_ENERGY_00_07_e       = 3,    //!< 0-3 napetosti bateri; 4-7 tokovi baterij (SPREMEMBA)
    AD_ENERGY_08_15_e       = 4,    //!< power sections (poraba podsklopov); tok gorivne celice; napetost napajanja sistema (SPREMEMBA)
    FUEL_CELL_STATUS_e      = 5,    //!< status gorivne celice (SPREMEMBA)
    ENERGY_STATUS_e         = 6,
    GPS_TIME_e              = 7,
    GPS_POSITION_e          = 8,
    GPS_STATUS_e            = 9,
    BOARD_STATUS_e          = 10,   //!< interni statusi CPE in IO plosc (komunikacije)
    MAIN_BOARD_e            = 11,   //!< temp, vlaga v skatli + poraba CPE (odstranjena el.napetost)
    DATA_CO2_e              = 12,   //!< CO2 senzor v trupu boje zaradi gorivnih celic
    BUOY_INTERIOR_SHT21_e   = 13,   //!< temperatura in vlaga zonotraj trupa
    ALARM_DATA_e            = 20,   //!< alarmi za SCADO

    DATA_WIND_e             = 32,
    DATA_COMPASS_e          = 33,
    DATA_ADC_e              = 34,   //!< tok instrumentov - ??? verjetno poraba na IO ploscah
    DATA_HUMIDITY_e         = 35,
    DATA_SEA_WATER_e        = 36,
    DATA_OXYGEN3835_e       = 37,   //!< kisikov senzor #0 (2m pod bojo, z brisalcem)
    EXT_DATA_AWAC1_e        = 38,   //!< old AWAC - it was obsoleted due to malfunction on 2017-01-24; last data from the unit were received on 2017-01-23
    IO_ALARMS_e             = 39,   //!< alarmi iz IO plosc
    IO_ALARM_TIME_e         = 40,   //!< za sinhronizacijo med IO ploscami in GPS (alarmi interni)
    //DATA_AWAC_DBG_RCV_e     = 41,
    //DATA_AWAC_DBG_SND_e     = 42,
    //DATA_AWAC_DBG_CMP_e     = 43,
    DATA_PAR_e              = 45,
    DATA_OXYGEN4835_1_e     = 46,   //!< kisikov senzor #1 (na morskem dnu, z brisalcem)
    DATA_AIR_CO2_e          = 47,
    DATA_OXYGEN4835_2_e     = 48,   //!< kisikov senzor #2 (takoj pod bojo, brez brisalca)
    EXT_DATA_AWAC2_e        = 50,   //!< new AWAC - this one replaced the obsoleted EXT_DATA_AWAC1_e above; it was first connected to the buoy on 2017-07-20
    EXTENDED_HEADER_DATA_e  = 0xff
} data_type_e;


typedef enum {
    TBL_NA_e                = 0,
    TBL_NA_MANUAL_SPEC_e,
    TBL_AIR_e,
    TBL_BUOY_INTERIOR_SHT21_e,
    TBL_ALARMS_e,
    TBL_AWAC2_USER_CONFIG_e,
    TBL_AWAC2_HEAD_CONFIG_e,
    TBL_AWAC2_HW_CONFIG_e,
    TBL_AWAC2_CURRENTS_HEADER_e,
    TBL_AWAC2_CURRENTS_DATA_e,
    TBL_AWAC2_WAVES_e,
    TBL_AWAC2_WAVEBANDS_e,
    TBL_AWAC2_WAVESPECTRUM_HEADER_e,
    TBL_AWAC2_WAVESPECTRUM_DATA_e,
    TBL_AWAC2_FOURIER_HEADER_e,
    TBL_AWAC2_FOURIER_DATA_e,
    TBL_BATTERIES_e,
    TBL_BAT_VOLTAGE_e,
    TBL_CAMERA_PARAMETERS_e,
    TBL_CO2_e,
    TBL_COMPASS_e,
    TBL_COMSTAT_MAIN_IO_e,
    TBL_FUEL_CELL_STATUS_e,
    TBL_GPS_POSITION_e,
    TBL_GPS_STATUS_e,
    TBL_GPS_SYNC_e,
    TBL_IO_ALARMS_e,
    TBL_IO_POWER_e,
    TBL_MAIN_BOARD_e,
    TBL_MPPT_e,
    TBL_MPPT_STATUS_e,
    TBL_OXYGEN_e,
    TBL_POWER_SECTIONS_e,
    TBL_PROFILE_e,
    TBL_SEA_WATER_e,
    TBL_SUPERVISION_CAMERAS_e,
    TBL_TIME_SYNC_IO_e,
    TBL_TIME_SYNC_MAIN_e,
    TBL_UNDERWATER_CAMERA_e,
    TBL_WIND_e,
    TBL_WRITTER_LOG_e,
    TBL_PAR_e,
    TBL_AIR_CO2_e
} table_e;

// -- Struct and Typedefs --
struct _db_tables_s {
    table_e         tableId;
    const char      *tableName_p;
};
extern struct _db_tables_s g_buoyDbTables[];

struct _data_types_s {
    data_type_e     id;                     // data type ID (see data_type_e)
    table_e         tableId;
    const char      *name_p;                // ID name
};
extern struct _data_types_s g_buoyDataTypes[];


int BUOY_DATA_TYPES_LEN(void);


#pragma pack(push)
#pragma pack(2)
// Data header
typedef struct {
    uint8_t         id;                     // ident 00..FE for small data structures size < 256
    uint8_t         size;
} header_t;

// Extended data header
typedef struct {
    uint8_t         id_hi;
    uint8_t         size_hi;
    uint8_t         id_lo;
    uint8_t         size_lo;
} extended_header_t;

// Time data header
typedef struct {
    //uint8_t         id;
    //uint8_t         size;
    header_t        hdr;        // Record header
    uint16_t        msec;
    uint32_t        sec;
} time_header_t;

typedef struct {
    extended_header_t   hdr;    // Record header
    uint32_t            sec;
} time_extended_header_t;

// AWAC header
typedef struct {
    //extended_header_t   ext_header;
    //uint32_t            sec;
    time_extended_header_t  hdr;
    uint8_t                 data[1];
} awac_data_header_t;
#define BUOY_AWAC_DATA_HEADER_SIZE (sizeof(mbp::buoy::time_extended_header_t))


struct ad_channel_s {
    int32_t         val;
    int16_t         min;
    int16_t         max;
    int16_t         avg;
    uint8_t         is_active;
    char            reserved[1];
};

#define BUOY_AD_NO_OF_CHANNELS      (8)
typedef struct {
    time_header_t   hdr;
    struct ad_channel_s channel[BUOY_AD_NO_OF_CHANNELS];
} ad_data_t;

typedef struct {
    time_header_t   hdr;
    uint16_t        Uin_min;
    uint16_t        Uin_max;
    uint16_t        Uin_avg;
    uint16_t        Iin_min;
    uint16_t        Iin_max;
    uint16_t        Iin_avg;
    uint16_t        Ubat_min;
    uint16_t        Ubat_max;
    uint16_t        Ubat_avg;
    uint16_t        Iout_min;
    uint16_t        Iout_max;
    uint16_t        Iout_avg;
    uint32_t        output_charge;
    uint16_t        remaining_eq_time;
    uint8_t         days_last_full_charge;
    uint8_t         days_last_equalize;
    uint16_t        total_charge;
} mppt_t;

typedef struct {
    time_header_t   hdr;
    uint8_t         status;
    uint8_t         flags;
} mppt_status_t;

typedef struct {
    time_header_t   hdr;
    uint16_t        status;
} fuel_cell_status_t;

typedef struct {
    time_header_t   hdr;
    int32_t         timedif;
    uint32_t        old_timer;
    uint32_t        new_timer;
} energy_status_t;

#define BUOY_DEBUG_DATA_BUF_LEN (64)
typedef struct {
    time_header_t   hdr;
    char            string[1];              // Dynamic length (use the header header_t->size to determine the actual length)
} debug_data_buf_t;
#define BUOY_DEBUG_DATA_HEADER_SIZE (sizeof(mbp::buoy::debug_data_buf_t) - sizeof(char))

typedef struct {
    time_header_t   hdr;
    int32_t         time_diff;
} gps_sync_t;

typedef struct {
    time_header_t   hdr;
    int16_t         longitude_i;
    int16_t         latitude_i;
    int32_t         longitude_f;
    int32_t         latitude_f;
    int16_t         altitude;
    uint16_t        noSatellites;
} gps_position_t;

typedef struct {
    time_header_t   hdr;
    uint8_t         status;
    uint8_t         antenna;
    uint8_t         battery;
    uint8_t         almanac;
} gps_status_t;

typedef struct {
    time_header_t   hdr;
    uint32_t        pckt_error;
    uint32_t        multiple_pckt_error;
    uint32_t        soft_pckt_error;
    uint8_t         board;
    uint8_t         is_present;
    uint8_t         is_initialized;
    char            reserved[1];
} board_status_t;

typedef struct {
    time_header_t   hdr;
    uint16_t        current;
    int16_t         temperature;  
    uint16_t        humidity;
} main_board_t;

typedef struct {
    time_header_t   hdr;
    uint16_t        value;
} co2_t;

typedef struct {
    time_header_t   hdr;
    uint16_t        co2_filtered;
    uint16_t        co2_raw;
    int16_t         temperature;
    uint16_t        instr_code;
    uint16_t        err_code;
} air_co2_t;

typedef struct {
  time_header_t   hdr;
  int16_t         temperature;
  uint16_t        humidity;
} buoy_interior_sht21_t;

typedef struct {
    time_header_t   hdr;
    uint16_t        alarm_code;
} alarm_data_t;

typedef struct  {
    time_header_t   hdr;
    uint16_t        humidity;
    int16_t         temperature;
    uint16_t        err_code;
} humidity_t;

#define BUOY_ADC_NO_OF_PORTS    (2)
typedef struct {
    time_header_t   hdr;
    uint16_t        min[BUOY_ADC_NO_OF_PORTS];
    uint16_t        max[BUOY_ADC_NO_OF_PORTS];
    uint16_t        avg[BUOY_ADC_NO_OF_PORTS];
    uint16_t        board;
} adc_t;

struct sea_water_time_s {
    uint8_t         day;
    uint8_t         month;
    uint16_t        year;
    uint8_t         hour;
    uint8_t         minute;
    uint8_t         second;
    uint8_t         tenth_of_second;
};

typedef struct {
    time_header_t   hdr;
    int32_t         temperature;
    uint32_t        conductivity;
    uint32_t        salinity;
    uint16_t        err_code;
    uint16_t        chlorophile;
    struct sea_water_time_s acq_time;
} sea_water_t;

typedef struct {
    time_header_t   hdr;
    int32_t         U;
    int32_t         V;
    int32_t         W;
    uint16_t        sound_speed;
    int16_t         sonic_temperature;
    uint16_t        dev_err_code;
    uint16_t        err_code;
} wind_t;

typedef struct {
    time_header_t   hdr;
    float           accx;
    float           accy;
    float           accz;
    float           gyrx;
    float           gyry;
    float           gyrz;
    float           magx;
    float           magy;
    float           magz;
    float           roll;                   // TODO: is this 'precni_naklon'?
    float           pitch;                  // TODO: is this 'bocni_naklon'?
    float           yaw;                    // TODO: is this 'smer'?
    uint16_t        err_code;
} compass_t;

typedef struct {
    time_header_t   hdr;
    uint8_t         board;
    int8_t          device;
    uint16_t        err_code;               // TODO: not present in table!?!
    uint8_t         device_mode;
    uint8_t         device_state;
    char            device_name[1];         // Dynamic length (use the header header_t->size to determine the actual length)
} io_alarms_t;
// Define the 'raw' io_alarms header size - the one without the tailing character
// array. NOTE: this can't simply be sizeof(io_alarms_t) - sizeof(char), due to
// a 2 bytes memory alignement which produces a result 16-1 vs. 14.
//#define BUOY_IO_ALARMS_HEADER_SIZE  (sizeof(mbp::buoy::io_alarms_t) - sizeof(char))
#define BUOY_IO_ALARMS_HEADER_SIZE  (sizeof(mbp::buoy::time_header_t) + sizeof(uint16_t) + 4 * sizeof(uint8_t))

typedef struct {
    time_header_t   hdr;
    uint8_t         board;
    int8_t          device;
    uint16_t        alarm_code;
    int32_t         time_diff;
    uint32_t        old_timer;
    uint32_t        new_timer;
} io_alarm_time_t;

typedef struct {
   time_header_t    hdr;
   uint32_t         concentration;
   uint16_t         saturation;
   int16_t          temperature;
   uint16_t         err_code;
} oxygen3835_t;

typedef struct {
   time_header_t    hdr;
   uint32_t         concentration;
   uint32_t         saturation;
   int16_t          temperature;
   uint16_t         err_code;
} oxygen4835_t;

typedef struct {
    time_header_t   hdr;
    int16_t         value;
    uint16_t        err_code;
} par_t;



#pragma pack(pop)

const char* GetTableName(const table_e a_table);
const char* ResolveNameFromId(const mbp::buoy::data_type_e a_id);
const char* ResolveNameFromId(const uint8_t a_id);

mbp::buoy::table_e GetTableFromId(const mbp::buoy::data_type_e a_id);
mbp::buoy::table_e GetTableFromId(const uint8_t a_id);

} // namespace buoy
} // namespace mbp

#endif // _BUOY_DATATYPES_HPP
