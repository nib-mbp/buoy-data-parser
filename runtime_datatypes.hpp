#ifndef _RUNTIME_DATATYPES_HPP
#define _RUNTIME_DATATYPES_HPP

#include <stdexcept>


namespace mbp {
namespace buoy {

// -- Enums --
// -- Enums --
// ATTENTION - IMPORTANT:
//  All instruments should be at the beginning of the enum with
//  no exception!
//  The define #define BUOY_num_of_ALL_INSTRUMENTS should match the number
//  of the last instrument + 1!
//  NOTE: If any of the is not true, the code will fail horibly.
typedef enum {
//    _SENSOR__LOW_SENTINEL_e = 0,//!< special value - low array member sentinel - kind of: N/A
    INSTRUMENT_WIND_e       = 0,//!< instrument - WIND array position
    INSTRUMENT_HUMIDITY_e,      //!< instrument - HUMIDITY array position
    INSTRUMENT_COMPASS_e,       //!< instrument - COMASS array position
    INSTRUMENT_SALINITY_e,      //!< instrument - SALINITY array position
    INSTRUMENT_AWAC_e,          //!< instrument - AWAC array position
    INSTRUMENT_OXYGEN1_e,       //!< instrument - OXYGEN no.1 array position
    INSTRUMENT_OXYGEN2_e,       //!< instrument - OXYGEN no.2 array position
    INSTRUMENT_OXYGEN3_e,       //!< instrument - OXYGEN no.3 array position
    INSTRUMENT_PAR_e,           //!< instrument - PAR array position
    INSTRUMENT_BUOY_CO2_e,      //!< instrument - Air CO2 array position

    SENSOR_BUOY_DOOR_e,         //!< buoy door  (slo: loputa boje)
    SENSOR_WATER_LEAK_ALL_e,    //!< all water leakage detectors (slo: tipala za vodo)
    SENSOR_WATER_LEAK_BUOY_e,   //!< buoy internal water leakage detector (slo: voda v boji)
    SENSOR_WATER_LEAK_UW_CAM_e, //!< under water camera compartment water leakage detector (slo: voda v komori za kamero)
    SENSOR_SIGNAL_LIGHT_e,      //!< signal light sensor (slo: signalna luč)
    SENSOR_BUOY_CO2_e,          //!< buoy CO2 sensor (slo: merilnik CO2 v boji)
    SENSOR_WIFI_LINK_ALL_e,     //!< wifi link (slo: brezžična radia)
    SENSOR_WIFI_2_4GHz_e,       //!< wifi 2.5GHz (slo: povezava preko 2.4 GHz)
    SENSOR_WIFI_5GHz_e,         //!< wifi 5GHz (slo: povezava preko 5 GHz)
    SENSOR_SECURITY_CAM_ALL_e,  //!< all security cams (slo: nadzorne kamere)
    SENSOR_SECURITY_CAM1_e,     //!< security cam 1 (slo: nadzorna kamera st.1)
    SENSOR_SECURITY_CAM2_e,     //!< security cam 1 (slo: nadzorna kamera st.2)
    SENSOR_SECURITY_CAM3_e,     //!< security cam 1 (slo: nadzorna kamera st.3)
    SENSOR_SECURITY_CAM4_e,     //!< security cam 1 (slo: nadzorna kamera st.4)
    SENSOR_UW_CAM1_e,           //!< under-water cam 1 (slo: podvodna kamera st.1)
    SENSOR_UW_CAM2_e,           //!< under-water cam 2 (slo: podvodna kamera st.2)
    SENSOR_BUS_SPI_e,           //!< SPI bus (slo: vodilo SPI)
    SENSOR_BUS_I2C_WRITE_e,     //!< I2C bus - write events (slo: pisanje preko vodila I2C)
    SENSOR_BUS_I2C_READ_e,      //!< I2C bus - read events (slo: branje preko vodila I2C)
    SENSOR_BUS_RGB_e,           //!< RGB light controller bus (slo: pisanje v krmilnik za osvetlitev RGB)
    SENSOR_GPS_e,               //!< GPS
    SENSOR_POWER_e,             //!< power module status and communication (slo: energetski modul - stanje in komunikacije)
    SENSOR_BAT1_e,              //!< battery pack 1
    SENSOR_BAT2_e,              //!< battery pack 2
    _SENSOR__SENTINEL_e         //!< special value - enum sentinel
} buoy_sensor_e;
#define BUOY_num_of_ALL_INSTRUMENTS     (10)        // last instrument + 1, i.e. INSTRUMENT_BUOY_CO2_e + 1


mbp::buoy::buoy_sensor_e findInstrument(uint16_t a_board, uint16_t a_device) throw(std::invalid_argument);

} // namespace buoy
} // namespace mbp

#endif // _RUNTIME_DATATYPES_HPP
