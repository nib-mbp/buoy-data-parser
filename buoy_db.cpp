#include <iostream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <mysql++/mysql++.h>
#include <mysql++/transaction.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "buoy_data_holder_utils.hpp"
#include "buoy_db.hpp"
#include "buoy_log.hpp"
#include "buoy_utils.hpp"

namespace mbp {

//#define DB_MARKER_DB_NAME_c                 "buoy_markers"
//#define DB_MARKER_TABLE_NAME_c              "raw_data_import"
#define DB_MARKER_DATE_FIELD_NAME_c         "markerdate"
#define DB_MARKER_PACKET_ID_FIELD_NAME_c    "day_packet_id"
#define DB_MAX_TSX_PER_CONNECTION           2000000


std::ostream& operator<<(std::ostream& os, const DbDataMarker& obj)
{
    os << "[" << obj.getDateStr() << " @ " << obj.getPacketId() << "]";
    return os;
}


BuoyDb::BuoyDb(
    const std::string& a_user,
    const std::string& a_passwd,
    const std::string& a_dbPrefix,
    const std::string& a_dbMarkerTable,
    const std::string& a_host)
:
    m_user(a_user),
    m_passwd(a_passwd),
    m_dbPrefix(a_dbPrefix),
    m_dbMarkerTable(a_dbMarkerTable),
    m_host(a_host),
    m_maxTsxPerConnection(DB_MAX_TSX_PER_CONNECTION),
    m_packetId(0),
    m_isDataSet(false),
    m_isConnected(false),
    m_isTsxInitialized(false),
    m_db_sp(boost::shared_ptr<mysqlpp::Connection>(new mysqlpp::Connection(mysqlpp::use_exceptions))),
    m_tsx_sp(),
    m_query_sp(),
    m_tblFields(),
    m_rows(),
    m_numDuplicates(0),
    m_logQueries_v(),
    m_numOfProcessedTsx(0)
{
    return;
} // BuoyDb::BuoyDb()


BuoyDb::~BuoyDb()
{
    return;
} // BuoyDb::~BuoyDb()


void BuoyDb::StartTsx(void)
{
    if (! m_isConnected) {
        throw std::string("Not initialized!");
    }

    if (m_numOfProcessedTsx > m_maxTsxPerConnection) {
        L_(info) << "Flush MySQL++ connection (disconnect and connect again)";
        Disconnect();
        Connect();
        m_numOfProcessedTsx = 0;
    }

    if (! m_isConnected) {
        throw std::string("Failed re-init the connection!");
    }

    m_tsx_sp = boost::shared_ptr<mysqlpp::Transaction>(new mysqlpp::Transaction(*m_db_sp, false));
    m_isTsxInitialized = true;

    return;
} // BuoyDb::StartTsx()


void BuoyDb::CommitTsx(bool a_doCommit)
{
    if (! m_isConnected) {
        throw std::string("Not initialized!");
    }

    if (! m_isTsxInitialized) {
        throw std::string("DB TSX not initialized!");
    }

    if (a_doCommit) {
        m_tsx_sp->commit();
    } else {
        m_tsx_sp->rollback();
    }
    m_tsx_sp.reset();
    m_isTsxInitialized = false;

    ++m_numOfProcessedTsx;

    return;
} // BuoyDb::CommitTsx()


void BuoyDb::_InsertProfileRow(void)
{
    mysqlpp::StoreQueryResult res;
    mysqlpp::Row row;
    std::ostringstream sstr("");

    mbp::buoy::rows_it rowsIter = m_rows->begin();
    if ( rowsIter ==  m_rows->end() ) {
        sstr.str("");
        sstr << "ERROR: No data rows available!";
        throw sstr.str();
    }

    mbp::buoy::table_e tableId = rowsIter->getTable();
    mbp::buoy::field_names_lt fields = (*m_tblFields)[tableId];
    mbp::buoy::row_spt row_sp = rowsIter->getRow();
    mbp::buoy::row_fields_it timeRow = row_sp->begin();
    bool isTimeSet(false);
    for (mbp::buoy::field_names_it fieldIter = fields.begin();
         (fieldIter != fields.end()) && (isTimeSet == false);
         ++fieldIter, ++timeRow)
    {
        if ((*fieldIter).compare("time") == 0) {
            isTimeSet = true;
            break;
        }
    } // for (... fields ...)

    if ( ! isTimeSet ) {
        sstr.str("");
        sstr << "ERROR: First data row doesn't contain time (q: '" << _MkRowQuery(*rowsIter) << "')!";
        throw sstr.str();
    }

    // -- Values --
    if ( (*timeRow).type() != typeid(std::string) ) {
        throw std::string("Invalid row 'time' type");
    }

    boost::posix_time::ptime firstDataRowPt = boost::posix_time::time_from_string(boost::get<std::string>(*timeRow));

    std::ostringstream sstrDb("");
    sstr.str("");
    std::tm tmTime = boost::posix_time::to_tm(firstDataRowPt);
    int currentProfileYear = tmTime.tm_year + 1900 - 2000;  // Two digit year
    sstrDb << m_dbPrefix << std::setfill('0') << std::setw(2) << currentProfileYear;
    sstr << "USE `" << sstrDb.str() << "`";
    if ( ! m_query_sp->exec(sstr.str().c_str())) {
        sstr.str("");
        sstr << "Failed selecting current database ('" << sstrDb.str() << "')";
        throw sstr.str();
    }

    // -- Check if there is already an entry for the current date + packet id --
    sstr.str("");
    sstr
        << "SELECT `packet_id` FROM `" << mbp::buoy::GetTableName(mbp::buoy::TBL_PROFILE_e) << "`"
        << " WHERE"
        << " `dataset_date`=\"" << m_datasetMarker.getDateStr() << "\" AND"
        << " `dataset_packet_id`=" << m_datasetMarker.getPacketId();
    res = m_query_sp->store(sstr.str().c_str());
    if (res.size() != 0) {
        sstr.str("");
        sstr << "ERROR: Data for " << m_datasetMarker << " already exist!";
        throw sstr.str();
    }

    sstr.str("");
    sstr
        << "INSERT INTO `" << mbp::buoy::GetTableName(mbp::buoy::TBL_PROFILE_e) << "`"
        << " (datestart, dataset_date, dataset_packet_id, srv_end) VALUES ("
        << " \"" << mbp::buoy::PtimeToStr(firstDataRowPt) << "\","
        << " \"" << m_datasetMarker.getDateStr() << "\","
        << " " << m_datasetMarker.getPacketId() << ","
        << " NOW())";
    if (mbp::buoy::g_options.getDbgDoSqlDump()) {
        L_(info) << "q: " << sstr.str();
    }
    if ( ! m_query_sp->exec(sstr.str().c_str())) {
        throw std::string("Failed creating a new profile.");
    }
    m_packetId = m_query_sp->insert_id();

    sstr.str("");
    sstr << "SELECT @packet_id:=LAST_INSERT_ID()";
    if (! m_query_sp->store(sstr.str().c_str())) {
        throw std::string("Failed storing last @packet_id!");
    }

    L_(trace) << "profile -> using packet_id = " << m_packetId;

    return;
} // BuoyDb::_InsertProfileRow()


void BuoyDb::setData(
        const mbp::DbDataMarker& a_datasetMarker,
        const mbp::buoy::tbls_msp& a_tblFields,
        const mbp::buoy::rows_spt& a_rows)
{
    if (! m_isConnected) {
        throw std::string("Not initialized!");
    }

    m_datasetMarker = a_datasetMarker;

    m_tblFields = a_tblFields;
    m_rows = a_rows;

    m_isDataSet = true;

    return;
} // BuoyDb::setData()


void BuoyDb::clearData(void)
{
    m_tblFields.reset();
    m_rows.reset();

    m_isDataSet = false;

    return;
} // BuoyDb::clearData()

/**
 * Retrieves the transfer packet data marker from the dedicated table.
 */
mbp::DbDataMarker mbp::BuoyDb::getPacketMarker(void) const
{
    std::ostringstream sstrQuery("");
    sstrQuery
        << "SELECT"
        << " `" << DB_MARKER_DATE_FIELD_NAME_c << "` AS `date`, `" << DB_MARKER_PACKET_ID_FIELD_NAME_c << "` AS id"
        << " FROM " << m_dbMarkerTable;
    mysqlpp::StoreQueryResult res =
        m_query_sp->store(sstrQuery.str().c_str());
    if (res.size() != 1) {
        std::ostringstream sstr("");
        sstr << "ERROR: Incorrect initialization of the '" << m_dbMarkerTable <<  "'";
        throw std::string("Invalid marker query.");
    }
    mysqlpp::Row row = res.at(0);
    boost::gregorian::date date = boost::gregorian::from_string(std::string(row["date"]));
    uint32_t dayPacketId = row["id"];

    DbDataMarker dataMarker(date, dayPacketId);
    return dataMarker;
} // BuoyDb::getPacketMarker()


/**
 * Set transfer packet data marker
 */
void mbp::BuoyDb::setPacketMarker(
        const DbDataMarker& a_marker)
{
    std::ostringstream sstrQuery("");
    sstrQuery
        << "DELETE FROM " << m_dbMarkerTable;
    if (mbp::buoy::g_options.getDbgDoSqlDump()) {
        L_(info) << "q: " << sstrQuery.str();
    }
    if ( ! m_query_sp->exec(sstrQuery.str().c_str()) ) {
        std::ostringstream sstr("");
        sstr << "Failed clearing the '" << m_dbMarkerTable << "'";
        throw sstr.str();
    }

    sstrQuery.str("");
    sstrQuery
        << "INSERT INTO " << m_dbMarkerTable
        << " (" << DB_MARKER_DATE_FIELD_NAME_c << ", " << DB_MARKER_PACKET_ID_FIELD_NAME_c << ")"
        << " VALUES (\"" << a_marker.getDateStr() << "\", " << a_marker.getPacketId() << ")";
    if (mbp::buoy::g_options.getDbgDoSqlDump()) {
        L_(info) << "q: " << sstrQuery.str();
    }
    if ( ! m_query_sp->exec(sstrQuery.str().c_str()) ) {
        std::ostringstream sstr("");
        sstr << "Failed inserting the new marker into '" << m_dbMarkerTable << "'";
        throw sstr.str();
    }

    return;
} // mbp::BuoyDb::setPacketMarker()


void BuoyDb::Connect(void)
        throw(mysqlpp::ConnectionFailed)
{

    if (! m_db_sp) {
        m_db_sp = boost::shared_ptr<mysqlpp::Connection>(new mysqlpp::Connection(mysqlpp::use_exceptions));
    }

    m_db_sp->connect(NULL, m_host.c_str(), m_user.c_str(), m_passwd.c_str());

    m_query_sp = boost::shared_ptr<mysqlpp::Query>(new mysqlpp::Query(m_db_sp->query()));

    std::ostringstream sstrQuery("");
    sstrQuery << "SET time_zone='+00:00'";
    if ( ! m_query_sp->exec(sstrQuery.str().c_str()) ) {
        std::ostringstream sstr("");
        sstr << "DB connection failed (timezone setup failed)";
        throw sstr.str();
    }

    m_isConnected = true;

    return;
} // BuoyDb::Connect()


void BuoyDb::Disconnect(void)
{
    m_query_sp.reset();

    if (m_isConnected == true) {
        if (m_db_sp) {
            m_db_sp->disconnect();
        }
        m_db_sp.reset();
    }

    m_isConnected = false;

    return;
} // BuoyDb::Disconnect()


const std::string BuoyDb::_MkRowQuery(const mbp::buoy::TableRow& a_row) const
{
    std::ostringstream sstr("");

    mbp::buoy::table_e tableId = a_row.getTable();
    mbp::buoy::row_spt row_sp = a_row.getRow();
    mbp::buoy::field_names_lt fields = (*m_tblFields)[tableId];

    // -- Manual specified table handling --
    if (tableId == mbp::buoy::TBL_NA_MANUAL_SPEC_e) {
        // -- Query is manually specified --
        mbp::buoy::row_fields_it rowIter = row_sp->begin();
        if (rowIter == row_sp->end()) {
            throw std::string("Invalid use of TBL_NA_MANUAL_SPEC_e 1!");
        }
        if ((*rowIter).type() != typeid(mbp::buoy::DataHolder)) {
            throw std::string("Invalid use of TBL_NA_MANUAL_SPEC_e 2!");
        }
        mbp::buoy::DataHolder pckt = boost::get<mbp::buoy::DataHolder>(*rowIter);
        if (! pckt.isRawSql()) {
            throw std::string("Invalid use of TBL_NA_MANUAL_SPEC_e 3!");
        }
        return pckt.getRawSql();
    }

    /*
     * BUG: sea_water duplicated time entries, where first data is invalid - Apr / May 2019,
     *      scheduled to be fixed by end of May 2019!
     */
    if (tableId == mbp::buoy::TBL_SEA_WATER_e) {
        mbp::buoy::row_fields_it rowIter = row_sp->end();
        mbp::buoy::row_types_t lastElement = *(--rowIter);
        if (   (lastElement.type() == typeid(uint16_t))
            && (boost::get<uint16_t>(lastElement) == 32768) )
        {
            L_(debug) << "SEA_WATER (bug Apr/May 2019) - found probably invalid data, skipping";
            return std::string("SELECT 1");
        }
    }

    sstr <<
        "INSERT INTO `" << mbp::buoy::GetTableName(tableId) << "` (";

    // -- Column names --
    for (mbp::buoy::field_names_it fieldIter = fields.begin();
         fieldIter != fields.end();
         ++fieldIter)
    {
        if (fieldIter == fields.begin()) {
            // first
            sstr << "`" << *fieldIter << "`";
        } else {
            sstr << ", `" << *fieldIter << "`";
        }
    } // for (... fields ...)
    sstr << ") VALUES (";

    // -- Values --
    for (mbp::buoy::row_fields_it rowIter = row_sp->begin();
         rowIter != row_sp->end();
         ++rowIter)
    {
        if (rowIter != row_sp->begin()) {
            sstr << ", ";
        }
        if ((*rowIter).type() == typeid(std::string)) {
            sstr << "'" << boost::get<std::string>(*rowIter) << "'";
        } else if ((*rowIter).type() == typeid(double)) {
            sstr << boost::get<double>(*rowIter);
        } else if ((*rowIter).type() == typeid(uint16_t)) {
            sstr << boost::get<uint16_t>(*rowIter);
        } else if ((*rowIter).type() == typeid(uint32_t)) {
            sstr << boost::get<uint32_t>(*rowIter);
        } else if ((*rowIter).type() == typeid(int32_t)) {
            sstr << boost::get<int32_t>(*rowIter);
        } else if ((*rowIter).type() == typeid(int16_t)) {
            sstr << boost::get<int16_t>(*rowIter);
        } else if ((*rowIter).type() == typeid(bool)) {
            sstr << boost::get<bool>(*rowIter);
        } else if ((*rowIter).type() == typeid(std::string)) {
            sstr << boost::get<std::string>(*rowIter);
        } else if ((*rowIter).type() == typeid(mbp::buoy::DataHolder)) {
            mbp::buoy::DataHolder pckt = boost::get<mbp::buoy::DataHolder>(*rowIter);
            if (pckt.isVar()) {
                sstr << pckt.getVar();
            } else if (pckt.isNull()) {
                sstr << "NULL";
            } else if (pckt.isRawSql()) {
                sstr << pckt.getRawSql();
            } else {
                sstr << m_packetId;
            }
        }
    } // for (... fields ...)
    sstr << ")";

    return sstr.str();
} // BuoyDb::_MkRowQuery()


bool BuoyDb::_IncMsec(
        const mbp::buoy::TableRow& a_row)
{
    mbp::buoy::table_e tableId = a_row.getTable();
    mbp::buoy::row_spt row_sp = a_row.getRow();
    mbp::buoy::field_names_lt fields = (*m_tblFields)[tableId];

    // -- Find the 'msec' column and the coresponding value --
    mbp::buoy::row_fields_it timeRow = row_sp->begin();
    mbp::buoy::row_fields_it msecRow = row_sp->begin();
    bool isTimeSet(false);
    bool isMsecSet(false);
    for (mbp::buoy::field_names_it fieldIter = fields.begin();
         fieldIter != fields.end();
         ++fieldIter)
    {
        if ((*fieldIter).compare("msec") == 0) {
            isMsecSet = true;
        }
        if ((*fieldIter).compare("time") == 0) {
            isTimeSet = true;
        }
        if (! isMsecSet) {
            ++msecRow;
        }
        if (! isTimeSet) {
            ++timeRow;
        }
    } // for (... fields ...)

    if (! (isMsecSet && isTimeSet))
    {
        // Can't find both fields
        return false;
    }

    // -- Values --
    if (   ((*msecRow).type() != typeid(uint16_t))
        || ((*timeRow).type() != typeid(std::string))
       )
    {
        throw std::string("Invalid 'time' or 'msec' types");
    }

    uint16_t msec = boost::get<uint16_t>(*msecRow);
    msec += 100;
    if (msec >= 1000) {
        msec -= 1000;

        boost::posix_time::ptime pt = boost::posix_time::from_iso_string(boost::get<std::string>(*timeRow));
        pt += boost::posix_time::seconds(1);
        *timeRow = mbp::buoy::PtimeToStr(pt);
        //time += 1;
    }
    *msecRow = msec;

    return true;
} // BuoyDb::_IncMsec()


void BuoyDb::_AddLogEntry(
        const std::string& a_logMessage)
{

    mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
    row_sp->clear();
    row_sp->push_back(mbp::buoy::DataHolder());
    std::ostringstream sstr("");
    sstr << mysqlpp::escape << a_logMessage;
    row_sp->push_back(sstr.str());
// TODO:
//  org row: 
//   row_sp->push_back(m_fileBasename);
//  -> remove the file name dependency
    row_sp->push_back("");
    mbp::buoy::TableRow logRow(mbp::buoy::TBL_WRITTER_LOG_e, row_sp);
    std::string query = _MkRowQuery(logRow);

    m_logQueries_v.push_back(query);

    return;
} // BuoyDb::_AddLogEntry()

void BuoyDb::_AddLogEntry(
        const mbp::buoy::TableRow& a_row)
{
    std::string query = _MkRowQuery(a_row);
    m_logQueries_v.push_back(query);

    return;
} // BuoyDb::_AddLogEntry()


void BuoyDb::CommitData(void)
{
    _InsertProfileRow();
    for (mbp::buoy::rows_it rowIter = m_rows->begin();
         rowIter != m_rows->end();
         ++rowIter)
    {
        // -- Check if the current row contains a log entry which should
        //    be processed separatelly - the SQL query should be added to the
        //    logs vector --
        // TODO
        if (rowIter->getTable() == mbp::buoy::TBL_WRITTER_LOG_e) {
            std::string logQuery(_MkRowQuery(*rowIter));
            m_logQueries_v.push_back(logQuery);
            continue;
        }

        // -- Process a data row --
        std::string query(_MkRowQuery(*rowIter));

        try {
            m_query_sp->store(query);
        }
        catch (const mysqlpp::BadQuery &e) {
            std::string what(e.what());
            L_(warning) << "DB err: " << what;
            L_(trace) <<
                "m_numDuplicates / conf.db-duplicates / conf.ts-scalling: " <<
                m_numDuplicates << " / " <<
                mbp::buoy::g_options.getNumIgnoreDuplicates() << " / " <<
                mbp::buoy::g_options.getDoTimestampScalling();
            if (what.find("Duplicate entry") != std::string::npos) {
                L_(info) << "Duplicate entry ERR found - query: " << query;

                L_(trace) << "m_numDuplicates: " << m_numDuplicates;
                if (m_numDuplicates >= mbp::buoy::g_options.getNumIgnoreDuplicates()) {
                    // -- Limit reached - re-throw --
                    std::ostringstream sstr("");
                    sstr << "Max. duplicates number reached - DB err: " << what << "!";
                    _AddLogEntry(sstr.str());
                    throw sstr.str();
                } else {
                    std::ostringstream sstr("");
                    ++m_numDuplicates;
                    // -- Try to fix the problem --
                    if (! mbp::buoy::g_options.getDoTimestampScalling()) {
                        // Just ignore the duplicate
                        sstr << "DUPLICATED_ROW: Skipping processing of a duplicated DB entry ...";
                        L_(info) << " -> " << sstr.str();
                        sstr <<
                            " (query: " << mysqlpp::escape << query << " /" <<
                            " DBerr: " << mysqlpp::escape << what << ")";
                        _AddLogEntry(sstr.str());
                    } else {
                        // Try correcting the time
                        if (! _IncMsec(*rowIter)) {
                            sstr << "Failed correcting timestamp when trying to rescue from " << what << "!";
                            _AddLogEntry(sstr.str());
                            throw sstr.str();
                        } else {
                            query = _MkRowQuery(*rowIter);
                            sstr << "DUPLICATED_ROW: New query: " << mysqlpp::escape << query;
                            L_(trace) << " -> " << sstr.str();
                            _AddLogEntry(sstr.str());
                            if ( ! m_query_sp->exec(query)) {
                                sstr.str("");
                                sstr << "Failed executing corrected time query!";
                                _AddLogEntry(sstr.str());
                                throw sstr.str();
                            }
                        } // if (! _IncMsec(*rowIter))
                    } // if (! ... .getDoTimestampScalling())
                } // (m_numDuplicates++ >= ...) ... else ...
            } else { // if (what.find("Duplicate entry") ...) ... else ...
                // -- Other SQL error - not a 'Duplicated entry' --
                std::ostringstream sstr("");
                sstr << "DB Err: " << what;
                _AddLogEntry(sstr.str());
                throw sstr.str();
            } // if (what.find("Duplicate entry") ...)
        } 
        catch (std::exception &e) {
            std::ostringstream sstr("");
            sstr << "UNKNOWN Err: " << std::string(e.what());
            _AddLogEntry(sstr.str());
            throw sstr.str();
        } // switch ... catch ...
    } // for (... m_rows ...)

    L_(trace) << " [DONE] TSX ...";

    return;
} // BuoyDb::CommitData()


// Outputs data to debug log level
// NOTE: profile row will be selected / inserted, but transaction will be
//       rejected - this is needed to deduct the PID value.
void BuoyDb::DumpData(void)
{
    // -- NOTE:
    //     The data dump is executed within a nested transaction that is
    //     always rolled back.
    std::ostringstream sstr("");
    sstr << "SAVEPOINT datadump";
    if ( ! m_query_sp->exec(sstr.str().c_str())) {
        sstr.str("");
        sstr << "Failed initializing nested transaction for data dump!";
        throw sstr.str();
    }

    _InsertProfileRow();

    for (mbp::buoy::rows_it rowIter = m_rows->begin();
         rowIter != m_rows->end();
         ++rowIter)
    {
        std::string query(_MkRowQuery(*rowIter));
        L_(info) << "q: " << query;
    }

    sstr.str("");
    sstr << "ROLLBACK TO SAVEPOINT datadump";
    if ( ! m_query_sp->exec(sstr.str().c_str())) {
        sstr.str("");
        sstr << "Failed rolling back from nested transaction for data dump!";
        throw sstr.str();
    }

    sstr.str("");
    sstr << "RELEASE SAVEPOINT datadump";
    if ( ! m_query_sp->exec(sstr.str().c_str())) {
        sstr.str("");
        sstr << "Failed rolling back from nested transaction for data dump!";
        throw sstr.str();
    }

    return;
} // BuoyDb::DumpData()


void BuoyDb::CommitLogs(void)
{
    // TODO: enable nested transactions
//    _StartTsx();
    for (std::vector<std::string>::const_iterator iter = m_logQueries_v.begin();
         iter != m_logQueries_v.end();
         ++iter)
    {
        if ( ! m_query_sp->exec(*iter)) {
            L_(error) << "FAILED commiting log entry!!!";
            std::ostringstream sstr("");
            sstr << "Failed commiting a log entry (q: " << *iter << ")!";
            throw sstr.str();
        }
    } // for (... m_logQueries_v ...)
    // TODO: enable nested transactions
//    _CommitTsx(true);

    return;
} // BuoyDb:CommitLogs()


void BuoyDb::RollbackTsx(void)
{
    L_(error) << "RollBackTsx";
    try {
        CommitTsx(false);
    }
    catch (const std::string& strE) {
        L_(error) << strE;
        throw strE;
    }
    return;
} // BuoyDb::RollbackTsx()


void BuoyDb::test(void)
{
    DbDataMarker marker;
    try {
        marker = getPacketMarker();
    } catch (std::string& eStr) {
        std::cout << eStr << std::endl;
    }
    std::cout << "getLastDate = " << marker.getDateStr() << std::endl;
    std::cout << "getPacketId = " << marker.getPacketId() << std::endl;
} // BuoyDb::test()

} // namespace mbp
