#ifndef _BUOY_UDP_CONNECTOR_HPP
#define _BUOY_UDP_CONNECTOR_HPP

#include <string>
#include <vector>
#include <utility>      // std::pair, std::make_pair

#include <boost/cstdint.hpp>
#include <boost/asio.hpp>

#include "buoy_shared_array.hpp"
#include "buoy_datatypes.hpp"

#define UDP_PACKET_LEN          16*1024
#define MAX_UDP_DATA_POINTS     100

#pragma pack(push)
#pragma pack(1)
typedef struct {
    uint16_t data_type;
    uint16_t data_ident;  // board number etc.
} buoy_udp_pkt_runtime_dpoint_t;

// -- MySQL data structure --
typedef struct {
    uint16_t year;
    uint8_t month;
    uint8_t day;
    int32_t packet_no;
} buoy_udp_pkt_mysql_hdr_t;

typedef struct {
    buoy_udp_pkt_mysql_hdr_t hdr;
    int32_t len;
    uint8_t data[1];
} mysql_packet_data_t;
#define BUOY_MYSQL_DATA_PKT_HEADER_SIZE     (sizeof(mysql_packet_data_t) - sizeof(uint8_t))

// -- Runtime data structure --
/*
typedef struct {
    uint8_t     ident;
    uint8_t     size;
    uint16_t    msecs;      // record milliseconds
    uint32_t    secs;       // record epoch time
} runtime_packet_data_hdr_t;
*/
typedef struct {
//    runtime_packet_data_hdr_t   hdr;
    mbp::buoy::time_header_t               hdr;
    uint8_t                     data[1];
} runtime_packet_data_t;
#define BUOY_RUNTIME_DATA_PKT_HEADER_SIZE   (sizeof(runtime_packet_data_t) - sizeof(uint8_t))

// -- UDP packet data structure --
typedef struct {
    uint8_t   flag;
    uint8_t   command;
    uint16_t  data_len;
    uint32_t  utc_secs;
    uint32_t  utc_usecs;
    uint16_t  packet;
    uint16_t  crc;
} buoy_udp_pkt_hdr_t;

typedef struct {
    buoy_udp_pkt_hdr_t hdr;
    uint8_t data[1];
} buoy_udp_pkt_t;
#define BUOY_UDP_PKT_HEADER_SIZE            (sizeof(buoy_udp_pkt_t) - sizeof(uint8_t))
#pragma pack(pop)


namespace mbp {

namespace buoy {

#pragma pack(push)
#pragma pack(1)
struct _instruments_matrix_s {
    uint8_t         board;
    uint8_t         port;
};

typedef struct {
    struct _instruments_matrix_s    instrument;
    uint8_t                         command;
    uint8_t                         res;
} instrument_command_data_t;

typedef struct {
    instrument_command_data_t   cmd;
    int16_t                     status;             // command execution status
} instrument_command_reply_t;
#pragma pack(pop)


typedef enum {
    BUOY_INSTRUMENT_WIND_e  = 0,
    BUOY_INSTRUMENT_HUMIDITY_e = 1,
    BUOY_INSTRUMENT_COMPASS_e = 2,
    BUOY_INSTRUMENT_SALINITY_e = 3,
    BUOY_INSTRUMENT_AWAC_e = 4,
    BUOY_INSTRUMENT_OXYGEN1_e = 5,
    BUOY_INSTRUMENT_OXYGEN2_e = 6,
    BUOY_INSTRUMENT_OXYGEN3_e = 7,
    BUOY_INSTRUMENT_PAR_e = 8,
    BUOY_INSTRUMENT_CO2_e = 9,
    BUOY_INSTRUMENT_NA_e
} instruments_index_e;

// -- instrument command enums --
typedef enum {
    BUOY_INSTRUMENT_CMD_STOP_e  = 0,
    BUOY_INSTRUMENT_CMD_START_e = 1,
    BUOY_INSTRUMENT_CMD_EXEC_STATUS_e = 2,
    BUOY_INSTRUMENT_CMD_SKIP_e,
    BUOY_INSTRUMENT_CMD_NA_e
} instrument_command_e;



typedef std::pair<uint16_t, uint16_t>    runtime_hdr_pair_t; 


namespace internal {
    class UdpClient {
    private:
        boost::asio::ip::udp::socket &socket_;
        boost::asio::io_service &io_service_;
        boost::asio::deadline_timer deadline_;

    public:
        UdpClient(boost::asio::ip::udp::socket &a_socket, boost::asio::io_service &a_ioService);
        ~UdpClient();
        std::size_t receive(const boost::asio::mutable_buffer &buffer,
                            boost::posix_time::time_duration timeout, boost::system::error_code &ec);

    private:
        void check_deadline();
        static void handle_receive(
                const boost::system::error_code &ec, std::size_t length,
                boost::system::error_code *out_ec, std::size_t *out_length) {
            *out_ec = ec;
            *out_length = length;
        }
    }; // class UdpClient
} // namespace internal

} // namespace buoy


class BuoyUdpConnector {
private:
    bool m_isInitialized;
    boost::asio::io_service m_ioService;
    const std::string m_remoteHost;
    const uint16_t m_remoteUdpPort;
    boost::asio::ip::udp::endpoint m_serverEndpoint;
    bool m_isMysqlPacketOutOfRange;
    buoy_udp_pkt_runtime_dpoint_t *m_udpDataPoints_p;
    const size_t m_udpPacketBufSize;
    uint8_t* m_udpPacketBuf_p;

    boost::asio::ip::udp::socket m_socket;
    mbp::buoy::internal::UdpClient m_client;


public:
    BuoyUdpConnector(std::string a_host, uint16_t a_port);
    ~BuoyUdpConnector();

    void Init(void);
    // void QueryRuntime(mbp::BuoySharedArray<uint8_t>& a_buoyArray_sp, std::vector<mbp::buoy::data_type_e> dataPoints_v);
    void QueryRuntime(
            mbp::BuoySharedArray<uint8_t>& a_buoyArray_sp,
            const std::vector<mbp::buoy::runtime_hdr_pair_t>& dataPoints_v)
        throw(std::invalid_argument, std::string);
    void RuntimeInstrumentControl(
            mbp::BuoySharedArray<uint8_t>& a_buoyArray_sp,
            const std::string &a_instrumentStr,
            mbp::buoy::instrument_command_e a_cmd
            )
        throw(std::invalid_argument, std::string);

    int QueryMysql(
            mbp::BuoySharedArray<uint8_t>& a_buoyArray_sp,
            uint16_t a_year,
            uint8_t a_month,
            uint8_t a_day,
            int32_t a_packetNumber)
        throw(std::string);
    bool isMysqlPacketOutOfRange() const;

    mbp::buoy::instruments_index_e getInstrument(uint8_t a_board, uint8_t a_port) const;
    mbp::buoy::instruments_index_e getInstrument(const std::string& a_instrument) const;
    std::string getInstrument(mbp::buoy::instruments_index_e a_instrument_e) const;
}; // class BuoyUdpConnector

}; // namespace mbp

#endif // _BUOY_UDP_CONNECTOR_HPP
