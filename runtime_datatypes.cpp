#include <iostream>
#include <string>
#include <sstream>


#include "runtime_datatypes.hpp"

namespace mbp {
namespace buoy {

mbp::buoy::buoy_sensor_e findInstrument(
        uint16_t a_board,
        uint16_t a_device)
    throw(std::invalid_argument)
{
    // find the instrument index
    mbp::buoy::buoy_sensor_e instrumentIdx = mbp::buoy::_SENSOR__SENTINEL_e;
    switch (a_board) {
        case 1:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_WIND_e : mbp::buoy::INSTRUMENT_HUMIDITY_e;
            break;
        case 2:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_COMPASS_e : mbp::buoy::INSTRUMENT_SALINITY_e;
            break;
        case 3:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_AWAC_e : mbp::buoy::INSTRUMENT_OXYGEN1_e;
            break;
        case 4:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_PAR_e : mbp::buoy::INSTRUMENT_OXYGEN2_e;
            break;
        case 5:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_BUOY_CO2_e : mbp::buoy::INSTRUMENT_OXYGEN3_e;
            break;
        default:
            std::ostringstream sstr;
            sstr.str("");
            sstr << "Invalid board:device specified (" << a_board << ":" << a_device << ")";
            throw std::invalid_argument(sstr.str().c_str());
            break;
    } // switch (board)
    return instrumentIdx;
} // mbp::buoy::getInstrument()

} // namespace buoy
} // namespace mbp
