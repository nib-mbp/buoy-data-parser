#ifndef _TCP_SERVER_HPP
#define _TCP_SERVER_HPP

#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/deadline_timer.hpp>

#define MBP_BUOY_TCP_SERVER_CMD_TIMEOUT_s    10

namespace mbp {
namespace buoy {


class TcpServerSession {
public:
    TcpServerSession(
            boost::asio::io_service& io_service,
            boost::posix_time::time_duration a_cmdTimeout);

    boost::asio::ip::tcp::socket& socket() { return m_socket; };
    void start();

private:
    void check_deadline();
    void handle_read(
            const boost::system::error_code& error,
            size_t bytes_transferred);
    void handle_write(const boost::system::error_code& error);

    boost::asio::io_service& m_ioService;
    boost::asio::ip::tcp::socket m_socket;
    boost::asio::deadline_timer m_deadlineTimer;
    boost::asio::streambuf m_streamBuffer;
    boost::posix_time::time_duration m_cmdTimeout;
}; // class session

class TcpServer {
public:
    TcpServer(
            uint16_t port,
            boost::posix_time::time_duration a_cmdTimeout =
                boost::posix_time::seconds(MBP_BUOY_TCP_SERVER_CMD_TIMEOUT_s));

    void run() { m_ioService.run(); };
private:
    void start_accept();

    void handle_accept(
            TcpServerSession *a_newSession_p,
            const boost::system::error_code& error);

    boost::asio::io_service m_ioService;
    boost::asio::ip::tcp::acceptor m_tcpAcceptor;
    boost::posix_time::time_duration m_cmdTimeout;
};


}; // namespace buoy
}; // namespace mbp

#endif // _TCP_SERVER_HPP
