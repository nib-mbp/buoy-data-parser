#include <iostream>
#include <string>
#include <sstream>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>

#include "buoy_data.hpp"
#include "buoy_log.hpp"
#include "buoy_data_holder_utils.hpp"
#include "buoy_datatypes.hpp"
#include "buoy_utils.hpp"



#define CHECK_MIN_STRUCT_SIZE(_minSize, _size)              \
    if ((_size) < (_minSize)) {                             \
        std::ostringstream sstr("");                        \
        sstr <<                                             \
            "Invalid structure size provided (got: " <<     \
            (_size) << " / should be min: " <<              \
            (_minSize) << ").";                             \
        throw sstr.str();                                   \
    }
/* -- Modified on June 23rd 2011 by Damir D.:
 *    The buoy software modifications introduced by Damjan S.
 *    changed the size of data structures received by the buoy.
 *    The old version behaved like #pragma pack(4) was used;
 *    however, the new version removed some redundant spare
 *    bytes from the end of sturcts and now mostly behaves
 *    like #pragma pack(2) is used. The problem is it isn't
 *    100% consistent and so the struct FULL_CHECK can't be
 *    used - switch to 'MIN_CHECK' just to guarantee no
 *    buffer overread will happen */
/*
#define CHECK_FULL_STRUCT_SIZE(_type, _size)                \
    if ((_size) != sizeof(_type)) {                         \
        std::ostringstream sstr("");                        \
        sstr <<                                             \
            "Invalid size provided for struct '" <<         \
            #_type << "' (got: " << (_size) <<              \
            " / should be: " << sizeof(_type) << ").";      \
        throw sstr.str();                                   \
    }
*/
#define CHECK_FULL_STRUCT_SIZE(_type, _size)                \
    if ((_size) < sizeof(_type)) {                          \
        std::ostringstream sstr("");                        \
        sstr <<                                             \
            "Invalid size provided for struct '" <<         \
            #_type << "' (got: " << (_size) <<              \
            " / should be min: " << sizeof(_type) << ").";  \
        throw sstr.str();                                   \
    }







mbp::BuoyData::BuoyData(mbp::BuoySharedArray<uint8_t>& a_buoyArray_sp)
    :
        m_buoyArray_sp(a_buoyArray_sp),
        m_tblFields(new mbp::buoy::tbls_mt()),
        m_rows(new mbp::buoy::rows_lt()),
        m_awacData(m_rows),
        m_parsingType(PARSING_TYPE_NA_e),
        m_runtimeData_p(NULL)
{
    BOOST_LOG_FUNCTION();

    //m_awacData.Reset(m_rows);

    _FillColumns();
} // mbp::BuoyData::BuoyData()


/**
 * This function parses the content of the internal BuoySharedArray buffer.
 *
 * @return N/A
 */
void mbp::BuoyData::ParseBuffer(
        mbp::parsing_type_e a_parsingType,
        mbp::RuntimeData *a_runtimeData_p)
    throw(std::runtime_error)
{
    BOOST_LOG_FUNCTION();
    if (a_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
        if (a_runtimeData_p == NULL) {
            throw std::string("TODO: DamirD: Invalid value");
        }
        m_runtimeData_p = a_runtimeData_p;
    }
    m_parsingType = a_parsingType;

    // -- Purge old data (if any) - need to be done for multiple input file parsing support --
    m_rows.reset();
    m_rows = mbp::buoy::rows_spt(new mbp::buoy::rows_lt());
    m_awacData.Reset(m_rows);

    while (m_buoyArray_sp.isMoreDataAvailable()) {
        _MapNextDataset();
    }

    m_parsingType = PARSING_TYPE_NA_e;

    return;
} //mbp::BuoyData::ParseBuffer()



/**
 * This function fills in the metadata about the data that this class compose
 * by parsing the given buffer.
 *
 * @return N/A
 */
void mbp::BuoyData::_FillColumns(void)
{
    BOOST_LOG_FUNCTION();

    mbp::buoy::field_names_lt tblFields;
    mbp::buoy::table_e tblType = mbp::buoy::TBL_NA_e;

    // TBL_WIND_e
    tblType = mbp::buoy::TBL_WIND_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("msec");
    tblFields.push_back("packet_id");
    tblFields.push_back("U");
    tblFields.push_back("V");
    tblFields.push_back("W");
    tblFields.push_back("sound_speed");
    tblFields.push_back("sonic_temperature");
    tblFields.push_back("device_error_code");
    tblFields.push_back("error_code");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_COMPASS_e
    tblType = mbp::buoy::TBL_COMPASS_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("msec");
    tblFields.push_back("packet_id");
    tblFields.push_back("accx");
    tblFields.push_back("accy");
    tblFields.push_back("accz");
    tblFields.push_back("gyrx");
    tblFields.push_back("gyry");
    tblFields.push_back("gyrz");
    tblFields.push_back("magx");
    tblFields.push_back("magy");
    tblFields.push_back("magz");
    tblFields.push_back("roll");
    tblFields.push_back("pitch");
    tblFields.push_back("yaw");
    tblFields.push_back("error_code");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));
    
    // TBL_CO2_e
    tblType = mbp::buoy::TBL_CO2_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("value");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AIR_CO2_e
    tblType = mbp::buoy::TBL_AIR_CO2_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("co2_filtered");
    tblFields.push_back("co2_raw");
    tblFields.push_back("temperature");
    tblFields.push_back("instrument_code");
    tblFields.push_back("error_code");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_OXYGEN_e
    tblType = mbp::buoy::TBL_OXYGEN_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("device_id");
    tblFields.push_back("concentration");
    tblFields.push_back("saturation");
    tblFields.push_back("temperature");
    tblFields.push_back("error_code");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_GPS_POSITION_e
    tblType = mbp::buoy::TBL_GPS_POSITION_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("longitude");
    tblFields.push_back("latitude");
    tblFields.push_back("altitude");
    tblFields.push_back("satelites");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_GPS_STATUS_e
    tblType = mbp::buoy::TBL_GPS_STATUS_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("status");
    tblFields.push_back("antenna");
    tblFields.push_back("battery");
    tblFields.push_back("almanac");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_GPS_SYNC_e
    tblType = mbp::buoy::TBL_GPS_SYNC_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("time_difference");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_IO_ALARMS_e
    tblType = mbp::buoy::TBL_IO_ALARMS_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("msec");
    tblFields.push_back("packet_id");
    tblFields.push_back("board");
    tblFields.push_back("device");
    tblFields.push_back("error_code");
    tblFields.push_back("device_mode");
    tblFields.push_back("device_state");
    tblFields.push_back("device_name");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_BUOY_INTERIOR_SHT21_e
    tblType = mbp::buoy::TBL_BUOY_INTERIOR_SHT21_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("temperature");
    tblFields.push_back("humidity");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_ALARMS_e
    tblType = mbp::buoy::TBL_ALARMS_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("msec");
    tblFields.push_back("packet_id");
    tblFields.push_back("alarm_code");
//    tblFields.push_back("priority");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_MAIN_BOARD_e
    tblType = mbp::buoy::TBL_MAIN_BOARD_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("current");
    tblFields.push_back("temperature");
    tblFields.push_back("humidity");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_MPPT_e
    tblType = mbp::buoy::TBL_MPPT_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("Uin_min");
    tblFields.push_back("Uin_max");
    tblFields.push_back("Uin_avg");
    tblFields.push_back("Iin_min");
    tblFields.push_back("Iin_max");
    tblFields.push_back("Iin_avg");
    tblFields.push_back("Ubat_min");
    tblFields.push_back("Ubat_max");
    tblFields.push_back("Ubat_avg");
    tblFields.push_back("Iout_min");
    tblFields.push_back("Iout_max");
    tblFields.push_back("Iout_avg");
    tblFields.push_back("output_charge");
    tblFields.push_back("remaining_eq_time");
    tblFields.push_back("days_last_full_charge");
    tblFields.push_back("days_last_equalize");
    tblFields.push_back("total_charge");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_MPPT_STATUS_e
    tblType = mbp::buoy::TBL_MPPT_STATUS_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("msec");
    tblFields.push_back("packet_id");
    tblFields.push_back("status");
    tblFields.push_back("flags");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_SEA_WATER_e
    tblType = mbp::buoy::TBL_SEA_WATER_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("temperature");
    tblFields.push_back("conductivity");
    tblFields.push_back("salinity");
    tblFields.push_back("chlorophile");
    tblFields.push_back("acquisition_time");
    tblFields.push_back("error_code");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AIR_e
    tblType = mbp::buoy::TBL_AIR_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("humidity");
    tblFields.push_back("temperature");
    tblFields.push_back("error_code");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_POWER_SECTIONS_e
    tblType = mbp::buoy::TBL_POWER_SECTIONS_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("channel");
    tblFields.push_back("min_current");
    tblFields.push_back("max_current");
    tblFields.push_back("avg_current");
    tblFields.push_back("accumulated_charge");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_IO_POWER_e
    tblType = mbp::buoy::TBL_IO_POWER_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("board");
    tblFields.push_back("port");
    tblFields.push_back("min_current");
    tblFields.push_back("max_current");
    tblFields.push_back("avg_current");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_BATTERIES_e
    tblType = mbp::buoy::TBL_BATTERIES_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("channel");
    tblFields.push_back("min_current");
    tblFields.push_back("max_current");
    tblFields.push_back("avg_current");
    tblFields.push_back("accumulated_charge");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_BAT_VOLTAGE_e
    tblType = mbp::buoy::TBL_BAT_VOLTAGE_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("min_voltage");
    tblFields.push_back("max_voltage");
    tblFields.push_back("avg_voltage");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_FUEL_CELL_STATUS_e
    tblType = mbp::buoy::TBL_FUEL_CELL_STATUS_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("msec");
    tblFields.push_back("packet_id");
    tblFields.push_back("switched_on");
    tblFields.push_back("charging");
    tblFields.push_back("temperature_overrange");
    tblFields.push_back("fuel_low");
    tblFields.push_back("no_fuel");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_TIME_SYNC_IO_e
    tblType = mbp::buoy::TBL_TIME_SYNC_IO_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("board");
    tblFields.push_back("time_difference");
    tblFields.push_back("old_counter");
    tblFields.push_back("new_counter");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_COMSTAT_MAIN_IO_e
    tblType = mbp::buoy::TBL_COMSTAT_MAIN_IO_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("board");
    tblFields.push_back("present");
    tblFields.push_back("initialized");
    tblFields.push_back("packet_errors");
    tblFields.push_back("multiple_errors");
    tblFields.push_back("soft_errors");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_USER_CONFIG_e
    tblType = mbp::buoy::TBL_AWAC2_USER_CONFIG_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("T1");
    tblFields.push_back("T2");
    tblFields.push_back("T3");
    tblFields.push_back("T4");
    tblFields.push_back("T5");
    tblFields.push_back("n_pings");
    tblFields.push_back("avg_interval");
    tblFields.push_back("n_beams");
    tblFields.push_back("timing_ctrl_reg_hex");
    tblFields.push_back("power_ctrl_reg_hex");
    tblFields.push_back("compass_update");
    tblFields.push_back("coord_system");
    tblFields.push_back("n_cells");
    tblFields.push_back("cell_size");
    tblFields.push_back("meas_interval");
    tblFields.push_back("deploy_name");
    tblFields.push_back("wrap_mode");
    tblFields.push_back("deployment_time");
    tblFields.push_back("diagnostic");
    tblFields.push_back("mode_status_hex");
    tblFields.push_back("adj_sound_speed");
    tblFields.push_back("n_samp_diag");
    tblFields.push_back("n_beamscell_diag");
    tblFields.push_back("n_pings_diag");
    tblFields.push_back("test_mode_status_hex");
    tblFields.push_back("ana_in_adddr");
    tblFields.push_back("sw_version");
    tblFields.push_back("vel_adj_table_bin");
    tblFields.push_back("file_comments");
    tblFields.push_back("wave_mode_hex");
    tblFields.push_back("dyn_perc_pos");
    tblFields.push_back("wT1");
    tblFields.push_back("wT2");
    tblFields.push_back("wT3");
    tblFields.push_back("n_samples");
    tblFields.push_back("ana_out_scale");
    tblFields.push_back("corr_treshold");
    tblFields.push_back("ti_lag2");
    tblFields.push_back("qual_const_bin");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_HEAD_CONFIG_e
    tblType = mbp::buoy::TBL_AWAC2_HEAD_CONFIG_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("config_hex");
    tblFields.push_back("frequency");
    tblFields.push_back("head_type");
    tblFields.push_back("head_sn");
    tblFields.push_back("n_beams");
    tblFields.push_back("system_data_bin");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_HW_CONFIG_e
    tblType = mbp::buoy::TBL_AWAC2_HW_CONFIG_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("sys_sn");
    tblFields.push_back("prolog_sn");
    tblFields.push_back("prolog_ver");
    tblFields.push_back("config_hex");
    tblFields.push_back("board_frequency");
    tblFields.push_back("pic_code");
    tblFields.push_back("hw_revision");
    tblFields.push_back("rec_size");
    tblFields.push_back("status_hex");
    tblFields.push_back("firmware_ver");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_CURRENTS_HEADER_e
    tblType = mbp::buoy::TBL_AWAC2_CURRENTS_HEADER_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("profile_start_time");
    tblFields.push_back("awac_time");
    tblFields.push_back("error_code");
    tblFields.push_back("ana_in");
    tblFields.push_back("battery");
    tblFields.push_back("sound_speed");
    tblFields.push_back("heading");
    tblFields.push_back("pitch");
    tblFields.push_back("roll");
    tblFields.push_back("pressure");
    tblFields.push_back("temperature");
    tblFields.push_back("status");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_CURRENTS_DATA_e
    tblType = mbp::buoy::TBL_AWAC2_CURRENTS_DATA_e;
    tblFields.clear();
    tblFields.push_back("currents_hdr_id");
    tblFields.push_back("cell_no");
    tblFields.push_back("current_E");
    tblFields.push_back("current_N");
    tblFields.push_back("current_W");
    tblFields.push_back("amp_E");
    tblFields.push_back("amp_N");
    tblFields.push_back("amp_W");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_WAVES_e
    tblType = mbp::buoy::TBL_AWAC2_WAVES_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("profile_start_time");
    tblFields.push_back("awac_time");
    tblFields.push_back("spectrum_type");
    tblFields.push_back("proc_method");
    tblFields.push_back("Hm0");
    tblFields.push_back("H3");
    tblFields.push_back("H10");
    tblFields.push_back("H_max");
    tblFields.push_back("Tm02");
    tblFields.push_back("Tp");
    tblFields.push_back("Tz");
    tblFields.push_back("dir_Tp");
    tblFields.push_back("spr_Tp");
    tblFields.push_back("dir_mean");
    tblFields.push_back("UI");
    tblFields.push_back("pressure_mean");
    tblFields.push_back("no_detect");
    tblFields.push_back("bad_detect");
    tblFields.push_back("mean_current_speed");
    tblFields.push_back("mean_current_direction");
    tblFields.push_back("error_code");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_WAVEBANDS_e
    tblType = mbp::buoy::TBL_AWAC2_WAVEBANDS_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("profile_start_time");
    tblFields.push_back("awac_time");
    tblFields.push_back("spectrum_type");
    tblFields.push_back("proc_method");
    tblFields.push_back("low_freq");
    tblFields.push_back("hi_freq");
    tblFields.push_back("Hm0");
    tblFields.push_back("Tm02");
    tblFields.push_back("Tp");
    tblFields.push_back("dir_Tp");
    tblFields.push_back("dir_mean");
    tblFields.push_back("spr_Tp");
    tblFields.push_back("error_code");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_WAVESPECTRUM_HEADER_e
    tblType = mbp::buoy::TBL_AWAC2_WAVESPECTRUM_HEADER_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("profile_start_time");
    tblFields.push_back("awac_time");
    tblFields.push_back("spectrum_type");
    tblFields.push_back("n_spectrum");
    tblFields.push_back("low_freq");
    tblFields.push_back("hi_freq");
    tblFields.push_back("step_freq");
    tblFields.push_back("energy_mult");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_WAVESPECTRUM_DATA_e
    tblType = mbp::buoy::TBL_AWAC2_WAVESPECTRUM_DATA_e;
    tblFields.clear();
    tblFields.push_back("wave_spectrum_hdr_id");
    tblFields.push_back("idx");
    tblFields.push_back("energy");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_FOURIER_HEADER_e
    tblType = mbp::buoy::TBL_AWAC2_FOURIER_HEADER_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("packet_id");
    tblFields.push_back("profile_start_time");
    tblFields.push_back("awac_time");
    tblFields.push_back("proc_method");
    tblFields.push_back("n_spectrum");
    tblFields.push_back("low_freq");
    tblFields.push_back("hi_freq");
    tblFields.push_back("step_freq");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_AWAC2_FOURIER_DATA_e
    tblType = mbp::buoy::TBL_AWAC2_FOURIER_DATA_e;
    tblFields.clear();
    tblFields.push_back("wave_fourier_hdr_id");
    tblFields.push_back("idx");
    tblFields.push_back("A1");
    tblFields.push_back("B1");
    tblFields.push_back("A2");
    tblFields.push_back("B2");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_WRITTER_LOG_e
    tblType = mbp::buoy::TBL_WRITTER_LOG_e;
    tblFields.clear();
    tblFields.push_back("packet_id");
    tblFields.push_back("log");
    tblFields.push_back("filename");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    // TBL_PAR_e
    tblType = mbp::buoy::TBL_PAR_e;
    tblFields.clear();
    tblFields.push_back("time");
    tblFields.push_back("msec");
    tblFields.push_back("packet_id");
    tblFields.push_back("value_raw");
    tblFields.push_back("error_code");
    m_tblFields->insert(mbp::buoy::tbls_pt(tblType, tblFields));

    return;
} // mbp::BuoyData::_FillColumns()




/**
 * This function maps the current data set to the buoy data structure and
 * stores the parsed data into the row_sp.
 *
 * @return N/A
 */
void mbp::BuoyData::_MapNextDataset(void)
    throw(std::runtime_error)
{
    mbp::buoy::time_header_t *hdr_p =
        reinterpret_cast<mbp::buoy::time_header_t *>(
                &m_buoyArray_sp.get()[
                        m_buoyArray_sp.getPosition()
                        ]);

    mbp::buoy::data_type_e currDataType = mbp::buoy::DATA_NA_e;

    currDataType = mbp::buoy::data_type_e(hdr_p->hdr.id);
/*
    do {
        std::ostringstream sstr("");
        sstr
            << "Processing data type = " << currDataType << " (" <<  boost::format("0x%02X") % currDataType
            << " / current buffer position = "<< m_buoyArray_sp.getPosition() << ")";
        L_(debug) << sstr.str();
    } while (0);
*/

    size_t currDataStrucSize;
    uint32_t recEpochTime = 0;
    if (currDataType == mbp::buoy::EXTENDED_HEADER_DATA_e) {
        // -- Extended header used (encapsulated data) --
        mbp::buoy::time_extended_header_t *extHdr_p(NULL);
        extHdr_p = reinterpret_cast<mbp::buoy::time_extended_header_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

        // Update the record ID to the embeded one + construct the correct record size
        currDataType = mbp::buoy::data_type_e(extHdr_p->hdr.id_lo);
        currDataStrucSize = ((extHdr_p->hdr.size_hi)<<8) + extHdr_p->hdr.size_lo;
        recEpochTime = extHdr_p->sec;
    } else {
        // -- Normal header --
        currDataStrucSize = hdr_p->hdr.size;
        recEpochTime = hdr_p->sec;
    }
    if (currDataStrucSize > m_buoyArray_sp.getDataLength()) {
        std::ostringstream sstr("");
/*
        sstr << "curr size = " << currDataStrucSize << " / usable len = " << m_buoyArray_sp.getUsableLength() << std::endl;
        sstr
            << "Statistics:" << std::endl
            << "buoyArray_sp.getDataLength()   = " << m_buoyArray_sp.getDataLength() << std::endl
            << "buoyArray_sp.getUsableLength() = " << m_buoyArray_sp.getUsableLength() << std::endl
            << "buoyArray_sp.getTotalLength()  = " << m_buoyArray_sp.getTotalLength() << std::endl
            << "buoyArray_sp.getPosition()     = " << m_buoyArray_sp.getPosition() << std::endl;
        sstr
            << "Exceed file buffer boundaries (total data available: " << m_buoyArray_sp.getDataLength()
            << " / record end: " << (m_buoyArray_sp.getPosition() + currDataStrucSize) << ").";
*/
        throw sstr.str();
    }

    // -- Check the current data record time if it is within the allowed time period --
    boost::posix_time::ptime recPTime =
        boost::posix_time::from_time_t(recEpochTime);
/*
// ATTENTION:
//  Currently remove this check as we don't have a reference value any more.
//
    if (
          boost::posix_time::time_period(m_currFilePTime, recPTime).length()
        > m_maxSamplingDuration)
    {
        // Log the message both to logging facility and DB backend
        std::ostringstream sstr("");
        sstr <<
            "Data record timestamp out of allowed range - " <<
            "recId: " << mbp::buoy::ResolveNameFromId(currDataType) << " / " <<
            "recTime: " << mbp::buoy::PtimeToStr(recPTime) << " / " <<
            "timeRange: " << mbp::buoy::PtimeToStr(m_currFilePTime) << " -> +" << m_maxSamplingDuration;
        L_(warning) << sstr.str();

        mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
        row_sp->clear();
        row_sp->push_back(mbp::buoy::DataHolder());
        row_sp->push_back(sstr.str());
        row_sp->push_back(m_filenames[m_currFile]);
        m_rows->push_back(mbp::buoy::TableRow(mbp::buoy::TBL_WRITTER_LOG_e, row_sp));

        // Point to the next header
        m_currBufPos += currDataStrucSize;

        return;
    } // if (... recordPTime out of range ...)
*/

    // -- Parse the current data record (the one the m_currBufPos points to) --
    switch (currDataType) {
        case mbp::buoy::DATA_WIND_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::wind_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::wind_t *data_p =
                    reinterpret_cast<mbp::buoy::wind_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(data_p->hdr.msec);
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(double(data_p->U)/1000.0);
                row_sp->push_back(double(data_p->V)/1000.0);
                row_sp->push_back(double(data_p->W)/1000.0);
                row_sp->push_back(double(data_p->sound_speed)/100.0);
                row_sp->push_back(double(data_p->sonic_temperature)/100.0);
                row_sp->push_back(uint16_t(data_p->dev_err_code));
                row_sp->push_back(uint16_t(data_p->err_code));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setWind(
                            recPTime,
                            double(data_p->U)/1000.0, double(data_p->V)/1000.0, double(data_p->W)/1000.0,
                            uint16_t(data_p->err_code));
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::MPPT_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::mppt_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::mppt_t *data_p =
                    reinterpret_cast<mbp::buoy::mppt_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(double(data_p->Uin_min)/10.0);
                row_sp->push_back(double(data_p->Uin_max)/10.0);
                row_sp->push_back(double(data_p->Uin_avg)/100.0);
                row_sp->push_back(double(data_p->Iin_min)/10.0);
                row_sp->push_back(double(data_p->Iin_max)/10.0);
                row_sp->push_back(double(data_p->Iin_avg)/100.0);
                row_sp->push_back(double(data_p->Ubat_min)/10.0);
                row_sp->push_back(double(data_p->Ubat_max)/10.0);
                row_sp->push_back(double(data_p->Ubat_avg)/100.0);
                row_sp->push_back(double(data_p->Iout_min)/10.0);
                row_sp->push_back(double(data_p->Iout_max)/10.0);
                row_sp->push_back(double(data_p->Iout_avg)/100.0);
                row_sp->push_back(double(data_p->output_charge)/10000.0);
                row_sp->push_back(uint16_t(data_p->remaining_eq_time));
                row_sp->push_back(uint16_t(data_p->days_last_full_charge));
                row_sp->push_back(uint16_t(data_p->days_last_equalize));
                row_sp->push_back(double(data_p->total_charge));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setMppt(
                            recPTime,
                            double(data_p->Iin_avg)/100.0,
                            double(data_p->Iout_avg)/100.0,
                            double(data_p->Uin_avg)/100.0,
                            double(data_p->Ubat_avg)/100.0);
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::MPPT_STATUS_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::mppt_status_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::mppt_status_t *data_p =
                    reinterpret_cast<mbp::buoy::mppt_status_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(data_p->hdr.msec);
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(uint16_t(data_p->status));
                row_sp->push_back(uint16_t(data_p->flags));

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::AD_ENERGY_00_07_e:
/*
                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setWind(
                            recPTime,
                            double(data_p->U)/1000.0, double(data_p->V)/1000.0, double(data_p->W)/1000.0,
                            uint16_t(data_p->err_code));
                }
*/
            // -- faill trough --
        case mbp::buoy::AD_ENERGY_08_15_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::ad_data_t, currDataStrucSize);
                mbp::buoy::table_e tableId(mbp::buoy::TBL_NA_e);
                mbp::buoy::ad_data_t *data_p =
                    reinterpret_cast<mbp::buoy::ad_data_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                if (currDataType == mbp::buoy::AD_ENERGY_00_07_e) {
                    // -- sprememba --
                    for (size_t i=0; i<4; ++i) {
                        // -- napetost baterij [V] --
                        mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                        row_sp->clear();
                        if (! data_p->channel[i].is_active) {
                            continue;
                        }

                        row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                        row_sp->push_back(mbp::buoy::DataHolder());
                        row_sp->push_back(int32_t(i));
                        row_sp->push_back((double((uint16_t)data_p->channel[i].min) + 3080.0)/500.0);
                        row_sp->push_back((double((uint16_t)data_p->channel[i].max) + 3080.0)/500.0);
                        row_sp->push_back((double((uint16_t)data_p->channel[i].avg) + 30800.0)/5000.0);
                        row_sp->push_back(double(0));

                        tableId = mbp::buoy::TBL_BATTERIES_e;
                        m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
                    }
                    for (size_t i=4; i<8; ++i) {
                        // -- tok v / iz baterij [A] --
                        mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                        row_sp->clear();
                        if (! data_p->channel[i].is_active) {
                            continue;
                        }

                        row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                        row_sp->push_back(mbp::buoy::DataHolder());
                        row_sp->push_back(int32_t(i));
                        row_sp->push_back(double(data_p->channel[i].min) * 0.0078125);
                        row_sp->push_back(double(data_p->channel[i].max) * 0.0078125);
                        row_sp->push_back(double(data_p->channel[i].avg) * 0.00078125);
                        row_sp->push_back(double(data_p->channel[i].val) * 0.0078125 / 3600.0);

                        tableId = mbp::buoy::TBL_BATTERIES_e;
                        m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
                    }
                    if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                        m_runtimeData_p->setBatteries(
                                recPTime,
                                double(data_p->channel[4].min) * 0.0078125, double(data_p->channel[5].min) * 0.0078125, double(data_p->channel[6].min) * 0.0078125, double(data_p->channel[7].min) * 0.0078125,
                                double(data_p->channel[4].max) * 0.0078125, double(data_p->channel[5].max) * 0.0078125, double(data_p->channel[6].max) * 0.0078125, double(data_p->channel[7].max) * 0.0078125,
                                double(data_p->channel[4].avg) * 0.00078125, double(data_p->channel[5].avg) * 0.00078125, double(data_p->channel[6].avg) * 0.00078125, double(data_p->channel[7].avg) * 0.00078125,
                                (double((uint16_t)data_p->channel[0].min) + 3080.0)/500.0, (double((uint16_t)data_p->channel[2].min) + 3080.0)/500.0,
                                (double((uint16_t)data_p->channel[0].max) + 3080.0)/500.0, (double((uint16_t)data_p->channel[2].max) + 3080.0)/500.0,
                                (double((uint16_t)data_p->channel[0].avg) + 30800.0)/5000.0, (double((uint16_t)data_p->channel[2].avg) + 30800.0)/5000.0);
                    }
                } else if (currDataType == mbp::buoy::AD_ENERGY_08_15_e) {
                    for (size_t i=0; i<BUOY_AD_NO_OF_CHANNELS; ++i) {
                        mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                        row_sp->clear();
                        if (! data_p->channel[i].is_active) {
                            continue;
                        }

                        row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                        row_sp->push_back(mbp::buoy::DataHolder());
                        switch(i) {
                            case 0:
                            case 1:
                            case 4:
                                {
                                    // tokovne veje: 0, 1: omarce; 4: gorivna celica
                                    // -- TBL_POWER_SECTIONS_e --
                                    row_sp->push_back(int32_t(i));
                                    row_sp->push_back(double(data_p->channel[i].min)*0.0029296875);
                                    row_sp->push_back(double(data_p->channel[i].max)*0.0029296875);
                                    row_sp->push_back(double(data_p->channel[i].avg)*0.00029296875);
                                    row_sp->push_back(double(data_p->channel[i].val)*0.0029296875/3600);

                                    tableId = mbp::buoy::TBL_POWER_SECTIONS_e;

                                    if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e)
                                    {
                                        double current = 0.0;
                                        current = double(data_p->channel[i].avg)*0.00029296875;

                                        if (
                                               (i == 0)
                                            || (i == 1)
                                           )
                                        {
                                            m_runtimeData_p->setCurrentLoop(i, recPTime, current);
                                        }
                                        else if (i == 4)
                                        {
                                            m_runtimeData_p->setFuelCell(
                                                    recPTime,
                                                    current);
                                        }
                                    }
                                }
                                break;
                            case 2:
                                {
                                    // tokovne veje -> signalna luc + se nekaj
                                    // -- TBL_POWER_SECTIONS_e --
                                    row_sp->push_back(int32_t(i));
                                    row_sp->push_back(double(data_p->channel[i].min)*0.0009765625);
                                    row_sp->push_back(double(data_p->channel[i].max)*0.0009765625);
                                    row_sp->push_back(double(data_p->channel[i].avg)*0.00009765625);
                                    row_sp->push_back(double(data_p->channel[i].val)*0.0009765625/3600);

                                    tableId = mbp::buoy::TBL_POWER_SECTIONS_e;

                                    if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e)
                                    {
                                        double current = 0.0;
                                        current = double(data_p->channel[i].avg)*0.00009765625;

                                        m_runtimeData_p->setCurrentLoop(i, recPTime, current);
                                    }
                                }
                                break;
                            case 3:
                                {
                                    // napetost porabnikov (napajanje sistema)
                                    // -- TBL_BAT_VOLTAGE_e --
                                    row_sp->push_back((double((uint16_t)data_p->channel[i].min)+4000.0)/500.0);
                                    row_sp->push_back((double((uint16_t)data_p->channel[i].max)+4000.0)/500.0);
                                    row_sp->push_back((double((uint16_t)data_p->channel[i].avg)+40000.0)/5000.0);

                                    tableId = mbp::buoy::TBL_BAT_VOLTAGE_e;

                                    if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                                        double voltage = 0.0;
                                        voltage = (double((uint16_t)data_p->channel[i].avg)+40000.0) / 5000.0;

                                        m_runtimeData_p->setCmnVoltage(
                                                recPTime,
                                                voltage);
                                    }
                                }
                                break;
                        } // switch(i)

                        m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
                    } // for (... i<BUOY_AD_NO_OF_CHANNELS ...)
                } else {
                    std::ostringstream sstr("");
                    sstr << "Internal error - data type " << currDataType << " shouldn't appear here!";
                    throw sstr.str();
                }
            }
            break;
        case mbp::buoy::FUEL_CELL_STATUS_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::fuel_cell_status_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::fuel_cell_status_t *data_p =
                    reinterpret_cast<mbp::buoy::fuel_cell_status_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(data_p->hdr.msec);
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(bool(data_p->status & (1<<0)));
                row_sp->push_back(bool(data_p->status & (1<<1)));
                row_sp->push_back(bool(data_p->status & (1<<2)));
                row_sp->push_back(bool(data_p->status & (1<<3)));
                row_sp->push_back(bool(data_p->status & (1<<4)));

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::ENERGY_STATUS_e:
            // TODO / FIXME: currently skipping as the received data don't conform to the new type yet
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::energy_status_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::energy_status_t *data_p =
                    reinterpret_cast<mbp::buoy::energy_status_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(uint16_t(255));
                row_sp->push_back(int32_t(data_p->timedif));
                row_sp->push_back(uint32_t(data_p->old_timer));
                row_sp->push_back(uint32_t(data_p->new_timer));

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::GPS_TIME_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::gps_sync_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::gps_sync_t *data_p =
                    reinterpret_cast<mbp::buoy::gps_sync_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(int32_t(data_p->time_diff));

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::GPS_POSITION_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::gps_position_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::gps_position_t *data_p =
                    reinterpret_cast<mbp::buoy::gps_position_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(double(data_p->longitude_i) + double(data_p->longitude_f)/10000000.0);
                row_sp->push_back(double(data_p->latitude_i) + double(data_p->latitude_f)/10000000.0);
                row_sp->push_back(double(data_p->altitude));
                row_sp->push_back(uint16_t(data_p->noSatellites));

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::GPS_STATUS_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::gps_status_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::gps_status_t *data_p =
                    reinterpret_cast<mbp::buoy::gps_status_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(uint16_t(data_p->status));
                row_sp->push_back(uint16_t(data_p->antenna));
                row_sp->push_back(uint16_t(data_p->battery));
                row_sp->push_back(uint16_t(data_p->almanac));

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::BOARD_STATUS_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::board_status_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::board_status_t *data_p =
                    reinterpret_cast<mbp::buoy::board_status_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(uint16_t(data_p->board));
                row_sp->push_back(bool(data_p->is_present));
                row_sp->push_back(bool(data_p->is_initialized));
                row_sp->push_back(uint32_t(data_p->pckt_error));
                row_sp->push_back(uint32_t(data_p->multiple_pckt_error));
                row_sp->push_back(uint32_t(data_p->soft_pckt_error));

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::MAIN_BOARD_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::main_board_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::main_board_t *data_p =
                    reinterpret_cast<mbp::buoy::main_board_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
//                row_sp->push_back(double(data_p->voltage)/1000.0);
                row_sp->push_back(double(data_p->current)/1000.0);
                row_sp->push_back(double(data_p->temperature)/10.0);
                row_sp->push_back(double(data_p->humidity));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setBBoard(
                            recPTime,
                            double(data_p->current)/1000.0,
                            double(data_p->temperature)/10.0,
                            double(data_p->humidity));
/*
std::cout
    << "MAIN_Board ["
    << "[current=" << double(data_p->current)/1000.0
    << ", temperature=" << double(data_p->temperature)/10.0
    << ", humidity=" << double(data_p->humidity)
    << "]" << std::endl;
*/
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::DATA_CO2_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::co2_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::co2_t *data_p =
                    reinterpret_cast<mbp::buoy::co2_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(double(data_p->value));                   // value - TODO: normalize!!!

//std::cout << "CO2 data !!!" << std::endl;
                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setBuoyCO2(
                            recPTime,
                            double(data_p->value),
                            uint16_t(0));
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::DATA_AIR_CO2_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::air_co2_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::air_co2_t *data_p =
                    reinterpret_cast<mbp::buoy::air_co2_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(double(data_p->co2_filtered)/10.0);
                row_sp->push_back(double(data_p->co2_raw)/10.0);
                row_sp->push_back(double(data_p->temperature)/10.0);
                row_sp->push_back(uint16_t(data_p->instr_code));
                row_sp->push_back(uint16_t(data_p->err_code));

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::BUOY_INTERIOR_SHT21_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::buoy_interior_sht21_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::buoy_interior_sht21_t *data_p =
                    reinterpret_cast<mbp::buoy::buoy_interior_sht21_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(double(int16_t(data_p->temperature))/100.0);
                row_sp->push_back(uint16_t(data_p->humidity));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setBInterior(
                            recPTime,
                            double(int16_t(data_p->temperature))/100.0,
                            uint16_t(data_p->humidity));
                }


                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::DATA_COMPASS_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::compass_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::compass_t *data_p =
                    reinterpret_cast<mbp::buoy::compass_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(data_p->hdr.msec);
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(double(data_p->accx));
                row_sp->push_back(double(data_p->accy));
                row_sp->push_back(double(data_p->accz));
                row_sp->push_back(double(data_p->gyrx));
                row_sp->push_back(double(data_p->gyry));
                row_sp->push_back(double(data_p->gyrz));
                row_sp->push_back(double(data_p->magx));
                row_sp->push_back(double(data_p->magy));
                row_sp->push_back(double(data_p->magz));
                row_sp->push_back(double(data_p->roll));
                row_sp->push_back(double(data_p->pitch));
                row_sp->push_back(double(data_p->yaw));
                row_sp->push_back(uint16_t(data_p->err_code));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setCompass(
                            recPTime,
                            double(data_p->roll),
                            double(data_p->pitch),
                            double(data_p->yaw),
                            uint16_t(data_p->err_code));
                }
                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::DATA_ADC_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::adc_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::adc_t *data_p =
                    reinterpret_cast<mbp::buoy::adc_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                for (size_t i=0; i<BUOY_ADC_NO_OF_PORTS; ++i) {
                    mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                    row_sp->clear();

                    row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                    row_sp->push_back(mbp::buoy::DataHolder());
                    row_sp->push_back(uint16_t(data_p->board));
                    row_sp->push_back(uint16_t(i));
                    row_sp->push_back(double(data_p->min[i])*3.0 / 1000.0);
                    row_sp->push_back(double(data_p->max[i])*3.0 / 1000.0);
                    row_sp->push_back(double(data_p->avg[i])*3.0 / 10000.0);

                    if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                        m_runtimeData_p->setInstrumentCurrent(
                                uint16_t(data_p->board), uint16_t(i),
                                recPTime,
                                double(data_p->min[i])*3.0 / 1000.0,
                                double(data_p->max[i])*3.0 / 1000.0,
                                double(data_p->avg[i])*3.0 / 10000.0);
/*
std::cout
    << "DATA_ADC_e ["
    << "pTime=" << recPTime
    << " / board=" << uint16_t(data_p->board)
    << " / port=" << uint16_t(i)
    << " / min=" << double(data_p->min[i])*3.0 / 1000.0
    << " / max=" << double(data_p->max[i])*3.0 / 1000.0
    << " / avg=" << double(data_p->avg[i])*3.0 / 10000.0
    << std::endl;
*/
                    }

                    m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
                } // for (... i<BUOY_ADC_NO_OF_PORTS ...)
            }
            break;
        case mbp::buoy::DATA_HUMIDITY_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::humidity_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::humidity_t *data_p =
                    reinterpret_cast<mbp::buoy::humidity_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(double(data_p->humidity));
                row_sp->push_back(double(data_p->temperature) / 10.0);
                row_sp->push_back(uint16_t(data_p->err_code));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setAir(
                            recPTime,
                            double(data_p->temperature) / 10.0,
                            double(data_p->humidity),
                            uint16_t(data_p->err_code));
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::DATA_SEA_WATER_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::sea_water_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::sea_water_t *data_p =
                    reinterpret_cast<mbp::buoy::sea_water_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(double(data_p->temperature)/10000.0);
                row_sp->push_back(double(data_p->conductivity)/100000.0);
                row_sp->push_back(double(data_p->salinity)/10000.0);
                row_sp->push_back(uint16_t(data_p->chlorophile));
                std::ostringstream sstrTime("");
                sstrTime <<
                    std::setfill('0') <<
                    std::setw(4) << uint16_t(data_p->acq_time.year) << std::setw(0) << "-" <<
                    std::setw(2) << uint16_t(data_p->acq_time.month) << std::setw(0) << "-" <<
                    std::setw(2) << uint16_t(data_p->acq_time.day) << std::setw(0) << "T" <<
                    std::setw(2) << uint16_t(data_p->acq_time.hour) << std::setw(0) << ":" <<
                    std::setw(2) << uint16_t(data_p->acq_time.minute) << std::setw(0) << ":" <<
                    std::setw(2) << uint16_t(data_p->acq_time.second);
                row_sp->push_back(sstrTime.str());
                row_sp->push_back(uint16_t(data_p->err_code));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setSeaWater(
                            recPTime,
                            double(data_p->temperature)/10000.0,
                            double(data_p->conductivity)/100000.0,
                            double(data_p->salinity)/10000.0,
                            uint16_t(data_p->chlorophile),
                            uint16_t(data_p->err_code));
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::DATA_OXYGEN3835_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::oxygen3835_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::oxygen3835_t *data_p =
                    reinterpret_cast<mbp::buoy::oxygen3835_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(int16_t(mbp::buoy::OXYGEN_DEVICE_ID_3835_e));
                row_sp->push_back(double(data_p->concentration)/100000.0);
                row_sp->push_back(double(data_p->saturation)/100.0);
                row_sp->push_back(double(data_p->temperature)/100.0);
                row_sp->push_back(uint16_t(data_p->err_code));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setOxygen(
                            mbp::buoy::OXYGEN_DEVICE_ID_3835_e,
                            recPTime,
                            double(data_p->concentration)/100000.0,
                            uint16_t(data_p->err_code));
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::DATA_OXYGEN4835_1_e:
        case mbp::buoy::DATA_OXYGEN4835_2_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::oxygen4835_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::oxygen4835_t *data_p =
                    reinterpret_cast<mbp::buoy::oxygen4835_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                if (currDataType == mbp::buoy::DATA_OXYGEN4835_1_e) {
                    row_sp->push_back(int16_t(mbp::buoy::OXYGEN_DEVICE_ID_4835_1_e));
                } else {
                    row_sp->push_back(int16_t(mbp::buoy::OXYGEN_DEVICE_ID_4835_2_e));
                }
                row_sp->push_back(double(data_p->concentration)/1000000.0);
                row_sp->push_back(double(data_p->saturation)/1000.0);
                row_sp->push_back(double(data_p->temperature)/100.0);
                row_sp->push_back(uint16_t(data_p->err_code));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    int devID=0;
                    if (currDataType == mbp::buoy::DATA_OXYGEN4835_1_e) {
                        devID = mbp::buoy::OXYGEN_DEVICE_ID_4835_1_e;
                    } else {
                        devID = mbp::buoy::OXYGEN_DEVICE_ID_4835_2_e;
                    }
                    m_runtimeData_p->setOxygen(
                            devID,
                            recPTime,
                            double(data_p->concentration)/1000000.0,
                            uint16_t(data_p->err_code));
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::IO_ALARMS_e:
            {
                CHECK_MIN_STRUCT_SIZE((BUOY_IO_ALARMS_HEADER_SIZE), currDataStrucSize);
                mbp::buoy::table_e tableId(mbp::buoy::TBL_NA_e);
                mbp::buoy::io_alarms_t *data_p =
                    reinterpret_cast<mbp::buoy::io_alarms_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                // -- TBL_IO_ALARMS_e --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(data_p->hdr.msec);
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(uint16_t(data_p->board));
                row_sp->push_back(uint16_t(data_p->device));
                row_sp->push_back(uint16_t(data_p->err_code));
                row_sp->push_back(uint16_t(data_p->device_mode));
                row_sp->push_back(uint16_t(data_p->device_state));
                if (data_p->hdr.hdr.size > BUOY_IO_ALARMS_HEADER_SIZE) {
                    row_sp->push_back(std::string(reinterpret_cast<char *>(data_p->device_name), (currDataStrucSize-BUOY_IO_ALARMS_HEADER_SIZE)));
                } else {
                    row_sp->push_back(std::string(""));
                }

                tableId = mbp::buoy::TBL_IO_ALARMS_e;
                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
/*
std::cout
    << "IO_ALARM_DATA -> t: '" << mbp::buoy::PtimeToStr(recPTime) << "'"
    << " / board:device = " << uint16_t(data_p->board) << ":" << uint16_t(data_p->device)
    << " / mode = " << uint16_t(data_p->device_mode)
    << " / state = " << uint16_t(data_p->device_state)
    << " / erro_code = " << uint16_t(data_p->err_code)
    << std::endl;
*/
                    try {
                        m_runtimeData_p->setIoAlarm(
                                recPTime,
                                uint16_t(data_p->board),
                                uint16_t(data_p->device),
                                uint16_t(data_p->device_mode),
                                uint16_t(data_p->device_state),
                                uint16_t(data_p->err_code));
                    } catch (const std::invalid_argument &e) {
                        std::ostringstream sstr("");
                        sstr << "ARG ERROR: Failed setting IO ALARM - " << e.what();
                        std::cout << sstr.str() << std::endl;
                    }
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
/*
                // -- TBL_ALARMS_e --
                row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                row_sp->clear();
                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(data_p->hdr.msec);
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(uint16_t(data_p->err_code + 1000));
                row_sp->push_back(uint16_t(0));

                tableId = mbp::buoy::TBL_ALARMS_e;
                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
*/
            }
            break;
        case mbp::buoy::ALARM_DATA_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::alarm_data_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::alarm_data_t *data_p =
                    reinterpret_cast<mbp::buoy::alarm_data_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(data_p->hdr.msec);
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(uint16_t(data_p->alarm_code));
//                row_sp->push_back(uint16_t(0));
                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
/*
std::cout
    << "ALARM_DATA -> t: '"
    << mbp::buoy::PtimeToStr(recPTime)
    << "' / code = "
    << uint16_t(data_p->alarm_code)
    << std::endl;
*/
                    m_runtimeData_p->setAlarm(recPTime, uint16_t(data_p->alarm_code));
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::IO_ALARM_TIME_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::io_alarm_time_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::io_alarm_time_t *data_p =
                    reinterpret_cast<mbp::buoy::io_alarm_time_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(mbp::buoy::DataHolder());
                row_sp->push_back(uint16_t(data_p->board));
                row_sp->push_back(int32_t(data_p->time_diff));
                row_sp->push_back(uint32_t(data_p->old_timer));
                row_sp->push_back(uint32_t(data_p->new_timer));

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;
        case mbp::buoy::DATA_PAR_e:
            {
                CHECK_FULL_STRUCT_SIZE(mbp::buoy::par_t, currDataStrucSize);
                mbp::buoy::table_e tableId = mbp::buoy::GetTableFromId(currDataType);
                mbp::buoy::par_t *data_p =
                    reinterpret_cast<mbp::buoy::par_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);

                // -- Populate the row values --
                mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                row_sp->clear();

                row_sp->push_back(mbp::buoy::PtimeToStr(recPTime));
                row_sp->push_back(data_p->hdr.msec);
                row_sp->push_back(mbp::buoy::DataHolder());

                row_sp->push_back(int16_t(data_p->value));
                row_sp->push_back(uint16_t(data_p->err_code));

                if (m_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                    m_runtimeData_p->setPar(
                            recPTime,
                            int16_t(data_p->value),
                            uint16_t(data_p->err_code));
                }

                m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
            }
            break;

        case mbp::buoy::EXT_DATA_AWAC1_e:
            {
                std::ostringstream sstr("");
                sstr.str("");
                sstr << "Found old AWAC data packet, which was used until Jan 2017; mentioned data format isn't supported by this program version any more!";
                L_(trace) << sstr.str();
                throw std::runtime_error(sstr.str().c_str());
            }
            break;
        case mbp::buoy::EXT_DATA_AWAC2_e:
            {
/*
                std::ostringstream sstr("");
                sstr.str("");
                sstr << "NYI: New AWAC (as per Jul 2017) data handling isn't yet implemented!";
                L_(trace) << sstr.str();
                throw std::runtime_error(sstr.str().c_str());
*/
                CHECK_MIN_STRUCT_SIZE(
                        (sizeof(mbp::buoy::extended_header_t) + 4),    // +4: uint32_t UNIXTIME
                        currDataStrucSize);
                mbp::buoy::awac_data_header_t *data_p =
                    reinterpret_cast<mbp::buoy::awac_data_header_t *>(&m_buoyArray_sp.get()[ m_buoyArray_sp.getPosition() ]);
                m_awacData.ProcessDataRecord(data_p, currDataStrucSize, m_parsingType, m_runtimeData_p);
            }
            break;
        default:
            {
                std::ostringstream sstr("");
                sstr << "Found an unknown data type ID = " << currDataType << " (hex: 0x" << std::hex << std::setw(2) << std::uppercase << currDataType << ")";
                L_(trace) << sstr.str();
            }
            break;
    } // switch (m_curDataType)

    // Point to the next header
    m_buoyArray_sp.incPosition(currDataStrucSize);

    return;
} // mbp::BuoyData::_MapNextDataset()


const std::string mbp::BuoyData::Dump(void) const
{
    // -- Construct the output --
    std::ostringstream sstr("");
    bool isFirst(true);

    // Columns
    mbp::buoy::tbls_msp tables_spt = getTables();
    for (mbp::buoy::tbls_mt::iterator hdrIter = tables_spt->begin();
         hdrIter != tables_spt->end();
         ++hdrIter)
    {
        mbp::buoy::table_e tableId = hdrIter->first;
        mbp::buoy::field_names_lt fields = hdrIter->second;

        isFirst = true;
        sstr << "[TableID: " << tableId << "] ";
        for (mbp::buoy::field_names_it iter = fields.begin();
             iter != fields.end();
             ++iter)
        {
            if (isFirst) {
                sstr << *iter;
                isFirst = false;
            } else {
                sstr << ", " << *iter;
            }
        } // for (... fields ...)
        sstr << std::endl;
    } // for (... tables_spt ...)
    
    for (mbp::buoy::rows_it rowsIter = m_rows->begin();
            rowsIter != m_rows->end();
            ++rowsIter)
    {
        isFirst = true;
        mbp::buoy::table_e table = rowsIter->getTable();
        sstr << "table (" << table << "): ";
        mbp::buoy::row_spt row_sp = rowsIter->getRow();
        for (mbp::buoy::row_fields_it rowIter = row_sp->begin();
             rowIter != row_sp->end();
             ++rowIter)
        {
            if (! isFirst) {
                sstr << ", ";
            }
            if ((*rowIter).type() == typeid(std::string)) {
                sstr << "'" << boost::get<std::string>(*rowIter) << "'";
            } else if ((*rowIter).type() == typeid(double)) {
                sstr << boost::get<double>(*rowIter);
            } else if ((*rowIter).type() == typeid(uint16_t)) {
                sstr << boost::get<uint16_t>(*rowIter);
            } else if ((*rowIter).type() == typeid(uint32_t)) {
                sstr << boost::get<uint32_t>(*rowIter);
            } else if ((*rowIter).type() == typeid(int16_t)) {
                sstr << boost::get<int16_t>(*rowIter);
            } else if ((*rowIter).type() == typeid(int32_t)) {
                sstr << boost::get<int32_t>(*rowIter);
            } else if ((*rowIter).type() == typeid(bool)) {
                sstr << boost::get<bool>(*rowIter);
            } else if ((*rowIter).type() == typeid(std::string)) {
                sstr << boost::get<std::string>(*rowIter);
            } else if ((*rowIter).type() == typeid(mbp::buoy::DataHolder)) {
                sstr << mbp::buoy::DataHolder::SEQ_VAR;
            }

            isFirst = false;
        } // for (... row ...)
        sstr << std::endl;
    } // for (... m_rows ...)

    return sstr.str();
} // mbp::BuoyData::Dump()
