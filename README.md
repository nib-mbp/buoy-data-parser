# Buoy Data Parser

This is the NIB/MBP Oceanographic buoy Vida raw data processor. That is, it communicates with the 
buoy data server from which it retrieves raw binary data, parses it and writes parsed data into
MariaDB SQL server.

## Prerequisites

```bash
apt install git g++ cmake libboost-all-dev libmysql++-dev
```

## Build Instructions

```bash
mkdir build
cd build
cmake ..
make -j 4
```
