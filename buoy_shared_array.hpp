#ifndef _BUOY_SHARED_ARRAY_HPP
#define _BUOY_SHARED_ARRAY_HPP

#include <cstring>
#include <string>
#include <sstream>

#include <boost/shared_array.hpp>


namespace mbp {

/**
 * \class BuoySharedArray
 *
 * \ingroup Buoy
 *
 * \brief Raw buoy binary data holder.
 *
 * This class is used to hold the buoy raw binary data. The data contained
 * within is supposed to be 
 */
template <class T>
class BuoySharedArray : public boost::shared_array<T> {
public:
    typedef boost::shared_array<T> super;

    /**
     * The 
     */
    BuoySharedArray(T *a_buf_p, size_t a_bufLen, size_t a_dataLen, size_t a_currentPos=0)
    :
        super(a_buf_p),
        m_bufLen(a_bufLen),
        m_usableDataLen(0),
        m_totalDataLen(0),
        m_currentPos(a_currentPos)
    {
    };


    ~BuoySharedArray() {};


    BuoySharedArray &operator=(BuoySharedArray const &a_array)
    {
        super::operator=(a_array);
        m_bufLen = a_array.m_bufLen;
    };

    void ClearData() { memset(super::get(), 0, m_bufLen); };


    /**
     * Sets the usable data length and the total data length if passed (the one include the padding)
     */
    void setUsableDataLength(size_t a_usableDataLength) { m_usableDataLen = a_usableDataLength; }   //!< Sets usable data length; NOTE: The retrival function returns the length from the current data pointer to the end of the length passed as input to this function
    void setTotalDataLength(size_t a_totalDataLength) { m_totalDataLen = a_totalDataLength; };      //!< Sets the total data length (e.g. total bytes in the UDP packet including the padding)
    void setPosition(size_t a_position) { m_currentPos = a_position; };     //!< Set the current buffer position
    void incPosition(size_t a_increment) { m_currentPos += a_increment; };  //!< Increment the current buffer position by the given argument

    size_t length() const { return m_bufLen; };                             //!< Returns the total size of the buffer
    size_t getPosition() const { return m_currentPos; };                    //!< Returns current buffer position (@see m_currentPos)
    size_t getFreeLength() const { return (m_bufLen - m_currentPos); };     //!< Returns the free buffer length (size from the last occupied byte till the end).
    size_t getDataLength() const { return m_usableDataLen; };                     //!< Returns the total buffer usable data length
    size_t getUsableLength() const { return (m_usableDataLen - m_currentPos); };  //!< Returns the remaining size of the usable data in the buffer (total data - current buffer position)
    // ^^^
    // TODO / FIXME: This function should doesn't really make sense - check in reader if it is used at all.
    //               The problem -> setUsableDataLength sets m_usableDataLen, whiles getUsableLength() substract the current pos from this!
    size_t getTotalLength() const { return m_totalDataLen; };               //!< Returns the total data length including the padding

    std::string getDebugDetails() {
        std::ostringstream sstr;
        sstr.str("");
        sstr
            << "BuoyShareArray ["
            << "length=" << length()
            << " / position=" << getPosition()
            << " / freeLength=" << getFreeLength()
            << " / dataLength=" << getDataLength()
            << " / usableLength=" << getUsableLength()
            << " / totalLength=" << getTotalLength()
            << "]";
        return sstr.str();
    };

    /**
     * The function indicates if the buffer current position pointer already reached the
     * end of the usable data.
     */
    bool isMoreDataAvailable() const {
        if (m_currentPos < m_usableDataLen) {
            return true;
        } else {
            return false;
        }
    };


private:
    size_t m_bufLen;        //!< Stores the total size of the buffer
    size_t m_usableDataLen; //!< Stores the size of the usable buffer (the actual data stored in the buffer)
    size_t m_totalDataLen;  //!< Meant to stores the total data len (including the padding if any)
    size_t m_currentPos;    //!< Stores the current position in the buffer (kind of a pointer to the next byte to be processed)
}; // class BuoySharedArray

} // namespace mbp

#endif // _BUOY_SHARED_ARRAY_HPP
