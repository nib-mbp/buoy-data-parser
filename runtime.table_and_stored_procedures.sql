USE `buoy_data_4_web`;
DROP TABLE IF EXISTS `runtime_data`;
CREATE TABLE `runtime_data` (
  `id` int unsigned NOT NULL,
  `instruments_data` VARCHAR(2048) NOT NULL DEFAULT '{}',
  `sensors_data` VARCHAR(2048) NOT NULL DEFAULT '{}',
  PRIMARY KEY (`id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Buoy runtime data';

DROP PROCEDURE IF EXISTS `p_setRuntimeInstrumentsData`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_setRuntimeInstrumentsData` (
    IN instrumentsJson VARCHAR(2048))
  LANGUAGE SQL
  CONTAINS SQL
  NOT DETERMINISTIC
  MODIFIES SQL DATA
  SQL SECURITY DEFINER
  COMMENT 'Updates instruments runtime data'
BEGIN
  DECLARE _num INT;

  SELECT COUNT(*) INTO @num FROM runtime_data;

  SET _num = @num;

  IF _num > 1 THEN
    DELETE FROM runtime_data;
    SET _num = 0;
  END IF;
  IF _num = 0 THEN
    INSERT INTO `runtime_data` (`instruments_data`) VALUES (instrumentsJson);
  ELSE
    UPDATE `runtime_data` SET `instruments_data`=instrumentsJson;
  END IF;
END;;
DELIMITER ;

DROP PROCEDURE IF EXISTS `p_setRuntimeSensorsData`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_setRuntimeSensorsData` (
    IN sensorsJson VARCHAR(2048))
  LANGUAGE SQL
  CONTAINS SQL
  NOT DETERMINISTIC
  MODIFIES SQL DATA
  SQL SECURITY DEFINER
  COMMENT 'Updates sensors runtime data'
BEGIN
  DECLARE _num INT;

  SELECT COUNT(*) INTO @num FROM runtime_data;

  SET _num = @num;

  IF _num > 1 THEN
    DELETE FROM runtime_data;
    SET _num = 0;
  END IF;
  IF _num = 0 THEN
    INSERT INTO `runtime_data` (`sensors_data`) VALUES (sensorsJson);
  ELSE
    UPDATE `runtime_data` SET `sensors_data`=sensorsJson;
  END IF;
END;;
DELIMITER ;


DROP FUNCTION IF EXISTS `f_getRuntimeInstrumentsData`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_getRuntimeInstrumentsData` ()
RETURNS VARCHAR(2048)
  LANGUAGE SQL
  CONTAINS SQL
  NOT DETERMINISTIC
  READS SQL DATA
  SQL SECURITY DEFINER
  COMMENT 'Retrieves instruments runtime data'
BEGIN
  DECLARE _rv VARCHAR(2048);

  SELECT `instruments_data` INTO _rv FROM runtime_data ORDER BY id DESC LIMIT 1;

  IF _rv IS NULL THEN
    SET _rv = "{}";
  END IF;

  RETURN _rv;
END ;;
DELIMITER ;


DROP FUNCTION IF EXISTS `f_getRuntimeSensorsData`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_getRuntimeSensorsData` ()
RETURNS VARCHAR(2048)
  LANGUAGE SQL
  CONTAINS SQL
  NOT DETERMINISTIC
  READS SQL DATA
  SQL SECURITY DEFINER
  COMMENT 'Retrieves sensors runtime data'
BEGIN
  DECLARE _rv VARCHAR(2048);

  SELECT `sensors_data` INTO _rv FROM runtime_data ORDER BY id DESC LIMIT 1;

  IF _rv IS NULL THEN
    SET _rv = "{}";
  END IF;

  RETURN _rv;
END ;;
DELIMITER ;

GRANT USAGE ON *.* TO 'runtime_rw'@'localhost' IDENTIFIED BY 'BuoyRuntimeRW';
GRANT EXECUTE ON PROCEDURE buoy_data_4_web.p_setRuntimeInstrumentsData TO runtime_rw@'localhost';
GRANT EXECUTE ON PROCEDURE buoy_data_4_web.p_setRuntimeSensorsData TO runtime_rw@'localhost';

GRANT USAGE ON *.* TO 'runtime_ro'@'%' IDENTIFIED BY 'runtimeREADER--';
GRANT EXECUTE ON FUNCTION buoy_data_4_web.f_getRuntimeInstrumentsData TO 'runtime_ro'@'%';
GRANT EXECUTE ON FUNCTION buoy_data_4_web.f_getRuntimeSensorsData TO 'runtime_ro'@'%';
