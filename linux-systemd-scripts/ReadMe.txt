Copy these .service scripts to /etc/systemd/system and for each of them, issue:
----
systemctl enable <script_name>
----
