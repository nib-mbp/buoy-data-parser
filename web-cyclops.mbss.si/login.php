<form class="form-signin" method="post" style="margin-top: 30px; border-radius: 0;">
  <h2 class="form-signin-heading">Prijavi se</h2>
  <input type="text" class="form-control" placeholder="Up. ime ali Email naslov" name="uname"/>
  <input type="password" class="form-control" placeholder="Geslo" name="passwd"/>
  <div class="form-group">
    <input type="checkbox" value="remember-me" id="remember-me"/>
    <label for="remember-me" style="font-weight: normal">Zapomni si me</label>
  </div>
  <button class="btn btn-primary" type="submit" name="login">Prijava</button>
</form>