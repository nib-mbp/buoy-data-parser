<?php
if ($logged) {
  switch(key($_REQUEST)) {
    case 'energetika':
      include('energetika.php');
      break;

    case 'stanje':
      include('stanje.php');
      break;

    case 'logi':
      include('logi.php');
      break;
      
    case 'ukazi':
      include('ukazi.php');
      break;

    case 'nastavitve':
      include('nastavitve.php');
      break;

    default:
      include('merilniki.php');
  }
} else {
  switch(key($_REQUEST)) {
    default:
      include('login.php');
  }
}
