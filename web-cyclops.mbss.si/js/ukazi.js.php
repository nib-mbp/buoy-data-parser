<?php header("Content-type: text/javascript"); ?>
function instrumentCmdOn(instrument) {
  alert('-> ON [instrument = ' + instrument + ']');
} // instrumentCmdOn()

function instrumentCmdOff(instrument) {
  alert('-> OFF [instrument = ' + instrument + ']');
} // instrumentCmdOff()

function disableAllAButtons() {
  $('[data-toggle="buoyCmd"]').each(function(key, item) {
    $(item).removeClass('btn-success btn-danger').addClass('btn-default').attr('disabled', true);
  });
  return;
} // disableAllButtons()

function enableButtons(instrumentsArr) {
  $.ajax({
    url: "/api/get/data.php",
    method: "GET",
    dataType: "json",
    context: { instrumentsArr: instrumentsArr }
    })
    .fail(function (jqxhr, textStatus, error) {
      var err = textStatus + ", " + error;
      console.log("Request Failed: " + err);
    })
    .done(function (data, textStatus, jqXHR) {
      for (var i=0; i<this.instrumentsArr.length; ++i) {
        var inst = this.instrumentsArr[i];
        if (data.inst[inst].status == 0)  // OK - enabled
          $('#' + inst + '-off').removeClass('btn-default').addClass('btn-danger').removeProp('disabled');
        else
          $('#' + inst + '-on').removeClass('btn-default').addClass('btn-success').removeProp('disabled');
      }
    });
  return;
} // enableButtons()

function enableAllButtons() {
  enableButtons(['wind', 'air', 'compass', 'seaWater', 'o2', 'awac', 'par']);
  return;
}

$(function () {
  $('[data-toggle="buoyCmd"]').each(function(key, item) {
    $(item).popover({
      html: true,
      placement: 'auto top',
      title: '<b>Ste prepričani?</b>',
      trigger: 'click',
      content: 'Potrditev bo sprožila spremembo stanja instrumenta!<p>&nbsp;</p>' +
      '<button class="btn btn-md btn-danger closePopover">Prekliči</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-md btn-success confirmPopover">Potrdi</button>'
    });
    return;
  });
/*
  $('#wind-off').popover({
    html: true,
    placement: 'auto top',
    title: '<b>Ste prepričani?</b>',
    trigger: 'click',
    content: 'Potrditev bo sprožila spremembo stanja instrumenta!<p>&nbsp;</p>' +
             '<button class="btn btn-md btn-danger closePopover">Prekliči</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-md btn-success confirmPopover">Potrdi</button>'
  });
*/
  disableAllAButtons();
  enableAllButtons();

  $('body').on('click', '.buoy-cmd', function() {
    disableAllAButtons();
  });
  $('body').on('click', '.closePopover', function() {
    var id=$(this).closest('.popover').prop('id');
    $('[aria-describedby="'+id+'"]').popover('hide');
    $('[aria-describedby="'+id+'"]').click();
    enableAllButtons();
  });

  $('body').on('click', '.confirmPopover', function() {
    var id = $(this).closest('.popover').prop('id');
    var funcName = $('[aria-describedby="'+id+'"]').data('cmd-func');
    var instrument = $('[aria-describedby="'+id+'"]').attr('id').split('-')[0];
    $('[aria-describedby="'+id+'"]').popover('hide');
    window[funcName](instrument);
  });
});
