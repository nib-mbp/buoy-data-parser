<?php header("Content-type: text/javascript"); ?>

$(function() {

	$('.confirmAction').click(function() {
		var inst_id = $(this).data('instrument-id');
		$.confirm({
		    text: "Si prepričan, da želiš inštrument #"+inst_id+" vklopiti/izklopiti?",
		    confirm: function(button) {
		    	activateButton(inst_id);
		        return true;
		    },
		    cancel: function(button) {
		        return false;
		    },
		    confirmButton: "Sem preprican",
		    cancelButton: "Nisem preprican"
		});
	});

	function activateButton(button) {
		//event.preventDefault();
		
		// Create an instance of the clicked button (so we can work with it inside the ajax function).
		var $button = $('input[data-instrument-id="' + button + '"]');

		// If the button is unlocked.
		if ($button.data('locked') == '0') {

			// Lock the button.
			$button.data('locked', '1');

			// Ask for confirmation.
			//if (getConfirmation()) {

			if (confirm('Last chance.')) {

				// Disable button.
				$button.attr('disabled', 'disabled');

				// Get the instrument ID.
				submitId = $button.data('instrument-id');

				// Build a request.
				dataStr = '{ "instrument_id": ' + submitId + ', "new_state": ' + $button.data('new-state') + ' }';

				// Make the AJAX call.
				$.ajax({
					url: "/api/set/io_control.php",
					dataType: "json",
					type: 'GET',
					data: 'json=' + encodeURI(dataStr),

					// If successfull.
					success: function (data) { 
						$.each(data, function(index, element) {
							// The response will have to be more useful.
							if (index == 'msg') {
								if (element == 'New instrument state = 1') {
									$button.data('new-state', '0');
									$button.removeClass('btn-success').addClass('btn-danger');
									$button.val('Izklopi');
									$button.blur();
								} else {
									$button.data('new-state', '1');
									$button.removeClass('btn-danger').addClass('btn-success');
									$button.val('Vklopi');
									$button.blur();
								}
							}
							// Enable the button.
							$button.removeAttr('disabled');
						});
						// Unlock the button.
						$button.data('locked', '0');
					}
				});
			} else {
				// Unlock button if cancelled.
				$button.removeAttr('disabled');
				$button.data('locked', '0');
			}

			// Unlock the button after 10 seconds of inactivity.
			setInterval(function unlock() { $button.data('locked', '0'); }, 10000);

		} 

		// If the button is locked alert so.
		else {
			alert('Inštrument je že v uporabi.\nProsim, poskusi kasneje.');
		}

	}//);

});
