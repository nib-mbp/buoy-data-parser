$(function() {

  $('#container').css('height', $(document).height());

  if ($('#sidebar').hasClass('hidden')) {
    $('#content').addClass('noSidebar');
    $('#main').addClass('noSidebar');
    $('#nav').addClass('noSidebar');
  }

  $('.toggle-subnav').click(function(e) {
    e.preventDefault()
    $(this).parent().toggleClass('subnav-hidden');
    if ($(this).parent().hasClass('subnav-hidden')) {
      $('span:first', this).html('&#x25B8;');
    } else {
      $('span:first', this).html('&#x25BE;');
    }
  });

  $('.reload').hover(function(e) {
    $(this).tooltip('show');
  });

  /*
  $(window).resize(function(e) {
    $('#container').css('height', $(document).height());
  });
  */

  /* dinamična ura */
  var serverTime = new Date();
  function updateTime() {
      serverTime = new Date(serverTime.getTime() + 1000);
      $('#time').html(serverTime.getUTCHours() + ":" + (serverTime.getUTCMinutes()<10?'0':'') + serverTime.getUTCMinutes());
  }
  updateTime();
  setInterval(updateTime, 1000);

});
