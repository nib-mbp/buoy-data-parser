<div class="page-header">
	<h1 class="pull-left">Nastavitve <small>// <?php echo $user->getClassName(); ?></small></h1>
	<div class="pull-right">
		<div class="calendar">
			<?= strtolower(strtr(date('j. F Y', time()), $mesci)); ?><br>
			<small><?= strtr(date('w', time()), $dnevi) . ', <span id="time">' . date('H:i', time()); ?></span></small>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span4">
		<form method="post">
			<h3>Osebni podatki</h3>
			<div class="form-control">
				<label for="name">Ime in priimek</label>
				<input type="text" class="input-block-level" name="name" id="name" value="<?= $user->name; ?>">
			</div>
			<div class="form-control">
				<label for="uname">Up. ime</label>
				<input type="text" class="input-block-level" name="uname" id="uname" value="<?= $user->uname; ?>">
			</div>
			<div class="form-control">
				<label for="email">Email naslov</label>
				<input type="text" class="input-block-level" name="email" id="email" value="<?= $user->email; ?>">
			</div>
			<div class="form-control">
				<input type="submit" class="btn btn-success" name="change_settings" value="Shrani spremembe">
			</div>
		</form>
	</div>
	<div class="span4">
		<form method="post">
			<h3>Spremeni geslo</h3>
			<div class="form-control">
				<label for="old-passwd">Trenutno geslo</label>
				<input type="password" class="input-block-level" name="old-passwd" id="old-passwd">
			</div>
			<div class="form-control">
				<label for="passwd">Novo geslo</label>
				<input type="password" class="input-block-level" name="passwd" id="passwd">
			</div>
			<div class="form-control">
				<label for="passwd2">Ponovitev gesla</label>
				<input type="password" class="input-block-level" name="passwd2" id="passwd2">
			</div>
			<div class="form-control">
				<input type="submit" class="btn btn-success" name="change_password" value="Spremeni">
			</div>
		</form>
	</div>
	<div class="span4">
		<?php
		if ($user->isAdmin()):
			?>
			<form method="post">
				<h3>Dodaj uporabnika</h3>
				<div class="form-control">
					<label for="name">Ime in priimek</label>
					<input type="text" class="input-block-level" name="name" id="name">
				</div>
				<div class="form-control">
					<label for="uname">Up. ime</label>
					<input type="text" class="input-block-level" name="uname" id="uname">
				</div>
				<div class="form-control">
					<label for="email">Email naslov</label>
					<input type="text" class="input-block-level" name="email" id="email">
				</div>
				<div class="form-control">
					<label for="class">Razred</label>
					<select name="class" class="input-block-level">
						<option value="-">Izberi</option>
						<?php
						$q = $db->query('select * from buoy_classes order by id asc');
						while ($f = $q->fetch_assoc()) {
							echo '<option value="' . $f['id'] . '">' . $f['name'] . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-control">
					<input type="submit" class="btn btn-success" name="add_user" value="Dodaj uporabnika">
				</div>
			</form>
			<?php
		endif;
		?>
	</div>
</div>
