<script src="js/merilniki.js.php"></script>
<div class="page-header">
  <h1 class="pull-left">Merilniki</h1>
  <div class="pull-right">
    <div class="calendar">
      <?= strtolower(strtr(date('j. F Y', time()), $mesci)); ?><br>
      <small><?= strtr(date('w', time()), $dnevi) . ', <span id="time">' . date('H:i', time()); ?></span></small>
    </div>
  </div>
</div>

<?php
/*
<ul class="breadcrumb" style="background: #e5e5e5; font-size: 11px; padding: 5px 15px; border-radius: 0px;">
  <li><a href="#">Home</a> <span class="divider">/</span></li>
  <li><a href="#">Library</a> <span class="divider">/</span></li>
  <li class="active">Data</li>
</ul>
*/
?>

<!-- WIND -->
<div class="row">
  <div class="box-outer col-md-12">
    <div class="box" id="wind">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="wind-icon"></i> Vetromer (<span id="wind-dt" class="updateTime">nalagam</span>)
        <span class="pull-right">
          Err.koda = <span id="wind-err">nalagam</span>
        </span>
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-9">
            <strong>U</strong> = <span id="wind-l0">nalagam</span><br/>
            <strong>V</strong> = <span id="wind-l1">nalagam</span><br/>
            <strong>W</strong> = <span id="wind-l2">nalagam</span><br/>
            <strong>Hitrost</strong> = <span id="wind-l3">nalagam</span> m/s<br/>
            <strong>Smer</strong> = <span id="wind-l4">nalagam</span> °
          </div>
          <div class="col-md-3">
            <strong>Stanje</strong> = <span id="wind-status">nalagam</span><br/>
            <br/>
            <strong>El.tok (min / max / avg)</strong><br/>
            <span id="wind-r0">nalagam</span> /
            <span id="wind-r1">nalagam</span> /
            <span id="wind-r2">nalagam</span> A<br/>
            (<span id="wind-r3">nalagam</span>)
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- AIR (HUMIDITY) -->
<div class="row">
  <div class="box-outer col-md-12">
    <div class="box" id="air">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="air-icon"></i> Vlagomer (<span id="air-dt" class="updateTime">nalagam</span>)
        <span class="pull-right">
          Err.koda = <span id="air-err">nalagam</span>
        </span>
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-9">
            <strong>Temperatura</strong> = <span id="air-l0">nalagam</span> °C<br/>
            <strong>Vlaga zraka</strong> = <span id="air-l1">nalagam</span> %
          </div>
          <div class="col-md-3">
            <strong>Stanje</strong> = <span id="air-status">nalagam</span><br/>
            <br/>
            <strong>El.tok (min / max / avg)</strong><br/>
            <span id="air-r0">nalagam</span> /
            <span id="air-r1">nalagam</span> /
            <span id="air-r2">nalagam</span> A<br/>
            (<span id="air-r3">nalagam</span>)
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- COMPASS -->
<div class="row">
  <div class="box-outer col-md-12">
    <div class="box" id="compass">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="compass-icon"></i> Kompas (<span id="compass-dt" class="updateTime">nalagam</span>)
        <span class="pull-right">
          Err.koda = <span id="compass-err">nalagam</span>
        </span>
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-9">
            <strong>Heading</strong> = <span id="compass-l0">nalagam</span> °<br/>
            <strong>Pitch</strong> = <span id="compass-l1">nalagam</span> °<br/>
            <strong>Roll</strong> = <span id="compass-l2">nalagam</span> °
          </div>
          <div class="col-md-3">
            <strong>Stanje</strong> = <span id="compass-status">nalagam</span><br/>
            <br/>
            <strong>El.tok (min / max / avg)</strong><br/>
            <span id="compass-r0">nalagam</span> /
            <span id="compass-r1">nalagam</span> /
            <span id="compass-r2">nalagam</span> A<br/>
            (<span id="compass-r3">nalagam</span>)
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- SALINITY (SeaBird) -->
<div class="row">
  <div class="box-outer col-md-12">
    <div class="box" id="seaWater">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="seaWater-icon"></i> SeaBird (<span id="seaWater-dt" class="updateTime">nalagam</span>)
        <span class="pull-right">
          Err.koda = <span id="seaWater-err">nalagam</span>
        </span>
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-9">
            <strong>Temperatura</strong> = <span id="seaWater-l0">nalagam</span> °C<br/>
            <strong>Prevodnost</strong> = <span id="seaWater-l1">nalagam</span> S<br/>
            <strong>Slanost</strong> = <span id="seaWater-l2">nalagam</span> %<br/>
            <strong>Chl-A</strong> = <span id="seaWater-l3">nalagam</span> mg/m<sup>3</sup>
          </div>
          <div class="col-md-3">
            <strong>Stanje</strong> = <span id="seaWater-status">nalagam</span><br/>
            <br/>
            <strong>El.tok (min / max / avg)</strong><br/>
            <span id="seaWater-r0">nalagam</span> /
            <span id="seaWater-r1">nalagam</span> /
            <span id="seaWater-r2">nalagam</span> A<br/>
            (<span id="seaWater-r3">nalagam</span>)
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- OXYGEN -->
<div class="row">
  <div class="box-outer col-md-12">
    <div class="box" id="o2">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="o2-icon"></i> Kisikomer (<span id="o2-dt" class="updateTime">nalagam</span>)
        <span class="pull-right">
          Err.koda = <span id="o2-err">nalagam</span>
        </span>
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-9">
            <strong>Koncentracija O<sub>2</sub></strong> = <span id="o2-l0">nalagam</span> mol/m<sup>3</sup>
          </div>
          <div class="col-md-3">
            <strong>Stanje</strong> = <span id="o2-status">nalagam</span><br/>
            <br/>
            <strong>El.tok (min / max / avg)</strong><br/>
            <span id="o2-r0">nalagam</span> /
            <span id="o2-r1">nalagam</span> /
            <span id="o2-r2">nalagam</span> A<br/>
            (<span id="o2-r3">nalagam</span>)
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- AWAC -->
<div class="row">
  <div class="box-outer col-md-12">
    <div class="box" id="awac">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="awac-icon"></i> Tokomer AWAC (<span id="awac-dt" class="updateTime">nalagam</span>)
        <span class="pull-right">
          Err.koda = <span id="awac-err">nalagam</span>
        </span>
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-9">
            <strong>Temperatura</strong> = <span id="awac-l0">nalagam</span> °C<br/>
            <strong>Pritisk</strong> = <span id="awac-l1">nalagam</span> hPa
          </div>
          <div class="col-md-3">
            <strong>Stanje</strong> = <span id="awac-status">nalagam</span><br/>
            <br/>
            <strong>El.tok (min / max / avg)</strong><br/>
            <span id="awac-r0">nalagam</span> /
            <span id="awac-r1">nalagam</span> /
            <span id="awac-r2">nalagam</span> A<br/>
            (<span id="awac-r3">nalagam</span>)
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- PAR -->
<div class="row">
  <div class="box-outer col-md-12">
    <div class="box" id="par">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="par-icon"></i> PAR tipalo (<span id="par-dt" class="updateTime">nalagam</span>)
        <span class="pull-right">
          Err.koda = <span id="par-err">nalagam</span>
        </span>
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-9">
            <strong>Meritev</strong> = <span id="par-l0">nalagam</span> &#181;mol/sm2
          </div>
          <div class="col-md-3">
            <strong>Stanje</strong> = <span id="par-status">nalagam</span><br/>
            <br/>
            <strong>El.tok (min / max / avg)</strong><br/>
            <span id="par-r0">nalagam</span> /
            <span id="par-r1">nalagam</span> /
            <span id="par-r2">nalagam</span> A<br/>
            (<span id="par-r3">nalagam</span>)
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
