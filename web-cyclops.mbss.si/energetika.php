<script src="js/energetika.js.php"></script>
<div class="page-header">
  <h1 class="pull-left">Energetika</h1>
  <div class="pull-right">
    <div class="calendar">
      <?= strtolower(strtr(date('j. F Y', time()), $mesci)); ?><br>
      <small><?= strtr(date('w', time()), $dnevi) . ', <span id="time">' . date('H:i', time()); ?></span></small>
    </div>
  </div>
</div>

<!-- ROW.1 -->
<div class="row">
  <!-- napetost sistema -->
  <div class="box-outer col-md-6">
    <div class="box" id="voltage">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="voltage-icon"></i> Napetost sistema (<span id="voltage-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <div class="row"">
          <div class="col-md-8">
            <strong>Napetost</strong> = <span id="voltage-d0">nalagam</span> V<br/>
            &nbsp;<br/>
            &nbsp;
          </div>
          <div class="col-md-4">
            <strong>Stanje</strong> = <span id="voltage-status">nalagam</span><br/>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- CPE -->
  <div class="box-outer col-md-6">
    <div class="box" id="cpe">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="cpe-icon"></i> CPE (<span id="cpe-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Temperatura</strong> = <span id="cpe-d0">nalagam</span> °C<br/>
        <strong>Vlaga</strong> = <span id="cpe-d1">nalagam</span> %<br/>
        <strong>Tok</strong> = <span id="cpe-d2">nalagam</span> A
      </div>
    </div>
  </div>
</div>


<!-- ROW.2 -->
<div class="row">
  <!-- battery pack 1 -->
  <div class="box-outer col-md-6">
    <div class="box" id="bat1">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="bat1-icon"></i> Akumulatorski sklop 1 (<span id="bat1-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Napetost</strong> = <span id="bat1-d0">nalagam</span> V<br/>
        <strong>El.tok (min / max / avg)</strong><br/>
        <ul>
          <li><strong>Bat1:</strong> <span id="bat1-d1">nalagam</span> A</li>
          <li><strong>Bat2:</strong> <span id="bat1-d2">nalagam</span> A</li>
        </ul><br/>
        <strong>Stanje</strong> = <span id="bat1-d3">nalagam</span> (<span id="bat1-d4">nalagam</span>)<br/>
      </div>
    </div>
  </div>

  <!-- battery pack 2 -->
  <div class="box-outer col-md-6">
    <div class="box" id="bat2">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="bat2-icon"></i> Akumulatorski sklop 2 (<span id="bat2-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Napetost</strong> = <span id="bat2-d0">nalagam</span> V<br/>
        <strong>El.tok (min / max / avg)</strong><br/>
        <ul>
          <li><strong>Bat3:</strong> <span id="bat2-d1">nalagam</span> A</li>
          <li><strong>Bat4:</strong> <span id="bat2-d2">nalagam</span> A</li>
        </ul><br/>
        <strong>Stanje</strong> = <span id="bat2-d3">nalagam</span> (<span id="bat2-d4">nalagam</span>)<br/>
      </div>
    </div>
  </div>
</div>


<!-- ROW.3 -->
<!-- Current consumption -->
<div class="row">
  <div class="box-outer col-md-4">
    <div class="box" id="elCurr1">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="elCurr1-icon"></i> Tok.veja #1 (<span id="elCurr1-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Tok</strong> = <span id="elCurr1-d0">nalagam</span> A
      </div>
    </div>
  </div>
  <div class="box-outer col-md-4">
    <div class="box" id="elCurr2">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="elCurr2-icon"></i> Tok.veja #2 (<span id="elCurr2-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Tok</strong> = <span id="elCurr2-d0">nalagam</span> A
      </div>
    </div>
  </div>
  <div class="box-outer col-md-4">
    <div class="box" id="elCurr3">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="elCurr3-icon"></i> Tok.veja #3 (<span id="elCurr3-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Tok</strong> = <span id="elCurr3-d0">nalagam</span> A
      </div>
    </div>
  </div>
</div>

<!-- ROW.4 -->
<div class="row">
  <div class="box-outer col-md-12">
    <div class="box" id="mppt">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="mppt-icon"></i> Solarni paneli (<span id="mppt-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <div class="row">
          <div class="col-md-9">
            <strong>Celice</strong><br>
            <strong>Napetost</strong> = <span id="mppt-d0">nalagam</span> V<br>
            <strong>Tok</strong> = <span id="mppt-d1">nalagam</span> A
          </div>
          <div class="col-md-3">
            <strong>Baterije</strong><br>
            <strong>Napetost</strong> = <span id="mppt-d2">nalagam</span> V<br>
            <strong>Tok</strong> = <span id="mppt-d3">nalagam</span> A
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ROW.5 -->
<div class="row">
  <div class="box-outer col-md-12">
    <div class="box" id="fuel">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="fuel-icon"></i> Gorivna celica (<span id="fuel-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="fuel-d0">nalagam</span> <br>
        <strong>Tok</strong> = <span id="fuel-d1">nalagam</span> A
      </div>
    </div>
  </div>
</div>
