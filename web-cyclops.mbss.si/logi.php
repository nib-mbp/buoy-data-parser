<?php if ($user->isAdmin()): ?>
<div class="page-header">
	<h1 class="pull-left">Zgodovina sprememb</h1>
	<div class="pull-right">
		<div class="calendar">
			<?= strtolower(strtr(date('j. F Y', time()), $mesci)); ?><br>
			<small><?= strtr(date('w', time()), $dnevi) . ', <span id="time">' . date('H:i', time()); ?></span></small>
		</div>
	</div>
</div>

<div class="well" style="background: #f0f0f0;">
	<p>Tukaj bo zgodovina sprememb vklopa/izklopa inštrumentov.</p>
	<p>
		Pomembni podatki:
		<ul>
			<li>kdo je naredil spremembo,</li>
			<li>kdaj je naredil spremembo,</li>
			<li>kateri inštrument je bil spremenjen,</li>
			<li>prejšnje / novo stanje.</li>
		</ul>
	</p>
	<p>Tukaj se zabeležijo tudi napačne prijave in razni napačni poskusi drugih stvari.</p>
	<p>Tole stran vidi samo administrator.</p>
</div>
<?php 
else: 
?>
<div class="page-header">
	<h1 class="pull-left">Oppa!</h1>
	<div class="pull-right">
		<div class="calendar">
			<?= strtolower(strtr(date('j. F Y', time()), $mesci)); ?><br>
			<small><?= strtr(date('w', time()), $dnevi) . ', <span id="time">' . date('H:i', time()); ?></span></small>
		</div>
	</div>
</div>
<?php
$alert = new Alert('danger');
$alert->show('Do te strani nimaš dostopa. Administrator je bil o tem poskusu obveščen.');
endif; 
