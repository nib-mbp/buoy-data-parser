<?php

/**
User actions
*/

if (isset($_POST['login'])) {
  $user->login($_POST['uname'], $_POST['passwd']);
}

if (isset($_GET['logout'])) {
  $user->logout();
}

/**
User settings.
*/

if (isset($_POST['change_settings'])) {
	$q = $db->prepare('update buoy_users set name = ?, uname = ?, email = ? where id = ? limit 1');
	$q->bind_param('sssi', $_POST['name'], $_POST['uname'], $_POST['email'], $a = (int)substr($_SESSION['user'], 0, 1));
	if ($q->execute()) {
		$user->logout();
		// Update settings. Output OK message.
	} else {
		// Update unsuccessful. Output BAD message.
	}
}

if (isset($_POST['change_password'])) {
	if (md5($_POST['old-passwd']) == substr($_SESSION['user'], 2)) {
		if ($_POST['passwd'] == $_POST['passwd2']) {
			$q = $db->prepare('update buoy_users set passwd = ? where id = ? limit 1');
			$q->bind_param('si', md5($_POST['passwd']), $a = (int)substr($_SESSION['user'], 0, 1));
			if ($q->execute()) {
				$_SESSION['user'] = $id['id'] . ':' . md5($_POST['passwd']);
				// Update password. Output OK message.
			} else {
				// Password change unsuccessful. Display BAD message.
			}
		} else {
			// Passwords don't match.
		}
	} else {
		// Old password is wrong.
	}
}

if (isset($_POST['add_user'])) {
	if ($user->isAdmin()) {
		$pass = generateRandomString();
		$q = $db->prepare('insert into buoy_users (name, uname, passwd, email) values (?, ?, ?, ?)');
		$q->bind_param('ssss', $_POST['name'], $_POST['uname'], md5($pass), $_POST['email']);
		if ($q->execute()) {
			mail($_POST['email'], 'Ustvarjen je bil račun na cyclops.mbss.org', 'Podatki za prijavo:' . "\n" . 'Up. ime: ' . $_POST['uname'] . "\n" . 'Geslo: ' . $pass, 'From: noreply@waka.us');
			// User added. Output OK message.
		} else {
			// User not added. Output BAD message.
		}
	} else {
		// User is not admin. Log hack attempt.
	}
}
