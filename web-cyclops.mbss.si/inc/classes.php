<?php

class User
{

  var $id;
  var $name;
  var $email;
  var $uname;
  var $class;

  /**
   * Actions
   */

  function login($username, $password)
  {
    global $db;
    $query = $db->query('SELECT id FROM buoy_users WHERE uname = "' . $username . '" AND passwd = "' . md5($password) . '"');

    if ($query->num_rows) {
      $id = $query->fetch_assoc();
      $_SESSION['user'] = $id['id'] . ':' . md5($password);
      $query->free();
      header('location: ./');
      exit();
    } else {
      $error = 1;
    }
  }

  function logout()
  {
    session_start();
    session_unset();
    session_destroy();
    session_write_close();
    setcookie(session_name(), '', 0, '/');
    session_regenerate_id(true);
    header('location: ./');
  }

  /**
   * Class related
   */

  function hasClass($class)
  {
    global $db;
    $query = $db->query('select null from buoy_users where id = "' . substr($_SESSION['user'], 0, 1) . '" and class = "' . $class . '"');
    if ($query->num_rows) {
      return true;
    }
    return false;
  }

  function getClass()
  {
    global $db;
    if ($this->isLogged()) {
      $query = $db->query('select class from buoy_users where id = "' . substr($_SESSION['user'], 0, 1) . '"');
      if ($query->num_rows) {
        $fetch = $query->fetch_assoc();
        return $fetch['class'];
        $query->free();
      }
    }
    return null;
  }

  function getClassName()
  {
    global $db;
    if ($this->isLogged()) {
      $query = $db->query('select name from buoy_classes where id = "' . $this->getClass() . '" limit 1');
      $fetch = $query->fetch_assoc();
      return $fetch['name'];
      $query->free();
    }
    return null;
  }

  function isAdmin()
  {
    global $db;
    if ($this->isLogged()) {
      if ($this->getClass() == '1')
        return true;
    }
    return false;
  }

  /**
   * Data fetchers
   */

  function isLogged()
  {
    global $db;
    if (isset($_SESSION['user'])) {
      $query = $db->query('select class from buoy_users where id = "' . (int)substr($_SESSION['user'], 0, 1) . '" and passwd = "' . $db->escape_string(substr($_SESSION['user'], 2)) . '"');
      //$query = $db->query('select class from buoy_users where id = "' . substr($_SESSION['user'], 0, 1) . '"');
      if ($query->num_rows) {
        return true;
      }
    }
    return false;
  }

  /**
   * Kind of obsolete (added getData())
   */

  function getName()
  {
    global $db;
    if ($this->isLogged()) {
      $query = $db->query('select name from buoy_users where id = "' . (int)substr($_SESSION['user'], 0, 1) . '" and passwd = "' . $db->escape_string(substr($_SESSION['user'], 2)) . '" limit 1')->fetch_assoc();
      return $query['name'];
      $query->free();
    }
    return null;
  }

  function getEmail()
  {
    global $db;
    if ($this->isLogged()) {
      $query = $db->query('select email from buoy_users where id = "' . (int)substr($_SESSION['user'], 0, 1) . '" and passwd = "' . $db->escape_string(substr($_SESSION['user'], 2)) . '" limit 1')->fetch_assoc();
      return $query['email'];
      $query->free();
    }
    return null;
  }

  function getData()
  {
    global $db;
    if ($this->isLogged()) {
      $query = $db->query('select * from buoy_users where id = "' . (int)substr($_SESSION['user'], 0, 1) . '" and passwd = "' . $db->escape_string(substr($_SESSION['user'], 2)) . '" limit 1')->fetch_assoc();
      $this->id = $query['id'];
      $this->name = $query['name'];
      $this->email = $query['email'];
      $this->uname = $query['uname'];
      $this->class = $query['class'];
    }
  }

}

class Alert
{

  var $type;
  var $title;
  var $strong;
  var $message;

  function __construct($type)
  {
    $this->type = $type;
    if ($type == 'danger') {
      $this->title = 'Oppa!';
      $this->strong = 'Kaj počneš?';
    } else {
      $this->title = 'Ok!';
      $this->strong = 'Uspeh!';
    }
  }

  function show($message)
  {
    echo '<div class="alert alert-' . $this->type . '"><strong>' . $this->strong . '</strong> ' . $message . '</div>';
  }

}
