<?php
// date('n') za mesec 1-12
$mesci = array(
    'January' => 'Januar',
    'February' => 'Februar',
    'March' => 'Marec',
    'April' => 'April',
    'May' => 'Maj',
    'June' => 'Junij',
    'July' => 'Julij',
    'August' => 'Avgust',
    'September' => 'September',
    'October' => 'Oktober',
    'November' => 'November',
    'December' => 'December',
    'Monday' => 'Ponedeljek',
    'Tuesday' => 'Torek',
    'Wednesday' => 'Sreda',
    'Thursday' => 'Èetrtek',
    'Friday' => 'Petek',
    'Saturday' => 'Sobota',
    'Sunday' => 'Nedelja'
    /*
    '1' => 'Januar',
    '2' => 'Februar',
    '3' => 'Marec',
    '4' => 'April',
    '5' => 'Maj',
    '6' => 'Junij',
    '7' => 'Julij',
    '8' => 'Avgust',
    '9' => 'September',
    '10' => 'Oktober',
    '11' => 'November',
    '12' => 'December'
    */
);

// date('w') za dan 0-6
$dnevi = array(
	0 => 'Nedelja',
	1 => 'Ponedeljek',
	2 => 'Torek',
	3 => 'Sreda',
	4 => 'Četrtek',
	5 => 'Petek',
	6 => 'Sobota'
	);
