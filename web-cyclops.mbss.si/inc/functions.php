<?php

function getClassName ($id) {
	global $db;
	$query = $db->query('select name from buoy_classes where id = "' . (int)$id . '" limit 1');
	$fetch = $query->fetch_assoc();
	return $fetch['name'];
	$query->free();
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function alertNoAccess() {
	// Zapiši v bazo.
	echo '<div class="alert alert-danger" style="margin-top: 20px;">
	<strong>Kaj počneš?</strong> Do te strani nimaš dostopa. Administrator je bil o tem poskusu obveščen.
	</div>';
}
