<script src="js/stanje.js.php"></script>
<div class="page-header">
  <h1 class="pull-left">Stanje boje</h1>
  <div class="pull-right">
    <div class="calendar">
      <?= strtolower(strtr(date('j. F Y', time()), $mesci)); ?><br>
      <small><?= strtr(date('w', time()), $dnevi) . ', <span id="time">' . date('H:i', time()); ?></span></small>
    </div>
  </div>
</div>

<!-- ROW.1 -->
<div class="row">
  <!-- buoy interior -->
  <div class="box-outer col-md-6">
    <div class="box" id="bInterior">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="bInterior-icon"></i> Trup boje (<span id="bInterior-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Temperatura</strong> = <span class="row12" id="bInterior-d0">nalagam</span><br/>
        <strong>Vlaga zraka</strong> = <span class="row12" id="bInterior-d1">nalagam</span>
      </div>
    </div>
  </div>

  <!-- co2 -->
  <div class="box-outer col-md-6">
    <div class="box" id="bCo2">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="bCo2-flag"></i> CO2 (<span id="bCo2-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="bCo2-d0">nalagam</span><br/>
        <strong>Koncentracija</strong> = <span class="row12" id="bCo2-d1">nalagam</span>
      </div>
    </div>
  </div>
</div>

<!-- ROW.2 -->
<div class="row">
  <!-- loputa -->
  <div class="box-outer col-md-6">
    <div class="box" id="door">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="door-icon"></i> Loputa (<span id="door-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="door-status">nalagam</span>
      </div>
    </div>
  </div>

  <!-- energetika / energetski modul -->
  <div class="box-outer col-md-6">
    <div class="box" id="energy">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="energy-icon"></i> Energetika (<span id="energy-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="energy-status">nalagam</span>
      </div>
    </div>
  </div>
</div>

<!-- ROW.3 -->
<div class="row">
  <!-- radijski povezavi (summary) -->
  <div class="box-outer col-md-4">
    <div class="box" id="wifi">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="wifi-icon"></i> Radijski povezavi (<span id="wifi-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="wifi-status">nalagam</span>
      </div>
    </div>
  </div>

  <!-- vdor vode (summary) -->
  <div class="box-outer col-md-4">
    <div class="box" id="waterCmn">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="waterCmn-icon"></i> Vdor vode (<span id="waterCmn-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="waterCmn-status">nalagam</span>
      </div>
    </div>
  </div>

  <!-- kamere -->
  <div class="box-outer col-md-4">
    <div class="box" id="camCmn">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="camCmn-icon"></i> Kamere (<span id="camCmn-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="camCmn-status">nalagam</span>
      </div>
    </div>
  </div>
</div>

<!-- ROW.4 -->
<div class="row">
  <!-- gps -->
  <div class="box-outer col-md-12">
    <div class="box" id="gps">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="gps-icon"></i> GPS (<span id="gps-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="gps-status">nalagam</span>
      </div>
    </div>
  </div>
</div>

<!-- ROW.5 -->
<div class="row">
  <!-- WiFI 2.4 GHz -->
  <div class="box-outer col-md-6">
    <div class="box" id="wifi24g">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="wifi24g-icon"></i> WiFI 2.4 GHz (<span id="wifi24g-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="wifi24g-status">nalagam</span>
      </div>
    </div>
  </div>

  <!-- WiFI 5 GHz -->
  <div class="box-outer col-md-6">
    <div class="box" id="wifi5g">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="wifi5g-icon"></i> WiFI 5 GHz (<span id="wifi5g-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="wifi5g-status">nalagam</span>
      </div>
    </div>
  </div>
</div>

<!-- ROW.6 -->
<div class="row">
  <!-- Vdor vode - telo boje -->
  <div class="box-outer col-md-6">
    <div class="box" id="waterBody">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="waterBody-icon"></i> Vdor vode - telo boje (<span id="waterBody-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="waterBody-status">nalagam</span>
      </div>
    </div>
  </div>

  <!-- Vdor vode - komora kamere -->
  <div class="box-outer col-md-6">
    <div class="box" id="waterCam">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="waterCam-icon"></i> Vdor vode - komora kamere (<span id="waterCam-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="waterCam-status">nalagam</span>
      </div>
    </div>
  </div>
</div>

<!-- ROW.7 -->
<div class="row">
  <!-- kontrolna kamera 1 -->
  <div class="box-outer col-md-3">
    <div class="box" id="cam1">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="cam1-icon"></i> Cam.1<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<span id="cam1-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="cam1-status">nalagam</span>
      </div>
    </div>
  </div>

  <!-- kontrolna kamera 2 -->
  <div class="box-outer col-md-3">
    <div class="box" id="cam2">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="cam2-icon"></i> Cam.2<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<span id="cam2-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="cam2-status">nalagam</span>
      </div>
    </div>
  </div>

  <!-- kontrolna kamera 3 -->
  <div class="box-outer col-md-3">
    <div class="box" id="cam3">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="cam3-icon"></i> Cam.3 <br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<span id="cam3-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="cam3-status">nalagam</span>
      </div>
    </div>
  </div>

  <!-- kontrolna kamera 4 -->
  <div class="box-outer col-md-3">
    <div class="box" id="cam4">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="cam4-icon"></i> Cam.4 <br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<span id="cam4-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="cam4-status">nalagam</span>
      </div>
    </div>
  </div>
</div>

<!-- ROW.8 -->
<div class="row">
  <!-- podvodna kamera -->
  <div class="box-outer col-md-12">
    <div class="box" id="uwCam">
      <div class="box-header">
        <i class="glyphicon glyphicon-repeat" id="uwCam-icon"></i> Podvodna kamera (<span id="uwCam-dt" class="updateTime">nalagam</span>)
      </div>
      <div class="box-content">
        <strong>Stanje</strong> = <span class="row12" id="uwCam-status">nalagam</span>
      </div>
    </div>
  </div>
</div>
