<nav class="navbar navbar-default navbar-fixed-top navbar-inverse">
  <div class="container">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Boja Vida</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <?php if (! $logged): ?>
            <li class="active"><a href="#">Prijava<span class="sr-only">(current)</span></a></li>
          <?php else: ?>
            <li <?= (key($_REQUEST) == 'merilniki' ? 'class="active"' : ''); ?>><a href="/?merilniki">Merilniki<span class="sr-only">(current)</span></a></li>
            <li <?= (key($_REQUEST) == 'energetika' ? 'class="active"' : ''); ?>><a href="/?energetika">Energetika</a></li>
            <li <?= (key($_REQUEST) == 'stanje' ? 'class="active"' : ''); ?>><a href="/?stanje">Stanje boje</a></li>
            <li <?= (key($_REQUEST) == 'ukazi' ? 'class="active"' : ''); ?>><a href="/?ukazi">Kontrola instrumentov</a></li>
          <?php endif; ?>
        </ul>
        <?php if ($logged): ?>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= $user->name; ?>&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/?logout">Odjava</a></li>
              </ul>
            </li>
          </ul>
        <?php endif; ?>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </div>
</nav>