#include <iostream>
#include <string>
#include <sstream>
#include <typeinfo> // for typeid, std::type_info
#include <map>
#include <list>
#include <vector>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cerrno>
#include <cstring>
#include <cstdio>
#include <cstdlib>

#include <boost/shared_ptr.hpp>
#include <boost/variant.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "buoy_log.hpp"
#include "buoy_utils.hpp"
#include "buoy_awac_data.hpp"
#include "buoy_data_holder_utils.hpp"
#include "buoy_datatypes.hpp"

#include "awac/nortek_awac.hpp"
#include "awac/awac_data.hpp"


namespace mbp {

BuoyAwacData::BuoyAwacData(mbp::buoy::rows_spt a_rows_sp)
:
    m_rows(a_rows_sp)
{
    return;
} // BuoyAwacData::BuoyAwacData()


BuoyAwacData::~BuoyAwacData()
{
    return;
} // BuoyAwacData::~BuoyAwacData()


void BuoyAwacData::Reset(
        mbp::buoy::rows_spt a_rows_sp)
{
    m_rows = a_rows_sp;
    return;
} // BuoyAwacData::Reset()


void BuoyAwacData::ProcessDataRecord(
        mbp::buoy::awac_data_header_t *a_awac_p,
        size_t a_currDataStrucSize,
        const mbp::parsing_type_e& a_parsingType,
        mbp::RuntimeData *a_runtimeData_p)
    throw (std::runtime_error)
{
    size_t  dataSize = a_currDataStrucSize - BUOY_AWAC_DATA_HEADER_SIZE;
    size_t  dataPos = 0;

    uint32_t currRecordSeconds = a_awac_p->hdr.sec;
    uint32_t recEpochTime = a_awac_p->hdr.sec;
    boost::posix_time::ptime recPTime =
        boost::posix_time::from_time_t(recEpochTime);

    while (dataPos < dataSize) {
        Nortek::PdGenericHeader *pktHeader_p =
            reinterpret_cast<Nortek::PdGenericHeader *>(&a_awac_p->data[dataPos]);
        if (pktHeader_p->cSync != 0xA5) {
            throw std::runtime_error("Invalid AWAC data received (no Sync packet present)!");
        }
        mbp::buoy::table_e tableId = mbp::buoy::TBL_NA_e;
        mbp::buoy::row_spt row_sp;

        switch (pktHeader_p->cID) {
            case Nortek::PD_USER_CONFIGURATION :
                {
                    // -- TBL_AWAC2_USER_CONFIG_e --
// TODO:            CHECK_FULL_STRUCT_SIZE(mbp::buoy::par_t, currDataStrucSize);
                    tableId = mbp::buoy::TBL_AWAC2_USER_CONFIG_e;

                    std::ostringstream sstr("");

                    // Populate the row values
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();

                    Nortek::PdUserConfiguration *data_p =
                        reinterpret_cast<Nortek::PdUserConfiguration *>(&a_awac_p->data[dataPos]);

                    row_sp->push_back( mbp::buoy::PtimeToStr(recPTime) );
                    row_sp->push_back( mbp::buoy::DataHolder() );
                    row_sp->push_back( data_p->T1 );
                    row_sp->push_back( data_p->T2 );
                    row_sp->push_back( data_p->T3 );
                    row_sp->push_back( data_p->T4 );
                    row_sp->push_back( data_p->T5 );
                    row_sp->push_back( data_p->nPings );
                    row_sp->push_back( data_p->avgInterval );
                    row_sp->push_back( data_p->nBeams );
                    row_sp->push_back( mbp::buoy::Ushort2Hex(data_p->timCtrlReg.raw, false) );
                    row_sp->push_back( mbp::buoy::Ushort2Hex(data_p->pwrCtrlReg.raw, false) );
                    row_sp->push_back( data_p->compassUpdRate );
                    row_sp->push_back( data_p->coordSystem );
                    row_sp->push_back( data_p->nBins );
                    row_sp->push_back( double(data_p->binLength) );
                    row_sp->push_back( data_p->measInterval );
                    row_sp->push_back( std::string(data_p->deployName, 6) );
                    row_sp->push_back( data_p->wrapMode );
                    row_sp->push_back( mbp::buoy::PdClock2String(data_p->clockDeploy) );
                    row_sp->push_back( data_p->diagInterval );
                    row_sp->push_back( mbp::buoy::Ushort2Hex(data_p->mode.raw, false) );
                    row_sp->push_back( data_p->adjSoundSpeed );
                    row_sp->push_back( data_p->nSampDiag );
                    row_sp->push_back( data_p->nBeamsCellDiag );
                    row_sp->push_back( data_p->nPingsDiag );
                    row_sp->push_back( mbp::buoy::Ushort2Hex(data_p->modeTest.raw, false) );
                    row_sp->push_back( data_p->anaInAddr );
                    row_sp->push_back( data_p->swVersion );
                    sstr.str("");
                    sstr
                        << "UNHEX('"
                        << mbp::buoy::BufferToHexString(data_p->velAdjTable, 180, 48, false, false, false, true)
                        << "')";
                    row_sp->push_back( mbp::buoy::DataHolder(sstr.str()) );
                    sstr.str("");
                    sstr
                        << "UNHEX('"
                        << mbp::buoy::BufferToHexString(data_p->comments, 180, 48, false, false, false, true)
                        << "')";
                    row_sp->push_back( mbp::buoy::DataHolder(sstr.str()) );
                    row_sp->push_back( mbp::buoy::Ushort2Hex(data_p->waveMode.raw, false) );
                    row_sp->push_back( data_p->dynPercPos );
                    row_sp->push_back( data_p->wT1 );
                    row_sp->push_back( data_p->wT2 );
                    row_sp->push_back( data_p->wT3 );
                    row_sp->push_back( data_p->nSamp );
                    row_sp->push_back( data_p->anaOutScale );
                    row_sp->push_back( data_p->corrThresh );
                    row_sp->push_back( data_p->tiLag2 );
                    sstr.str("");
                    sstr
                        << "UNHEX('"
                        << mbp::buoy::BufferToHexString(data_p->qualConst, 16, 48, false, false, false, true)
                        << "')";
                    row_sp->push_back( mbp::buoy::DataHolder(sstr.str()) );

                    m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));

                    // -- Retrieve config AutoID
//                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
//                    row_sp->clear();
//
//                    std::ostringstream sstr("");
//                    sstr <<
//                        "SELECT " << buoy::DataHolder::SEQ_VAR << mbp::buoy::SQL_VAR_AWAC_CONFIG_e << ":=LAST_INSERT_ID()";
//                    row_sp->push_back(mbp::buoy::DataHolder(sstr.str()));
//                    m_rows->push_back(mbp::buoy::TableRow(mbp::buoy::TBL_NA_MANUAL_SPEC_e, row_sp));

                    if ( mbp_IsEnabled(trace) ) {
                        sstr.str("");
                        sstr
                            << "awacUserConf (0xA500) - acquireDt=" << recPTime << " ["
                            << "hSize=" << data_p->hSize << " (" << data_p->hSize * 2 << "B) / "
                            << "T1=" << data_p->T1 << " / "
                            << "T2=" << data_p->T2 << " / "
                            << "T3=" << data_p->T3 << " / "
                            << "T4=" << data_p->T4 << " / "
                            << "T5=" << data_p->T5 << " / "
                            << "nPings=" << data_p->nPings << " / "
                            << "avgInterval=" << data_p->avgInterval << " / "
                            << "nBeams=" << data_p->nBeams << " / "
                            << "timCtrlReg ("
                                << "profile=" << (data_p->timCtrlReg.data.profile == 0 ? "single" : "continuous") << ", "
                                << "mode=" << (data_p->timCtrlReg.data.mode == 0 ? "burst" : "continuous") << ", "
                                << "synchout=" << (data_p->timCtrlReg.data.synchout == 0 ? "middle" : "end") << ", "
                                << "sampleOnSync=" << (data_p->timCtrlReg.data.sampleOnSync ? "disabled" : "enabled") << ", "
                                << "startOnSync=" << (data_p->timCtrlReg.data.startOnSync ? "disabled" : "enabled")
                                << ") - hex: " << mbp::buoy::Ushort2Hex(data_p->timCtrlReg.raw, true) << " / "
                            << "pwrCtrlReg hex: " << mbp::buoy::Ushort2Hex(data_p->pwrCtrlReg.raw, true) << " / "
                            << "tA1=" << data_p->tA1 << " / "
                            << "tB0=" << data_p->tB0 << " / "
                            << "tB1=" << data_p->tB1 << " / "
                            << "compassUpdRate=" << data_p->compassUpdRate << " / "
                            << "coordSystem=" << data_p->coordSystem << " / "
                            << "nBins=" << data_p->nBins << " / "
                            << "binLength=" << data_p->binLength << " / "
                            << "measInterval=" << data_p->measInterval << " / "
                            << "deployName=" << std::string(data_p->deployName, 6) << " / "
                            << "wrapMode=" << data_p->wrapMode << " / "
                            << "clockDeploy=" << mbp::buoy::PdClock2String(data_p->clockDeploy) << " / "
                            << "diagInterval=" << data_p->diagInterval << " / "
                            << "mode ("
                                << "userSoundSpd=" << data_p->mode.data.userSoundSpd << ", "
                                << "diagWaveMode=" << data_p->mode.data.diagWaveMode << ", "
                                << "analogOutput=" << data_p->mode.data.analogOutput << ", "
                                << "outputFormat=" << (data_p->mode.data.outputFormat == 0 ? "Vector" : "ADV") << ", "
                                << "scaling=" << (data_p->mode.data.scaling == 0 ? "1 mm" : "0.1 mm") << ", "
                                << "serialOutput=" << data_p->mode.data.serialOutput << ", "
                                << "stage=" << data_p->mode.data.stage << ", "
                                << "outPower=" << data_p->mode.data.outPower
                                << ") - hex: " << mbp::buoy::Ushort2Hex(data_p->mode.raw, true) << " / "
                            << "adjSoundSpeed=" << data_p->adjSoundSpeed << " / "
                            << "nSampDiag=" << data_p->nSampDiag << " / "
                            << "nBeamsCellDiag=" << data_p->nBeamsCellDiag << " / "
                            << "nPingsDiag=" << data_p->nPingsDiag << " / "
                            << "modeTest ("
                                << "dspFilter=" << data_p->modeTest.data.dspFilter << ", "
                                << "filterDataOutput=" << data_p->modeTest.data.filterDataOutput
                                << ") - hex: " << mbp::buoy::Ushort2Hex(data_p->modeTest.raw, true) << " / "
                            << "anaInAddr=" << data_p->anaInAddr << " / "
                            << "swVersion=" << data_p->swVersion << " / "
                            << "waveMode ("
                                << "dataRate=" << data_p->waveMode.data.dataRate << ", "
                                << "waveCellPos=" << (data_p->waveMode.data.waveCellPos == 0 ? "fixed" : "dynamic") << ", "
                                << "dynamicPosType=" << data_p->waveMode.data.dynamicPosType
                                << ") - hex: " << mbp::buoy::Ushort2Hex(data_p->waveMode.raw, true) << " / "
                            << "dynPercPos=" << data_p->dynPercPos << " / "
                            << "wT1=" << data_p->wT1 << " / "
                            << "wT2=" << data_p->wT2 << " / "
                            << "wT3=" << data_p->wT3 << " / "
                            << "nSamp=" << data_p->nSamp << " / "
                            << "wA1=" << data_p->wA1 << " / "
                            << "wB0=" << data_p->wB0 << " / "
                            << "wB1=" << data_p->wB1 << " / "
                            << "anaOutScale=" << data_p->anaOutScale << " / "
                            << "corrThresh=" << data_p->corrThresh << " / "
                            << "tiLag2=" << data_p->tiLag2 << " / "
                            << "hChecksum=" << mbp::buoy::Ushort2Hex(data_p->hChecksum) << "]";
                        L_(debug) << sstr.str();
                    } // if ( mbp_IsEnabled(trace) )
                } // case PD_USER_CONFIGURATION
                break;

            case Nortek::PD_HEAD_CONFIGURATION:
                {
                    // -- TBL_AWAC2_HEAD_CONFIG_e --
// TODO:            CHECK_FULL_STRUCT_SIZE(mbp::buoy::par_t, currDataStrucSize);
                    tableId = mbp::buoy::TBL_AWAC2_HEAD_CONFIG_e;

                    std::ostringstream sstr("");

                    // Populate the row values
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();

                    Nortek::PdHeadConfiguration *data_p =
                        reinterpret_cast<Nortek::PdHeadConfiguration *>(&a_awac_p->data[dataPos]);

                    row_sp->push_back( mbp::buoy::PtimeToStr(recPTime) );
                    row_sp->push_back( mbp::buoy::DataHolder() );
                    row_sp->push_back( mbp::buoy::Ushort2Hex(data_p->config.raw, false) );
                    row_sp->push_back( data_p->hHeadFrequency );
                    row_sp->push_back( data_p->hHeadType );
                    row_sp->push_back( std::string(data_p->headSerNo, 12) );
                    row_sp->push_back( data_p->nBeams );
                    sstr.str("");
                    sstr
                        << "UNHEX('"
                        << mbp::buoy::BufferToHexString(data_p->systemData, 176, 48, false, false, false, true)
                        << "')";
                    row_sp->push_back( mbp::buoy::DataHolder(sstr.str()) );

                    m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));

                    if ( mbp_IsEnabled(trace) ) {
                        sstr.str("");
                        sstr
                            << "awacHeadConf (0xA504) - acquireDt=" << recPTime << " ["
                            << "hSize=" << data_p->hSize << " (" << data_p->hSize * 2 << "B) / "
                            << "config (pressure sens.=" << data_p->config.data.pressureSens << ", "
                                    << "magnetometer=" << data_p->config.data.magnetometer << ", "
                                    << "tilt sens.=" << data_p->config.data.tiltSens << ", "
                                    << "tilt orient.=" << (data_p->config.data.tiltSens ? "down" : "up")
                                    << ") / "
                            << "hHeadFrequency=" << data_p->hHeadFrequency << " / "
                            << "hHeadType=" << data_p->hHeadType << " / "
                            << "s/n=" << std::string(data_p->headSerNo, 12) << " / "
                            << "nBeams=" << data_p->nBeams << " / "
                            << "hChecksum=" << mbp::buoy::Ushort2Hex(data_p->hChecksum) << " / "
                            << "systemData=(hex)<" << std::endl
                            << mbp::buoy::BufferToHexString(data_p->systemData, 176, 48, true, true, false) << std::endl
                            << ">]";
                        L_(debug) << sstr.str();
                    } // if ( mbp_IsEnabled(trace) )
                } // case PD_HEAD_CONFIGURATION
                break;

            case Nortek::PD_HARDWARE_CONFIGURATION:
                {
                    // -- TBL_AWAC2_HW_CONFIG_e --
// TODO:            CHECK_FULL_STRUCT_SIZE(mbp::buoy::par_t, currDataStrucSize);
                    tableId = mbp::buoy::TBL_AWAC2_HW_CONFIG_e;

                    std::ostringstream sstr("");

                    // Populate the row values
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();

                    Nortek::PdHardwareConfiguration *data_p =
                        reinterpret_cast<Nortek::PdHardwareConfiguration *>(&a_awac_p->data[dataPos]);

                    row_sp->push_back( mbp::buoy::PtimeToStr(recPTime) );
                    row_sp->push_back( mbp::buoy::DataHolder() );
                    row_sp->push_back( std::string(data_p->info.data.serNo, 8) );
                    row_sp->push_back( data_p->info.data.prologSerNo );
                    row_sp->push_back( std::string(data_p->info.data.prologVer, 4) );
                    row_sp->push_back( mbp::buoy::Ushort2Hex(data_p->config.raw, false) );
                    row_sp->push_back( data_p->hFrequency );
                    row_sp->push_back( data_p->nPICversion );
                    row_sp->push_back( data_p->nHWrevision );
                    row_sp->push_back( data_p->hRecSize );
                    row_sp->push_back( mbp::buoy::Ushort2Hex(data_p->status.raw, false) );
                    row_sp->push_back( uint32_t(data_p->hFWversion) );

                    m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));

                    if ( mbp_IsEnabled(trace) ) {
                        sstr.str("");
                        sstr
                            << "awacHwConf (0xA505) - acquireDt=" << recPTime << " ["
                            << "hSize=" << data_p->hSize << " (" << data_p->hSize * 2 << "B) / "
                            << "instrument (sys s/n=" << std::string(data_p->info.data.serNo, 8) << ", "
                                         << "prolog s/n=" << data_p->info.data.prologSerNo << ", "
                                         << "prolog ver=" << std::string(data_p->info.data.prologVer, 4)
                                         << ") / "
                            << "config (recorder=" << data_p->config.data.recorder << ", compass=" << data_p->config.data.compass << ") / "
                            << "hFrequency=" << data_p->hFrequency << " / "
                            << "nPICversion=" << data_p->nPICversion << " / "
                            << "nHWrevision=" << data_p->nHWrevision << " / "
                            << "hRecSize=" << data_p->hRecSize << " / "
                            << "status (vrange=" << data_p->status.data.vRange << ") / "
                            << "hFWversion=" << data_p->hFWversion << " / "
                            << "hChecksum=" << mbp::buoy::Ushort2Hex(data_p->hChecksum) << "]";
                        L_(debug) << sstr.str();
                    } // if ( mbp_IsEnabled(trace) )
                } // case PD_HARDWARE_CONFIGURATION
                break;

            case Nortek::PD_CURRENTS_VELOCITY_PROFILE:
                {
                    // -- TBL_AWAC2_CURRENTS_HEADER_e --
// TODO:            CHECK_FULL_STRUCT_SIZE(mbp::buoy::par_t, currDataStrucSize);

                    std::ostringstream sstr("");

                    // Populate the row values
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();

                    Nortek::PdProfHeader *data_p =
                        reinterpret_cast<Nortek::PdProfHeader *>(&a_awac_p->data[dataPos]);

                    tableId = mbp::buoy::TBL_AWAC2_CURRENTS_HEADER_e;
                    row_sp->push_back( mbp::buoy::PtimeToStr(recPTime) );
                    row_sp->push_back( mbp::buoy::DataHolder() );
                    row_sp->push_back( mbp::buoy::PdClock2ProfileDtStartString(data_p->clock) );
                    row_sp->push_back( mbp::buoy::PdClock2String(data_p->clock) );
                    row_sp->push_back( data_p->hError );
                    row_sp->push_back( data_p->hAnaIn1 );
                    row_sp->push_back( double(data_p->hBattery) / 10.0 );
                    row_sp->push_back( double(data_p->u.hSoundspeed) / 10.0 );
                    row_sp->push_back( double(data_p->hHeading) / 10.0 );
                    row_sp->push_back( double(data_p->hPitch) / 10.0 );
                    row_sp->push_back( double(data_p->hRoll) / 10.0 );
                    row_sp->push_back( double((((uint16_t)data_p->cPressureMSB << 8) + (uint16_t)data_p->hPressureLSW)) / 1000.0 );
                    row_sp->push_back( double(data_p->hTemperature) / 100.0 );
                    row_sp->push_back( (int16_t)data_p->cStatus );
                    m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));

                    // Retrieve currents header AutoID
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();
                    sstr.str("");
                    sstr << "SELECT " << buoy::DataHolder::SEQ_VAR << mbp::buoy::SQL_VAR_AWAC2_CURRENTS_HDR << ":=LAST_INSERT_ID()";
                    row_sp->push_back(mbp::buoy::DataHolder(sstr.str()));
                    m_rows->push_back(mbp::buoy::TableRow(mbp::buoy::TBL_NA_MANUAL_SPEC_e, row_sp));

                    if (a_parsingType == mbp::PARSING_TYPE_RUNTIME_e) {
                        double pressure = -999.0;
                        double temperature = -999.0;

                        pressure = double((((uint16_t)data_p->cPressureMSB << 8) + (uint16_t)data_p->hPressureLSW)) / 1000.0;
                        temperature = double(data_p->hTemperature) / 100.0;

                        a_runtimeData_p->setAwac(recPTime, temperature, pressure);
                    }

                    // Add actual currents
                    size_t noBins = AWAC_PROFILE_GET_NUM_BINS(data_p);
                    size_t velocityOffset = AWAC_PROFILE_GET_VEL_OFFSET(data_p);
                    size_t amplitudeOffset = AWAC_PROFILE_GET_AMP_OFFSET(data_p);
                    int16_t *vel_p = reinterpret_cast<int16_t *>(&data_p->data[velocityOffset]);
                    uint8_t *amp_p = reinterpret_cast<uint8_t *>(&data_p->data[amplitudeOffset]);
                    tableId = mbp::buoy::TBL_AWAC2_CURRENTS_DATA_e;
                    for (size_t i=0; i < noBins; ++i) {
                        mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                        row_sp->clear();
                        row_sp->push_back( mbp::buoy::DataHolder(mbp::buoy::SQL_VAR_AWAC2_CURRENTS_HDR) );
                        row_sp->push_back( int16_t(i) );
                        row_sp->push_back( double(vel_p[i+0]) / 10.0 );
                        row_sp->push_back( double(vel_p[i+noBins]) / 10.0 );
                        row_sp->push_back( double(vel_p[i+(2*noBins)]) / 10.0 );
                        row_sp->push_back( uint16_t(amp_p[i+0]) );
                        row_sp->push_back( uint16_t(amp_p[i+noBins]) );
                        row_sp->push_back( uint16_t(amp_p[i+(2*noBins)]) );
                        m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
                    }

                    if ( mbp_IsEnabled(trace) ) {
                        sstr.str("");
                        sstr
                            << "awacProf (0xA520) - acquireDt=" << recPTime << " ["
                            << "hSize=" << data_p->hSize << " (" << data_p->hSize * 2 << "B) / "
                            << "clock=" << mbp::buoy::PdClock2String(data_p->clock) << " / "
                            << "hError=" << data_p->hError << " / "
                            << "hAnaIn1=" << data_p->hAnaIn1 << " / "
                            << "hBattery=" << data_p->hBattery << " / "
                            << "hSoundspeed=" << data_p->u.hSoundspeed << " / "
                            << "hHeading=" << data_p->hHeading << " / "
                            << "hPitch=" << data_p->hPitch << " / "
                            << "hRoll=" << data_p->hRoll << " / "
                            << "pressure=" << (((uint16_t)data_p->cPressureMSB << 8) + (uint16_t)data_p->hPressureLSW) << " / "
                            << "cStatus=" << data_p->cStatus << " / "
                            << "hTemperature=" << data_p->hTemperature << " / "
                            << "velocity and amplitude=<" << std::endl;
                        for (size_t i=0; i < noBins; ++i) {
                            sstr
                                << "cell #" << (i+1) << ". "
                                << "vel [E=" << (float)vel_p[i+0] / 10 << ", N=" << (float)vel_p[i+noBins] / 10 << ", U=" << (float)vel_p[i+(2*noBins)] / 10 << "], "
                                << "amp [amp1=" << (uint16_t)amp_p[i+0] << ", amp2=" << (uint16_t)amp_p[i+noBins] << ", amp3=" << (uint16_t)amp_p[i+(2*noBins)] << "]"
                                << std::endl;
                        }
                        sstr
                            << "> / "
                            << "hChecksum=" << mbp::buoy::Ushort2Hex(
                                    *(reinterpret_cast<uint16_t *>(&data_p->data[AWAC_PROFILE_GET_CHECKSUM_OFFSET(data_p)]))
                                    ) << "]";
                        L_(debug) << sstr.str();
                    } // if ( mbp_IsEnabled(trace) )
                } // case PD_CURRENTS_VELOCITY_PROFILE
                break;

            case Nortek::PD_WAVE_PARAMETER_DATA:
                {
                    // -- TBL_AWAC2_WAVES_e --
// TODO:            CHECK_FULL_STRUCT_SIZE(mbp::buoy::par_t, currDataStrucSize);
                    tableId = mbp::buoy::TBL_AWAC2_WAVES_e;

                    std::ostringstream sstr("");

                    // Populate the row values
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();

                    Nortek::PdWaveParData *data_p =
                        reinterpret_cast<Nortek::PdWaveParData *>(&a_awac_p->data[dataPos]);

                    row_sp->push_back( mbp::buoy::PtimeToStr(recPTime) );
                    row_sp->push_back( mbp::buoy::DataHolder() );
                    row_sp->push_back( mbp::buoy::PdClock2ProfileDtStartString(data_p->clock) );
                    row_sp->push_back( mbp::buoy::PdClock2String(data_p->clock) );
                    row_sp->push_back( uint16_t(data_p->cSpectrumType) );
                    row_sp->push_back( uint16_t(data_p->cProcMethod) );
                    row_sp->push_back( double(data_p->hHm0) / 1000.0 );
                    row_sp->push_back( double(data_p->hH3) / 1000.0 );
                    row_sp->push_back( double(data_p->hH10) / 1000.0 );
                    row_sp->push_back( double(data_p->hHmax) / 1000.0 );
                    row_sp->push_back( double(data_p->hTm02) / 100.0 );
                    row_sp->push_back( double(data_p->hTp) / 100.0 );
                    row_sp->push_back( double(data_p->hTz) / 100.0 );
                    row_sp->push_back( double(data_p->hDirTp) / 100.0 );
                    row_sp->push_back( double(data_p->hSprTp) / 100.0 );
                    row_sp->push_back( double(data_p->hDirMean) / 100.0 );
                    row_sp->push_back( double(data_p->hUI) / 65535.0 );
                    row_sp->push_back( double(data_p->lPressureMean) / 1000.0 );
                    row_sp->push_back( data_p->hNumNoDet );
                    row_sp->push_back( data_p->hNumBadDet );
                    row_sp->push_back( double(data_p->hCurSpeedMean) / 10.0 );
                    row_sp->push_back( double(data_p->hCurDirMean) / 100.0 );
                    row_sp->push_back( data_p->lError );
                    m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));

                    if ( mbp_IsEnabled(trace) ) {
                        sstr.str("");
                        sstr
                            << "awacWaveParData (0xA560) - acquireDt=" << recPTime << " ["
                            << "hSize=" << data_p->hSize << " (" << data_p->hSize * 2 << "B) / "
                            << "clock=" << mbp::buoy::PdClock2String(data_p->clock) << " / "
                            << "cSpectrumType=" << (uint16_t) data_p->cSpectrumType << " / "
                            << "cProcMethod=" << (uint16_t) data_p->cProcMethod << " / "
                            << "hHm0=" << data_p->hHm0 << " / "
                            << "hH3=" << data_p->hH3 << " / "
                            << "hH10=" << data_p->hH10 << " / "
                            << "hHmax=" << data_p->hHmax << " / "
                            << "hTm02=" << data_p->hTm02 << " / "
                            << "hTp=" << data_p->hTp << " / "
                            << "hTz=" << data_p->hTz << " / "
                            << "hDirTp=" << data_p->hDirTp << " / "
                            << "hSprTp=" << data_p->hSprTp << " / "
                            << "hDirMean=" << data_p->hDirMean << " / "
                            << "hUI=" << data_p->hUI << " / "
                            << "lPressureMean=" << data_p->lPressureMean << " / "
                            << "hNumNoDet=" << data_p->hNumNoDet << " / "
                            << "hNumBadDet=" << data_p->hNumBadDet << " / "
                            << "hCurSpeedMean=" << data_p->hCurSpeedMean << " / "
                            << "hCurDirMean=" << data_p->hCurDirMean << " / "
                            << "lError=" << data_p->lError << " / "
                            << "hChecksum" << mbp::buoy::Ushort2Hex( *(reinterpret_cast<uint16_t *>(&data_p->hChecksum)) ) << "]";
                        L_(debug) << sstr.str();
                    } // if ( mbp_IsEnabled(trace) )
                } // case PD_WAVE_PARAMETER_DATA
                break;


            case Nortek::PD_WAVE_BAND_DATA:
                {
                    // -- TBL_AWAC2_WAVEBANDS_e --
// TODO:            CHECK_FULL_STRUCT_SIZE(mbp::buoy::par_t, currDataStrucSize);
                    tableId = mbp::buoy::TBL_AWAC2_WAVEBANDS_e;

                    std::ostringstream sstr("");

                    // Populate the row values
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();

                    Nortek::PdWaveBandsData *data_p =
                        reinterpret_cast<Nortek::PdWaveBandsData *>(&a_awac_p->data[dataPos]);

                    row_sp->push_back( mbp::buoy::PtimeToStr(recPTime) );
                    row_sp->push_back( mbp::buoy::DataHolder() );
                    row_sp->push_back( mbp::buoy::PdClock2ProfileDtStartString(data_p->clock) );
                    row_sp->push_back( mbp::buoy::PdClock2String(data_p->clock) );
                    row_sp->push_back( uint16_t(data_p->cSpectrumType) );
                    row_sp->push_back( uint16_t(data_p->cProcMethod) );
                    row_sp->push_back( double(data_p->hLowFrequency) / 1000.0 );
                    row_sp->push_back( double(data_p->hHighFrequency) / 1000.0 );
                    row_sp->push_back( double(data_p->hHm0) / 1000.0 );
                    row_sp->push_back( double(data_p->hTm02) / 100.0 );
                    row_sp->push_back( double(data_p->hTp) / 100.0 );
                    row_sp->push_back( double(data_p->hDirTp) / 100.0 );
                    row_sp->push_back( double(data_p->hDirMean) / 100.0 );
                    row_sp->push_back( double(data_p->hSprTp) / 100.0 );
                    row_sp->push_back( data_p->lError );
                    m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));

                    if ( mbp_IsEnabled(trace) ) {
                        sstr.str("");
                        sstr
                            << "awacWaveBandData (0xA561) - acquireDt=" << recPTime << " ["
                            << "hSize=" << data_p->hSize << " (" << data_p->hSize * 2 << "B) / "
                            << "clock=" << mbp::buoy::PdClock2String(data_p->clock) << " / "
                            << "cSpectrumType=" << (uint16_t) data_p->cSpectrumType << " / "
                            << "cProcMethod=" << (uint16_t) data_p->cProcMethod << " / "
                            << "hLowFrequency=" << data_p->hLowFrequency << " / "
                            << "hHighFrequency=" << data_p->hHighFrequency << " / "
                            << "hHm0=" << data_p->hHm0 << " / "
                            << "hTm02=" << data_p->hTm02 << " / "
                            << "hTp=" << data_p->hTp << " / "
                            << "hDirTp=" << data_p->hDirTp << " / "
                            << "hDirMean=" << data_p->hDirMean << " / "
                            << "hSprTp=" << data_p->hSprTp << " / "
                            << "lError=" << data_p->lError << " / "
                            << "hChecksum" << mbp::buoy::Ushort2Hex( *(reinterpret_cast<uint16_t *>(&data_p->hChecksum)) ) << "]";
                        L_(debug) << sstr.str();
                    } // if ( mbp_IsEnabled(trace) )
                } // case PD_WAVE_BAND_DATA
                break;


            case Nortek::PD_WAVE_SPECTRUM_DATA:
                {
                    // -- TBL_AWAC2_WAVESPECTRUM_HEADER_e --
// TODO:            CHECK_FULL_STRUCT_SIZE(mbp::buoy::par_t, currDataStrucSize);

                    std::ostringstream sstr("");

                    // Populate the row values
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();

                    Nortek::PdWaveSpectrumHeader *data_p =
                        reinterpret_cast<Nortek::PdWaveSpectrumHeader *>(&a_awac_p->data[dataPos]);

                    tableId = mbp::buoy::TBL_AWAC2_WAVESPECTRUM_HEADER_e;
                    row_sp->push_back( mbp::buoy::PtimeToStr(recPTime) );
                    row_sp->push_back( mbp::buoy::DataHolder() );
                    row_sp->push_back( mbp::buoy::PdClock2ProfileDtStartString(data_p->clock) );
                    row_sp->push_back( mbp::buoy::PdClock2String(data_p->clock) );
                    row_sp->push_back( uint16_t(data_p->cSpectrumType) );
                    row_sp->push_back( data_p->hNumSpectrum );
                    row_sp->push_back( double(data_p->hLowFrequency) / 1000.0 );
                    row_sp->push_back( double(data_p->hHighFrequency) / 1000.0 );
                    row_sp->push_back( double(data_p->hStepFrequency) / 1000.0 );
                    row_sp->push_back( data_p->lEnergyMultiplier );
                    m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));

                    // Retrieve currents header AutoID
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();
                    sstr.str("");
                    sstr << "SELECT " << buoy::DataHolder::SEQ_VAR << mbp::buoy::SQL_VAR_AWAC2_WAVESPECTRUM_e << ":=LAST_INSERT_ID()";
                    row_sp->push_back(mbp::buoy::DataHolder(sstr.str()));
                    m_rows->push_back(mbp::buoy::TableRow(mbp::buoy::TBL_NA_MANUAL_SPEC_e, row_sp));

                    // Add actual currents
                    size_t noFreq = AWAC_WAVE_SPECTRUM_GET_NUM_FREQ(data_p);
                    size_t freqOffset = AWAC_WAVE_SPECTRUM_GET_FREQ_OFFSET(data_p);
                    uint16_t *energy_p = reinterpret_cast<uint16_t *>(&data_p->data[freqOffset]);
                    tableId = mbp::buoy::TBL_AWAC2_WAVESPECTRUM_DATA_e;
                    for (size_t i=0; i < noFreq; ++i) {
                        mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                        row_sp->clear();
                        row_sp->push_back( mbp::buoy::DataHolder(mbp::buoy::SQL_VAR_AWAC2_WAVESPECTRUM_e) );
                        row_sp->push_back( int16_t(i) );
                        row_sp->push_back( double(energy_p[i]) * data_p->lEnergyMultiplier / 65535.0 / 1000.0 );
                        m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
                    }

                    if ( mbp_IsEnabled(trace) ) {
                        sstr.str("");
                        sstr
                            << "awacWaveSpectrum (0xA562) - acquireDt=" << recPTime << " ["
                            << "hSize=" << data_p->hSize << " (" << data_p->hSize * 2 << "B) / "
                            << "clock=" << mbp::buoy::PdClock2String(data_p->clock) << " / "
                            << "cSpectrumType=" << (uint16_t) data_p->cSpectrumType << " / "
                            << "hNumSpectrum=" << data_p->hNumSpectrum << " / "
                            << "hLowFrequency=" << data_p->hLowFrequency << " / "
                            << "hHighFrequency=" << data_p->hHighFrequency << " / "
                            << "hStepFrequency=" << data_p->hStepFrequency << " / "
                            << "lEnergyMultiplier=" << data_p->lEnergyMultiplier << " / "

                            << "hEnergy=<" << std::endl;
                        for (size_t i=0; i < noFreq; ++i) {
                            sstr
                                << "energy #" << (i+1) << " -> " << energy_p[i]
                                << std::endl;
                        }
                        sstr
                            << "> / "
                            << "hChecksum=" << mbp::buoy::Ushort2Hex(
                                    *(reinterpret_cast<uint16_t *>(&data_p->data[AWAC_WAVE_SPECTRUM_GET_CHECKSUM_OFFSET(data_p)]))
                                    ) << "]";
                        L_(debug) << sstr.str();
                    } // if ( mbp_IsEnabled(trace) )
                } // case PD_WAVE_SPECTRUM_DATA
                break;



            case Nortek::PD_WAVE_FOURIER_COEFF:
                {
                    // -- TBL_AWAC2_FOURIER_HEADER_e --
// TODO:            CHECK_FULL_STRUCT_SIZE(mbp::buoy::par_t, currDataStrucSize);

                    std::ostringstream sstr("");

                    // Populate the row values
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();

                    Nortek::PdWaveFourierCoeff *data_p =
                        reinterpret_cast<Nortek::PdWaveFourierCoeff *>(&a_awac_p->data[dataPos]);

                    tableId = mbp::buoy::TBL_AWAC2_FOURIER_HEADER_e;
                    row_sp->push_back( mbp::buoy::PtimeToStr(recPTime) );
                    row_sp->push_back( mbp::buoy::DataHolder() );
                    row_sp->push_back( mbp::buoy::PdClock2ProfileDtStartString(data_p->clock) );
                    row_sp->push_back( mbp::buoy::PdClock2String(data_p->clock) );
                    row_sp->push_back( uint16_t(data_p->cProcMethod) );
                    row_sp->push_back( data_p->hNumSpectrum );
                    row_sp->push_back( double(data_p->hLowFrequency) / 1000.0 );
                    row_sp->push_back( double(data_p->hHighFrequency) / 1000.0 );
                    row_sp->push_back( double(data_p->hStepFrequency) / 1000.0 );
                    m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));

                    // Retrieve currents header AutoID
                    row_sp = mbp::buoy::row_spt(new mbp::buoy::row_fields_lt());
                    row_sp->clear();
                    sstr.str("");
                    sstr << "SELECT " << buoy::DataHolder::SEQ_VAR << mbp::buoy::SQL_VAR_AWAC2_FOURIERCOEFF_e << ":=LAST_INSERT_ID()";
                    row_sp->push_back(mbp::buoy::DataHolder(sstr.str()));
                    m_rows->push_back(mbp::buoy::TableRow(mbp::buoy::TBL_NA_MANUAL_SPEC_e, row_sp));

                    // Add actual currents
                    size_t noCoeff = AWAC_WAVE_FOURIER_COEFF_GET_NUM(data_p);
                    size_t hA1Offset = AWAC_WAVE_FOURIER_COEFF_GET_hA1_OFFSET(data_p);
                    size_t hB1Offset = AWAC_WAVE_FOURIER_COEFF_GET_hB1_OFFSET(data_p);
                    size_t hA2Offset = AWAC_WAVE_FOURIER_COEFF_GET_hA2_OFFSET(data_p);
                    size_t hB2Offset = AWAC_WAVE_FOURIER_COEFF_GET_hB2_OFFSET(data_p);
                    int16_t *hA1_p = reinterpret_cast<int16_t *>(&data_p->data[hA1Offset]);
                    int16_t *hB1_p = reinterpret_cast<int16_t *>(&data_p->data[hB1Offset]);
                    int16_t *hA2_p = reinterpret_cast<int16_t *>(&data_p->data[hA2Offset]);
                    int16_t *hB2_p = reinterpret_cast<int16_t *>(&data_p->data[hB2Offset]);
                    tableId = mbp::buoy::TBL_AWAC2_FOURIER_DATA_e;
                    for (size_t i=0; i < noCoeff; ++i) {
                        mbp::buoy::row_spt row_sp(new mbp::buoy::row_fields_lt());
                        row_sp->clear();
                        row_sp->push_back( mbp::buoy::DataHolder(mbp::buoy::SQL_VAR_AWAC2_FOURIERCOEFF_e) );
                        row_sp->push_back( int16_t(i) );
                        row_sp->push_back( double(hA1_p[i]) / 32767 );
                        row_sp->push_back( double(hB1_p[i]) / 32767 );
                        row_sp->push_back( double(hA2_p[i]) / 32767 );
                        row_sp->push_back( double(hB2_p[i]) / 32767 );
                        m_rows->push_back(mbp::buoy::TableRow(tableId, row_sp));
                    }

                    if ( mbp_IsEnabled(trace) ) {
                        sstr.str("");
                        sstr
                            << "awacWaveFourierCoeff (0xA563) - acquireDt=" << recPTime << " ["
                            << "hSize=" << data_p->hSize << " (" << data_p->hSize * 2 << "B) / "
                            << "clock=" << mbp::buoy::PdClock2String(data_p->clock) << " / "
                            << "cProcMethod=" << (uint16_t) data_p->cProcMethod << " / "
                            << "hNumSpectrum=" << data_p->hNumSpectrum << " / "
                            << "hLowFrequency=" << data_p->hLowFrequency << " / "
                            << "hHighFrequency=" << data_p->hHighFrequency << " / "
                            << "hStepFrequency=" << data_p->hStepFrequency << " / "
                            << "fourier coeffs=<" << std::endl;
                        for (size_t i=0; i < noCoeff; ++i) {
                            sstr
                                << "coeff #" << (i+1) << ". "
                                << "[hA1=" << hA1_p[i] << ", hB1=" << hB1_p[i] << ", hA2=" << hA2_p[i] << ", hB2=" << hB2_p[i] << "]"
                                << std::endl;
                        }
                        sstr
                            << "> / "
                            << "hChecksum=" << mbp::buoy::Ushort2Hex(
                                    *(reinterpret_cast<uint16_t *>(&data_p->data[AWAC_WAVE_FOURIER_COEFF_GET_CHECKSUM_OFFSET(data_p)]))
                                    ) << "]";
                        L_(debug) << sstr.str();
                    } // if ( mbp_IsEnabled(trace) )
                } // case PD_WAVE_FOURIER_COEFF
                break;

            default:
                {
                    std::ostringstream sstr;
                    boost::posix_time::ptime pt = boost::posix_time::from_time_t(currRecordSeconds);
                    std::string dtStr = mbp::buoy::PtimeToStr(pt);

                    sstr << "AWAC [acquireDt=" << dtStr << " / "
                        << "cSync=" << mbp::buoy::Uchar2Hex(pktHeader_p->cSync) << " / "
                        << "cID=" << mbp::buoy::Uchar2Hex(pktHeader_p->cID) << "], "
                        << "dataSize=" << dataSize;
                    L_(debug) << sstr.str();
                    sstr.str("");
                    sstr
                        << "Hex dump:" << std::endl
                        << "----" << std::endl
                        << mbp::buoy::BufferToHexString(
                                &a_awac_p->data[dataPos], (dataSize-dataPos),
                                16,
                                true,
                                true,
                                true) << std::endl
                        << "----";
                    L_(debug) << sstr.str();

                    if (mbp_IsEnabled(trace)) {
                        int fd = open("awac.data.dat", O_APPEND | O_WRONLY | O_CREAT | O_DSYNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
                        if (fd < 0)
                            L_(warning) << "open() fd=" << fd << " (error=" << strerror(errno) << ")";
                        else
                            L_(info) << "open() fd=" << fd;
                        int rv = write(fd, &a_awac_p->data[dataPos], dataSize);
                        if (rv < 0)
                            L_(warning) << "write() rv=" << rv << " (error=" << strerror(errno) << ")";
                        syncfs(fd);
                        close(fd);
                    }

                    //throw std::runtime_error(sstr.str().c_str());
                    break;
                }
        } // switch (pktHeader_p->cID)
        dataPos += pktHeader_p->hSize * 2;  // TODO: what's this
    } // while (dataPos < dataSize)

    return;
} // BuoyAwacData::ProcessDataRecord()

} // namespace mbp
