#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>

#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include <boost/cstdint.hpp>
#include <boost/log/detail/snprintf.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "buoy_utils.hpp"
#include "buoy_log.hpp"

#include "generated/version.hpp"


namespace po = boost::program_options;

namespace mbp {
namespace buoy {

OptionParser g_options;

OptionParser::OptionParser()
    :
        m_dbUser(),
        m_dbPasswd(),
        m_dbHost("localhost"),
        m_dbDbPrefix(),
        m_dbMarkerTable(),
        m_configFile(""),
        m_logLevel(mbp::info),
        m_logFile("-"),
        m_doTimeScalling(false),
        m_ignoreDuplicates(0),
        m_dbgDoDbInsert(true),
        m_dbgDoSqlDump(false),
        m_serverIp("127.0.0.1"),
        m_serverPort(10101),
        m_doDaemonize(false)
{
    return;
} // OptionParser::OptionParser()


OptionParser::~OptionParser()
{
    return;
} // OptionParser::~OptionParser()


bool OptionParser::Parse(int a_argc, char **a_argv_pp, mbp::buoy::config_type_e a_configType)
{
    try {
        std::ostringstream sstr;

        // -- Usage options --
        sstr.str("");
        sstr <<
            "General options:" << std::endl <<
            "Usage: " << a_argv_pp[0] << " <options>";
        po::options_description usageOpt(sstr.str());
        usageOpt.add_options()
            ("help", "produce help")
            ("version", "output version number");

        // -- Daemonization --
        sstr.str("");
        sstr <<
             "Daemonization";
        po::options_description daemonOpt(sstr.str());
        daemonOpt.add_options()
                ("daemon", "Become a daemon (NOTE: this requires logging to __non__ <stdout> / <stderr>");

        // -- Network connection options --
        sstr.str("");
        sstr <<
             "Network options";
        po::options_description netOpt(sstr.str());
        netOpt.add_options()
                ("server-ip", po::value<std::string>()->default_value("127.0.0.1"), "Buoy data server IP address")
                ("server-port", po::value<uint16_t>()->default_value(10101), "Buoy data server TCP port");

        // -- Log options --
        sstr.str("");
        sstr <<
            "Logging options";
        po::options_description logOpt(sstr.str());
        logOpt.add_options()
            ("log-level", po::value<std::string>()->default_value("info"), "Log level (trace | debug | info | warning | error)")
            ("log-file", po::value<std::string>()->default_value(m_logFile), "Log file name (specify '-' for <STDOUT>)")
            ("log-extra", "Do extra logging output");

        // -- Config file options --
        sstr.str("");
        sstr <<
            "Config file options";
        po::options_description confOpt(sstr.str());
        confOpt.add_options()
            ("conf", po::value<std::string>()->default_value(m_configFile), "Config file");

        // -- Database options --
        sstr.str("");
        sstr <<
            "Database connection options (a config file can be used too - see above)";
        po::options_description dbOpt(sstr.str());
        dbOpt.add_options()
            ("db-user", po::value<std::string>(), "MySQL username")
            ("db-pass", po::value<std::string>(), "MySQL password")
            ("db-host", po::value<std::string>()->default_value(m_dbHost), "MySQL hostname");
        if (a_configType == CONFIG_READER_e) {
            dbOpt.add_options()
                    ("db-prefix", po::value<std::string>(), "MySQL data database prefix (e.g. buoy)")
                    ("db-marker-table", po::value<std::string>(), "Data read marker db and table (e.g. buoy_markers.raw_data_import)");
        }


        // -- Bug workarounds options --
        sstr.str("");
        sstr <<
            "[ATTENTION] Do not use this options unless you know what you're doing!!!" << std::endl <<
            "Data bug workaround options";
        po::options_description bugOpt(sstr.str());
        bugOpt.add_options()
            ("bug-ts-scalling", "Do timestamp scalling (DISABLED by default)")
            ("bug-db-duplicates", po::value<uint32_t>()->default_value(m_ignoreDuplicates), "Ignore specified number of DB row duplicates before throwing an exception");

        // -- Debug options --
        sstr.str("");
        sstr <<
            "[ATTENTION] Below options are usefull only for debugging purposes!!!" << std::endl <<
            "Debug options";
        po::options_description dbgOpt(sstr.str());
        dbgOpt.add_options()
            ("dbg-skip-db", "Do NOT insert any data into DB (DISABLED by default)")
            ("dbg-dump-data", "Dump parsed data to log facility (DISABLED by default)");

        // -- All options instance --
        po::options_description allOpts("");
        allOpts.add(usageOpt).add(daemonOpt).add(netOpt).add(logOpt).add(confOpt).add(dbOpt);
        if (a_configType == CONFIG_READER_e) {
            allOpts.add(bugOpt).add(dbgOpt);
        }

        po::variables_map vm;
        po::store(po::parse_command_line(a_argc, a_argv_pp, allOpts), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << allOpts << std::endl;
            return false;
        }

        if (vm.count("version")) {
            std::cout
              << "Program details:" << std::endl
              << " - executable:   '" << boost::filesystem::basename(a_argv_pp[0]) << "'" << std::endl
              << " - version no.:  '" << mbp::buoy::GetProgramVersion() << "'" << std::endl;
            return false;
        }

        // -- Config file --
        if (vm.count("conf")) {
            m_configFile = vm["conf"].as<std::string>();
        }
        std::ifstream ifs(m_configFile.c_str(), std::ios::in);
        po::store(po::parse_config_file(ifs, allOpts), vm);
        ifs.close();
        po::notify(vm);

        // -- Logging --
        bool doExtraLogging(false);
        if (vm.count("log-level")) {
            std::string strLevel(vm["log-level"].as<std::string>());
            if (strLevel.compare("trace") == 0) {
                m_logLevel = mbp::trace;
            } else if (strLevel.compare("debug") == 0) {
                m_logLevel = mbp::debug;
            } else if (strLevel.compare("info") == 0) {
                m_logLevel = mbp::info;
            } else if (strLevel.compare("warning") == 0) {
                m_logLevel = mbp::warning;
            } else if (strLevel.compare("error") == 0) {
                m_logLevel = mbp::error;
            } else if (strLevel.compare("fatal") == 0) {
                m_logLevel = mbp::fatal;
            }
        }
        if (vm.count("log-file")) {
            m_logFile = vm["log-file"].as<std::string>();
        }
        if (vm.count("log-extra")) {
            doExtraLogging = true;
        }

        mbp_LogInit(doExtraLogging, m_logFile);
        mbp_SetLogLevel(m_logLevel);

        // -- Daemonization --
        if (vm.count("daemon")) {
            m_doDaemonize = true;
            if (m_logFile.compare("-") == 0) {
                throw(std::invalid_argument("Stdout logging (--log-file=-) can't be used with daemonization (--daemon)!"));
            }
        }

        // Output parsed configuration so far
        L_(trace) << "Config: general ("
                  << "config file='" << m_configFile << "')";
        L_(trace) << "Config: logging ("
                  << "log-file='" << m_logFile << "' / "
                  << "level=" << (int)m_logLevel << " / "
                  << "log-extra=" << doExtraLogging << ")";
        L_(trace) << "Config: run mode (daemon=" << m_doDaemonize << ")";

        // -- Network options --
        if (vm.count("server-ip")) {
            m_serverIp = vm["server-ip"].as<std::string>();
        }
        if (vm.count("server-port")) {
            m_serverPort = vm["server-port"].as<uint16_t>();
        }
        L_(trace) << "Config: RAW data source ("
                  << "server-ip='" << m_serverIp << "' / "
                  << "server-port=" << m_serverPort << ")";

        // -- Database --
        if (vm.count("db-user")) {
            m_dbUser = vm["db-user"].as<std::string>();
        }
        if (vm.count("db-pass")) {
            m_dbPasswd = vm["db-pass"].as<std::string>();
        }
        if (vm.count("db-host")) {
            m_dbHost = vm["db-host"].as<std::string>();
        }
        if (a_configType == CONFIG_READER_e && vm.count("db-prefix")) {
            m_dbDbPrefix = vm["db-prefix"].as<std::string>();
        }
        if (vm.count("db-marker-table")) {
            m_dbMarkerTable = vm["db-marker-table"].as<std::string>();
        }
        L_(trace) << "Config: DB ("
                  << "user='" << m_dbUser << "' / "
                  << "pass='" << m_dbPasswd << "' / "
                  << "host='" << m_dbHost << "' / "
                  << "db_prefix='" << m_dbDbPrefix << "' / "
                  << "marker_tbl_prefix='" << m_dbMarkerTable << "')";

        // -- Bug workarounds options --
        if (vm.count("bug-ts-scalling")) {
            m_doTimeScalling = true;
        }
        if (vm.count("bug-db-duplicates")) {
            m_ignoreDuplicates = vm["bug-db-duplicates"].as<uint32_t>();
        }
        L_(trace) << "Config: duplicates ("
                  << "timestamp scalling=" << m_doTimeScalling << " / "
                  << "# duplicates to ignore=" << m_ignoreDuplicates << ")";

        // -- Debug options --
        if (vm.count("dbg-skip-db")) {
            m_dbgDoDbInsert = false;
        }
        if (vm.count ("dbg-dump-data")) {
            m_dbgDoSqlDump = true;
        }
        L_(trace) << "Config: persistence ("
                  << "do-db-inserts=" << m_dbgDoDbInsert << " / "
                  << "print-sql-queries=" << m_dbgDoSqlDump << ")";
    }
    catch(std::exception& e) {
        L_(fatal) << "ERROR: " << e.what();
        return false;
    }

    return true;
} // OptionParser::Parse()

const std::string PtimeToStr(boost::posix_time::ptime posixTime) {
    static char const* const fmt = "%Y-%m-%d %H:%M:%S";
    std::ostringstream sstr;
    sstr.imbue(std::locale(std::cout.getloc(), new boost::posix_time::time_facet(fmt)));
    sstr << posixTime;
    return sstr.str();
} // BuoyDb::PtimeToSql

const std::string PtimeToDateStr(const boost::posix_time::ptime& posixTime) {
    static char const* const fmt = "%Y-%m-%d";
    std::ostringstream sstr;
    sstr.imbue(std::locale(std::cout.getloc(), new boost::posix_time::time_facet(fmt)));
    sstr << posixTime;
    return sstr.str();
} // BuoyDb::PtimeToSql


std::string BufferToHexString(
        void *a_buffer_p, size_t bufLen,
        size_t a_perLine,
        bool a_delim,
        bool a_addr,
        bool a_ascii,
        bool a_skipLineBreaks)
{
    boost::uint8_t hexDigits[] = "0123456789ABCDEF";
    char lineBuf[230];
    char *line_p;
    std::ostringstream sstr;

    // Support up to 24 bytes per line (due to lineBuf size)
    if ((a_perLine <= 0) || (a_perLine > 48)) {
        a_perLine = 48;
    }

    boost::uint8_t *buffer_p=reinterpret_cast<boost::uint8_t *>(a_buffer_p);

    size_t bufIdx=0;
    size_t i;
    while (bufIdx < bufLen) {
        line_p = lineBuf;
        if (a_addr) {
            boost::log::aux::snprintf(line_p, sizeof(lineBuf), "%05d: ", (int)bufIdx);
            line_p += 7;
        }

        for (
             i=0;
             (bufIdx<bufLen) && (i<a_perLine);
             ++i, ++bufIdx)
        {
            if (i>0 && a_delim) {
                *line_p++ = ' ';
            }
            *line_p++ = hexDigits[(boost::uint8_t)buffer_p[i] >> 4];
            *line_p++ = hexDigits[(boost::uint8_t)buffer_p[i] & 0x0F];
        } // for (... (i<bufLen) && (i<a_perLine) ...)

        if (a_ascii) {
            bufIdx -= i;
            for (size_t j=i; j<a_perLine; ++j) {
                *line_p++ = ' '; // hex digit 1
                *line_p++ = ' '; // hex digit 2
                if (a_delim) {
                    *line_p++ = ' ';
                }
            }
            *line_p++ = ' ';

            for (
                 i=0;
                 (bufIdx<bufLen) && (i<a_perLine);
                 ++i, ++bufIdx)
            {
                if (isgraph(buffer_p[i])) {
                    *line_p++ = buffer_p[i];
                } else {
                    *line_p++ = '.';
                }
            } // for (... (i<bufLen) && (i<a_perLine) ...)
        } // if (a_ascii)

        *line_p++ = '\0';

        buffer_p += i;

        sstr <<  std::string(lineBuf);
        if ( !a_skipLineBreaks && (bufIdx < bufLen)) {
            sstr << std::endl;
        }
    } // while (bufIdx < bufLen)

    return sstr.str();
} // BufferToHexString()


/*
 * Output the provided char (uint8_t) as hex string
 */
const std::string Uchar2Hex(uint8_t inchar, bool outputBase) {
    std::ostringstream sstr(std::ostringstream::out);
    sstr << ((outputBase) ? "0x" : "") << std::setw(2) << std::setfill('0') << std::hex << (int)(inchar);
    return sstr.str();
} // Uchar2Hex()


/*
 * Output the provided unsigned short (uint16_t) as hex string
 */
const std::string Ushort2Hex(uint16_t in, bool outputBase) {
    std::ostringstream sstr(std::ostringstream::out);
    sstr << ((outputBase) ? "0x" : "") << std::setw(4) << std::setfill('0') << std::hex << (uint16_t)(in);
    return sstr.str();
} // Ushort2Hex()








/*
 * Daemonize current process based on passed variable.
 *
 * ATTENTION:
 *  On demonization, if required, the process gets its working dir changed to '/'.
 *  Therefore relative file paths should be open before calling this function; this
 *  most probably applies to log file.
 *  Also, please note, the function uses syslog and Boost::Log to output error messages.
 *
 * Most of the code was copied from Boost boost_asio/example/fork/daemon.cpp
 */
void DaemonizeOnRequest(bool doDaemonize)
{
    if (! doDaemonize) {
        return;
    }

    // 5. Call fork(), to create a background process.
    if (pid_t pid = fork())
    {
        if (pid > 0)
        {
            // the parent sleeps for 1 second, which allows the child to start normally
            usleep(1 * 1000 * 1000);
            exit(0);
        } else  {
            syslog(LOG_ERR | LOG_USER, "First fork failed: %m");
            std::ostringstream sstr;
            sstr << "First fork failed (" << strerror(errno) << ")!";
            throw std::string(sstr.str());
        }
    }

    // 6. In the child, call setsid() to detach from any terminal and create an independent session.
    //setpgrp();
    setsid();

    // 7. In the child, call fork() again, to ensure that the daemon can never re-acquire a terminal again.
    // 8. Call exit() in the first child, so that only the second child (the actual daemon process) stays
    //    around. This ensures that the daemon process is re-parented to init/PID 1, as all daemons should be.
    if (pid_t pid = fork()) {
        if (pid > 0) {
            exit(0);
        } else {
            syslog(LOG_ERR | LOG_USER, "Second fork failed: %m");
            std::ostringstream sstr;
            sstr << "Second fork failed (" << strerror(errno) << ")!";
            throw std::string(sstr.str());
        }
    }

    // 9. In the daemon process, connect /dev/null to standard input, output, and error.
    close(0);   close(1);   close(2);
    if (open("/dev/null", O_RDONLY) < 0) {
        syslog(LOG_ERR | LOG_USER, "Unable to open /dev/null: %m");
        std::ostringstream sstr;
        sstr << "Unable to open /dev/null (" << strerror(errno) << ")!";
        throw std::string(sstr.str());
    }
    if (open("/dev/null", O_WRONLY) < 0) {
        syslog(LOG_ERR | LOG_USER, "Unable to open /dev/null: %m");
        std::ostringstream sstr;
        sstr << "Unable to open /dev/null (" << strerror(errno) << ")!";
        throw std::string(sstr.str());
    }
    if (open("/dev/null", O_WRONLY) < 0) {
        syslog(LOG_ERR | LOG_USER, "Unable to open /dev/null: %m");
        std::ostringstream sstr;
        sstr << "Unable to open /dev/null (" << strerror(errno) << ")!";
        throw std::string(sstr.str());
    }

    // 10. In the daemon process, reset the umask to 0, so that the file modes passed to open(), mkdir() and
    //     suchlike directly control the access mode of the created files and directories.
    umask(0);

    // 11. In the daemon process, change the current directory to the root directory (/), in order to avoid that
    //     the daemon involuntarily blocks mount points from being unmounted.
    if (! chdir("/")) {
        // ignore a failure to the call
    }

    return;
} // DaemonizeOnRequest()


} // namespace buoy
} // namespace mbp
