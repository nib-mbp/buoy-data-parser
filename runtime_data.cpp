#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
#include <stdexcept>

#include <boost/cstdint.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "buoy_utils.hpp"
#include "runtime_data.hpp"

mbp::RuntimeData::RuntimeData()
    :
        // -- buoy interior --
        m_bInteriorPTime(boost::posix_time::from_time_t(0)),
        m_bInteriorTemperature(-999.0),
        m_bInteriorHumidity(-999),

        // -- buoy board --
        m_bBoardPTime(boost::posix_time::from_time_t(0)),
        m_bBoardCurrent(-999.0),
        m_bBoardTemperature(-999.0),
        m_bBoardHumidity(-999.0),

        // -- buoy co2 --
        m_bCo2PTime(boost::posix_time::from_time_t(0)),
        m_bCo2Concentration(-999),

        // -- wind --
        m_windPTime(boost::posix_time::from_time_t(0)),
        m_windU(-999.0), m_windV(-999.0), m_windW(-999.0), m_windVSpeed(-999.0), m_windVDirection(-999.0),

        // -- air --
        m_airPTime(boost::posix_time::from_time_t(0)),
        m_airTemperature(-999.0), m_airHumidity(-999.0),

        // -- compass --
        m_compassPTime(boost::posix_time::from_time_t(0)),
        m_compassRoll(-999.0), m_compassPitch(-999.0), m_compassYaw(-999.0),

        // -- sea_water --
        m_seawaterPTime(boost::posix_time::from_time_t(0)),
        m_seawaterTemperature(-999.0), m_seawaterConductivity(-999.0), m_seawaterSalinity(-999.0), m_seawaterChlorophyll(-999.0),

        // -- oxygen --
        // will be initialized in the function body

        // -- par --
        m_parPTime(boost::posix_time::from_time_t(0)),
        m_parValueRaw(-999),

        // -- awac --
        m_awacPTime(boost::posix_time::from_time_t(0)),
        m_awacTemperature(-999.0), m_awacPressure(-999.0),
        m_awacWavesHeight(-999.0), m_awacWavesPeakPeriod(-999.0), m_awacWavesDirection(-999.0),
        m_awacIsCurrentPresent(-1),

        // -- batteries currents and battery packs voltage --
        m_batPTime(boost::posix_time::from_time_t(0)),
        // will be initialized in the function body

        // -- common system voltage --
        m_cmnVoltagePTime(boost::posix_time::from_time_t(0)),
        m_cmnVoltage(-999.0),
        m_cmnVoltageStatus(0),

        // -- ethanol power cells --
        m_fuelCellPTime(boost::posix_time::from_time_t(0)),
        m_fuelCellStatus(0),
        m_fuelCellCurrent(-999.0),

        // -- 3 current loops --
        // will be initialized in the function body

        // -- solar cells (MPPT) --
        m_mpptPTime(boost::posix_time::from_time_t(0)),
        m_mpptInI(-999.0),
        m_mpptOutI(-999.0),
        m_mpptInU(-999.0),
        m_mpptOutU(-999.0),

        m_alarms()
{
    for (size_t idx=0; idx<NUM_of_BATTERIES; ++idx) {
        m_batCurrMin[idx] = -999.0;
        m_batCurrMax[idx] = -999.0;
        m_batCurrAvg[idx] = -999.0;
    }
    for (size_t idx=0; idx<NUM_of_BATTERY_PACKS; ++idx) {
        m_batPackVoltageMin[idx] = -999.0;
        m_batPackVoltageMax[idx] = -999.0;
        m_batPackVoltageAvg[idx] = -999.0;
    }

    for (size_t idx=0; idx<3; ++idx) {
        m_oxygenPTime[idx] = boost::posix_time::from_time_t(0);
        m_oxygenConcentration[idx] = -999.0;
    }

    for (size_t idx=0; idx<RUNTIME_NUM_CURRENT_LOOPS; ++idx) {
        m_currentLoopPTime[idx] = boost::posix_time::from_time_t(0);
        m_currentLoopCurrent[idx] = -999.0;
    }
} // mbp::RuntimeData::RuntimeData()


void mbp::RuntimeData::Init()
    throw(std::invalid_argument)
{
    m_alarms.Init();

    for (int idx=0; idx<BUOY_num_of_ALL_INSTRUMENTS; ++idx) {
        m_instrumentsError[idx] = -1;
        m_instrumentsStatus[idx] = -1;
        m_instrumentCurrentPtime[idx] = boost::posix_time::from_time_t(0);
        m_instrumentCurrentMin[idx] = -999.0;
        m_instrumentCurrentMax[idx] = -999.0;
        m_instrumentCurrentAvg[idx] = -999.0;
    }

    return;
} // mbp::RuntimeData::Init()


void mbp::RuntimeData::setBInterior(boost::posix_time::ptime recPTime, double temperature, uint16_t humidity)
{
    m_bInteriorPTime = recPTime;

    m_bInteriorTemperature = temperature;
    m_bInteriorHumidity = humidity;

    return;
} // RuntimeData::setBInterior()


void mbp::RuntimeData::setBBoard(boost::posix_time::ptime recPTime, double current, double temperature, double humidity) {
    m_bBoardPTime = recPTime;
    m_bBoardCurrent = current;
    m_bBoardTemperature = temperature;
    m_bBoardHumidity = humidity;
    return;
}


void mbp::RuntimeData::setWind(boost::posix_time::ptime recPTime, double u, double v, double w, int error)
{
    m_windPTime = recPTime;

    m_windU = u;
    m_windV = v;
    m_windW = w;

    m_windVSpeed = std::sqrt(
              std::pow(m_windU, 2)
            + std::pow(m_windV, 2)
            + std::pow(m_windW, 2)
            );

    m_windVDirection = std::atan2(m_windU, m_windV);
    m_windVDirection *= 180 / M_PI;
    m_windVDirection += 90;
    if (m_windVDirection > 360) {
        m_windVDirection -= 360;
    } else if (m_windVDirection < 0) {
        m_windVDirection += 360;
    }

    m_instrumentsError[mbp::buoy::INSTRUMENT_WIND_e] = error;

    return;
} // RuntimeData::setWind()


void mbp::RuntimeData::setAir(boost::posix_time::ptime recPTime, double temperature, double humidity, int error)
{
    m_airPTime = recPTime;

    m_airTemperature = temperature;
    m_airHumidity = humidity;

    m_instrumentsError[mbp::buoy::INSTRUMENT_HUMIDITY_e] = error;

    return;
} // RuntimeData::setAir()


void mbp::RuntimeData::setCompass(boost::posix_time::ptime recPTime, double roll, double pitch, double yaw, int error)
{
    m_compassPTime = recPTime;
    m_compassRoll = roll;
    m_compassPitch = pitch;
    m_compassYaw = yaw;

    m_instrumentsError[mbp::buoy::INSTRUMENT_COMPASS_e] = error;

    return;
} // RuntimeData::setCompass()


void mbp::RuntimeData::setSeaWater(boost::posix_time::ptime recPTime, double temperature, double conductivity, double salinity, double chlorophyll, int error)
{
    m_seawaterPTime = recPTime;

    m_seawaterTemperature = temperature;
    m_seawaterConductivity = conductivity;
    m_seawaterSalinity = salinity;
    m_seawaterChlorophyll = chlorophyll;

    m_instrumentsError[mbp::buoy::INSTRUMENT_SALINITY_e] = error;

    return;
} // RuntimeData::setSeaWater()


void mbp::RuntimeData::setAwac(boost::posix_time::ptime recPTime, double temperature, double pressure)
{
    m_awacPTime = recPTime;
    m_awacTemperature = temperature;
    m_awacPressure = pressure;

    boost::posix_time::ptime now = boost::posix_time::second_clock::universal_time();
    boost::posix_time::time_duration timeDuration = now - recPTime;
    if (timeDuration.total_seconds() < 3600) {
        // if last data is younger than 1 hour, there is communication
        m_instrumentsError[mbp::buoy::INSTRUMENT_AWAC_e] = 0;
    } else {
        m_instrumentsError[mbp::buoy::INSTRUMENT_AWAC_e] = 1;
    }
} // RuntimeData::setAwac()


void mbp::RuntimeData::setOxygen(int devID, boost::posix_time::ptime recPTime, double concentration, int error)
{
    m_oxygenPTime[devID] = recPTime;

    m_oxygenConcentration[devID] = concentration;

    switch (devID) {
        case 0:
            m_instrumentsError[mbp::buoy::INSTRUMENT_OXYGEN1_e] = error;
            break;
        case 1:
            m_instrumentsError[mbp::buoy::INSTRUMENT_OXYGEN2_e] = error;
            break;
        case 2:
            m_instrumentsError[mbp::buoy::INSTRUMENT_OXYGEN3_e] = error;
            break;
    }

    return;
} // RuntimeData::setOxygen()


void mbp::RuntimeData::setPar(boost::posix_time::ptime recPTime, int valueRaw, int error)
{
    m_parPTime = recPTime;

    m_parValueRaw = valueRaw;

    m_instrumentsError[mbp::buoy::INSTRUMENT_PAR_e] = error;

    return;
} // RuntimeData::setPar()


void mbp::RuntimeData::setBuoyCO2(boost::posix_time::ptime recPTime, double concentration, int error)
{
    m_bCo2PTime = recPTime;

    m_bCo2Concentration = concentration;

    m_instrumentsError[mbp::buoy::INSTRUMENT_BUOY_CO2_e] = error;

    return;
} // RuntimeData::setBuoyCO2()


void mbp::RuntimeData::setBatteries(boost::posix_time::ptime recPTime,
                  double currBatMin1, double currBatMin2, double currBatMin3, double currBatMin4,
                  double currBatMax1, double currBatMax2, double currBatMax3, double currBatMax4,
                  double currBatAvg1, double currBatAvg2, double currBatAvg3, double currBatAvg4,
                  double voltagePackMin1, double voltagePackMin2,
                  double voltagePackMax1, double voltagePackMax2,
                  double voltagePackAvg1, double voltagePackAvg2)
{
    m_batPTime = recPTime;
    size_t idx = 0;
    m_batCurrMin[idx] = currBatMin1;
    m_batCurrMax[idx] = currBatMax1;
    m_batCurrAvg[idx] = currBatAvg1;
    idx = 1;
    m_batCurrMin[idx] = currBatMin2;
    m_batCurrMax[idx] = currBatMax2;
    m_batCurrAvg[idx] = currBatAvg2;
    idx = 2;
    m_batCurrMin[idx] = currBatMin3;
    m_batCurrMax[idx] = currBatMax3;
    m_batCurrAvg[idx] = currBatAvg3;
    idx = 3;
    m_batCurrMin[idx] = currBatMin4;
    m_batCurrMax[idx] = currBatMax4;
    m_batCurrAvg[idx] = currBatAvg4;

    idx = 0;
    m_batPackVoltageMin[idx] = voltagePackMin1;
    m_batPackVoltageMax[idx] = voltagePackMax1;
    m_batPackVoltageAvg[idx] = voltagePackAvg1;
    idx = 1;
    m_batPackVoltageMin[idx] = voltagePackMin2;
    m_batPackVoltageMax[idx] = voltagePackMax2;
    m_batPackVoltageAvg[idx] = voltagePackAvg2;
    return;
} // mbp::RuntimeData::setBatteries()


void mbp::RuntimeData::setCmnVoltage(boost::posix_time::ptime recPTime, double voltage)
{
    m_cmnVoltagePTime = recPTime;

    m_cmnVoltage = voltage;

    return;
} // RuntimeData::setCmnVoltage()


void mbp::RuntimeData::setFuelCell(boost::posix_time::ptime recPTime, double current)
{
    m_fuelCellPTime = recPTime;
    m_fuelCellCurrent = current;
    if (fabs(current) > 0.1) {
        m_fuelCellStatus = 1;
        m_cmnVoltageStatus = 2;        // battery is charging
    } else {
        m_fuelCellStatus = 0;
        m_cmnVoltageStatus = 1;        // battery is being used - it is getting discharged
    }
} // RuntimeData::setFuelCell()


void mbp::RuntimeData::setCurrentLoop(int currentLoopId, boost::posix_time::ptime recPTime, double current)
    throw(std::invalid_argument)
{
    if (currentLoopId >= RUNTIME_NUM_CURRENT_LOOPS) {
        std::ostringstream sstr;
        sstr.str("");
        sstr << "Current loop out of defined range";
        throw std::invalid_argument(sstr.str().c_str());
    }

    m_currentLoopPTime[currentLoopId] = recPTime;
    m_currentLoopCurrent[currentLoopId] = current;
} // RuntimeData::setCurrentLoop()


void mbp::RuntimeData::setMppt(boost::posix_time::ptime recPTime, double inI, double outI, double inU, double outU)
{
    m_mpptPTime = recPTime;
    m_mpptInI = inI;
    m_mpptOutI = outI;
    m_mpptInU = inU;
    m_mpptOutU = outU;
} // mbp::RuntimeData::setMppt()


void mbp::RuntimeData::_getInstrumentsJson(std::ostringstream& sstr) const
{
    mbp::buoy::buoy_sensor_e instrumentIdx = mbp::buoy::_SENSOR__SENTINEL_e;
    sstr << "{ ";
    // -- buoy interior --
    sstr
        << "\"bInterior\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_bInteriorPTime) << "\","
        << " \"temp\": " << m_bInteriorTemperature << ","
        << " \"humid\": " << m_bInteriorHumidity
        << "}, ";

    // -- buoy board --
    sstr
    << "\"bBoard\": {"
    << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_bBoardPTime) << "\","
    << " \"temp\": " << m_bBoardTemperature << ","
    << " \"humid\": " << m_bBoardHumidity << ","
    << " \"curr\": " << m_bBoardCurrent
    << "}, ";

    // -- buoy_co2 --
    instrumentIdx = mbp::buoy::INSTRUMENT_BUOY_CO2_e;
    sstr
    << "\"bCo2\": {"
    << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_bCo2PTime) << "\","
    << " \"concentration\": " << m_bCo2Concentration
    << "}, ";

    // -- wind --
    instrumentIdx = mbp::buoy::INSTRUMENT_WIND_e;
    sstr
        << "\"wind\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_windPTime) << "\","
        << " \"u\": " << m_windU << ", \"v\": " << m_windV << ", \"w\": " << m_windW << ","
        << " \"vspd\": " << m_windVSpeed << ", \"vdir\": " << m_windVDirection << ","
        << " \"err\": " << m_instrumentsError[instrumentIdx] << ","
        << " \"status\": " << m_instrumentsStatus[instrumentIdx] << ","
        << " \"currDt\": \"" << mbp::buoy::PtimeToStr(m_instrumentCurrentPtime[instrumentIdx]) << "\","
        << " \"currMin\": " << m_instrumentCurrentMin[instrumentIdx] << ","
        << " \"currMax\": " << m_instrumentCurrentMax[instrumentIdx] << ","
        << " \"currAvg\": " << m_instrumentCurrentAvg[instrumentIdx]
        << "}, ";

    // -- air --
    instrumentIdx = mbp::buoy::INSTRUMENT_HUMIDITY_e;
    sstr
        << "\"air\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_airPTime) << "\","
        << " \"temp\": " << m_airTemperature << ", \"humid\": " << m_airHumidity << ","
        << " \"err\": " << m_instrumentsError[instrumentIdx] << ","
        << " \"status\": " << m_instrumentsStatus[instrumentIdx] << ","
        << " \"currDt\": \"" << mbp::buoy::PtimeToStr(m_instrumentCurrentPtime[instrumentIdx]) << "\","
        << " \"currMin\": " << m_instrumentCurrentMin[instrumentIdx] << ","
        << " \"currMax\": " << m_instrumentCurrentMax[instrumentIdx] << ","
        << " \"currAvg\": " << m_instrumentCurrentAvg[instrumentIdx]
        << "}, ";

    // -- compass --
    instrumentIdx = mbp::buoy::INSTRUMENT_COMPASS_e;
    sstr
        << "\"compass\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_compassPTime) << "\","
        << " \"roll\": " << m_compassRoll << ","
        << " \"pitch\": " << m_compassPitch << ","
        << " \"heading\": " << m_compassYaw << ","
        << " \"err\": " << m_instrumentsError[instrumentIdx] << ","
        << " \"status\": " << m_instrumentsStatus[instrumentIdx] << ","
        << " \"currDt\": \"" << mbp::buoy::PtimeToStr(m_instrumentCurrentPtime[instrumentIdx]) << "\","
        << " \"currMin\": " << m_instrumentCurrentMin[instrumentIdx] << ","
        << " \"currMax\": " << m_instrumentCurrentMax[instrumentIdx] << ","
        << " \"currAvg\": " << m_instrumentCurrentAvg[instrumentIdx]
        << "}, ";

    // -- sea_water --
    instrumentIdx = mbp::buoy::INSTRUMENT_SALINITY_e;
    sstr
        << "\"seaWater\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_seawaterPTime) << "\","
        << " \"temp\": " << m_seawaterTemperature << ","
        << " \"cond\": " << m_seawaterConductivity << ","
        << " \"salinity\": " << m_seawaterSalinity << ","
        << " \"chla\": " << m_seawaterChlorophyll << ","
        << " \"err\": " << m_instrumentsError[instrumentIdx] << ","
        << " \"status\": " << m_instrumentsStatus[instrumentIdx] << ","
        << " \"currDt\": \"" << mbp::buoy::PtimeToStr(m_instrumentCurrentPtime[instrumentIdx]) << "\","
        << " \"currMin\": " << m_instrumentCurrentMin[instrumentIdx] << ","
        << " \"currMax\": " << m_instrumentCurrentMax[instrumentIdx] << ","
        << " \"currAvg\": " << m_instrumentCurrentAvg[instrumentIdx]
        << "}, ";

    // -- awac --
    instrumentIdx = mbp::buoy::INSTRUMENT_AWAC_e;
    sstr
        << "\"awac\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_awacPTime) << "\","
        << " \"temp\": " << m_awacTemperature << ",\"pressure\": " << m_awacPressure << ","
/*
        << " \"waves\":"
        << " ["
        << " {\"height\": " << m_awacWavesHeight << ", \"peak_period\": " << m_awacWavesPeakPeriod << ","
        << " \"meanDir\": " << m_awacWavesDirection << "}"
        << " ],"
        << " \"isDataPresent\": " << m_awacIsCurrentPresent << ","
*/
        << " \"err\": " << m_instrumentsError[instrumentIdx] << ","
        << " \"status\": " << m_instrumentsStatus[instrumentIdx] << ","
        << " \"currDt\": \"" << mbp::buoy::PtimeToStr(m_instrumentCurrentPtime[instrumentIdx]) << "\","
        << " \"currMin\": " << m_instrumentCurrentMin[instrumentIdx] << ","
        << " \"currMax\": " << m_instrumentCurrentMax[instrumentIdx] << ","
        << " \"currAvg\": " << m_instrumentCurrentAvg[instrumentIdx]
        << "}, ";

    // -- oxygen --
    int o2InstrumentIdx = 1;
    switch (o2InstrumentIdx) {
        case 0:
            instrumentIdx = mbp::buoy::INSTRUMENT_OXYGEN1_e;
            break;
        case 1:
            instrumentIdx = mbp::buoy::INSTRUMENT_OXYGEN2_e;
            break;
        case 2:
            instrumentIdx = mbp::buoy::INSTRUMENT_OXYGEN3_e;
            break;
    }
    sstr
        << "\"o2\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_oxygenPTime[o2InstrumentIdx]) << "\","
        << " \"conc\": " << m_oxygenConcentration[o2InstrumentIdx] << ","
        << " \"err\": " << m_instrumentsError[instrumentIdx] << ","
        << " \"status\": " << m_instrumentsStatus[instrumentIdx] << ","
        << " \"currDt\": \"" << mbp::buoy::PtimeToStr(m_instrumentCurrentPtime[instrumentIdx]) << "\","
        << " \"currMin\": " << m_instrumentCurrentMin[instrumentIdx] << ","
        << " \"currMax\": " << m_instrumentCurrentMax[instrumentIdx] << ","
        << " \"currAvg\": " << m_instrumentCurrentAvg[instrumentIdx]
        << "}, ";

    // -- par --
    instrumentIdx = mbp::buoy::INSTRUMENT_PAR_e;
    sstr
        << "\"par\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_parPTime) << "\","
        << " \"value_raw\": " << m_parValueRaw << ","
        << " \"err\": " << m_instrumentsError[instrumentIdx] << ","
        << " \"status\": " << m_instrumentsStatus[instrumentIdx] << ","
        << " \"currDt\": \"" << mbp::buoy::PtimeToStr(m_instrumentCurrentPtime[instrumentIdx]) << "\","
        << " \"currMin\": " << m_instrumentCurrentMin[instrumentIdx] << ","
        << " \"currMax\": " << m_instrumentCurrentMax[instrumentIdx] << ","
        << " \"currAvg\": " << m_instrumentCurrentAvg[instrumentIdx]
        << "}";

    // -- end --
    sstr << "}";

    return;
} // RuntimeData::getInstrumentsJson()


void mbp::RuntimeData::_getPowerJson(std::ostringstream& sstr) const
{
    sstr << "{";
    // -- common voltage (cmnVoltage) --
    sstr
        << " \"cmnVoltage\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_cmnVoltagePTime) << "\","
        << " \"voltage\": " << m_cmnVoltage
        << "},";

    // -- batteries --
    sstr
        << " \"bat\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_batPTime) << "\","
        << " \"volMin\": [" << m_batPackVoltageMin[0] << ", " << m_batPackVoltageMin[1] << "],"
        << " \"volMax\": [" << m_batPackVoltageMax[0] << ", " << m_batPackVoltageMax[1] << "],"
        << " \"volAvg\": [" << m_batPackVoltageAvg[0] << ", " << m_batPackVoltageAvg[1] << "],"
        << " \"currMin\": [" << m_batCurrMin[0] << ", " << m_batCurrMin[1] << ", " << m_batCurrMin[2] << ", " << m_batCurrMin[3] << "],"
        << " \"currMax\": [" << m_batCurrMax[0] << ", " << m_batCurrMax[1] << ", " << m_batCurrMax[2] << ", " << m_batCurrMax[3] << "],"
        << " \"currAvg\": [" << m_batCurrAvg[0] << ", " << m_batCurrAvg[1] << ", " << m_batCurrAvg[2] << ", " << m_batCurrAvg[3] << "]"
        << "},";

    // -- ethanol power cells --
    sstr
        << " \"fuelCell\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_fuelCellPTime) << "\","
        << " \"status\": " << m_fuelCellStatus << ","
        << " \"current\": " << m_fuelCellCurrent
        << "},";

    // -- current loops --
    sstr
        << " \"currentLoops\": [";
    sstr
        << "{"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_currentLoopPTime[0]) << "\","
        << " \"current\": " << m_currentLoopCurrent[0]
        << "}";
    for (int i=1; i<RUNTIME_NUM_CURRENT_LOOPS; ++i) {
        sstr
            << ", {"
            << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_currentLoopPTime[i]) << "\","
            << " \"current\": " << m_currentLoopCurrent[i]
            << "}";
    }
    sstr << "],";

    // -- MPPT --
    sstr
        << " \"mppt\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_currentLoopPTime[0]) << "\","
        << " \"inI\": " << m_mpptInI << ","
        << " \"inU\": " << m_mpptInU << ","
        << " \"outI\": " << m_mpptOutI << ","
        << " \"outU\": " << m_mpptOutU
        << "}";

    // -- end --
    sstr << " }";

    return;
} // RuntimeData::getPowerJson()



void mbp::RuntimeData::_getStatusJson(std::ostringstream& sstr) const
{
    m_alarms.getStatusJson(sstr);
    return;
} // mbp::RuntimeData::getPowerJson()


std::string mbp::RuntimeData::getJson() const
{
    std::ostringstream sstr;
    sstr.str("");

    sstr << "{";
    sstr << "\"inst\": ";
    _getInstrumentsJson(sstr);
    sstr << ", \"power\":";
    _getPowerJson(sstr);
    sstr << ", \"status\":";
    _getStatusJson(sstr);
    sstr << "}";


    return sstr.str();
} // mbp::RuntimeData::getJson()


void mbp::RuntimeData::setInstrumentCurrent(
        uint16_t a_board,
        uint16_t a_device,
        boost::posix_time::ptime a_recPTime,
        double a_min,
        double a_max,
        double a_avg)
{
    mbp::buoy::buoy_sensor_e idx = mbp::buoy::findInstrument(a_board, a_device);
    m_instrumentCurrentPtime[idx] = a_recPTime;
    m_instrumentCurrentMin[idx] = a_min;
    m_instrumentCurrentMax[idx] = a_max;
    m_instrumentCurrentAvg[idx] = a_avg;
    return;
} // mbp::RuntimeData::setInstrumentCurrent



// Returned JSON statuses
//  0  -> OK    / Normal operation
//  1  -> FAIL  / STOPPED / Service mode
//  2  -> Initializing
//  3  -> Old data
//         + data older than OLD_INSTRUMENT_DATA_ALARM_TRIGGER_AGE seconds
//         + triggered only in normal operation (where otherwise the status
//           would be 0 = OK)
//  4  -> N/A   / no info received so far
void mbp::RuntimeData::setIoAlarm(
        boost::posix_time::ptime recPTime,
        uint16_t board,
        uint16_t device,
        uint16_t mode,
        uint16_t status,
        uint16_t code)
{
    mbp::buoy::buoy_sensor_e instrumentIdx = mbp::buoy::_SENSOR__SENTINEL_e;
    instrumentIdx = m_alarms.setIoAlarm(recPTime, board, device, mode, status, code);
    if (instrumentIdx == mbp::buoy::_SENSOR__SENTINEL_e)
        return;

    mbp::buoy::alarm_status_e inStatus = m_alarms.getSensorStatus(instrumentIdx);
    int outStatus = 4;
    if ( (inStatus == mbp::buoy::ALARM_STATUS_OK)  ||  (inStatus == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL) )
        outStatus = 0;
    else if ( (inStatus == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE)  ||  (inStatus == mbp::buoy::IO_ALARM_INSTRUMENT_INITIALIZING) )
        outStatus = 2;
    else if ( (inStatus == mbp::buoy::ALARM_STATUS_FAIL)  ||  (inStatus == mbp::buoy::IO_ALARM_INSTRUMENT_STOPPED) )
        outStatus = 1;

    m_instrumentsStatus[instrumentIdx] = outStatus;

    return;
} // mbp::RuntimeData::setIoAlarm()


void mbp::RuntimeData::setAlarm(
        boost::posix_time::ptime recPTime,
        uint16_t code)
{
    m_alarms.setAlarm(recPTime, code);
    return;
} // mbp::RuntimeData::setAlarm()


std::ostream& mbp::operator<<(std::ostream& a_os, const mbp::RuntimeData& a_runtimeData)
{
    a_os
        << "JSON [" << a_runtimeData.getJson() << "]" << std::endl;
    return a_os;
} // operator<<()
