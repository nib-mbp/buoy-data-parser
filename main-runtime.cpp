#include <iostream>
#include <sstream>
//#include <string>
//#include <cstring>

#include <syslog.h>

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include "buoy_log.hpp"
#include "buoy_utils.hpp"
#include "tcp_server.hpp"
#include "runtime_processor.hpp"


// -- Global variables --
static bool g_doLoop = true;    // global as it is updated by signal handler


// -- Signal action handler (handling -HUP and -TERM
static void signalHdl (int sig, siginfo_t *siginfo, void *context)  {
    std::ostringstream sstr;
    sstr << "Got signal ("
         << "signal=" << sig << " / "
         << "sender PID=" << (long)siginfo->si_pid
         << " / UID=" << (long)siginfo->si_uid << ")";
    L_(warning) << sstr.str();
    if (sig == SIGTERM) {
        sstr.str("");
        sstr << "SCADA runtime stopping (got SIGTERM) @ "
             << mbp::buoy::PtimeToStr(boost::posix_time::second_clock::universal_time());
        L_(warning) << sstr.str();
        syslog(LOG_NOTICE | LOG_USER, "%s", sstr.str().c_str());
        g_doLoop = false;
    }
    return;
} // signalHdl()


// -- Main program --
int main(int argc, char* argv[]) {
    BOOST_LOG_FUNCTION();
    struct sigaction signalAct;

    if (!mbp::buoy::g_options.Parse(argc, argv, mbp::buoy::CONFIG_RUNTIME_e)) {
        return 1;
    }

    if ( mbp::buoy::g_options.getDoDaemonize() == true ) {
        // x. Check if a daemon is already running

        // 1. Close all open file descriptors except standard input, output, and error (i.e. the
        //    first three file descriptors 0, 1, 2), etc. -> just log is open

        // 2. Reset all signal handlers to their default. This is best done by iterating through the
        //    available signals up to the limit of _NSIG and resetting them to SIG_DFL.
        L_(trace) << "Resetting all signals to their defaults ...";
        for (int i=0; i < _NSIG; ++i) {
            signal(i, SIG_DFL);
        }

        // x. Register signal handling for SIGTERM and SIGHUP
        L_(trace) << "Set signal handling for SIGTERM and SIGHUP ...";
        memset(&signalAct, '\0', sizeof(signalAct));
        signalAct.sa_sigaction = &signalHdl;
        signalAct.sa_flags = SA_SIGINFO;
        if (
                (sigaction(SIGTERM, &signalAct, NULL) < 0)
                || (sigaction(SIGHUP, &signalAct, NULL) < 0)
                ) {
            L_(fatal) << "Error occured when registering sigaction (" << std::strerror(errno) << ")!";
            exit(1);
        }

        // 3. Reset the signal mask using sigprocmask().
        sigset_t mask;
        sigemptyset(&mask);
        sigprocmask(SIG_SETMASK, &mask, NULL);

        mbp::buoy::DaemonizeOnRequest(mbp::buoy::g_options.getDoDaemonize());
    }

    // -- log startup of the reader --
    std::ostringstream sstr;
    sstr.str("");
    sstr << "SCADA runtime started @ "
         << mbp::buoy::PtimeToStr(boost::posix_time::second_clock::universal_time())
         << " (daemon = " << mbp::buoy::g_options.getDoDaemonize() << ")";
    L_(warning) << sstr.str();
    syslog(LOG_NOTICE | LOG_USER, "%s", sstr.str().c_str());

    try {
/*
        if (argc != 2) {
            std::cerr << "Usage: " << argv[0] << " <port>" << std::endl;
            return 1;
        }
*/
        // -- TCP command server --
        //mbp::buoy::TcpServer tcpServer(std::atoi(argv[1]));
        //boost::thread tcpServerThread(boost::bind(&mbp::buoy::TcpServer::run, &tcpServer));

        // -- Runtime processor --
        mbp::buoy::RuntimeProcessor runtime;
        boost::thread runtimeThread(boost::bind(&mbp::buoy::RuntimeProcessor::run, &runtime, &g_doLoop));


        //tcpServerThread.join();
        runtimeThread.join();
        std::cout << "joined ..." << std::endl;
    }
    catch (std::exception& e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
} // main()
