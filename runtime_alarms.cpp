#include <iostream>
#include <string>
#include <sstream>

#include "buoy_utils.hpp"
#include "runtime_alarms.hpp"
#include "runtime_datatypes.hpp"


namespace mbp {
namespace buoy {

/*
struct _db_tables_s g_buoyDbTables[] = {
    {TBL_NA_e                   , "N/A - UNKNOWN"},
    {TBL_AIR_e                  , "air"},
    {TBL_AIR_CO2_e              , "air_co2"}
}; // g_buoyDbTables
*/

} // namespace buoy
} // namespace mbp


mbp::buoy::RuntimeAlarms::RuntimeAlarms()
{
    for (size_t i=0; i<_SENSOR__SENTINEL_e; ++i) {
        m_pTime[i] = boost::posix_time::from_time_t(0);
        m_status[i] = mbp::buoy::ALARM_STATUS_NA;
        m_code[i] = mbp::buoy::ALARM_STATUS_OK;
    }
} // mbp::buoy::RuntimeAlarms()


void mbp::buoy::RuntimeAlarms::Init()
    throw(std::invalid_argument)
{
    if (   (BUOY_num_of_ALL_INSTRUMENTS != mbp::buoy::SENSOR_BUOY_DOOR_e)
        || (BUOY_num_of_ALL_INSTRUMENTS != (mbp::buoy::INSTRUMENT_BUOY_CO2_e + 1))
       )
    {
        std::ostringstream sstr;
        sstr.str("");
        sstr << "Invalid instruments order in 'buoy_sensor_e' - check comment";
        throw std::invalid_argument(sstr.str().c_str());
    }
} // mbp::buoy::RuntimeAlarms::Init()



/*
 * Update the internal instrument alarm status variable with the given value if
 * and only if the received alarm is newer than the existing one.
 *
 * Return value:
 *  The instrument index from mbp::buoy::buoy_sensor_e is returned, identifying
 *  the instrument for which the alarm was updated.
 *  In case the alarm is not updated (the timestamp of the alarm is <= of the
 *  existing one), the mbp::buoy::_SENSOR__SENTINEL_e is returned.
 *  ^^^
 *  ATTENTION: the returned value may be big and way out of range of possible
 *             used array variables.
 */
mbp::buoy::buoy_sensor_e mbp::buoy::RuntimeAlarms::setIoAlarm(
        const boost::posix_time::ptime& a_recPTime,
        uint16_t a_board,
        uint16_t a_device,
        uint16_t a_mode,
        uint16_t a_state,
        uint16_t a_code)
    throw(std::invalid_argument)
{
/*
    if (
           ( (a_board<1) || (a_board>5))
        || ( (a_device<0) || (a_device>1))
       )
    {
        std::ostringstream sstr;
        sstr.str("");
        sstr << "Invalid instrument specified [board=" << a_board << " / device=" << a_device << "]";
        throw std::invalid_argument(sstr.str().c_str());
    }
*/

    // find the instrument index
    mbp::buoy::buoy_sensor_e instrumentIdx = mbp::buoy::findInstrument(a_board, a_device);
//std::cout << "Got IO ALARM (dt=" << mbp::buoy::PtimeToStr(a_recPTime) << " / instrument=" << instrumentIdx << " / board:device=" << a_board << ":" << a_device << " / mode:state=" << a_mode << ":" << a_state << " / code=" << a_code << ")." << std::endl;

/*
    mbp::buoy::buoy_sensor_e instrumentIdx = mbp::buoy::_SENSOR__SENTINEL_e;
    switch (a_board) {
        case 1:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_WIND_e : mbp::buoy::INSTRUMENT_HUMIDITY_e;
            break;
        case 2:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_COMPASS_e : mbp::buoy::INSTRUMENT_SALINITY_e;
            break;
        case 3:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_AWAC_e : mbp::buoy::INSTRUMENT_OXYGEN1_e;
            break;
        case 4:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_PAR_e : mbp::buoy::INSTRUMENT_OXYGEN2_e;
            break;
        case 5:
            instrumentIdx = a_device == 0 ? mbp::buoy::INSTRUMENT_BUOY_CO2_e : mbp::buoy::INSTRUMENT_OXYGEN3_e;
            break;
    } // switch (board)
*/

    // -- check if the received record is newer than the existing one;
    //    if not, just return - no update needed --
    if (a_recPTime <= m_pTime[instrumentIdx]) {
        return mbp::buoy::_SENSOR__SENTINEL_e;
    }

    //  mode: 0 – ustavljen, 1 – deluje
    //  state:  0 – inicializacija še ni izvedena,
    //          1 – postopek inicializacije zaključen (to ne pomeni, da instrument dela in pošilja podatke, ampak samo da
    //              se je zaključila rutina za inicializacijo
    //
    // Returned JSON statuses
    //  0  -> OK    / Normal operation
    //  1  -> FAIL  / STOPPED / Service mode
    //  2  -> Initializing
    //  3  -> N/A   / no info received so far
    mbp::buoy::alarm_status_e status = mbp::buoy::ALARM_STATUS_NA;
    if (a_mode == 0) {
        status = mbp::buoy::IO_ALARM_INSTRUMENT_STOPPED;
    } else {
        if (a_state == 0) {
            status = mbp::buoy::IO_ALARM_INSTRUMENT_INITIALIZING;
        } else {
            status = mbp::buoy::ALARM_STATUS_OK;
        }
    }

    m_pTime[instrumentIdx] = a_recPTime;
    m_status[instrumentIdx] = status;
    m_code[instrumentIdx] = a_code;

    return instrumentIdx;
} // mbp::buoy::RuntimeAlarms::setIoAlarm()



void mbp::buoy::RuntimeAlarms::setAlarm(
        boost::posix_time::ptime recPTime,
        uint16_t code)
{
//std::cout << "Got ALARM (dt=" << mbp::buoy::PtimeToStr(recPTime) << " / alarm.ID=" << code << ")." << std::endl;
    switch (code) {
        case 1:
            // 1 // loputa boje v normalnem obratovanju
            if (recPTime <= m_pTime[SENSOR_BUOY_DOOR_e]) break;
            m_pTime[SENSOR_BUOY_DOOR_e] = recPTime;
            m_status[SENSOR_BUOY_DOOR_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;
        case 2:
            // 2 // loputa boje v servisnem načinu
            if (recPTime <= m_pTime[SENSOR_BUOY_DOOR_e]) break;
            m_pTime[SENSOR_BUOY_DOOR_e] = recPTime;
            m_status[SENSOR_BUOY_DOOR_e] = mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE;
            break;
        case 3:
            // 3 // loputa zaprta
            if (recPTime <= m_pTime[SENSOR_BUOY_DOOR_e]) break;
            m_pTime[SENSOR_BUOY_DOOR_e] = recPTime;
            m_status[SENSOR_BUOY_DOOR_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 4:
            // 4 // loputa odprta (alarm!)
            if (recPTime <= m_pTime[SENSOR_BUOY_DOOR_e]) break;
            m_pTime[SENSOR_BUOY_DOOR_e] = recPTime;
            m_status[SENSOR_BUOY_DOOR_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 10:
            // 10 // tipala za vodo v normalnem obratovanju
            if (recPTime <= m_pTime[SENSOR_WATER_LEAK_ALL_e]) break;
            m_pTime[SENSOR_WATER_LEAK_ALL_e] = recPTime;
            m_status[SENSOR_WATER_LEAK_ALL_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;

        case 11:
            // 11 // tipala za vodo v servisnem načinu
            if (recPTime <= m_pTime[SENSOR_WATER_LEAK_ALL_e]) break;
            m_pTime[SENSOR_WATER_LEAK_ALL_e] = recPTime;
            m_status[SENSOR_WATER_LEAK_ALL_e] = mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE;
            break;

        case 12:
            // 12 // ni vode v boji
            if (recPTime <= m_pTime[SENSOR_WATER_LEAK_BUOY_e]) break;
            m_pTime[SENSOR_WATER_LEAK_BUOY_e] = recPTime;
            m_status[SENSOR_WATER_LEAK_BUOY_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 13:
            // 13 // vdor vode v bojo
            if (recPTime <= m_pTime[SENSOR_WATER_LEAK_BUOY_e]) break;
            m_pTime[SENSOR_WATER_LEAK_BUOY_e] = recPTime;
            m_status[SENSOR_WATER_LEAK_BUOY_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 14:
            // 14 // ni vode v komori za kamero
            if (recPTime <= m_pTime[SENSOR_WATER_LEAK_UW_CAM_e]) break;
            m_pTime[SENSOR_WATER_LEAK_UW_CAM_e] = recPTime;
            m_status[SENSOR_WATER_LEAK_UW_CAM_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 15:
            // 15 // vdor vode v komoro za kamero
            if (recPTime <= m_pTime[SENSOR_WATER_LEAK_UW_CAM_e]) break;
            m_pTime[SENSOR_WATER_LEAK_UW_CAM_e] = recPTime;
            m_status[SENSOR_WATER_LEAK_UW_CAM_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

// As per 2016-02-28
// it isn't connected to the buoy and so it was REMOVED from queries
/*
        case 20:
            // 20 // signalna luč v redu
            if (recPTime <= m_pTime[SENSOR_SIGNAL_LIGHT_e]) break;
            m_pTime[SENSOR_SIGNAL_LIGHT_e] = recPTime;
            m_status[SENSOR_SIGNAL_LIGHT_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 21:
            // 21 // signalna luč - okvara
            if (recPTime <= m_pTime[SENSOR_SIGNAL_LIGHT_e]) break;
            m_pTime[SENSOR_SIGNAL_LIGHT_e] = recPTime;
            m_status[SENSOR_SIGNAL_LIGHT_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;
*/

        case 30:
            // 30 // merilnik za CO2 v boji v normalnem obratovanju
            if (recPTime <= m_pTime[SENSOR_BUOY_CO2_e]) break;
            m_pTime[SENSOR_BUOY_CO2_e] = recPTime;
            m_status[SENSOR_BUOY_CO2_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;
        case 31:
            // 31 // merilnik za CO2 v boji v servisnem načinu
            if (recPTime <= m_pTime[SENSOR_BUOY_CO2_e]) break;
            m_pTime[SENSOR_BUOY_CO2_e] = recPTime;
            m_status[SENSOR_BUOY_CO2_e] = mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE;
            break;
        case 32:
            // 32 // količina CO2 v boji nizka
            if (recPTime <= m_pTime[SENSOR_BUOY_CO2_e]) break;
            m_pTime[SENSOR_BUOY_CO2_e] = recPTime;
            m_status[SENSOR_BUOY_CO2_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 33:
            // 33 // količina CO2 v boji nevarno visoka
            if (recPTime <= m_pTime[SENSOR_BUOY_CO2_e]) break;
            m_pTime[SENSOR_BUOY_CO2_e] = recPTime;
            m_status[SENSOR_BUOY_CO2_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 40:
            // 40 // radija v normalnem načinu obratovanja
            if (recPTime <= m_pTime[SENSOR_WIFI_LINK_ALL_e]) break;
            m_pTime[SENSOR_WIFI_LINK_ALL_e] = recPTime;
            m_status[SENSOR_WIFI_LINK_ALL_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;
        case 41:
            // 41 // radija v servisnem načinu
            if (recPTime <= m_pTime[SENSOR_WIFI_LINK_ALL_e]) break;
            m_pTime[SENSOR_WIFI_LINK_ALL_e] = recPTime;
            m_status[SENSOR_WIFI_LINK_ALL_e] = mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE;
            break;

        case 42:
            // 42 // povezava preko 2.4 GHz v redu
            if (recPTime <= m_pTime[SENSOR_WIFI_2_4GHz_e]) break;
            m_pTime[SENSOR_WIFI_2_4GHz_e] = recPTime;
            m_status[SENSOR_WIFI_2_4GHz_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 43:
            // 43 // ni povezave preko 2.4 GHz
            if (recPTime <= m_pTime[SENSOR_WIFI_2_4GHz_e]) break;
            m_pTime[SENSOR_WIFI_2_4GHz_e] = recPTime;
            m_status[SENSOR_WIFI_2_4GHz_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 44:
            // 44 // povezava preko 5 GHz v redu
            if (recPTime <= m_pTime[SENSOR_WIFI_5GHz_e]) break;
            m_pTime[SENSOR_WIFI_5GHz_e] = recPTime;
            m_status[SENSOR_WIFI_5GHz_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 45:
            // 45 // ni povezave preko 5 GHz
            if (recPTime <= m_pTime[SENSOR_WIFI_5GHz_e]) break;
            m_pTime[SENSOR_WIFI_5GHz_e] = recPTime;
            m_status[SENSOR_WIFI_5GHz_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 50:
            // 50 // nadzorne kamere v normalnem načinu obratovanja
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM_ALL_e]) break;
            m_pTime[SENSOR_SECURITY_CAM_ALL_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM_ALL_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;
        case 51:
            // 51 // nadzorne kamere v servisnem načinu
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM_ALL_e]) break;
            m_pTime[SENSOR_SECURITY_CAM_ALL_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM_ALL_e] = mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE;
            break;

        case 52:
            // 52 // nadzorna kamera št 1. dela
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM1_e]) break;
            m_pTime[SENSOR_SECURITY_CAM1_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM1_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 53:
            // 53 // nadzorna kamera št 1. ne dela
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM1_e]) break;
            m_pTime[SENSOR_SECURITY_CAM1_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM1_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 54:
            // 54 // nadzorna kamera št 2. dela
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM2_e]) break;
            m_pTime[SENSOR_SECURITY_CAM2_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM2_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 55:
            // 55 // nadzorna kamera št 2. ne dela
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM2_e]) break;
            m_pTime[SENSOR_SECURITY_CAM2_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM2_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 56:
            // 56 // nadzorna kamera št 3. dela
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM3_e]) break;
            m_pTime[SENSOR_SECURITY_CAM3_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM3_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 57:
            // 57 // nadzorna kamera št 3. ne dela
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM3_e]) break;
            m_pTime[SENSOR_SECURITY_CAM3_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM3_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 58:
            // 58 // nadzorna kamera št 4. dela
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM4_e]) break;
            m_pTime[SENSOR_SECURITY_CAM4_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM4_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 59:
            // 59 // nadzorna kamera št 4. ne dela
            if (recPTime <= m_pTime[SENSOR_SECURITY_CAM4_e]) break;
            m_pTime[SENSOR_SECURITY_CAM4_e] = recPTime;
            m_status[SENSOR_SECURITY_CAM4_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 60:
            // 60 // podvodna kamera št. 1 v normalnem načinu obratovanja
            if (recPTime <= m_pTime[SENSOR_UW_CAM1_e]) break;
            m_pTime[SENSOR_UW_CAM1_e] = recPTime;
            m_status[SENSOR_UW_CAM1_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;
        case 62:
            // 62 // podvodna kamera št. 1 v servisnem načinu
            if (recPTime <= m_pTime[SENSOR_UW_CAM1_e]) break;
            m_pTime[SENSOR_UW_CAM1_e] = recPTime;
            m_status[SENSOR_UW_CAM1_e] = mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE;
            break;
        case 64:
            // 64 // napaka na podvodni kameri št. 1
            if (recPTime <= m_pTime[SENSOR_UW_CAM1_e]) break;
            m_pTime[SENSOR_UW_CAM1_e] = recPTime;
            m_status[SENSOR_UW_CAM1_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;
        case 66:
            // 66 // potekel čas pri zajemu slike na podvodni kameri št. 1
            if (recPTime <= m_pTime[SENSOR_UW_CAM1_e]) break;
            m_pTime[SENSOR_UW_CAM1_e] = recPTime;
            m_status[SENSOR_UW_CAM1_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 61:
            // 61 // podvodna kamera št. 2 v normalnem načinu obratovanja
            if (recPTime <= m_pTime[SENSOR_UW_CAM2_e]) break;
            m_pTime[SENSOR_UW_CAM2_e] = recPTime;
            m_status[SENSOR_UW_CAM2_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;
        case 63:
            // 63 // podvodna kamera št. 2 v servisnem načinu
            if (recPTime <= m_pTime[SENSOR_UW_CAM2_e]) break;
            m_pTime[SENSOR_UW_CAM2_e] = recPTime;
            m_status[SENSOR_UW_CAM2_e] = mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE;
            break;
        case 65:
            // 65 // napaka na podvodni kameri št. 2
            if (recPTime <= m_pTime[SENSOR_UW_CAM2_e]) break;
            m_pTime[SENSOR_UW_CAM2_e] = recPTime;
            m_status[SENSOR_UW_CAM2_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;
        case 67:
            // 67 // potekel čas pri zajemu slike na podvodni kameri št. 2
            if (recPTime <= m_pTime[SENSOR_UW_CAM2_e]) break;
            m_pTime[SENSOR_UW_CAM2_e] = recPTime;
            m_status[SENSOR_UW_CAM2_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 70:
            // 70 // napaka pri branju preko vodila SPI
            if (recPTime <= m_pTime[SENSOR_BUS_SPI_e]) break;
            m_pTime[SENSOR_BUS_SPI_e] = recPTime;
            m_status[SENSOR_BUS_SPI_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;
        case 71:
            // 71 // napaka pri pisanju preko vodila I2C
            if (recPTime <= m_pTime[SENSOR_BUS_I2C_WRITE_e]) break;
            m_pTime[SENSOR_BUS_I2C_WRITE_e] = recPTime;
            m_status[SENSOR_BUS_I2C_WRITE_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;
        case 72:
            // 72 // napaka pri branju preko vodila I2C
            if (recPTime <= m_pTime[SENSOR_BUS_I2C_READ_e]) break;
            m_pTime[SENSOR_BUS_I2C_READ_e] = recPTime;
            m_status[SENSOR_BUS_I2C_READ_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;
        case 73:
            // 73 // napaka pri pisanju v krmilnik za osvetlitev RGB
            if (recPTime <= m_pTime[SENSOR_BUS_RGB_e]) break;
            m_pTime[SENSOR_BUS_RGB_e] = recPTime;
            m_status[SENSOR_BUS_RGB_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 80:
            // 80 // modul GPS v normalnem načinu obratovanja
            if (recPTime <= m_pTime[SENSOR_GPS_e]) break;
            m_pTime[SENSOR_GPS_e] = recPTime;
            m_status[SENSOR_GPS_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;
        case 81:
            // 81 // modul GPS v servisnem načinu
            if (recPTime <= m_pTime[SENSOR_GPS_e]) break;
            m_pTime[SENSOR_GPS_e] = recPTime;
            m_status[SENSOR_GPS_e] = mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE;
            break;
        case 82:
            // 82 // začetna sinhronizacija časa preko modula GPS
            if (recPTime <= m_pTime[SENSOR_GPS_e]) break;
            m_pTime[SENSOR_GPS_e] = recPTime;
            m_status[SENSOR_GPS_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;
        case 83:
            // 83 // časovna sinhronizacija preko modula GPS
            if (recPTime <= m_pTime[SENSOR_GPS_e]) break;
            m_pTime[SENSOR_GPS_e] = recPTime;
            m_status[SENSOR_GPS_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;

        case 90:
            // 90 // energetski modul v normalnem načinu obratovanja
            if (recPTime <= m_pTime[SENSOR_POWER_e]) break;
            m_pTime[SENSOR_POWER_e] = recPTime;
            m_status[SENSOR_POWER_e] = mbp::buoy::ALARM_STATUS_OPERATION_NORMAL;
            break;
        case 91:
            // 91 // energetski modul v servisnem načinu
            if (recPTime <= m_pTime[SENSOR_POWER_e]) break;
            m_pTime[SENSOR_POWER_e] = recPTime;
            m_status[SENSOR_POWER_e] = mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE;
            break;
        case 92:
            // 92 // vzpostavljena komunikacija z energetskim modulom
            if (recPTime <= m_pTime[SENSOR_POWER_e]) break;
            m_pTime[SENSOR_POWER_e] = recPTime;
            m_status[SENSOR_POWER_e] = mbp::buoy::ALARM_STATUS_OK;
            break;
        case 93:
            // 93 // ni komunikacije z energetskim modulom
            if (recPTime <= m_pTime[SENSOR_POWER_e]) break;
            m_pTime[SENSOR_POWER_e] = recPTime;
            m_status[SENSOR_POWER_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;
        case 94:
            // 94 // izguba (preliv) podatkov v energijskem modulu
            if (recPTime <= m_pTime[SENSOR_POWER_e]) break;
            m_pTime[SENSOR_POWER_e] = recPTime;
            m_status[SENSOR_POWER_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;
        case 95:
            // 95 // ponastavitev (reset) energijskega modula
            if (recPTime <= m_pTime[SENSOR_POWER_e]) break;
            m_pTime[SENSOR_POWER_e] = recPTime;
            m_status[SENSOR_POWER_e] = mbp::buoy::ALARM_STATUS_FAIL;
            break;

        case 2000:
            if (recPTime <= m_pTime[SENSOR_BAT1_e]) break;
            m_pTime[SENSOR_BAT1_e] = recPTime;
            m_status[SENSOR_BAT1_e] = mbp::buoy::ALARM_BAT_STATUS_IN_USE;
            break;
        case 2020:
            if (recPTime <= m_pTime[SENSOR_BAT1_e]) break;
            m_pTime[SENSOR_BAT1_e] = recPTime;
            m_status[SENSOR_BAT1_e] = mbp::buoy::ALARM_BAT_STATUS_FULL;
            break;
        case 2030:
            if (recPTime <= m_pTime[SENSOR_BAT1_e]) break;
            m_pTime[SENSOR_BAT1_e] = recPTime;
            m_status[SENSOR_BAT1_e] = mbp::buoy::ALARM_BAT_STATUS_CHARGING;
            break;
        case 2001:
            if (recPTime <= m_pTime[SENSOR_BAT2_e]) break;
            m_pTime[SENSOR_BAT2_e] = recPTime;
            m_status[SENSOR_BAT2_e] = mbp::buoy::ALARM_BAT_STATUS_IN_USE;
            break;
        case 2021:
            if (recPTime <= m_pTime[SENSOR_BAT2_e]) break;
            m_pTime[SENSOR_BAT2_e] = recPTime;
            m_status[SENSOR_BAT2_e] = mbp::buoy::ALARM_BAT_STATUS_FULL;
            break;
        case 2031:
            if (recPTime <= m_pTime[SENSOR_BAT2_e]) break;
            m_pTime[SENSOR_BAT2_e] = recPTime;
            m_status[SENSOR_BAT2_e] = mbp::buoy::ALARM_BAT_STATUS_CHARGING;
            break;
        case 2050:
            if (recPTime > m_pTime[SENSOR_BAT1_e]) {
                m_pTime[SENSOR_BAT1_e] = recPTime;
                m_status[SENSOR_BAT1_e] = mbp::buoy::ALARM_BAT_STATUS_WARNING;
            }
            if (recPTime > m_pTime[SENSOR_BAT2_e]) {
                m_pTime[SENSOR_BAT2_e] = recPTime;
                m_status[SENSOR_BAT2_e] = mbp::buoy::ALARM_BAT_STATUS_WARNING;
            }
            break;
        case 2051:
        case 2100:
        case 2150:
        case 2200:
            if (recPTime > m_pTime[SENSOR_BAT1_e]) {
                m_pTime[SENSOR_BAT1_e] = recPTime;
                m_status[SENSOR_BAT1_e] = mbp::buoy::ALARM_STATUS_FAIL;
            }
            if (recPTime > m_pTime[SENSOR_BAT2_e]) {
                m_pTime[SENSOR_BAT2_e] = recPTime;
                m_status[SENSOR_BAT2_e] = mbp::buoy::ALARM_STATUS_FAIL;
            }
            break;
        default:
            // TODO:
            // ----
            // 1000 // izguba (preliv) podatkov iz V/I modula 1, vrat 0
            // 1001 // izguba (preliv) podatkov iz V/I modula 1, vrat 1
            // 1002 // izguba (preliv) podatkov iz V/I modula 2, vrat 0
            // 1003 // izguba (preliv) podatkov iz V/I modula 2, vrat 1
            // 1004 // izguba (preliv) podatkov iz V/I modula 3, vrat 0
            // 1005 // izguba (preliv) podatkov iz V/I modula 3, vrat 1
            // 1006 // izguba (preliv) podatkov iz V/I modula 4, vrat 0
            // 1007 // izguba (preliv) podatkov iz V/I modula 4, vrat 1
            // 1008 // izguba (preliv) podatkov iz V/I modula 5, vrat 0
            // 1009 // izguba (preliv) podatkov iz V/I modula 5, vrat 1
            // 2000 // komplet baterij št. 1 priključen na napajanje sistema
            // 2001 // komplet baterij št. 2 priključen na napajanje sistema
            // 2010 // komplet baterij št. 1 odklopljen od napajalnega sistema
            // 2011 // komplet baterij št. 2 odklopljen od napajalnega sistema
            // 2020 // komplet baterij št. 1 napolnjen
            // 2021 // komplet baterij št. 2 napolnjen
            // 2030 // komplet baterij št. 1 preklopljen na polnjenje
            // 2031 // komplet baterij št. 2 preklopljen na polnjenje
            // 2050 // nobene baterije ni na razpolago za napajanje sistema
            // 2051 // vse baterije so prazne
            // 2100 // preklop releja ni zaznan !!!
            // 2150 // kritična napaka v relejskem sklopu !!!
            // 2200 // kritična napaka na vodilu I2C – pisanje se ni izvedlo
            // ----
            break;
    } // switch (code)
    return;
} // mbp::buoy::RuntimeAlarms::setAlarm()


void mbp::buoy::RuntimeAlarms::getStatusJson(std::ostringstream& sstr) const
{
    // Returned JSON statuses
    //  0  -> OK    / Normal operation (color: green)
    //  1  -> FAIL  / STOPPED (color: red)
    //  2  -> Initializing / Service mode (color: orange)
    //
    //  3  -> N/A   / no info received so far (color: gray)
    //
    //  ---- special battery packs statusInfo (used for special text mapping) ----
    //  0  -> sensor status=ALARM_BAT_STATUS_IN_USE     / info text="NAPAJA"    / color=green
    //  1  -> sensor status=ALARM_BAT_STATUS_FULL       / info text="NAPOLNJEN" / color=green
    //  2  -> sensor status=ALARM_BAT_STATUS_CHARGING   / info text="SE POLNI"  / color=green
    //  3  -> sensor status=ALARM_BAT_STATUS_WARNING    / info text="OPOZORILO" / color=orange
    //  4  -> sensor status=ALARM_STATUS_FAIL           / info text="ALARM"     / color=red
    //  5  -> sensor status=<unknown>                   / info text="neznano"   / color=grey
    buoy_sensor_e sensor;

    sstr << "{ ";
    // -- buoy door --
    do {
        sensor = mbp::buoy::SENSOR_BUOY_DOOR_e;
        mbp::buoy::alarm_status_e inStatus = m_status[sensor];
        int outStatus = 3;  // -- N/A
        if ( (inStatus == mbp::buoy::ALARM_STATUS_OK)  ||  (inStatus == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL) )
            outStatus = 0;  // -- OK
        else if (inStatus == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE)
            outStatus = 2;  // -- Initializing / Service mode
        else if ( inStatus == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
            << "\"door\": {"
            << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[sensor]) << "\","
            << " \"status\": " << outStatus
            << "}, ";
    } while (0);

    // -- buoy CO2 sensor --
    do {
        sensor = mbp::buoy::SENSOR_BUOY_CO2_e;
        mbp::buoy::alarm_status_e inStatus = m_status[sensor];
        int outStatus = 3;  // -- N/A
        if ( (inStatus == mbp::buoy::ALARM_STATUS_OK)  ||  (inStatus == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL) )
            outStatus = 0;  // -- OK
        else if (inStatus == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE)
            outStatus = 2;  // -- Initializing / Service mode
        else if ( inStatus == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
        << "\"co2\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[sensor]) << "\","
        << " \"status\": " << outStatus
        << "}, ";
    } while (0);

    // -- energy --
    do {
        sensor = mbp::buoy::SENSOR_POWER_e;
        mbp::buoy::alarm_status_e inStatus = m_status[sensor];
        int outStatus = 3;  // -- N/A
        if ( (inStatus == mbp::buoy::ALARM_STATUS_OK)  ||  (inStatus == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL) )
            outStatus = 0;  // -- OK
        else if (inStatus == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE)
            outStatus = 2;  // -- Initializing / Service mode
        else if ( inStatus == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
        << "\"energy\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[sensor]) << "\","
        << " \"status\": " << outStatus
        << "},";
    } while (0);

    // -- WiFI --
    do {
        mbp::buoy::alarm_status_e inStatusCmn = m_status[mbp::buoy::SENSOR_WIFI_LINK_ALL_e];
        mbp::buoy::alarm_status_e inStatus24g = m_status[mbp::buoy::SENSOR_WIFI_2_4GHz_e];
        mbp::buoy::alarm_status_e inStatus5g = m_status[mbp::buoy::SENSOR_WIFI_5GHz_e];

        int outStatus = 3;  // -- N/A
        if (
               (inStatus24g == mbp::buoy::ALARM_STATUS_FAIL)
            && (inStatus5g == mbp::buoy::ALARM_STATUS_FAIL)
           )
            outStatus = 1;  // -- FAIL
        else if (   (inStatusCmn == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL)
                 && (
                        (inStatus24g == mbp::buoy::ALARM_STATUS_OK)
                     || (inStatus5g == mbp::buoy::ALARM_STATUS_OK)
                    )
                )
            outStatus = 0;  // -- OK
        else if ( inStatusCmn == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE )
            outStatus = 2;  // -- Initializing / Service mode
        else
            outStatus = 1;  // -- FAIL
        sstr
        << "\"wifi\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_WIFI_LINK_ALL_e]) << "\","
        << " \"status\": " << outStatus
        << "},";

        outStatus = 3;  // -- N/A
        if ( inStatus24g == mbp::buoy::ALARM_STATUS_OK )
            outStatus = 0;  // -- OK
        else if ( inStatus24g == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
        << "\"wifi24g\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_WIFI_2_4GHz_e]) << "\","
        << " \"status\": " << outStatus
        << "},";

        outStatus = 3;  // -- N/A
        if ( inStatus5g == mbp::buoy::ALARM_STATUS_OK )
            outStatus = 0;  // -- OK
        else if ( inStatus5g == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
        << "\"wifi5g\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_WIFI_5GHz_e]) << "\","
        << " \"status\": " << outStatus
        << "},";
    } while (0);


    // -- water leak sensor --
    do {
        mbp::buoy::alarm_status_e inStatusCmn = m_status[mbp::buoy::SENSOR_WATER_LEAK_ALL_e];
        mbp::buoy::alarm_status_e inStatusBody = m_status[mbp::buoy::SENSOR_WATER_LEAK_BUOY_e];
        mbp::buoy::alarm_status_e inStatusCam = m_status[mbp::buoy::SENSOR_WATER_LEAK_UW_CAM_e];

        int outStatus = 3;  // -- N/A
        if (
               (inStatusBody == mbp::buoy::ALARM_STATUS_FAIL)
            || (inStatusCam == mbp::buoy::ALARM_STATUS_FAIL)
           )
            outStatus = 1;  // -- FAIL
        else if (inStatusCmn == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE)
            outStatus = 2;  // -- Initializing / Service mode
        else if ( inStatusCmn == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL )
            outStatus = 0;  // -- OK
        sstr
            << "\"waterCmn\": {"
            << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_WATER_LEAK_ALL_e]) << "\","
            << " \"status\": " << outStatus
            << "},";

        outStatus = 3;  // -- N/A
        if ( inStatusBody == mbp::buoy::ALARM_STATUS_OK )
            outStatus = 0;  // -- OK
        else if ( inStatusBody == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
            << "\"waterBody\": {"
            << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_WATER_LEAK_BUOY_e]) << "\","
            << " \"status\": " << outStatus
            << "},";

        outStatus = 3;  // -- N/A
        if ( inStatusCam == mbp::buoy::ALARM_STATUS_OK )
            outStatus = 0;  // -- OK
        else if ( inStatusCam == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
            << "\"waterCam\": {"
            << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_WATER_LEAK_UW_CAM_e]) << "\","
            << " \"status\": " << outStatus
            << "},";
    } while (0);

    // -- control and under-water cameras --
    do {
        mbp::buoy::alarm_status_e inStatusCmn = m_status[mbp::buoy::SENSOR_SECURITY_CAM_ALL_e];
        mbp::buoy::alarm_status_e inStatusCam1 = m_status[mbp::buoy::SENSOR_SECURITY_CAM1_e];
        mbp::buoy::alarm_status_e inStatusCam2 = m_status[mbp::buoy::SENSOR_SECURITY_CAM2_e];
        mbp::buoy::alarm_status_e inStatusCam3 = m_status[mbp::buoy::SENSOR_SECURITY_CAM3_e];
        mbp::buoy::alarm_status_e inStatusCam4 = m_status[mbp::buoy::SENSOR_SECURITY_CAM4_e];
        mbp::buoy::alarm_status_e inStatusUwCam = m_status[mbp::buoy::SENSOR_UW_CAM1_e];

        int outStatus = 3;  // -- N/A
        if (
               (inStatusCam1 == mbp::buoy::ALARM_STATUS_FAIL)
            || (inStatusCam2 == mbp::buoy::ALARM_STATUS_FAIL)
            || (inStatusCam3 == mbp::buoy::ALARM_STATUS_FAIL)
            || (inStatusCam4 == mbp::buoy::ALARM_STATUS_FAIL)
            || (inStatusUwCam == mbp::buoy::ALARM_STATUS_FAIL)
           )
            outStatus = 1;  // -- FAIL
        else if (
                    (inStatusCmn == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE)
                 || (inStatusUwCam == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE)
                )
            outStatus = 2;  // -- Initializing / Service mode
        else if ( inStatusCmn == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL )
            outStatus = 0;  // -- OK

        sstr
        << "\"camCmn\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_SECURITY_CAM_ALL_e]) << "\","
        << " \"status\": " << outStatus
        << "},";

        outStatus = 3;
        if ( inStatusCam1 == mbp::buoy::ALARM_STATUS_OK )
            outStatus = 0;  // -- OK
        else if ( inStatusCam1 == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
        << "\"cam1\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_SECURITY_CAM1_e]) << "\","
        << " \"status\": " << outStatus
        << "},";

        outStatus = 3;
        if ( inStatusCam2 == mbp::buoy::ALARM_STATUS_OK )
            outStatus = 0;  // -- OK
        else if ( inStatusCam2 == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
        << "\"cam2\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_SECURITY_CAM2_e]) << "\","
        << " \"status\": " << outStatus
        << "},";

        outStatus = 3;
        if ( inStatusCam3 == mbp::buoy::ALARM_STATUS_OK )
            outStatus = 0;  // -- OK
        else if ( inStatusCam3 == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
        << "\"cam3\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_SECURITY_CAM3_e]) << "\","
        << " \"status\": " << outStatus
        << "},";

        outStatus = 3;
        if ( inStatusCam4 == mbp::buoy::ALARM_STATUS_OK )
            outStatus = 0;  // -- OK
        else if ( inStatusCam4 == mbp::buoy::ALARM_STATUS_FAIL )
            outStatus = 1;  // -- FAIL
        sstr
        << "\"cam4\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_SECURITY_CAM4_e]) << "\","
        << " \"status\": " << outStatus
        << "},";

        outStatus = 3;  // -- N/A
        if ( inStatusUwCam == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL )
            outStatus = 0;  // -- OK
        else if (inStatusUwCam == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE)
            outStatus = 2;  // -- Initializing / Service mode
        else if (inStatusUwCam == mbp::buoy::ALARM_STATUS_FAIL)
            outStatus = 1;  // -- FAIL
        sstr
        << "\"uwCam\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[mbp::buoy::SENSOR_UW_CAM1_e]) << "\","
        << " \"status\": " << outStatus
        << "}, ";
    } while (0);

    // -- gps --
    do {
        sensor = mbp::buoy::SENSOR_GPS_e;
        mbp::buoy::alarm_status_e inStatus = m_status[sensor];
        int outStatus = 3;  // -- N/A
        if ( inStatus == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL )
            outStatus = 0;  // -- OK
        else if ( inStatus == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE )
            outStatus = 2;  // -- Initializing / Service mode
        sstr
        << "\"gps\": {"
        << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[sensor]) << "\","
        << " \"status\": " << outStatus
        << "},";
    } while (0);

    // -- bat1 and bat 2 --
    do {
        sensor = mbp::buoy::SENSOR_BAT1_e;
        mbp::buoy::alarm_status_e inStatus = m_status[sensor];

        int outStatus = 3;  // -- N/A
        int outStatusInfo = 5;  // -- <unknown>
        if (inStatus == mbp::buoy::ALARM_BAT_STATUS_IN_USE) {
            outStatus = 0;  // -- OK (color: green)
            outStatusInfo = 0;
        } else if (inStatus == mbp::buoy::ALARM_BAT_STATUS_FULL) {
            outStatus = 0;  // -- OK (color: green)
            outStatusInfo = 1;
        } else if (inStatus == mbp::buoy::ALARM_BAT_STATUS_CHARGING) {
            outStatus = 0;  // -- OK (color: green)
            outStatusInfo = 2;
        } else if (inStatus == mbp::buoy::ALARM_BAT_STATUS_WARNING) {
            outStatus = 2;  // -- Service mode (color: orange)
            outStatusInfo = 3;
        } else if (inStatus == mbp::buoy::ALARM_STATUS_FAIL) {
            outStatus = 1;  // -- FAIL
            outStatusInfo = 4;
        }
        sstr
            << "\"bat1\": {"
            << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[sensor]) << "\","
            << " \"status\": " << outStatus << ","
            << " \"statusInfo\": " << outStatusInfo
            << "},";

        sensor = mbp::buoy::SENSOR_BAT2_e;
        inStatus = m_status[sensor];
        outStatus = 3;      // -- N/A
        outStatusInfo = 5;  // -- <unknown>
        if (inStatus == mbp::buoy::ALARM_BAT_STATUS_IN_USE) {
            outStatus = 0;  // -- OK (color: green)
            outStatusInfo = 0;
        } else if (inStatus == mbp::buoy::ALARM_BAT_STATUS_FULL) {
            outStatus = 0;  // -- OK (color: green)
            outStatusInfo = 1;
        } else if (inStatus == mbp::buoy::ALARM_BAT_STATUS_CHARGING) {
            outStatus = 0;  // -- OK (color: green)
            outStatusInfo = 2;
        } else if (inStatus == mbp::buoy::ALARM_BAT_STATUS_WARNING) {
            outStatus = 2;  // -- Service mode (color: orange)
            outStatusInfo = 3;
        } else if (inStatus == mbp::buoy::ALARM_STATUS_FAIL) {
            outStatus = 1;  // -- FAIL
            outStatusInfo = 4;
        }
        sstr
            << "\"bat2\": {"
            << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[sensor]) << "\","
            << " \"status\": " << outStatus << ","
            << " \"statusInfo\": " << outStatusInfo
            << "}";
    } while (0);

// As per 2016-02-28
// it isn't connected to the buoy and so it was REMOVED from queries
/*
    // -- signal light --
    do {
        sensor = mbp::buoy::SENSOR_SIGNAL_LIGHT_e;
        mbp::buoy::alarm_status_e inStatus = m_status[sensor];
        int outStatus = 3;  // -- N/A
        if ( inStatus == mbp::buoy::ALARM_STATUS_OPERATION_NORMAL )
            outStatus = 0;  // -- OK
        else if ( inStatus == mbp::buoy::ALARM_STATUS_OPERATION_SERVICE_MODE )
            outStatus = 1;  // -- FAIL
        sstr
            << "\"signalLight\": {"
            << " \"dt\": \"" << mbp::buoy::PtimeToStr(m_pTime[sensor]) << "\","
            << " \"status\": " << outStatus
            << "}, ";
    } while (0);
*/

    sstr << "}";

    return;
} // mbp::buoy::RuntimeAlarms::getStatusJson()
