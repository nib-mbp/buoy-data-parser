#include <iostream>
#include <string>
#include <sstream>

#include "buoy_datatypes.hpp"


namespace mbp {
namespace buoy {

struct _db_tables_s g_buoyDbTables[] = {
    {TBL_NA_e                   , "N/A - UNKNOWN"},
    {TBL_AIR_e                  , "air"},
    {TBL_BUOY_INTERIOR_SHT21_e  , "buoy_interior"},
    {TBL_ALARMS_e               , "alarms"},
    {TBL_AWAC2_USER_CONFIG_e    , "awac2_user_config"},
    {TBL_AWAC2_HEAD_CONFIG_e    , "awac2_head_config"},
    {TBL_AWAC2_HW_CONFIG_e      , "awac2_hw_config"},
    {TBL_AWAC2_CURRENTS_HEADER_e, "awac2_currents_header"},
    {TBL_AWAC2_CURRENTS_DATA_e  , "awac2_currents"},
    {TBL_AWAC2_WAVES_e          , "awac2_waves"},
    {TBL_AWAC2_WAVEBANDS_e      , "awac2_wave_bands"},
    {TBL_AWAC2_WAVESPECTRUM_HEADER_e, "awac2_wave_spectrum_header"},
    {TBL_AWAC2_WAVESPECTRUM_DATA_e  , "awac2_wave_spectrum_data"},
    {TBL_AWAC2_FOURIER_HEADER_e , "awac2_wave_fourier_header"},
    {TBL_AWAC2_FOURIER_DATA_e   , "awac2_wave_fourier_data"},
    {TBL_BAT_VOLTAGE_e          , "bat_voltage"},
    {TBL_BATTERIES_e            , "batteries"},
    {TBL_CAMERA_PARAMETERS_e    , "camera_parameters"},
    {TBL_CO2_e                  , "co2"},
    {TBL_COMPASS_e              , "compass"},
    {TBL_COMSTAT_MAIN_IO_e      , "comstat_main_io"},
    {TBL_FUEL_CELL_STATUS_e     , "fuel_cell_status"},
    {TBL_GPS_POSITION_e         , "gps_position"},
    {TBL_GPS_STATUS_e           , "gps_status"},
    {TBL_GPS_SYNC_e             , "gps_sync"},
    {TBL_IO_ALARMS_e            , "io_alarms"},
    {TBL_IO_POWER_e             , "io_power"},
    {TBL_MAIN_BOARD_e           , "main_board"},
    {TBL_MPPT_e                 , "mppt"},
    {TBL_MPPT_STATUS_e          , "mppt_status"},
    {TBL_OXYGEN_e               , "oxygen"},
    {TBL_POWER_SECTIONS_e       , "power_sections"},
    {TBL_PROFILE_e              , "profile"},
    {TBL_SEA_WATER_e            , "sea_water"},
    {TBL_SUPERVISION_CAMERAS_e  , "supervision_cameras"},
    {TBL_TIME_SYNC_IO_e         , "time_sync_io"},
    {TBL_UNDERWATER_CAMERA_e    , "underwater_camera"},
    {TBL_WIND_e                 , "wind"},
    {TBL_WRITTER_LOG_e          , "writter_log"},
    {TBL_PAR_e                  , "par"},
    {TBL_AIR_CO2_e              , "air_co2"}
}; // g_buoyDbTables

// Used for internal logging / debugging
struct _data_types_s g_buoyDataTypes[] = {
    {DATA_NA_e              , TBL_NA_e                  , "DATA_NA_e"},
    {MPPT_e                 , TBL_MPPT_e                , "MPPT_e"},
    {MPPT_STATUS_e          , TBL_MPPT_STATUS_e         , "TBL_MPPT_STATUS_e"},
    {AD_ENERGY_00_07_e      , TBL_NA_MANUAL_SPEC_e      , "AD_ENERGY_00_07_e"},
    {AD_ENERGY_08_15_e      , TBL_NA_MANUAL_SPEC_e      , "AD_ENERGY_08_15_e"},
    {FUEL_CELL_STATUS_e     , TBL_FUEL_CELL_STATUS_e    , "FUEL_CELL_STATUS_e"},
    {ENERGY_STATUS_e        , TBL_TIME_SYNC_IO_e        , "ENERGY_STATUS_e"},
    {GPS_TIME_e             , TBL_GPS_SYNC_e            , "GPS_TIME_e"},
    {GPS_POSITION_e         , TBL_GPS_POSITION_e        , "GPS_POSITION_e"},
    {GPS_STATUS_e           , TBL_GPS_STATUS_e          , "GPS_STATUS_e"},
    {BOARD_STATUS_e         , TBL_COMSTAT_MAIN_IO_e     , "BOARD_STATUS_e"},
    {MAIN_BOARD_e           , TBL_MAIN_BOARD_e          , "MAIN_BOARD_e"},
    {DATA_CO2_e             , TBL_CO2_e                 , "DATA_CO2_e"},
    {BUOY_INTERIOR_SHT21_e  , TBL_BUOY_INTERIOR_SHT21_e , "BUOY_INTERIOR_SHT21_e"},
    {ALARM_DATA_e           , TBL_ALARMS_e              , "ALARM_DATA_e"},
    {DATA_WIND_e            , TBL_WIND_e                , "DATA_WIND_e"},
    {DATA_COMPASS_e         , TBL_COMPASS_e             , "DATA_COMPASS_e"},
    {DATA_ADC_e             , TBL_IO_POWER_e            , "DATA_ADC_e"},
    {DATA_HUMIDITY_e        , TBL_AIR_e                 , "DATA_HUMIDITY_e"},
    {DATA_SEA_WATER_e       , TBL_SEA_WATER_e           , "DATA_SEA_WATER_e"},
    {DATA_OXYGEN3835_e      , TBL_OXYGEN_e              , "DATA_OXYGEN3835_e"},
    {DATA_OXYGEN4835_1_e    , TBL_OXYGEN_e              , "DATA_OXYGEN4835_1_e"},
    {DATA_OXYGEN4835_2_e    , TBL_OXYGEN_e              , "DATA_OXYGEN4835_2_e"},
    {EXT_DATA_AWAC1_e       , TBL_NA_e                  , "EXT_DATA_AWAC1_e"},
    {IO_ALARMS_e            , TBL_NA_MANUAL_SPEC_e      , "IO_ALARMS_e"},
    {IO_ALARM_TIME_e        , TBL_TIME_SYNC_IO_e        , "IO_ALARM_TIME_e"},
    {DATA_PAR_e             , TBL_PAR_e                 , "DATA_PAR_e"},
    {DATA_AIR_CO2_e         , TBL_AIR_CO2_e             , "DATA_AIR_CO2_e"},
    {EXTENDED_HEADER_DATA_e , TBL_NA_e                  , "EXTENDED_HEADER_DATA_e"},
    {EXT_DATA_AWAC2_e       , TBL_NA_e                  , "EXT_DATA_AWAC2_e"},
    {DATA_NA_e              , TBL_WRITTER_LOG_e         , "DATA_NA_e"}
}; // g_buoyDataTypes



int BUOY_DATA_TYPES_LEN(void) {
    return (sizeof(g_buoyDataTypes) / sizeof(struct _data_types_s));
}


const char* GetTableName(
    const table_e a_table)
{
    int arrLen = sizeof(g_buoyDbTables) / sizeof(struct _db_tables_s);

    for (int i=0; i<arrLen; ++i) {
        if (g_buoyDbTables[i].tableId == a_table) {
            return g_buoyDbTables[i].tableName_p;
        }
    } // for (... g_buoyDbTables ...)

    std::ostringstream sstr("");
    sstr << "NYI: Internal error - missing type name definition (tableId: " << a_table << ").";
    throw sstr.str();
} // GetTableName()


const char* ResolveNameFromId(
    const data_type_e a_id)
{
    int arrLen = BUOY_DATA_TYPES_LEN();
    for (int i=0; i<arrLen; ++i) {
        if (g_buoyDataTypes[i].id == a_id) {
            return g_buoyDataTypes[i].name_p;
        }
    } // for (... g_buoyDataDebug ...)

    std::ostringstream sstr;
    sstr.str();
    sstr << "NYI: Internal error - missing type definition (id: " << a_id << ").";
    throw sstr.str();
} // ResolveNameFromId()


const char* ResolveNameFromId(
    const uint8_t a_id)
{
    return mbp::buoy::ResolveNameFromId(data_type_e(a_id));
} // ResolveNameFromId()


mbp::buoy::table_e GetTableFromId(
    const data_type_e a_id)
{
    int arrLen = BUOY_DATA_TYPES_LEN();
    for (int i=0; i<arrLen; ++i) {
        if (g_buoyDataTypes[i].id == a_id) {
            return g_buoyDataTypes[i].tableId;
        }
    } // for (... _g_buoyDataDebug ...)

    std::ostringstream sstr;
    sstr.str();
    sstr << "NYI: Internal error - missing type definition (id: " << a_id << ").";
    throw sstr.str();
} // GetTableFromId()


mbp::buoy::table_e GetTableFromId(
    const uint8_t a_id)
{
    return mbp::buoy::GetTableFromId(data_type_e(a_id));
} // GetTableFromId()

} // namespace buoy
} // namespace mbp
